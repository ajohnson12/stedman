#!/usr/bin/python
# For peals of treble dominated major
# Decode rows into a peal in standard formatting
# Also do reversals, 4ths<>6ths place bob and 2nd<>8ths place methods
# Copyright (c) 2019,2021 Andrew Johnson
# March 2019 / March 2020 / May 2021

import sys

# Get the file name
if len(sys.argv) > 1 and sys.argv[1] != '-':
   fn = sys.argv[1]
else:
   fn = None

# Find the place notation from two successive rows
def getpn(row1, row2):
   pn = ''
   for i in range(0, len(row1)):
      if row1[i] == row2[i]:
         pn = pn + str(i+1)
   if pn == '':
      pn = 'X'
   return pn      

# Convert a call place notation to a symbol
def call(pn):
   if pn == '14':
      return '-'
   elif pn == '1234':
      return 'S'
   else:
      return ' '

# Find the position of a tenor after a call and convert to a position
def callingpos(row1,row2, tenor):
   p=row2.index(tenor)
   ps = '1IOFVMWH'
   return ps[p]

# Add a place notation to build a method, skipping unnecessary dots
def addpn(method,pn):
   if pn == 'X' or not method or method[-1] == 'X':
      method = method + pn
   else:
      method = method + '.' + pn
   return method

# 13 a
# 15 b
# 17 c
# 18 d
# 16 e
# 14 f
def leadcode(lh,le,pn):
   if pn == '12':
      # 2nds
      if le[2-1] == lh[3-1]:
         return 'a'
      if le[2-1] == lh[5-1]:
         return 'b'
      if le[2-1] == lh[7-1]:
         return 'c'
      if le[2-1] == lh[8-1]:
         return 'd'
      if le[2-1] == lh[6-1]:
         return 'e'
      if le[2-1] == lh[4-1]:
         return 'f'
   elif pn == '18':
      # 8ths
      if le[3-1] == lh[3-1]:
         return 'g'
      if le[3-1] == lh[5-1]:
         return 'h'
      if le[3-1] == lh[7-1]:
         return 'j'
      if le[3-1] == lh[8-1]:
         return 'k'
      if le[3-1] == lh[6-1]:
         return 'l'
      if le[3-1] == lh[4-1]:
         return 'm'
   return '?'


# Read and process all the rows in a peal
def readrows(f):
   rows=[]
   for r in f:
      r = r.strip()
      rows.append(r)
   return rows
   
# Read and process all the rows in a peal
def processrows(rows):
   leadmap = {}
   previousmethod = ''
   method = ''
   touch = ''
   tenor = ''
   leadhead = ''
   lc = '?'
   bc = '?'
   defaultbob = '?'
   r1 = ''
   for r in rows:
      r = r.strip()
      if r1:
         pn = getpn(r1, r)
         #print("%s %s" % (r, pn))
      else:
         # First row
         if "12345678".startswith(r):
            r1 = r
            if len(sys.argv) > 2:
               tenor = sys.argv[2]
            else:
               tenor=r1[-1]
            leadhead = r1
            continue
         else:
            r1 = "12345678" 
            if len(sys.argv) > 2:
               tenor = sys.argv[2]
            else:
               tenor=r1[-1]
            leadhead = r1
            pn = getpn(r1, r)         
       # Treble lead?
      if r[0] == '1':
         # Treble whole lead?
         if pn[0] == '1':
             pnplain = pn
             #print("%s %s %s" % (leadhead,r,pn))
             if '4' in pn:
                bc = '14'
             if '6' in pn:
                bc = '16'
             if leadhead[4] == r[4]:
                # Extending lead
                if '4' in pn:
                   pnplain = '18'
                
             if '4' in pn and leadhead[5] != r[5]:
                pnplain = '12'
             elif '6' in pn and leadhead[5] != r[5]:
                pnplain = '18'
             pnplain = '12'
             lc = leadcode(leadhead, r1, pnplain)

             method=addpn(method,pnplain)
             if method != previousmethod:
                print("Method old: %s" % (previousmethod))
                print("Method new: %s" % (method))
                pass
             previousmethod=method
             method=''
             leadhead = r

             # Treble lead
             if pn == '14':
                # bob
                cp = callingpos(r1, r, tenor)
                if lc in 'abcdefm':
                   cn = '-'
                   defaultbob = pn
                else:
                   cn = 'x'
                touch=touch+cn+cp                           
             elif pn == '1234':
                # single
                cp = callingpos(r1, r, tenor)
                if lc in 'abcdefm':
                   cn = 's'
                   defaultbob = '14'
                else:
                   cn = 'z'                               
                touch=touch+cn+cp 
             elif pn == '16':
                # bob
                cp = callingpos(r1, r, tenor)
                if lc in 'abcdefm':
                   cn = 'x'
                else:
                   cn = '-'
                   defaultbob = pn                            
                touch=touch+cn+cp
             elif pn == '1678':
                # single
                cp = callingpos(r1, r, tenor)  
                if lc in 'abcdefm':
                   cn = 'z'
                else:
                   cn = 's'                               
                touch=touch+cn+cp
             elif pn == '1478':
                # single
                cp = callingpos(r1, r, tenor)  
                cn = 'z'                               
                touch=touch+cn+cp                             
             leadmap[r1] = r 
         else:
            method=addpn(method,pn)
      else:
          method=addpn(method,pn)

      r1 = r
   return (touch, leadmap, lc, defaultbob)

def replace(touch, oldcalls, newcalls):
   nt = ''
   for c in touch:
      ix = oldcalls.find(c)
      if ix >= 0:
         nt = nt+newcalls[ix]
      else:
         nt = nt+c
   return nt

# Reverses the rows from penultimate row and renumbers them
# 13246587
# --------
# 12345678
#
# Go up from 13246587 treated as rounds

def reverserows(rows):
   last = rows[-1]
   last1 = rows[-2]
   rows1 = rows[-2::-1]
   rows2 = []
   for r in rows1:
      rows2.append(replace(r, last1, last))
   rows2.append(last)
   return rows2


if fn:
   with open(fn) as f:
      rows = readrows(f) 
else:
   rows = readrows(f)

touch,lm,lc,bc = processrows(rows)

print("%s:%s" % (lc,touch))
revtouch = processrows(reverserows(rows))[0]
print("%s:%s" % (lc,revtouch))

# Increment a bob count for a call location, return the modified
# call, or the original call if a new line is needed
def inc(c, cl):
   if "-12345678".find(c) < 0 or cl != '-':
      return cl
   if c == '-':
      return '2'
   else:
      return str(int(c)+1)

# Order of calling positions lead-end / bobs code a
# 2nds/4ths
# WVFOIMH
# 8ths/6ths
# XVOIFHW
# 2nds/6ths
# WXVOIFH
# 8ths/4ths
# VFOIMHW

# Superlative goes to l (or k) BMWH <> IVOH
# Brighton to Lavenham court near to near!
# Belgrave to Highbury a near to j far also double coslany (m) far calls
# bristol to edmonton with far calls
# E.g.
# 13254768  15372846  17583624  18765432  16847253  14628375  12436587
# 13527486  15738264  17856342  18674523  16482735  14263857  12345678
#
# 13254768  15372846  17583624  18765432  16847253  14628375  12436587
# 12357386  13578264  15786342  17864523  18642735  16423857  14235678
#
# reverse/invert/flip/backwards/retrograde
# flip end to end e.g.
# 2nds<>8ths
# 4ths<>6ths
# 6ths<>4ths
# also reverse
# so:
# W -> I
# V -> F
# F -> X
# O -> H
# I -> W
# M -> V
# H -> O
# WVFOIMH
# IFXHWVO
# Only for tenors together
# Not reversed:
#
# For forward changes
# W -> O
# V -> V
# F -> X
# O -> W
# I -> H
# M -> F
# H -> I
# WVFOIMH
# OVXWHFI


callingpos='WVFOIMH'
#callingpos='VOMWFIH'

def callingposn(lc, bc):
   cp2 = '' 
   if lc <= 'f':
      if '6' in bc:
         callingpos='WXVOIFH'
      else:
         callingpos='WVFOIMH'
      z = "abcdef".index(lc) + 1
      for i in range(0, len(callingpos)):
         j = (((i + 1) * z) - 1) % len(callingpos)
         cp2 = cp2 + callingpos[j]
   else:
      if '4' in bc:
         callingpos='VFOIMHW'
      else:
         callingpos='XVOIFHW'
      z = "ghjklm".index(lc) + 1
      for i in range(0, len(callingpos)):
         j = (((i + 1) * z) - 1) % len(callingpos)
         cp2 = cp2 + callingpos[j]
   return cp2

def reverse(touch, oldcalls, newcalls):
   cl = ''
   nt = ''
   # fix up
   if touch.endswith('H'):
      touch=touch[-2:]+touch[:-2]
   for c in touch:
      ix = oldcalls.find(c)
      if ix >= 0:
         nt = cl+newcalls[ix]+nt
      else:
         cl=c
   return nt

def printtouch(lc,touch, callingpos):
   print("Lead code: %s calling pos %s touch '%s'" % (lc,callingpos,touch))
   cp2 = ''
   for c in callingpos:
      if c in touch:
         cp2 = cp2 + ' ' + c
   callingpos = cp2[1:]
   print("%s" % callingpos)
   print('-'*len(callingpos))
   blankline=' '*len(callingpos)
   line=blankline
   cl = '-'
   for c in touch:
      ci = callingpos.find(c)
      if ci < 0:
         cl = c
         continue
      if line[ci:].strip():
         if line[ci+1:].strip():
            print(line)
            line = blankline
         else:
            cl2 = inc(line[ci], cl)
            if cl2 == cl:
               print(line)
               line = blankline
            cl = cl2

      line = line[0:ci]+cl+line[ci+1:]
      cl = '-'

   if line:
      print(line)
   print

# Convert each touch forward / reverse, then other lead end and other type of bob

# Effect on coursing order
# H 53246 -53246
# W 53246 -32546
# M 53246 -53462
# B 53246 -65324
# V 753246 -537246
# F 753246 -324675
# I 753246 -475326
#
# 15372846 
# 15738264
# 13578264

# 6ths place calls for 2nds place methods
# H 53246  x32465   = reverse B
# W 753246 x532476  = reverse I
# X 753246 x467532  = reverse F
# V 53246  x53624   = reverse M
# F 753246 x375246  = reverse V
# O 53246  x54326   = reverse H
# I 53246  x25346   = reverse W
# 
# 12436587
# 12345678
# 14263578 
# 
# 13254768
# 13527486
# 12345768
#
# 15372846
# 15738264
# 13527864
#
# 17583624
# 17856342
# 15738642
#
# 14628375
# 14263857
# 16482357
#
# 18765432
# 18674523
# 17856423
#
# 16847253
# 16482735
# 18674235

# Effect of reversing touch
# 12436587
# --------
# 12345678
# Go backwards, position of 7 replaces position of 8
# make calls based on position of 7 in normal
#
# 12436587
# --------
# 14235678
# H <> H
# 
# 13254768
# --------
# 12357486
# W > M
#
# 14628375
# --------
# 16423857
# M > W
#
# 15372846
# --------
# 13578264
# V > F
#
# 17583624
# --------
# 15786342
# F > I
#
# 18765432
# --------
# 17864523
# O > O
#
# 16847253
# --------
# 18642735
#
# I > V

def findleadcode(callingpos,bc):
   for c in "abcdefghjklm":
      cp = callingposn(c,bc)
      print("testing %s %s" % (cp, c))
      if cp == callingpos:
         return c
   print("oops %s %s" % (callingpos,bc))
   for c in "abcdefghjklm":
      cp = callingposn(c,bc)
      print("testing %s %s" % (cp, c))
      if cp.index('W') == callingpos.index('W'):
         return c
   return '?'

# call names for 14,16
def callsnames(lc):
   if lc in "abcdefm":
      return "-x"
   else:
      return "x-"

ix="abcdef".find(lc)
if ix >= 0:
   lc2='ghjklm'[5-ix]
else:
   ix="ghjklm".find(lc)
   if ix >= 0:
     lc2='abcdef'[5-ix]

lc1=lc
cn1=callsnames(lc) 
# Original and swapped lead-end
printrev = False
for lc in [lc1,lc2]:   

   sc1 = '-xsg'
   if lc != lc1 and lc not in 'abcdefm':
      sc2 = 'x-gs'
   else:
      sc2 = sc1
   callingpos = callingposn(lc,bc)
   print(callingpos)
   print("%s %s %s" % (touch,sc1,sc2))
   print(replace(touch,sc1,sc2))
   printtouch(lc,replace(touch,sc1,sc2),callingpos)
   if printrev:
      # The reversed touch
      printtouch(lc,replace(revtouch,sc1,sc2),callingpos)

   # Flip the calls
   if '4' in bc:
      bc2 = '16'
      cp2 = callingposn(lc, bc2)
      sc1 = '-xsg'
      if lc == lc1 or lc in 'abcdefm':
         sc2 = 'x-gs'
      else:
         sc2 = sc1
      printtouch(lc,replace(reverse(touch,   "WVFOIMH","IFXHWVO",),sc1,sc2),cp2)
      if printrev:
         printtouch(lc,replace(reverse(revtouch,"WVFOIMH","IFXHWVO",),sc1,sc2),cp2)
      #OFXWHFI
      printtouch(lc,replace(replace(touch,"WVFOIMH","OVXWHFI"),sc1,sc2),cp2)
      if printrev:
         printtouch(lc,replace(replace(revtouch,"WVFOIMH","OVXWHFI"),sc1,sc2),cp2)
   else:
      bc2 = '14'
      cp2 = callingposn(lc, bc2)
      sc1 = '-xsg'
      if lc == lc1 or lc in 'abcdefm':
         sc2 = 'x-gs'
      else:
         sc2 = sc1
      printtouch(lc,replace(reverse(touch,   "IFXHWVO","WVFOIMH"),sc1,sc2),cp2)
      if printrev:
         printtouch(lc,replace(reverse(revtouch,"IFXHWVO","WVFOIMH"),sc1,sc2),cp2)
      printtouch(lc,replace(replace(touch,"OVXWHFI","WVFOIMH",),sc1,sc2),cp2)
      if printrev:
         printtouch(lc,replace(replace(revtouch,"OVXWHFI","WVFOIMH"),sc1,sc2),cp2)

