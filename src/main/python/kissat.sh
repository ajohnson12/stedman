#!/bin/bash
# (c) 2022 Copyright Andrew Johnson
# Run kissat on a sat file with colours, and generate a result file
kissat --sat --color "$1" $2 | tee >(grep '^[sv] '>"${1}out")
exit "${PIPESTATUS[0]}"
