#!/usr/bin/python
# Random replace
# Used to perhaps generate variations of a peal by changing forcing of calls / sixes to a free choice
# filename
# random factor 1.0 means keep 0.0 means replace
# match
# replace
import sys
import random
part=sys.argv[1]
f=float(sys.argv[2])
m=sys.argv[3]
r=sys.argv[4]
if len(sys.argv) > 5:
   random.seed(sys.argv[5])
l=""
for c in part:
   if random.random() > f:
      if c in m:
         c = r[m.index(c)]
   l += c
print(l),
