#!/usr/bin/python
# Copyright (c) 2017,2022 Andrew Johnson
# Analyse Stedman / Erin touches of the form:
# '2314567QSP--PP---P*1(1)5432167SQ-*10(1)'
# Count up the number of sixes in each B-block type
# Also try to match against linkages
# Generate a linkage against complementary B-blocks
# Print the original touch with flexible options based on possible linkages

import sys
from itertools import permutations
#from str import maketrans 

def canon6(row):
   return row
   

# Generate the six-end after a plain
def plain(row):
   if row[8:9] == 'S':
      return plainS(row)
   else:
      return plainQ(row) 

# Generate the six-end after a bob
def bob(row):
   if row[8:9] == 'S':
      return bobS(row)
   else:
      return bobQ(row)

# Generate the six-end after a single
def single(row):
   if row[8:9] == 'S':
      return singleS(row)
   else:
      return singleQ(row)

# Generate the six-end after a plain on a quick six
def plainQ(row):
   row=place(row,7)
   
   row=place(row,3)
   return row
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   return row

# Generate the six-end after a plain on a slow six
def plainS(row):
   row=place(row,7)

   row=place(row,1)
   return  row
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   return row

# Generate the six-end after a bob on a quick six
def bobQ(row):
   row=place(row,5)

   row=place(row,3)
   return row

   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   return row

# Generate the six-end after a bob on a slow six
def bobS(row):
   row=place(row,5)

   row=place(row,1)
   return row

   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   return row

# Generate the six-end after a single on a quick six
def singleQ(row):
   row=place(row,5,8)

   row=place(row,3)
   return row
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   return row

# Generate the six-end after a single on a slow six
def singleS(row):
   row=place(row,5,8)

   row=place(row,1)
   return row
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   return row

# Generate a new row after this place
# Don't swap the QS indicator if the place is 1 or 3
def place(row,n,n2=0):
   for i in range(0, n-1, 2):
      a=row[i]
      b=row[i+1]
      row=row[0:i]+b+a+row[i+2:]

   if n <= 3:
      lm = len(row)-3
   else:
      lm = len(row)-1

   if n2 == 0:
      n2 = n

   for i in range(n2, lm, 2):
      a=row[i]
      b=row[i+1]
      row=row[0:i]+b+a+row[i+2:]

   return row

# Is this row out of course?
def ooc(row):
   for i in range(0, len(row) -2):
      if row[i] > row[i+1]:
         row=row[0:i]+row[i+1]+row[i]+row[i+2:]
         return not ooc(row)
   return False

# Find the row at the start of the six from the row at the end
def sixhead(row):
    if row[7] == 'Q':
        row = place(row,3)
    else: 
        row = place(row,1)
    return row
 
# Return the in-course version of this row
def incourse(row):
   if ooc(row):
      #print "%s %s ooc" % (row, ooc(row))
      # Find six-head row
      row = sixhead(row)
      #print "%s %s ooc" % (row, ooc(row))
   return row

def pairsix(row):
    # Find the complementary six
    return sixhead(row)[0:5]+row[5:]

# The six-type
def sixtype(row):
   return incourse(row)[3:7]

# The canonical version of the B-block
def canon(row):
    row=incourse(row)
    # Find the biggest
    rw = row
    rw2 = rw
    o = order(rw)
    while True:
        o2 = order(rw)
        if o2 > o:
            o = o2
            rw2 = rw
        rw = bob(rw)
        if rw == row:
            break
    
    # Find the index
    ix = 0
    rw = rw2
    while rw != row:
        ix += 1
        rw = bob(rw)

    #print "%s %s" % (row, rw2)
    return (rw2, ix)

# Check that that canonical version is in the B-block   
def checkCanon(row, rw2, ix):
    for i in range(0, ix):
        rw2 = bob(rw2)
    if row != rw2:
        print("Error %s %s %d" % (row, rw2, ix))

def order(row):
   return row[-1]+row[-2]+row[3:-2][::-1]+row[2]+row[1]+row[0]
   return row[3::-1]+row[0:2:-1]+row[2]
   return row[-1]+row[-2]+row[3:-2]+row[0:3]

# Find the complement of a row for the pair of B-blocks
# Perhaps only works for a quick six??
def complement(row):
   return row[2:4]+row[0:2]+row[4:]

# Does this row match a six in the preferred B-Blocks
# True if it matches a used six in the preferred B-blocks
def matchblock(row):
   cn,ix = canon(row)
   pair=cn[5:7]
   bp = bestpair[pair]
   if bp==cn or complement(bp)==cn:
      return sv[cn][ix]=='-'
   else:
      return False

def printbb(row):
    touches=""
    rl = len(sv[row])
    row1=row
    for i in range(0,rl):
        if sv[row][i]=='-' and sv[row][(i+1)%rl] == '.':
            touch=row1+'-' 
            for j in range(1,rl):
                if sv[row][(i+j)%rl] == '.':
                    touch+='-'
                else:
                     break
            touches += "%s*1(1)" % touch
        row1=bob(row1)
    return touches

# Reverse a touch, and swap 1 2 to keep in-course
def reverse(touch):
    row = touch[0:9]
    touch = touch[9:]
    for c in touch:
        if c == '-':
            row = bob(row)
        elif c == 'S' or c == '$':
            row = single(row)
        else:
            row = plain(row)
    row = sixhead(row)
    # Make in-course?
    #print(row)
    row=row.replace('1','@').replace('2','1').replace('@','2')
    return row+touch[::-1]

def reverseall(touches):
    end='*1(1)'
    touches = touches.split(end)
    #print(touches)
    newt = ''
    for touch in touches:
        if touch:
            newt += reverse(touch)+end
    return newt

# Hold a count of sixes present for a B-block, indexed by canonical row
sx = {}
# Holds the used sixes present for a B-block, indexed by canonical row
sv = {}
allsxs = {}
callat = {}

cb=0
cs=0
count={
   "-------": 0,
   "---------": 0
}
qset = {}
hasrung = {}

lns = sys.argv[1]

lns=lns.strip()
while lns:
 pr=''
 row=lns[0:9]
 ln=lns[9:]
 if '*' in ln:
   i=ln.index('*')
   j=ln.find('(', i)
   if j == -1:
      j = len(ln)
   mult=int(ln[i+1:j])
   k=ln.find(')', j)
   if  k== -1:
      lns=""
   else:
      lns=ln[k+1:].strip()
   ln=ln[0:i]*mult
   #print "count=%d" % mult
 else:
   lns=""
 print(row+ln)

 for i in range(0,len(ln)):
   c = ln[i]
   if c == '-':
      callat[row] = c
      row = bob(row)
   elif c == 'P':
      callat[row] = c
      row = plain(row)
   elif c == 'S' or c == '$':
      callat[row] = c
      row = single(row)
   else:
      continue

   allsxs[row] = i

   # Check for falseness
   sixt = sixtype(incourse(row))
   if sixt in hasrung:
      print("False six %s at %d and %d" % (row, hasrung[sixt], i))
   else:
      hasrung[sixt] = i

   # Count runs
   if pr and not pr.startswith(c):
      if pr not in count:
         count[pr] = 0
      count[pr] = count[pr] + 1
      pr=''
   pr=pr+c

   # See if all the B-block is present
   # Mark which parts are there
   cn,ix = canon(row)
   checkCanon(incourse(row), cn, ix)
   if cn not in sx:
      sx[cn] = 0
      sv[cn] = ['.']*10
   sx[cn] += 1
   sv[cn][ix]='-'
   #print ("%s %s %s" % (row, cn,sx[cn]))

   # See if all members of Q-set are present ABCDXXXQS
   qsr = row[0:4]+row[7:9]
   if qsr not in qset:
      qset[qsr] = 1
   else:
      qset[qsr] = qset[qsr] + 1

 # End of the touch, count the final run
 if pr:
    if pr not in count:
      count[pr] = 0
    count[pr] = count[pr] + 1

# Find the best complementary B-blocks for matching the most sixes
completebblocks = 0
bestbblocks={}
#print("sx item %s" % sx.items())
key=lambda k: k[0][6]+k[0][5]+k[0][4]+k[0][3]
for n in sorted(sx.items(), key=key):
   # used parts of a B-block
   tch = "".join(sv[n[0]])
   #print("%s %s" % (n,tch))
   rw=n[0]
   rwc=complement(rw)
   rwcn=canon(rwc)[0]
   if rwcn != rwc:
      print("Unexpected %s %s %s" % (rw, rwc, rwcn))
   if n[1] == 10:
      completebblocks = completebblocks + 1
   pair=rw[5:7]
   #print("%s %s" % (pair, n))
   if pair not in bestbblocks:
       bestbblocks[pair] = {} 
   d = bestbblocks[pair]
   d[n[0]] = n

#print("bestbblocks %s" % bestbblocks)
# Choose the best pair
nsixes=0
bestpair={}
for k,v in bestbblocks.items():
   bv=0
   bb=''
   for k2,v2 in v.items():
       n1 = v2[1]
       rwc = complement(v2[0])
       if rwc in v:
          n1 += v[rwc][1]
       if n1 > bv or n1 == bv and v2[1] == 10:
          #if n1 == bv:
            #print("Equal %s %s %d" % (bb,v2[0],n1))
          bv = n1
          bb = v2[0]
   bestpair[k] = bb
   nsixes+=bv
   #print("Best pair %s %s %s" % (k, bb, bv))

# Find paired sixes
npairedsixes=0
for row in allsxs:
    r1 = pairsix(row)
    if r1 in allsxs:
        npairedsixes=npairedsixes+1

nusedbblocks = len(allsxs) / 10

print ("%d complete B-blocks" % completebblocks)
print ("%d B-block sixes" % nsixes)
print ("%d paired sixes" % npairedsixes)
print ("%d used B-blocks" % nusedbblocks)
print ("%dx complete B-blocks" % (completebblocks + 84 - nusedbblocks))
#print ("%s" % bestbblocks)
#print ("%s" % bestpair)

# Reparse to identify parts not in B-blocks
starts=[]
lns = sys.argv[1]
alltouches=""
lns=lns.strip()
while lns:
 pr=''
 row=lns[0:9]
 ln=lns[9:]
 if '*' in ln:
   i=ln.index('*')
   j=ln.find('(', i)
   if j == -1:
      j = len(ln)
   mult=int(ln[i+1:j])
   k=ln.find(')', j)
   if  k== -1:
      lns=""
   else:
      lns=ln[k+1:].strip()
   ln=ln[0:i]*mult
   #print "count=%d" % mult
 else:
   lns=""
 # In a B-block
 inbb=True
 #print("%s%s*1(1)", % (row,ln))
 touch=''
 # Is there not a row at the start of the touch string?
 norow=False
 touchfirst=''

 #print("Considering %s" %ln)
 for i in range(0,len(ln)):
   inbb2 = matchblock(row)
   #print("%s inbb %s %s %d %s %s" % (row,inbb,inbb2,i,touch,touchfirst))
   if not inbb and inbb2:
      # Wasn't in a good B-block but now is
      if norow:
         # remember the first part of the touch to wrap
         touchfirst=touch
         norow=False
      else:
         #print("Adding a %s" % touch)
         alltouches += "%s*1(1)" % touch
      touch=''
   inbb = inbb2
   row1 = row
   c = ln[i]
   #print("c='%s'" % c)
   if c == '-':
      callat[row] = c
      row = bob(row)
      cb=cb+1
   elif c == 'P':
      callat[row] = c
      row = plain(row)
   elif c == 'S' or c == '$':
      callat[row] = c
      row = single(row)
      cs=cs+1
   else:
      if touch:
          #print("Adding b %s %s" % (touch,touchfirst))
          alltouches += "%s%s*1(1)" % (touch,touchfirst)
      touch=''
      continue
   if not inbb:
      if not touch:
          norow = True
      touch+=c
   elif not matchblock(row):
      #print("row1=%s matchblock=%s" % (row1, matchblock(row1)))
      touch=row1+c
      starts.append(row1)
   elif c != '-':
      # Assume Q-sets filtered out, then we indicate a switch from one B-block to another
      touch=row1+c
      starts.append(row1)
      #print("Adding c %s" % (touch))
      alltouches += "%s*1(1)" % (touch)
      touch = ''
 
 if touch:
    if norow:
        touch=row+touch
    #print("Adding d %s %s" % (touch,touchfirst))
    alltouches+="%s%s*1(1)" % (touch,touchfirst)
 elif touchfirst:
    #print("Adding e %s %s" % (touch,touchfirst))
    alltouches+="%s%s*1(1)" % (row,touchfirst)
 #print("end '%s' '%s' all='%s'" % (touch,touchfirst,alltouches)) 
 touch=''
print("1: %s" % alltouches)
alltouches1=alltouches

#print("starts=%s" % starts)   
for row in starts:
   cn,ix = canon(row)
   touch=row+'-'
   ix1=(ix+1) % len(sv[cn])
   #print("%s %d %s %s %s" % (row,ix,sv[cn],matchblock(row),matchblock(cn)))
   while sv[cn][ix1]=='.':
      touch+='-'
      ix1+=1 
      if ix1 >= len(sv[cn]):
         ix1=0
      if ix1 == ix:
         break
   #print touch

alltouches=""
for k,v in bestpair.items(): 
   tch = "".join(sv[v])
   #print("A: %s %s" % (v,tch))
   alltouches += printbb(v)
   v2=complement(v)
   if v2 in sv:
      tch = "".join(sv[v2])
      #print("B: %s %s" % (v2,tch))
      alltouches += printbb(v2)
   else:
      # Complement not present at all, so add it
      alltouches += v2+"----------*1(1)"

# Fix up singles
for row in starts:
   cn,ix = canon(row)
   touch=row+'-'
   ix1=(ix+1) % len(sv[cn])
   #print("%s %d %s %s %s" % (row,ix,sv[cn],matchblock(row),matchblock(cn)))
   if sv[cn][ix1]=='-':
       alltouches += touch + "*1(1)"

print("2: %s" % alltouches)

# Do the reverses
print("1: %s" % reverseall(alltouches1))
print("2: %s" % reverseall(alltouches))

scores = [0,1,3,6,10,15,21,28,36,45,55]
econ = 0
runs = 0 
for key, value in sorted(count.items(), key=lambda k: k): 
    print("%s: %s" % (key, value))
    if key.startswith('-'):
        runs=runs+value
        econ = econ + scores[len(key)] * value
#for key, value in sorted(qset.items(), key=lambda k,v: (k,v)): print "%s: %s" % (key, value)
print("%d bobs %d singles runs %d bobs per run %s economy %d" % (cb,cs,runs,(cb*1.0/runs),econ))

foundlink = {}
linkagesix = {}
links = 0
if len(sys.argv) > 2:
   fn = sys.argv[2]
   rnds = "1234567"
   with open(fn) as fp:  
      lines = fp.readlines()
      linen=0
      linkt = [0]*(len(lines)+1)
      for l in lines:
         l=l.strip()
         # Comment line
         if l[0] == "#":
            continue
         linen=linen+1
         ll = l.strip()[3:].replace(" ","")
         lls = ll.split("*1(1)")
         for indices in permutations(rnds):
            xform = "".join(indices)
            trantab = str.maketrans(rnds, xform)
            llnew = ""
            llnewa = []
            allsxs2={}
            for lls1 in lls:
               if not lls1:
                  continue
               row0=lls1[0:9]
               row = row0.translate(trantab)
               #print "translate %s by %s to %s" % (row0, xform, row)
               ln=lls1[9:]
               llnew=llnew+row+ln+"*1(1)"
               llnewa.append(row+ln+"*1(1)")
               #print "translate %s by %s to %s" % (row0, xform, row)
               for i in range(0,len(ln)):
                  if row not in allsxs:
                     #print "translate %s by %s to %s fail at %d" % (row0, xform, row, i)
                     llnew = ""
                     break;
                  c = ln[i]
                  if c != callat[row]:
                     # different call, so not a match
                     llnew = ""
                     #print "call mismatch"
                     break
                  if c == '-':
                     row = bob(row)
                  elif c == 'P':
                     row = plain(row)
                  elif c == 'S' or c == '$':
                     row = single(row)
               if row not in allsxs:
                  llnew = ""
                  break;
               if not llnew:
                  break
            if llnew:
               llnewas=str(sorted(llnewa))
               if llnewas not in foundlink:
                  foundlink[llnewas] = llnew
                  print("Found match %3d %s" % (linen, l))
                  print("      match        %s" % llnew)
                  links = links + 1
                  linkt[linen] = linkt[linen] + 1
               else:
                  continue
            else:
               continue

            # Remember these special sixes
            for lls1 in lls:
               if not lls1:
                  continue
               row0=lls1[0:9]
               row = row0.translate(trantab)
               ln=lls1[9:]
               for i in range(0,len(ln)):
                  c = ln[i]
                  if c == '-':
                     if i == 0:
                        if row not in linkagesix:
                           linkagesix[row] = "~"
                        elif linkagesix[row] != "~":
                           print("Changing %s %s to %s %s" % (row, linkagesix[row],".", linkagesix[row] == "~"))
                           linkagesix[row] = "."
                     else:
                        linkagesix[row] = "."
                     row = bob(row)
                  elif c == 'P':
                     if i == 0:
                        if row not in linkagesix:
                           linkagesix[row] = "r"
                        elif linkagesix[row] != "r":
                           print("Changing %s %s to %s %s" % (row, linkagesix[row],"L", linkagesix[row] == "r"))
                           linkagesix[row] = "L"
                     else:
                        linkagesix[row] = "L"
                     row = plain(row)
                  elif c == 'S' or c == '$':
                     row = single(row)
      for i in range(0, len(linkt)):
         if linkt[i] > 0:
            print("Link type %3d instances %2d" % (i, linkt[i]))
print("Found %d linkages" % links)


# Regenerate original peal
lns = sys.argv[1]
peal=""
lns=lns.strip()
while lns:
 row=lns[0:9]
 ln=lns[9:]
 if '*' in ln:
   i=ln.index('*')
   j=ln.find('(', i)
   if j == -1:
      j = len(ln)
   mult=int(ln[i+1:j])
   k=ln.find(')', j)
   if  k== -1:
      lns=""
   else:
      lns=ln[k+1:].strip()
   ln=ln[0:i]*mult
   #print "count=%d" % mult
 else:
   lns=""
 peal=peal+row
 for i in range(0,len(ln)):
   c = ln[i]
   if c == '-':
      if row in linkagesix:
         if linkagesix[row] == "~":
           peal=peal+linkagesix[row]
         else:
           if False and bob(row) in linkagesix:
             peal=peal+"~" # Experiment
           else:
             peal=peal+"."
      else:
         peal=peal+"="
      row = bob(row)
   elif c == 'P':
      if row in linkagesix:
         if linkagesix[row] == "r":
            peal=peal+linkagesix[row]
         else:
           if False and plain(row) in linkagesix:
             peal=peal+"r" # Experiment
           else:
             peal=peal+"L"
      else:
         peal=peal+"p"
      row = plain(row)
   elif c == 'S' or c == '$':      
      if row in linkagesix:
         peal=peal+linkagesix[row]
      else:
         peal=peal+"s"
      row = single(row)
   else:
      continue

 peal=peal+"*1(1)"
print(peal)
