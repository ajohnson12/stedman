#!/usr/bin/python
# Copyright (c) 2022 Andrew Johnson
# Give a canonical version of touches of the form:
# '2314567QSP--PP---P*1(1)5432167SQ-*10(1)'
# Searches for rotations / renumbering to allow two effectively identical lines are spotted

import sys
from itertools import permutations
from _ast import Or
import functools
#from str import maketrans 

def canon6(row):
   return row
   

# Generate the six-end after a plain
def plain(row):
   if row[8:9] == 'S':
      return plainS(row)
   else:
      return plainQ(row) 

# Generate the six-end after a bob
def bob(row):
   if row[8:9] == 'S':
      return bobS(row)
   else:
      return bobQ(row)

# Generate the six-end after a single
def single(row):
   if row[8:9] == 'S':
      return singleS(row)
   else:
      return singleQ(row)

# Generate the six-end after a plain on a quick six
def plainQ(row):
   row=place(row,7)
   
   row=place(row,3)
   return row
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   return row

# Generate the six-end after a plain on a slow six
def plainS(row):
   row=place(row,7)

   row=place(row,1)
   return row
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   return row

# Generate the six-end after a bob on a quick six
def bobQ(row):
   row=place(row,5)

   row=place(row,3)
   return row

   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   return row

# Generate the six-end after a bob on a slow six
def bobS(row):
   row=place(row,5)

   row=place(row,1)
   return row

   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   return row

# Generate the six-end after a single on a quick six
def singleQ(row):
   row=place(row,5,8)

   row=place(row,3)
   return row
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   return row

# Generate the six-end after a single on a slow six
def singleS(row):
   row=place(row,5,8)

   row=place(row,1)
   return row
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   return row

# Generate a new row after this place
# Don't swap the QS indicator if the place is 1 or 3
def place(row,n,n2=0):
   for i in range(0, n-1, 2):
      a=row[i]
      b=row[i+1]
      row=row[0:i]+b+a+row[i+2:]

   if n <= 3:
      lm = len(row)-3
   else:
      lm = len(row)-1

   if n2 == 0:
      n2 = n

   for i in range(n2, lm, 2):
      a=row[i]
      b=row[i+1]
      row=row[0:i]+b+a+row[i+2:]

   return row

# Is this row out of course?
def ooc(row):
   for i in range(0, len(row) -2):
      if row[i] > row[i+1]:
         row=row[0:i]+row[i+1]+row[i]+row[i+2:]
         return not ooc(row)
   return False

# Find the row at the start of the six from the row at the end
def sixhead(row):
    if row[7] == 'Q':
        row = place(row,3)
    else: 
        row = place(row,1)
    return row
 
# Return the in-course version of this row
def incourse(row):
   if ooc(row):
      #print "%s %s ooc" % (row, ooc(row))
      # Find six-head row
      row = sixhead(row)
      #print "%s %s ooc" % (row, ooc(row))
   return row

# The six-type
def sixtype(row):
   return incourse(row)[3:7]

# The canonical version of the B-block
def canon(row):
    row=incourse(row)
    # Find the biggest
    rw = row
    rw2 = rw
    o = order(rw)
    while True:
        o2 = order(rw)
        if o2 > o:
            o = o2
            rw2 = rw
        rw = bob(rw)
        if rw == row:
            break
    
    # Find the index
    ix = 0
    rw = rw2
    while rw != row:
        ix += 1
        rw = bob(rw)

    #print "%s %s" % (row, rw2)
    return (rw2, ix)

# Check that that canonical version is in the B-block   
def checkCanon(row, rw2, ix):
    for i in range(0, ix):
        rw2 = bob(rw2)
    if row != rw2:
        print("Error %s %s %d" % (row, rw2, ix))

def order(row):
   return row[-1]+row[-2]+row[3:-2][::-1]+row[2]+row[1]+row[0]
   return row[3::-1]+row[0:2:-1]+row[2]
   return row[-1]+row[-2]+row[3:-2]+row[0:3]

# Find the complement of a row for the pair of B-blocks
# Perhaps only works for a quick six??
def complement(row):
   return row[2:4]+row[0:2]+row[4:]

# Does this row match a six in the preferred B-Blocks
# True if it matches a used six in the preferred B-blocks
def matchblock(row):
   cn,ix = canon(row)
   pair=cn[5:7]
   bp = bestpair[pair]
   if bp==cn or complement(bp)==cn:
      return sv[cn][ix]=='-'
   else:
      return False

def printbb(row):
    touches=""
    rl = len(sv[row])
    row1=row
    for i in range(0,rl):
        if sv[row][i]=='-' and sv[row][(i+1)%rl] == '.':
            touch=row1+'-' 
            for j in range(1,rl):
                if sv[row][(i+j)%rl] == '.':
                    touch+='-'
                else:
                     break
            touches += "%s*1(1)" % touch
        row1=bob(row1)
    return touches

# Reverse a touch, and swap 1 2 to keep in-course
def reverse(touch):
    row = touch[0:9]
    touch = touch[9:]
    for c in touch:
        if c == '-':
            row = bob(row)
        elif c == 'S':
            row = single(row)
        else:
            row = plain(row)
    row = sixhead(row)
    # Make in-course?
    #print(row)
    row=row.replace('1','@').replace('2','1').replace('@','2')
    return row+touch[::-1]

def reverseall(touches):
    end=')'
    touches = touches.split(end)
    #print(touches)
    newt = ''
    for touch in touches:
        if touch:
            i1= touch.index('*')
            end1 = touch[i1:]
            touch = touch[0:i1]
            newt += reverse(touch)+end1+end
    return newt



def comptouch(t1, t2):
    #print("Compare %s %s" % (t1,t2))
    if len(t1) > len(t2):
        return -1;
    if len(t1) < len(t2):
        return 1;
    if t1[9:] < t2[9:]:
        return -1;
    if t1[9:] > t2[9:]:
        return 1;
    if t1[9:0:-1] > t2[9:0:-1]:
        return -1; 
    if t1[9:0:-1] < t2[9:0:-1]:
        return 1;
    return 0;

def complist(a,b):
    for i in range(0, len(a)):
        z = comptouch(a[i], b[i])
        if z < 0:
            return -1
        elif z > 0:
            return +1;
    return 0

def bestrot(lns, checkfalse, callat, allsxs, hasrung):
    pr=''
    row=lns[0:9]
    ln=lns[9:]
    if '*' in ln:
       i=ln.index('*')
       j=ln.find('(', i)
       if j == -1:
          j = len(ln)
       mult=int(ln[i+1:j])
       k=ln.find(')', j)
       if  k== -1:
          lns=""
       else:
          lns=ln[k+1:].strip()
       copies = ln[j:k+1]
       ln=ln[0:i]*mult
       #print "count=%d" % mult
    else:
       lns=""
    #print(row+ln)
       
    row0 = row;
    bestrotation = row+ln;
    for i in range(0,len(ln)):
        c = ln[i]
        if c == '-':
            if checkfalse:
                callat[row] = c
            row = bob(row)
        elif c == 'P':
            if checkfalse:
                callat[row] = c
            row = plain(row)
        elif c == 'S':
            if checkfalse:
                callat[row] = c
            row = single(row)
        else:
            continue
 
        if checkfalse:   
            allsxs[row] = i
    
            # Check for falseness
            sixt = sixtype(row)
            if sixt in hasrung:
                print("False six %s at %s and %d" % (row, hasrung[sixt], i))
            else:
                hasrung[sixt] = row

        rottouch = ln[i+1:] + ln[0:i+1]
        if row[7] == 'Q' and bestrotation[7] != 'Q' or (
            row[7] == bestrotation[7] and
            comptouch(row + rottouch, bestrotation) < 0):
            bestrotation = row + rottouch
    if row == row0:
        #print("bestrotation %s mult=%d len=%d" % (bestrotation, mult, len(ln)))
        bestrotation = bestrotation[0:(9+len(ln) // mult)] + '*' + str(mult) + copies
    #print(bestrotation)
    return bestrotation


def readtouch(lns):
    allsxs = {}
    callat = {}
    hasrung = {}
    sortedt = []
    lns=lns.strip()
    while lns:
        pr=''
        row=lns[0:9]
        ln=lns[9:]
        ln0=ln
        if '*' in ln:
           i=ln.index('*')
           j=ln.find('(', i)
           if j == -1:
              j = len(ln)
           mult=int(ln[i+1:j])
           k=ln.find(')', j)
           if  k== -1:
              lns=""
           else:
              lns=ln[k+1:].strip()
           copies = ln[j:k+1]
           ln0=ln[0:k+1]
           ln=ln[0:i]*mult
           #print "count=%d" % mult
        else:
           lns=""
        #print(row+ln)
        
        row0 = row;
        bestrotation = bestrot(row+ln0, True, callat, allsxs, hasrung);
        sortedt.append(bestrotation)
    #print("unsorted %s" % sortedt)
    sortedt.sort(key=functools.cmp_to_key(comptouch))
    #print("sorted   %s" % sortedt)
    #print("".join(sortedt))
    bestl = None
    for t in sortedt:
        if t[9:] != sortedt[0][9:]:
            break
        tx=[]
        r0 = t[0:7]
        r1 = '2314567'
        for t in sortedt:
            tt = ''
            for i in range(0,7):
                j = r0.find(t[i])
                tt += r1[j]
            tt += t[7:]
            tt = bestrot(tt, False, None, None, None)
            tx.append(tt)
        tx.sort(key=functools.cmp_to_key(comptouch)) 
        if bestl == None:
            bestl = tx
        else:
            z = complist(tx, bestl)
            if z < 0:
                bestl = tx 
    #print(sortedt)
    #print("".join(bestl))
    return (callat, hasrung, bestl)

if len(sys.argv) == 2:
    callat1, hasrung1,newtouch = readtouch(sys.argv[1])
    print("".join(newtouch))
    callat1r, hasrung1r,newtouchr = readtouch(reverseall(sys.argv[1]))
    print("".join(newtouchr))
elif len(sys.argv) == 6:
    callat1, hasrung1,newtouch = readtouch(sys.argv[3])
    callat1r, hasrung1r,newtouchr = readtouch(reverseall(sys.argv[3]))
    if complist(newtouch, newtouchr) <= 0:
        print("%s %s %s %s %s" % (sys.argv[1],sys.argv[2],"".join(newtouch),sys.argv[4],sys.argv[5]))
    else:
        print("%s %s %s %s %s" % (sys.argv[1],sys.argv[2],"".join(newtouchr),sys.argv[4],sys.argv[5]))



def matchtouch(lns,callat2,hasrung2):
    lns=lns.strip()
    rrx=""
    while lns:
        pr=''
        row=lns[0:9]
        ln=lns[9:]
        if '*' in ln:
           i=ln.index('*')
           j=ln.find('(', i)
           if j == -1:
              j = len(ln)
           mult=int(ln[i+1:j])
           k=ln.find(')', j)
           if  k== -1:
              lns=""
           else:
              lns=ln[k+1:].strip()
           ln=ln[0:i]*mult
           #print "count=%d" % mult
        else:
           lns=""
        #print(row+ln)
        matching = True
        rx=""
        for ps in [1,2]:
            for i in range(0,len(ln)):
                c = ln[i]
                sixt = sixtype(row)
                #print("i=%d row=%s%s matching=%s sixt=%s hasrung2=%s rx=%s" % (i,row,c,matching,sixt,hasrung2[sixt], rx))
                if ps == 2 and not matching and (sixt in hasrung2 and hasrung2[sixt] == row or
                    rx.startswith(row)):
                    rx += "*1(1)"
                    matching=True
                    break
                if sixt in hasrung2 and hasrung2[sixt] == row and callat2[row] == c:
                    if not matching:
                        rx += "*1(1)"
                    matching = True
                else:
                    if matching:
                        rx+=row
                    matching=False
                if c == '-':
                    row = bob(row)
                elif c == 'P':
                    row = plain(row)
                elif c == 'S':
                    row = single(row)
                else:
                    continue
                if not matching:
                    rx += c
            if matching:
                break;
        
        #if rx:
        #    rx += '*1(1)'
        if not matching and not rx:
            rx = '@' + row + len
        rrx += rx#if rx:
        #    print(rx)
    return rrx


#mt1=matchtouch(sys.argv[1],callat2,hasrung2)
#mt2=matchtouch(sys.argv[2],callat1,hasrung1)
#print("1: %s" % mt1)
#print("2: %s" % mt2)
#print("1: %s" % reverseall(mt1))
#print("2: %s" % reverseall(mt2))

