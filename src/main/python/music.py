#!/usr/bin/python
# Copyright (c) 2021 Andrew Johnson
# Music distribution for a peal
# Find the musical rows, and the intervals between them
import sys
import string
import re
import math
import os

if len(sys.argv) > 1:
    fn1=sys.argv[1]
    with open(fn1) as f1:
        lns1 = f1.readlines()
else:
    lns1 = sys.stdin.readlines()

if len(sys.argv) > 2:
    fn2=sys.argv[2]
else:
    fn2 = os.path.join(os.path.dirname(os.path.abspath(__file__)) ,"music.re")
with open(fn2) as f2:
   lns2 = f2.readlines()

mt = 0
lastm = -1
lastm = -8
mtd = 0
mtsq = 0
firstm = -1;
rownm = 0
subtotals = {}
usesubrow = False
n = 0
hist = {}
for i in range(0, len(lns1)):
   row = lns1[i].strip()
   if i == 0:
      lastm2 = -len(row)
   if i == 0 and row == lns1[-1].strip():
      lastm = 0
      lastm2 = 0
      continue
   m1 = 0
   type = ''
   n = n + 1
   matched = {}
   if usesubrow:
      bpm = len(row)
   else:
      bpm = 1
   for bellpos in range(0, bpm):
      for music in lns2:
         music = music.strip()
         if not music:
            continue
         if music.startswith("#"):
            type = music
            continue
         if re.search(music.strip(), row):
            # Extract just the bells
            bells=music.replace(".","").replace("*","").replace("^","").replace("$","")
            ix = row.index(bells[-1])
            #print("%s %s %s %s" % (music,bells,ix,bellpos))
            if usesubrow and ix != bellpos:
               continue
            idx = i+1.0*bellpos/len(row)
            idx2 = i*len(row) + bellpos
            d = idx - lastm
            d2 = idx2 - lastm2
            #print("%d row %s matches %s dist %f %d" % (i, row, music, d, d2))
            if firstm < 0:
               firstm = i
            mt = mt + 1
            m1 = m1 + 1
            mtd = mtd + d
            mtsq = mtsq + d*d
            lastm = idx
            lastm2 = idx2
            if type not in subtotals:
               subtotals[type] = 1
            else:
               subtotals[type] = subtotals[type] + 1
            if d not in hist:
               hist[d] = 1
            else:
               hist[d] = hist[d] + 1
   if m1:
      #print("%s %d %d" % (row, m1, (i - rownm)))
      rownm = i
#print("mt=%d" % mt)
mean = float(mtd) / mt
sd = math.sqrt(float(mtsq) / mt - mean)
print("Music total = %d rows = %d mean distance = %f sd = %f " % (mt, n, mean, sd))
for s in subtotals:
   print("%s %s" % (s,subtotals[s]))
if False:
   for t in sorted(hist):
      print("%d : %d" % (t, hist[t]))

   
      


