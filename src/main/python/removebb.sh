#!/bin/bash
# Copyright (c) 2021 Andrew Johnson
# Take sequence of round blocks, remove unpartnered B-blocks and force partnered B-blocks to stay in pairs
# Makes it easier to find a peal
r=$(echo "$1" | sed -e 's/.......[QS][QS][=#~-]*\*1(1)//g')
echo $r
rp=$(partner.py "$1" | grep "~")
rpr=$(partner.py "$1" | grep "#\*1(1).*)")
echo "${r//)/)$'\n'}" | grep -c 'P'
echo "${rp//)/)$'\n'}" | grep -c '~'
echo "${rpr//)/)$'\n'}" | grep -c '#'
echo $rp$r$rpr
