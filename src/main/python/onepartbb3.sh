#!/bin/sh
# Copyright (c) 2021,2022 Andrew Johnson
# From some odd blocks files attempt to find a 3-part peal
# options: <file> <#comp> <onepart3.sh options>
onepart.sh "$(findbb.sh "$(expand.py 3456127 1234567 $(head -$2 "$1"  | tail -1 | cut -d ' ' -f 3))" | tail -1)" "${@:3}"

