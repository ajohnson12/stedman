#!/usr/bin/python
# Copyright (c) 2019,2022 Andrew Johnson
# See how similar two peals are:
# Counts row and following rows which match as a pair the reference peal
# Chooses the best rotation 
# Results:
# c10029.rows=5600 rows ./c10162.rows=5090 rows rotate 224:>14235678 start=13425678 same rows=5056 99.33% same next row=5050; 99.88% of same rows; 99.21% of all rows
# 99.21% of all rows in the second composition are followed by the same row as in the first
# similarrot c12345.row c34567.rows 95 [-t|-T]
# -t consider all rotations, not just treble lead
# -T only consider treble leads for similarity
import sys
import string

fn1 = sys.argv[1]
fn2 = sys.argv[2]

if len(sys.argv) > 3:
    mintoprint = float(sys.argv[3])
else:
    mintoprint = 0

verbose=False
# Match rotations / reflections if treble in same place
matchTreble = True
matchTrebleLeadsOnly = False
if len(sys.argv) > 4:
    if sys.argv[4] == '-t':
        matchTreble = False
        matchTrebleLeadsOnly = False
    elif sys.argv[4] == '-T':
        matchTreble = True
        matchTrebleLeadsOnly = True

with open(fn1, "r") as f1:
   ls1 = f1.readlines()

with open(fn2, "r") as f2:
   ls2 = f2.readlines()

def trebleleads(ls):
    res = [ls[0]]
    prev = None
    for l in ls:
        if prev and prev[0] == '1' and l[0] == '1':
            res.append(prev)
            res.append(l)
        prev = l
    return res

next={}

n = 0

prev = None
for l in ls1:
   if prev:
      if not matchTrebleLeadsOnly or (l[0] == '1' and prev[0] == '1'):
        next[prev] = l
        n = n + 1
   prev = l

def match(ls2, next, bv):
   n = len(next)
   s0 = 0
   s1 = 0
   s2 = 0
   prev = None
   for l in ls2:
      if prev:
         if not matchTrebleLeadsOnly or (l[0] == '1' and prev[0] == '1'):
            s0 = s0 + 1
            if prev in next:
               s1 = s1 + 1
               if next[prev] == l:
                  s2 = s2 + 1
      prev = l
      if len(ls2) - s0 + s2 < bv:
         # early break as cannot beat previous
         #print "Break at %d" % s0
         break
 
   return (n, s0, s1, s2)

# Find the best rotation or reversal
best=None
bv = 0
nls2 = len(ls2) - 1
if matchTrebleLeadsOnly:
    prev = None
    nls2 = 0
    for l in ls1:
       if prev and prev[0] == '1' and l[0] == '1':
           nls2 = nls2 + 1
bv = int(nls2 * mintoprint // 100)
i = 0
mod = 32
msg = "%s=%d rows %s=%d rows rotate %d:%s start=%s same rows=%d %.2f%% same next row=%d; %.2f%% of same rows; %.2f%% of all rows"
for r in ls2:
   i = i + 1
   if matchTreble:
     if r.find('1') != ls1[0].find('1'):
        continue
   trans = str.maketrans(r, ls1[0])
   ls3 = []
   for r2 in ls2:
      ls3.append(r2.translate(trans))
   (r1,n2,r2,r3) = match(ls3, next, bv)
   if r3 > bv or r3 == bv and (best == None or best[3][0] == '<'):
      bv = r3
      best=(r1,n2,i-1, ">"+r.strip(), ls3[0].strip(), r2,r3)
      if verbose:
          print(msg % (fn1, r1, fn2, n2, best[2], best[3], best[4], r2, 100.0*r2/n2, r3, 100.0*r3/r2, 100.0*r3/n2))

   # Try a reversal
   ls4 = ls3[0:i][::-1]+ls3[i - 1:-1][::-1]
   (r1,n2,r2,r3) = match(ls4, next, bv)
   if r3 > bv:
      bv = r3
      best=(r1,n2,i-1, "<"+r.strip(), ls3[0].strip(), r2,r3)
      if verbose:
          print(msg % (fn1, r1, fn2, n2, best[2], best[3], best[4], r2, 100.0*r2/n2, r3, 100.0*r3/r2, 100.0*r3/n2))   


if best != None:
    (r1,n2,i,r, rs, r2,r3) = best
    if 100.0*r3/n2 >= mintoprint:
        print(msg % (fn1, r1, fn2, n2, i, r, rs, r2, 100.0*r2/n2, r3, 100.0*r3/r2, 100.0*r3/n2))
