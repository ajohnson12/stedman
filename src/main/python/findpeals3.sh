#!/bin/bash
# Copyright (c) 2021 Andrew Johnson
#See if any of the results of a testall search actually generate peals
#Call as:
#  findpeals3.sh "sted3_magic4/sted3.pb.68.*.sat"
for f in ${1}
do
   outf="${f%.*}.findpeal"
   echo ${f} ${outf}
   if [ ! -s ${outf} ]; then
       findpeal3part.sh ${f} | tee ${outf}
       if grep -q '5040!1 ' ${outf}; then break; fi
   fi
done
