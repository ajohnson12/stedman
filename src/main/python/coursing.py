#!/usr/bin/python
# Copyright (c) 2017,2021 Andrew Johnson
# Find best coursing pair

import sys

if len(sys.argv) > 1:
   fn = sys.argv[1]
else:
   fn = None

def getpn(row1, row2):
   pn = ''
   for i in range(0, len(row1)):
      if row1[i] == row2[i]:
         pn = pn + str(i+1)
   if pn == '':
      pn = 'X'
   return pn      

def call(pn):
   if pn == '14':
      return '-'
   elif pn == '1234':
      return 'S'
   else:
      return ' '

def callingpos(row1,row2, tenor):
   p=row2.index(tenor)
   ps = '1IOFVMWH'
   return ps[p]

def addpn(method,pn):
   if pn == 'X' or not method or method[-1] == 'X':
      method = method + pn
   else:
      method = method + '.' + pn
   return method
   
   

def processrows(f):
   previousmethod = ''
   method = ''
   touch = ''
   tenor = ''
   leadhead = ''
   r1 = ''
   totals = {}
   for r in f:
      r = r.strip()
      if r1:
         if r == r1:
            continue
      else:
         r1 = r
      for i in range(0, len(r)):
         for j in range(i + 1, len(r)):
            if i == j:
               continue
            b1 = r[i]
            b2 = r[j]
            dist = j - i
            if b1 > b2:
               b3 = b1
               b1 = b2
               b2 = b3
            pair = b1+b2
            if pair not in totals:
               totals[pair] = [[0]*len(r),[0]*len(r),[0]*len(r),[0]*len(r)]
            trebpos = r.find("1")
            if i <= trebpos and j <= trebpos:
               pos = 1
            elif i >= trebpos and j >= trebpos:
               pos = 3
            else:
               pos = 2
            totals[pair][0][dist] = totals[pair][0][dist] + 1
            totals[pair][pos][dist] = totals[pair][pos][dist] + 1

   best = [([],0,[]),([],0,[]),([],0,[]),([],0,[])]
   for k,v in totals.items():
      for i in range(0, len(v)):
         t = v[i]
         print("pair %s %s" % (k,t))
         for j in range(0, len(t)):
            if t[j] > best[i][1]:
               best[i] = ([j], t[j], [k])
               print("%s" % str(best[i]))
            elif t[j] == best[i][1]:
               best[i][0].append(j)
               best[i][2].append(k)
               
   print("All positions %s" % (str(best[0])))
   print("Below treble  %s" % (str(best[1])))
   print("Around treble %s" % (str(best[2])))
   print("Above treble  %s" % (str(best[3])))

if fn:
   with open(fn) as f:
      touch = processrows(f)
else:
   touch = processrows(sys.stdin)



