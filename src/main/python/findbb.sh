#!/bin/bash
# Copyright (c) 2021,2022 Andrew Johnson
# Find the most BB blocks available from a one-part set of blocks, by bobbing Q-sets
# findbb.py <touch> [<min number of bobs>]
ll=$(echo "${1}" | tr P- r~)
# count number of bobs
# Was 0x55
opt=0x0
bb="${ll//[^~]}"
nb=${#bb}
rr="${ll//[^r]}"
nr="${#rr}"
tc=$((nb+nr))
parts=$((840 / tc))
nr=$((nr * parts))
nb=$((840 - nr))
nb="${2-${nb}}"

#for ((i=${nb};i<=840;i=i+3));
i=$nb
echo "${i}"
java StedToSAT sted1.pb "${ll}${sp2}" 0 "" $opt sted1.sat ${i} 1 1 1
cadical --sat sted1.sat -w sted1.satout
if [[ $? == 10 ]]; then
  java StedToSAT sted1.pb "${ll}${sp2}" 0 sted1.satout $opt sted1.sat ${i} 1 1 1
  best=$(tail -1 sted1.sat)
  echo "best=${best}"
  bestt=$(echo "$best" | cut -d ' ' -f 3)
  np=$(echo "$best" | cut -d ' ' -f 4)

  bestrr="${bestt//[^r]}"
  bestnr="${#bestrr}"
else
  # No Q-sets
  bestt="${ll}${sp2}"
  bestnr=0
  np=$nr
fi
nb2=$((840 - np + bestnr))
echo "nb2=$nb2 np=$np bestnr=$bestnr"

echo "bestt=${bestt}"
java StedToSAT sted1.pb "$bestt" 0 "" $opt sted1.sat ${nb2} 1 1 0
cadical --sat sted1.sat -w sted1.satout
java StedToSAT sted1.pb "$bestt" 0 sted1.satout $opt sted1.sat ${nb2} 1 1 0
best=$(tail -1 sted1.sat)
echo ${best}
best=$(echo "${best//$'\r'/}" | cut -f 3 -d ' ')
echo "best = ${best}"
removebb.sh "${best}"
