#!python
# Copyright (c) 2021 Andrew Johnson
# Find 3-part blocks with a lot of paired B-blocks
# Select n of m paired B-blocks
# Usage: genbb3part.py <start> <n>
import itertools
import os
import sys
a=[
"3426175QS----------*1(3)3146275SQ----------*1(3)",
"4637251QS----------*1(3)4267351SQ----------*1(3)",
"1637452QS----------*1(3)1467352SQ----------*1(3)",
"1536472QS----------*1(3)1456372SQ----------*1(3)",
"4627153QS----------*1(3)4167253SQ----------*1(3)",
"6745312QS----------*1(3)6375412SQ----------*1(3)",
"6137254QS----------*1(3)6217354SQ----------*1(3)",
"6715324QS----------*1(3)6375124SQ----------*1(3)",
"4715623QS----------*1(3)4675123SQ----------*1(3)",
"6735421QS----------*1(3)6475321SQ----------*1(3)",
"7541326QS----------*1(3)7351426SQ----------*1(3)",
"6741325QS----------*1(3)6371425SQ----------*1(3)",
"5162437QS----------*1(3)5412637SQ----------*1(3)",
"4315267SQ----------*1(3)2314567QS----------*1(3)"];


if len(sys.argv) > 2:
    np=int(sys.argv[2])
else:
    np=5

w=False
if w:
   plen = 280
else:
   plen=280-np*20

ii = 0
st=0
if len(sys.argv) > 1:
    st = int(sys.argv[1])

for p in itertools.combinations(a, np):
    ii+=1
    if (ii < st):
        continue
    s="";
    for c in a:
        if c not in p:
            s=c[24:33]  
            break
    prefix=("".join(p)+s)
    if w:
        prefix=prefix.replace("-","#")
    cmd="java StedToSAT sted3_3456127.pb '%s' %d sted3.satout 0 sted3.sat" % (prefix, plen)
    print("%s: %s" % (ii,cmd))
    #continue
    rc=os.system(cmd)
    if rc != 0:
       break
    rc=os.system("cadical --sat sted3.sat -w sted3.satout")
    rc1=os.WEXITSTATUS(rc)
    print(rc1)
    if rc1 == 10:
        os.system(cmd)
        os.system("tail -1 sted3.sat")
        break




