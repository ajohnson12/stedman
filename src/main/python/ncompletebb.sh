#!/usr/bin/bash
# Copyright (c) 2022 Andrew Johnson
# Extracts blocks with exact nn complete B-blocks (or prints numbers of complete B-blocks if none specified)
if [ -z "$1" ]; then
    xargs -I{} bash -c 'procsixes2.py $(echo "{}" | cut -d " " -f 3) | grep "complete B-blocks"'
else
    xargs -I{} bash -c 'procsixes2.py $(echo "{}" | cut -d " " -f 3) | grep -q "^'$1' complete B-blocks" && echo "{}"'
fi
