#!/usr/bin/python
# Copyright (c) 2021 Andrew Johnson
# Finds point for minimum 87s at back
import sys
import string
import re
import math

fn1=sys.argv[1]
with open(fn1) as f1:
   lns1 = f1.readlines()

total = {}
firstrow = {}
most = {}
mostn=0
i = -1
for l in lns1:
   l = l.strip()
   i += 1
   last2 = l[-2:]
   last4 = l[-4:]
   if last4 not in most:
       most[last4] = 1
       firstrow[last4] = i
   else:
       most[last4] += 1
   if most[last4] > mostn:
       mostn = most[last4]
   if i % 2 == 0:
      # Backstroke
      if last2 not in total:
         total[last2] = 0
         if last2 not in firstrow:
            firstrow[last2] = i
      if l[0] == '1' and lns1[firstrow[last2]][0] != '1':
         firstrow[last2] = i
      if l[0] == '1' and lns1[firstrow[last4]][0] != '1':
         firstrow[last4] = i
      continue
   # handstroke
   if last2 not in total:
      total[last2] = 1
      if last2 not in firstrow:
         firstrow[last2] = i
   else:
      total[last2] += 1

bt = -1
for k in total:
   if total[k] == 0:
       print("Found good pair %s %s %d" % (k,lns1[firstrow[k]],firstrow[k]))
   if bt == -1 or total[k] < bt:
       bt = total[k]

if bt > 0:
   for k in total:
      if total[k] == bt:
          print("Found okay pair %s %d %s %d" % (k,total[k],lns1[firstrow[k]],firstrow[k]))

for k in most:
   if most[k] == mostn:
       print("Found best last4 %s %d %s %d" % (k,most[k], lns1[firstrow[k]], firstrow[k]))

