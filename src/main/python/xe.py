#!/usr/bin/python
# Copyright (c) 2017,2022 Andrew Johnson
# converts Erin Triples touch starting e.g.
# 2314567SSpp----ppp*2(1) to a B-block formatted touch
import fileinput

rounds='1234567'
courselen=7

print('7 bells;')
print('rounds \''+rounds+'\'')
# tabs=True for pasting into CompLib, false to display as text
tabs=True
if tabs:
    print(r'sep="\t\"')
    print(r'term=":\"')
    seplen=8
else:
    print(r'sep=" \"')
    print(r'term="\"')
    seplen=2
print(r'p=sep," \",+7.3.1.3.1.3')
print(r'b=sep,"-\",+5.3.1.3.1.3')
print(r's=sep,"s\",+567.3.1.3.1.3')

print(r'title="","5040 Erin Triples","Andrew Johnson"')
srr=''
for i in range(1, courselen+1):
   if tabs:
       colnum = i
   else:
       colnum = (i % 1)
   srr=srr + "sep,"+r'"%d\",' % (colnum)
if tabs:
   print('sr = %ssep' % srr)
   print('ln = sep,""')
else:
   print('sr = %ssep,"@"' % srr)
   print('ln = sep,"%s"' % ("_" * (seplen*(courselen) + len(rounds))))

if tabs:
    whenline='1234567'
else:
    whenline='*7'
nc = 1

print(r'eolnoterm=sep,"@",{/%s/: ln}' % (whenline))
print(r'eol=term,eolnoterm')

mn=130 # diff couses
mincalls=840 # diff calls

def repeats(r):
   rx = r.split(",")
   l = len(rx)
   for r in range(l, 1, -1):
      for i in range(0, l - r):
         for j in range(i + 1, l - r):
            if rx[i:i+r] == rx[j:j+r]:
                print("// Match %s" % ",".join(rx[i:i+r]))
                


for line in fileinput.input():
   m = {}
   mm = []
   ln = ''
   c = 0
   n=0
   rr=''
   st = len(line)
   rp = 1
   bc=0
   sc=0
   firstp = line.find('P')
   if firstp >= 0:
       st = firstp
   firstb = line.find('-')
   if firstb >= 0 and firstb < st:
       st = firstb
   firsts = line.find('$')
   if firsts >= 0 and firsts < st:
       st = firsts
   rounds2 = line[st-9:st-2]
   qs2 = line[st-2:st]

   if (qs2 == 'SS' or qs2 == 'SQ') and rounds2 != rounds and rounds2 != '':
       rounds = rounds2
       print('rounds \''+rounds+'\'')

   for i in range(st, len(line), 1):
       p = line[i:i+1]
       if p == 'P':
          q='p,'
       elif p == '-':
          q='b,'
          bc=bc+1
       elif p == '$':
          q='s,'
          sc=sc+1
       elif p.startswith('*'):
          q=''
          rp=int(line[i+1])
          bc=bc*rp
          sc=sc*rp
       else:
          continue
       ln = ln + q
       if len(ln) == courselen*2 or i+2 >= len(line) or (line[i+1] != 'P' and line[i+1] != '-' and line[i+1] != '$'):
           if ln != '':
               if len(ln) < courselen*2:
                  if seplen == 2:
                      ln = ln + r'": \",' + r'sep," \",'*(courselen - len(ln)//2 - 1)
                  else:
                      ln = ln + r'":\",' + r'sep," \",'*(courselen - len(ln)//2)
               if ln not in m:
                  n=n+1
                  m[ln]=n
                  rr=rr+"r%d," % n
               else:
                  c = c + 1
                  rr=rr+"r%d," % m[ln]
               ln = ''
   if n > 0 and n <= mn and bc <= mincalls:
      print(ln)
      for i in range(1,n+1):
          ln = list(m.keys())[list(m.values()).index(i)]
          if ':' in ln:
              print('r%d=%seolnoterm' % (i,ln))
          else:
              print('r%d=%seol' % (i,ln))
      print("part=%s" % (rr[:-1]))
      if rp > 1:
          if sc > 0:
              print('subtitle="Exact %d-part No %d","%d different courses, %d bobs, %d singles"' % (rp,nc,n,bc,sc))
          else:
              print('subtitle="Exact %d-part No %d","%d different courses, %d bobs"' % (rp,nc,n,bc))
      else:
          if sc > 0:
              print('subtitle="No %d","%d different courses, %d bobs, %d singles"' % (nc,n,bc,sc))
          else:
              print('subtitle="No %d","%d different courses, %d bobs"' % (nc,n,bc))
      print('start=title,subtitle,"",sr,ln')
      print('prove %dpart' % (rp))
   if n > 0:
      nc = nc + 1

