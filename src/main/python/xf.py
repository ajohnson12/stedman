#!/usr/bin/python
# Copyright (c) 2017,2022 Andrew Johnson
# converts Stedman Triples touch starting e.g.
# 2314567QSpp----pppppppp*2(1) to a B-block formatted touch
import fileinput

rounds='2314567'
courselen=10

print('7 bells;')
print('rounds \''+rounds+'\'')
# tabs=True for pasting into CompLib, false to display as text
tabs=True
if tabs:
    print(r'sep="\t\"')
    print(r'term=":\"')
    seplen=8
else:
    print(r'sep=" \"')
    print(r'term="\"')
    seplen=2
print(r'p_p=sep," \",+7.3.1.3.1.3,sep," \",+7.1.3.1.3.1')
print(r'p_b=sep," \",+7.3.1.3.1.3,sep,"-\",+5.1.3.1.3.1')
print(r'b_p=sep,"-\",+5.3.1.3.1.3,sep," \",+7.1.3.1.3.1')
print(r'b_b=sep,"-\",+5.3.1.3.1.3,sep,"-\",+5.1.3.1.3.1')
print(r'p_s=sep," \",+7.3.1.3.1.3,sep,"s\",+567.1.3.1.3.1')
print(r's_p=sep,"s\",+567.3.1.3.1.3,sep," \",+7.1.3.1.3.1')
print(r's_b=sep,"s\",+567.3.1.3.1.3,sep,"-\",+5.1.3.1.3.1')
print(r'b_s=sep,"-\",+5.3.1.3.1.3,sep,"s\",+567.1.3.1.3.1')
print(r's_s=sep,"s\",+567.3.1.3.1.3,sep,"s\",+567.1.3.1.3.1')
qs='QS'
print(r'title="","5040 Stedman Triples","Andrew Johnson"')
srr=''
for i in range(1, courselen+1):
   if tabs:
       colnum = i
   else:
       colnum = (i % 1)
   srr=srr + "sep,"+r'"%d\",' % (colnum)
if tabs:
   print('sr = %ssep' % srr)
   print('ln = sep,""')
else:
   print('sr = %ssep,"@"' % srr)
   print('ln = sep,"%s"' % ("_" * (seplen*(courselen) + len(rounds))))

whenline='*1234567'
# whenline='??????1'
#whenline='??1????'
#whenline='?????1?'
whenline1='2314567'
whenline2='3517246'
whenline3='5216374'
whenline1='2314567'
whenline2='4536127'
whenline3='6152347'
whenlinex='[235][352]1[476][523][647][764]'
nc = 1
if tabs:
    whenline=rounds

if tabs:
    print(r'eolnoterm=sep,""')
else:
    print(r'eolnoterm=sep,"@",{/%s/: ln; /%s/: ln; /%s/: ln; /%s/: ln}' % (whenline, whenline1, whenline2, whenline3))
print(r'eol=term,eolnoterm')

mn=150 # diff couses
mincalls=840 # diff calls

def repeats(r):
   rx = r.split(",")
   l = len(rx)
   for r in range(l, 1, -1):
      for i in range(0, l - r):
         for j in range(i + 1, l - r):
            if rx[i:i+r] == rx[j:j+r]:
                print("// Match %s" % ",".join(rx[i:i+r]))
                


for line in fileinput.input():
   m = {}
   mm = []
   ln = ''
   c = 0
   n=0
   rr=''
   st = len(line)
   rp = 1
   bc=0
   sc=0
   firstp = line.find('P')
   if firstp >= 0:
       st = firstp
   firstb = line.find('-')
   if firstb >= 0 and firstb < st:
       st = firstb
   firsts = line.find('$')
   if firsts >= 0 and firsts < st:
       st = firsts
   rounds2 = line[st-9:st-2]
   qs2 = line[st-2:st]

   if (qs2 == 'QS' or qs2 == 'SQ') and rounds2 != rounds and rounds2 != '':
       rounds = rounds2
       print('rounds \''+rounds+'\'')

   if qs != qs2 and qs2 != '':
       if qs2 == 'QS':
           qs = qs2
           print(r'p_p=sep," \",+7.3.1.3.1.3,sep," \",+7.1.3.1.3.1')
           print(r'p_b=sep," \",+7.3.1.3.1.3,sep,"-\",+5.1.3.1.3.1')
           print(r'b_p=sep,"-\",+5.3.1.3.1.3,sep," \",+7.1.3.1.3.1')
           print(r'b_b=sep,"-\",+5.3.1.3.1.3,sep,"-\",+5.1.3.1.3.1')
           print(r'p_s=sep," \",+7.3.1.3.1.3,sep,"-\",+567.1.3.1.3.1')
           print(r's_p=sep,"-\",+567.3.1.3.1.3,sep," \",+7.1.3.1.3.1')
           print(r's_b=sep,"-\",+567.3.1.3.1.3,sep,"-\",+5.1.3.1.3.1')
           print(r'b_s=sep,"-\",+5.3.1.3.1.3,sep,"-\",+567.1.3.1.3.1')
           print(r's_s=sep,"-\",+567.3.1.3.1.3,sep,"-\",+567.1.3.1.3.1')
       elif qs2 == 'SQ':
           qs = qs2
           print(r'p_p=sep," \",+7.1.3.1.3.1,sep," \",+7.3.1.3.1.3')
           print(r'p_b=sep," \",+7.1.3.1.3.1,sep,"-\",+5.3.1.3.1.3')
           print(r'b_p=sep,"-\",+5.1.3.1.3.1,sep," \",+7.3.1.3.1.3')
           print(r'b_b=sep,"-\",+5.1.3.1.3.1,sep,"-\",+5.3.1.3.1.3')
           print(r'p_s=sep," \",+7.1.3.1.3.1,sep,"-\",+567.3.1.3.1.3')
           print(r's_p=sep,"-\",+567.1.3.1.3.1,sep," \",+7.3.1.3.1.3')
           print(r's_b=sep,"-\",+567.1.3.1.3.1,sep,"-\",+5.3.1.3.1.3')
           print(r'b_s=sep,"-\",+5.1.3.1.3.1,sep,"-\",+567.3.1.3.1.3')
           print(r's_s=sep,"-\",+567.1.3.1.3.1,sep,"-\",+567.3.1.3.1.3')
   for i in range(st, len(line), 2):
       p = line[i:i+2]
       p = p.replace('p','P').replace('r','P').replace('R','p').replace('L','p').replace('=','-').replace('~','-').replace('_','-').replace('.','-')
       if p == 'PP':
          q='p_p,'
       elif p == 'P-':
          q='p_b,'
          bc=bc+1
       elif p == '-P':
          q='b_p,'
          bc=bc+1
       elif p == '--':
          q='b_b,'
          bc=bc+2
       elif p == '$$':
          q='s_s,'
          sc=sc+2
       elif p == 'P$':
          q='p_s,'
          sc=sc+1
       elif p == '$P':
          q='s_p,'
          sc=sc+1
       elif p == '-$':
          q='b_s,'
          bc=bc+1
          sc=sc+1
       elif p == '$-':
          q='s_b,'
          bc=bc+1
          sc=sc+1
       elif p.startswith('*'):
          q=''
          rp=int(p[1])
          bc=bc*rp
          sc=sc*rp
       else:
          continue
       ln = ln + q
       if len(ln) == courselen*2 or i+2 >= len(line) or (line[i+2] != 'P' and line[i+2] != '-' and line[i+2] != '$'):
           if ln != '':
               if len(ln) < courselen*2:
                  if seplen == 2:
                      ln = ln + r'": \",' + r'sep," \",'*(courselen - len(ln)//2 - 1)
                  else:
                      ln = ln + r'":\",' + r'sep," \",'*(courselen - len(ln)//2)
               if ln not in m:
                  n=n+1
                  m[ln]=n
                  rr=rr+"r%d," % n
               else:
                  c = c + 1
                  rr=rr+"r%d," % m[ln]
               ln = ''
   if n > 0 and n <= mn and bc <= mincalls:
      print(ln)
      for i in range(1,n+1):
          ln = list(m.keys())[list(m.values()).index(i)]
          if ':' in ln:
              print('r%d=%seolnoterm' % (i,ln))
          else:
              print('r%d=%seol' % (i,ln))
      print("part=%s" % (rr[:-1]))
      if rp > 1:
          if sc > 0:
              print('subtitle="Exact %d-part No %d","%d different courses, %d bobs, %d singles"' % (rp,nc,n,bc,sc))
          else:
              print('subtitle="Exact %d-part No %d","%d different courses, %d bobs"' % (rp,nc,n,bc))
      else:
          if sc > 0:
              print('subtitle="No %d","%d different courses, %d bobs, %d singles"' % (nc,n,bc,sc))
          else:
              print('subtitle="No %d","%d different courses, %d bobs"' % (nc,n,bc))
      if qs == 'SQ':
          print('start=title,subtitle,"Slow six start","",sr,ln')
      else:
          print('start=title,subtitle,"",sr,ln')
      print('prove %dpart' % (rp))
   if n > 0:
      nc = nc + 1

