#!/bin/bash
# Copyright (c) 2021,2022 Andrew Johnson
# Find a full peal given a sequence of round blocks, change P- to r~ to allow Q-set linkages
tch=$(echo "${1}2314567QS" | tr P- r~)
#was 0x55
opts=0x0
java StedToSAT sted1.pb "$tch" ${len:-840} sted1.satout $opts sted1.sat "${@:2}"
# Choose CaDiCal or Kissat
#cadical --sat sted1.sat -w sted1.satout
#rc=$?
kissat --sat --color sted1.sat | tee >(grep '^[sv] '>sted1.satout)
rc="${PIPESTATUS[0]}"
if [ $rc -eq 10 ]; then
    java StedToSAT sted1.pb "$tch" ${len:-840} sted1.satout $opts sted1.sat "${@:2}"
    tail -1 sted1.sat
fi
(exit $rc)
