#!python
# Copyright (c) 2021 Andrew Johnson
# Find partner B-blocks from the round blocks given, 
# mark with # to force partner B-blocks in StedToSAT
import sys
v = sys.argv[1]

l = v.split(')')
prev = {}
rest=''
for p in l:
    if '----------' in p:
        p=p+')'
        p=p.replace('-','#')
        pair = p[5:7] 
        if pair in prev:
            print(prev[pair])
            print(p)
            rest=rest+prev[pair]+p
            del prev[pair]
        else:
            prev[pair] = p
r=''
for t in prev:
    r += prev[t].replace('#','~')
print(r)
print(rest)

