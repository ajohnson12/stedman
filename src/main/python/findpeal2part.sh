#!/bin/bash
# Copyright (c) 2021 Andrew Johnson
# Find a peal given a result from a 2-part search finding round blocks containing magic links
tch=$(grep '\*' "$1" | head -1)
tch=${tch//$'c '/}
tch=${tch//$'\n'/}
tch=${tch//$'\r'/}
echo "'${tch}'"
best=$(tail -1 "$1")
for i in {405..420..3}
do
  echo "${i}"
  java StedToSAT sted2.pb "$tch" 840 sted2.satout 0x55 sted2.sat ${i}
  cadical --sat sted2.sat -w sted2.satout
  if [ $? -ne 10 ]; then break; fi
  java StedToSAT sted2.pb "$tch" 840 sted2.satout 0x55 sted2.sat ${i}
  best=$(tail -1 sted2.sat)
done  
echo ${best}
best=$(echo "${best//$'\r'/}" | cut -f 3 -d ' ')
echo "best = ${best}"
procsixes2.py $(expand.py 5432167 1234567 "$best") linkages_sted.txt >temp.txt

# extract Magic
sp=$(echo $(grep "      match        .*(.*(.*(.*(" temp.txt  | sed -e 's/.*\(.......[QS][QS].\).*/\1*1(1)/') | tr -d ' ')
# extract rest
ll=$(tail -1 temp.txt)
echo "${ll}${sp}"
# Replace to force odd/even change
sp2=${sp/-*1(1)/P*1(1)}
echo "${ll}${sp2}"
for i in {795..840..3}
do
  echo "${i}"
  java StedToSAT sted1.pb "${ll}${sp2}" 0 sted1.satout 0x55 sted1.sat ${i}
  best=$(tail -1 sted1.sat)
  cadical --sat sted1.sat -w sted1.satout
  if [ $? -ne 10 ]; then break; fi
done
echo ${best}
best=$(echo "${best//$'\r'/}" | cut -f 3 -d ' ')
echo "best = ${best}"
b2=$(removebb.sh "${best}" | tail -1)
onepart.sh "${b2}"
