#!/usr/bin/bash
# Copyright (c) 2022 Andrew Johnson
# Removes the B-blocks from touches of the form:
# '2314567QSP--PP---P*1(1)5432167SQ-*10(1)'
sed -e 's/.......[QS][QS]----------\*1([1-9])//g' | sort | uniq
