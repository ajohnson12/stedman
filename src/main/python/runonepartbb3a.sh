#!/bin/sh
# Copyright (c) 2022 Andrew Johnson
# From some odd blocks files attempt to find a peal
# Goes through all the blocks from the starting number
# options: <file> <#comp starting number> <onepart.sh options>
for i in $(seq $2 $(grep -c '!' $1)); do onepartbb3a.sh $1 $i "${@:3}"; rc=$?; echo $i; if [ $rc -ne 20 ]; then break; fi; done
