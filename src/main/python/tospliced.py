#!python
# Copyright (c) 2021 Andrew Johnson
# convert rows from CompLib / GSiril to spliced major
import fileinput
prev=None
methods={
    "X.18.X.18.X.18.X.18.X.18.X.18.X.18.X" :"P",
    "X.18.X.18.X.18.78.18.X.18.X.18.X" : "A",
    "X.18.X.18.X.16.X.18.X.18.X" : "G",
    "X.18.X.18.58.18.X.18.X" : "M",
    "X.18.X.14.X.18.X" : "L",
    "X.18.38.18.X" : "U",
    "X.12.X" : "B",
    "18" : "C"
}
calls={
    "12" : "",
    "14" : "-",
    "1234" : "s"
}
allpn=''
pnx="1234567890XETABCD"
callsline=""
methodline=""
cl=0
maxcl=0
courselen=31
line=""
for i in range(1, courselen+1):
    line=line+str(i)+"\t"
print("%sMethods" % line)
for line in fileinput.input():
   line=line.strip()
   # Handle GSiril without starting rounds 
   if not prev and not pnx.startswith(line):
      prev = pnx[0:len(line)]
   if prev:
      pn=''
      #print("'%s' '%s'" % (prev,line))
      for i in range(0, len(line)):
          if prev[i] == line[i]:
             pn+=pnx[i]
      if not pn:
         pn='X'
      if allpn:
         allpn+='.'
      allpn+=pn
   prev=line
   #print(allpn)
   if allpn in methods:
       methodline=methodline+methods[allpn]
       allpn=""
   elif allpn in calls:
       #print(line)
       callsline=callsline+calls[allpn]+"\t"
       if calls[allpn] and calls[allpn] != 'p':
           methodline=methodline+"."
       cl=cl+1
       #if line[-1] == '8' or cl > 7:
       if line[-1] == '8':
           callsline=callsline+"\t"*(courselen - cl)
           #callsline=callsline[0:-1]+":\t"+"\t"*(courselen - cl)
           print("%s%s" % (callsline,methodline))
           callsline=""
           methodline=""
           if cl > maxcl:
               maxcl = cl
           cl=0
       allpn=""
print("%s" % allpn)
print("Longest course %d" % maxcl)
