#!/usr/bin/python
# Copyright (c) 2017,2022 Andrew Johnson
# Analyse two  Stedman / Erin touches of the form:
# '2314567QSP--PP---P*1(1)5432167SQ-*10(1)'
# Remove common sixes
# Show remaining touches - as a pair for linkages_sted.txt etc.

import sys
from itertools import permutations
from _ast import Or
#from str import maketrans 

def canon6(row):
   return row
   

# Generate the six-end after a plain
def plain(row):
   if row[8:9] == 'S':
      return plainS(row)
   else:
      return plainQ(row) 

# Generate the six-end after a bob
def bob(row):
   if row[8:9] == 'S':
      return bobS(row)
   else:
      return bobQ(row)

# Generate the six-end after a single
def single(row):
   if row[8:9] == 'S':
      return singleS(row)
   else:
      return singleQ(row)

# Generate the six-end after a plain on a quick six
def plainQ(row):
   row=place(row,7)
   
   row=place(row,3)
   return row
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   return row

# Generate the six-end after a plain on a slow six
def plainS(row):
   row=place(row,7)

   row=place(row,1)
   return  row
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   return row

# Generate the six-end after a bob on a quick six
def bobQ(row):
   row=place(row,5)

   row=place(row,3)
   return row

   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   return row

# Generate the six-end after a bob on a slow six
def bobS(row):
   row=place(row,5)

   row=place(row,1)
   return row

   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   return row

# Generate the six-end after a single on a quick six
def singleQ(row):
   row=place(row,5,8)

   row=place(row,3)
   return row
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   return row

# Generate the six-end after a single on a slow six
def singleS(row):
   row=place(row,5,8)

   row=place(row,1)
   return row
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   row=place(row,1)
   row=place(row,3)
   return row

# Generate a new row after this place
# Don't swap the QS indicator if the place is 1 or 3
def place(row,n,n2=0):
   for i in range(0, n-1, 2):
      a=row[i]
      b=row[i+1]
      row=row[0:i]+b+a+row[i+2:]

   if n <= 3:
      lm = len(row)-3
   else:
      lm = len(row)-1

   if n2 == 0:
      n2 = n

   for i in range(n2, lm, 2):
      a=row[i]
      b=row[i+1]
      row=row[0:i]+b+a+row[i+2:]

   return row

# Is this row out of course?
def ooc(row):
   for i in range(0, len(row) -2):
      if row[i] > row[i+1]:
         row=row[0:i]+row[i+1]+row[i]+row[i+2:]
         return not ooc(row)
   return False

# Find the row at the start of the six from the row at the end
def sixhead(row):
    if row[7] == 'Q':
        row = place(row,3)
    else: 
        row = place(row,1)
    return row
 
# Return the in-course version of this row
def incourse(row):
   if ooc(row):
      #print "%s %s ooc" % (row, ooc(row))
      # Find six-head row
      row = sixhead(row)
      #print "%s %s ooc" % (row, ooc(row))
   return row

# The six-type
def sixtype(row):
   return incourse(row)[3:7]

# The canonical version of the B-block
def canon(row):
    row=incourse(row)
    # Find the biggest
    rw = row
    rw2 = rw
    o = order(rw)
    while True:
        o2 = order(rw)
        if o2 > o:
            o = o2
            rw2 = rw
        rw = bob(rw)
        if rw == row:
            break
    
    # Find the index
    ix = 0
    rw = rw2
    while rw != row:
        ix += 1
        rw = bob(rw)

    #print "%s %s" % (row, rw2)
    return (rw2, ix)

# Check that that canonical version is in the B-block   
def checkCanon(row, rw2, ix):
    for i in range(0, ix):
        rw2 = bob(rw2)
    if row != rw2:
        print("Error %s %s %d" % (row, rw2, ix))

def order(row):
   return row[-1]+row[-2]+row[3:-2][::-1]+row[2]+row[1]+row[0]
   return row[3::-1]+row[0:2:-1]+row[2]
   return row[-1]+row[-2]+row[3:-2]+row[0:3]

# Find the complement of a row for the pair of B-blocks
# Perhaps only works for a quick six??
def complement(row):
   return row[2:4]+row[0:2]+row[4:]

# Does this row match a six in the preferred B-Blocks
# True if it matches a used six in the preferred B-blocks
def matchblock(row):
   cn,ix = canon(row)
   pair=cn[5:7]
   bp = bestpair[pair]
   if bp==cn or complement(bp)==cn:
      return sv[cn][ix]=='-'
   else:
      return False

def printbb(row):
    touches=""
    rl = len(sv[row])
    row1=row
    for i in range(0,rl):
        if sv[row][i]=='-' and sv[row][(i+1)%rl] == '.':
            touch=row1+'-' 
            for j in range(1,rl):
                if sv[row][(i+j)%rl] == '.':
                    touch+='-'
                else:
                     break
            touches += "%s*1(1)" % touch
        row1=bob(row1)
    return touches

# Reverse a touch, and swap 1 2 to keep in-course
def reverse(touch):
    trimmed=touch.lstrip()
    lpad=touch[0:len(touch)-len(trimmed)]
    touch=trimmed
    row = touch[0:9]
    touch = touch[9:]
    for c in touch:
        if c == '-':
            row = bob(row)
        elif c == 'S':
            row = single(row)
        else:
            row = plain(row)
    row = sixhead(row)
    # Make in-course?
    #print(row)
    row=row.replace('1','@').replace('2','1').replace('@','2')
    return lpad+row+touch[::-1]

def reverseall(touches):
    end='*1(1)'
    touches = touches.split(end)
    #print(touches)
    newt = ''
    for touch in touches:
        if touch:
            newt += reverse(touch)+end
    return newt





def readtouch(lns):
    allsxs = {}
    callat = {}
    hasrung = {}
    lns=lns.strip()
    while lns:
        pr=''
        row=lns[0:9]
        ln=lns[9:]
        if '*' in ln:
           i=ln.index('*')
           j=ln.find('(', i)
           if j == -1:
              j = len(ln)
           mult=int(ln[i+1:j])
           k=ln.find(')', j)
           if  k== -1:
              lns=""
           else:
              lns=ln[k+1:].strip()
           ln=ln[0:i]*mult
           #print "count=%d" % mult
        else:
           lns=""
        #print(row+ln)
        
        for i in range(0,len(ln)):
            c = ln[i]
            if c == '-':
                callat[row] = c
                row = bob(row)
            elif c == 'P':
                callat[row] = c
                row = plain(row)
            elif c == 'S':
                callat[row] = c
                row = single(row)
            else:
                continue
        
            allsxs[row] = i
        
            # Check for falseness
            sixt = sixtype(row)
            if sixt in hasrung:
                print("False six %s at %d and %d" % (row, hasrung[sixt], i))
            else:
                hasrung[sixt] = row
    return (callat, hasrung)

callat1, hasrung1 = readtouch(sys.argv[1])
callat2, hasrung2 = readtouch(sys.argv[2])

def matchtouch(lns,callat2,hasrung2):
    goesto = {}
    lns=lns.strip()
    rrx=""
    while lns:
        pr=''
        row=lns[0:9]
        ln=lns[9:]
        if '*' in ln:
           i=ln.index('*')
           j=ln.find('(', i)
           if j == -1:
              j = len(ln)
           mult=int(ln[i+1:j])
           k=ln.find(')', j)
           if  k== -1:
              lns=""
           else:
              lns=ln[k+1:].strip()
           ln=ln[0:i]*mult
           #print "count=%d" % mult
        else:
           lns=""
        #print(row+ln)
        matching = True
        rx=""
        startrow=""
        for ps in [1,2]:
            for i in range(0,len(ln)):
                c = ln[i]
                sixt = sixtype(row)
                #print("ps=%d i=%d row=%s%s matching=%s sixt=%s hasrung2=%s rx=%s" % (ps,i,row,c,matching,sixt,hasrung2[sixt], rx))
                if ps == 2 and not matching and (sixt in hasrung2 and hasrung2[sixt] == row or
                    rx.startswith(row)):
                    if rx.startswith(row):
                        # rx=4123765QS--*1(1)4172365SQ-
                        # to
                        # rx=4172365SQ---*1(1)
                        ix = rx.rfind(')')
                        if ix >= 0:
                            goesto[rx[ix+1:ix+1+len(row)]] = goesto[row]
                            del goesto[row]
                            rx = rx[ix+1:] + rx[len(row):ix+1]
                            matching=True
                            break
                    rx += "*1(1)"
                    goesto[startrow] = row
                    matching=True
                    break
                if sixt in hasrung2 and hasrung2[sixt] == row and callat2[row] == c:
                    if not matching:
                        rx += "*1(1)"
                        goesto[startrow] = row
                    matching = True
                else:
                    if matching:
                        rx+=row
                        startrow=row
                    matching=False
                if c == '-':
                    row = bob(row)
                elif c == 'P':
                    row = plain(row)
                elif c == 'S':
                    row = single(row)
                else:
                    continue
                if not matching:
                    rx += c
            if matching:
                break;
        
        #if rx:
        #    rx += '*1(1)'
        if not matching and not rx:
            rx = '@' + row + len
        rrx += rx
        #if rx:
        #    print(rx)
    #print("%s" % rrx)
    #print("%s" % goesto)
    return (rrx,goesto)

def groupit(t, goesto1, goesto2):
   comesfrom2 = {}
   for x in goesto2:
     comesfrom2[goesto2[x]]=x
   tx=t.replace(')',') ').rstrip().split(" ")
   tx=sorted(tx)
   txd={}
   for tt in tx:
      txd[tt[0:9]] = tt
   #print("%s\n%s\n%s\n%s" % (tx,goesto1,goesto2,comesfrom2))
   rx=''
   while len(tx) > 0:
     t1=tx[0]
     rx=rx+t1
     tx.remove(t1)
     t2=goesto1[t1[0:9]]
     t2=comesfrom2[t2]
     t2=txd[t2]
     while t2 != t1:
       tx.remove(t2)
       rx=rx+t2
       t2=goesto1[t2[0:9]]
       t2=comesfrom2[t2]
       t2=txd[t2]
       #print("rx=%s" % rx)
     rx += ' '
     #print("rx=done")
   return rx.rstrip()

mt1,goesto1=matchtouch(sys.argv[1],callat2,hasrung2)
mt2,goesto2=matchtouch(sys.argv[2],callat1,hasrung1)
mt1a=groupit(mt1,goesto1,goesto2)
mt2a=groupit(mt2,goesto2,goesto1)
#print("1: %s" % mt1)
#print("2: %s" % mt2)
#print("1: %s" % reverseall(mt1))
#print("2: %s" % reverseall(mt2))

# Divided into groups
print("1: %s" % mt1a)
print("2: %s" % mt2a)
print("1: %s" % reverseall(mt1a))
print("2: %s" % reverseall(mt2a))

