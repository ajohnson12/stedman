#!/bin/sh
# Copyright (c) 2021 Andrew Johnson
# From some odd blocks files attempt to find a peal
# options: <file> <#comp> <onepart.sh options>
onepart.sh $(findbb.sh $(head -$2 "$1"  | tail -1 | cut -d ' ' -f 3) | tail -1) "${@:3}"
