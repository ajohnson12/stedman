#!/bin/sh
# Copyright (c) 2022 Andrew Johnson
# Bob q-sets and remove B-blocks
canontouches.py $(findbb.sh "$1" | grep '^c 5040' | canonbb.sh)
