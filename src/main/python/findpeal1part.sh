#!/bin/bash
# Copyright (c) 2021 Andrew Johnson
# Find a peal given a result from a 1-part search finding round blocks containing magic links
tch=$(grep '\*' "$1" | head -1)
tch=${tch//$'c '/}
tch=${tch//$'\n'/}
tch=${tch//$'\r'/}
echo "'${tch}'"
best=$(tail -1 "$1")
# search had 819
for i in {837..840..1}
do
  echo "${i}"
  java StedToSAT sted1.pb "$tch" 840 sted1.satout 0x55 sted1.sat ${i}
  cadical --sat sted1.sat -w sted1.satout
  if [ $? -ne 10 ]; then break; fi
  java StedToSAT sted1.pb "$tch" 840 sted1.satout 0x55 sted1.sat ${i}
  best=$(tail -1 sted1.sat)
done  
echo ${best}
pts=$(echo "${best//$'\r'/}" | cut -f 2 -d ' ')
npts=${pts/*!/}
echo "Number of round blocks: ${npts}."
best=$(echo "${best//$'\r'/}" | cut -f 3 -d ' ')
echo "best = ${best}"
procsixes2.py $(expand.py 1234567 1234567 "$best") linkages_sted.txt >temp.txt

# extract Magic
sp=$(echo $(grep "      match        .*(.*(.*(.*(" temp.txt  | sed -e 's/.*\(.......[QS][QS].\).*/\1*1(1)/') | tr -d ' ')
# extract rest
ll=$(tail -1 temp.txt)
echo "${ll}${sp}"
if [[ "${npts}" = *[13579] ]]; then
  echo "already odd" 
  sp2="${sp}"
else
  echo "force to odd number of round blocks"
  # Replace to force odd/even change
  sp2=$(echo "${sp}" | sed -e 's/.*\(.......[QS][QS]\)P.*/\1=*1(1)/;s/.*\(.......[QS][QS]\)-.*/\1p*1(1)/')
  #sp2=${sp/-*1(1)/P*1(1)}
fi

echo "${ll}${sp2}"
#771 = 260*3 - 9
for i in {771..840..3}
do
  echo "${i}"
  java StedToSAT sted1.pb "${ll}${sp2}" 0 sted1.satout 0x55 sted1.sat ${i}
  best=$(tail -1 sted1.sat)
  cadical --sat sted1.sat -w sted1.satout
  if [ $? -ne 10 ]; then break; fi
done
echo ${best}
best=$(echo "${best//$'\r'/}" | cut -f 3 -d ' ')
echo "best = ${best}"
b2=$(removebb.sh "${best}" | tail -1)
onepart.sh "${b2}"
