#!python
# Copyright (c) 2021 Andrew Johnson
# convert rows from CompLib / GSiril to place notation
import fileinput
prev=None
allpn=''
pnx="1234567890XETABCD"
for line in fileinput.input():
   line=line.strip()
   # Handle GSiril without starting rounds 
   if not prev and not pnx.startswith(line):
      prev = pnx[0:len(line)]
   if prev:
      pn=''
      #print("'%s' '%s'" % (prev,line))
      for i in range(0, len(line)):
          if prev[i] == line[i]:
             pn+=pnx[i]
      if not pn:
         pn='X'
      if allpn:
         allpn+='.'
      allpn+=pn
   prev=line
print("%s" % allpn)
