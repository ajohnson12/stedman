#!/bin/python
# Copyright (c) 2021 Andrew Johnson
# Expand a touch generate from a group search to round blocks in a one-part search
# usage: expandpy <gen1> <gen2> <touches>
import sys
import re
gen1 = sys.argv[1]
gen2 = sys.argv[2]

def place(row,n,n2=0):
   for i in range(0, n-1, 2):
      a=row[i]
      b=row[i+1]
      row=row[0:i]+b+a+row[i+2:]

   if n <= 3:
      lm = len(row)-3
   else:
      lm = len(row)-1

   if n2 == 0:
      n2 = n

   for i in range(n2, lm, 2):
      a=row[i]
      b=row[i+1]
      row=row[0:i]+b+a+row[i+2:]

   return row

def ooc(row):
   for i in range(0, len(row) -2):
      if row[i] > row[i+1]:
         row=row[0:i]+row[i+1]+row[i]+row[i+2:]
         return not ooc(row)
   return False

def incourse(row):
   if ooc(row):
      #print "%s %s ooc" % (row, ooc(row))
      # Find six-head row
      if row[7:8] == 'Q':
         row = place(row,3)
      else: 
         row = place(row,1) 
      #print "%s %s ooc" % (row, ooc(row))
   return row

def subst(inp1, to1, from1):
   out = ""
   for c in inp1:
      i = from1.find(c)
      if i >= 0:
          c = to1[i]
      out=out+c
   return out

g = ["1234567890E"[0:len(gen1)]]

all = {g[0]}
i = 0
while i < len(g):
   a = subst(g[i], gen1, g[0])
   if a not in all:
      all.add(a)
      g.append(a)
   b = subst(g[i], gen2, g[0])
   if b not in all:
      all.add(b)
      g.append(b)
   i = i + 1


ll = sys.argv[3]

oo = ""
while ll != '':
   l1 = re.search("([1234567890E]+[QS][QS])([PpRrL=_~.-]+)\\*([0-9]+)\\(([0-9])+\\)(.*)", ll)
   if not l1:
      break
   ll = l1.group(5)
   for gx in g:
      st = subst(l1.group(1), gx, g[0])
      tch = l1.group(2)
      if ooc(st):
          st = incourse(st)
          tch = tch[::-1]
      oo = oo + st + tch + "*1(1)"

print(oo)
