#!/usr/bin/python
# Copyright (c) 2019,2021 Andrew Johnson
# See how similar two peals are
# Counts rows which have the same following rows
import sys

fn1 = sys.argv[1]
fn2 = sys.argv[2]


with open(fn1, "r") as f1:
   ls1 = f1.readlines()

with open(fn2, "r") as f2:
   ls2 = f2.readlines()

next={}

n = 0

prev = None
for l in ls1:
   if prev:
      next[prev] = l
      n = n + 1
   prev = l

s1 = 0
s2 = 0
prev = None
for l in ls2:
   if prev:
      if prev in next:
         s1 = s1 + 1
         if next[prev] == l:
            s2 = s2 + 1
   prev = l

print("rows=%d same rows=%f%% same next row=%f%%" % (n, 100.0*s1/n, 100.0*s2/n))




