#!python
# Chop out non-run rows
import sys

m1=["5678","6578","4578","5478","4678","6478"]
m2=["5678","6578","6758","7658","7568","5768",
    "5687","6587","6857","8657","8567","5867",
    "5786","7586","7856","8756","8576","5876",
    "6785","7685","7865","8765","8675","6875"]
m3=[
"12345687",
"12345768",
"12346578",
"12354678",
"12435678",
"13245678",
"21345678",
"12346587",
"12354768",
"12435687",
"12436578",
"12436587",
"13245768",
"13254768",
"21345687",
"21346587"
"21436587"]

m4=[
"12345678",
"12563478",
"12753468",
"13245768",
"13254768",
"13527468",
"13572468",
"14327658",
"15263748",
"16745238",
"17652438",
"17654328",
"31247568",
"43215678",
"45362718",
"65432178",
"75312468",
"75321468",
"87123456",
"87654321",
]

m5=[
    "1357",
"2468",
"3468",
"3478",
"3578",
"5768",
"7468",
"7568",
"7658",
"8765"]

def hasmusic(x):
    front=x[0:4]
    back=x[-4:]
    if front in m1:
        return True;
    if back in m1:
        return True;
    if front in m2:
        return True;
    if back in m2:
        return True;   
    if x in m3:
        return True
    if x in m4:
        return True
    if back in m5:
        return True;
    if x[1]=='5'and x[3]=='6' and x[5]=='7' and x[7]=='8':
        return True
           

def hasrun(x):
    if False and hasmusic(x):
        return True
    return ("1234" in x or "2345" in x or "3456" in x or "4567" in x or "5678" in x or
           "4321" in x or "5432" in x or "6543" in x or "7654" in x or "8765" in x)

dbg=False

myfile = open(sys.argv[1], "r")
list = myfile.readlines()
for i in range(0,len(list)):
   list[i]=list[i].strip()
lx=len(list)+1
while (len(list) < lx):
 lx=len(list)
 for i in range(1,len(list) - 1):
   if i > len(list) - 2:
      break
   l0=list[i]
   if l0[0] == '1' and list[i+1][0] == '1':
      
      for j in range(1, 224 + 1):
          l=list[i+j]
          if hasrun(l):
             break
      if j == 224:
          l2=list[i+j+1]
          l1plain = l0[0:2]+l0[3]+l0[2]+l0[5]+l0[4]+l0[7]+l0[6]
          #print(l0)
          #print(l2)
          #print(l1plain)
          #print()
          if l1plain == l2:
             if dbg: 
                print ("%d %d" % (i,j))
             list=list[0:i+1]+list[i+j+1:]
             continue
          l1bob = l0[0]+l0[2]+l0[1]+l0[3]+l0[5]+l0[4]+l0[7]+l0[6]
          #print(l0)
          #print(l2)
          #print(l1plain)
          #print()
          if l1bob == l2:
             if dbg: 
                print ("%d %d" % (i,j))
             list=list[0:i+1]+list[i+j+1:]
             continue
if dbg:
   print(len(list))
lx=len(list)+1
if True:
 while (len(list) < lx):
  lx=len(list)
  for i in range(1,len(list) - 1):
   if i > len(list) - 2:
      break
   l0=list[i]
   p1=l0.index('1')
   l1=list[i+1]
   p2=l1.index('1')
   if (p2 > p1):      
      for j in range(1, 112 + 1):
          if i + j > len(list) - 2:
             break
          l=list[i+j]
          if hasrun(l):
             break
          l2=list[i+j+1]
          p3=l.index('1')
          p4=l2.index('1')
          #if p1 != 6: # debug Gainsborough
          #    continue
          if p1 >= 0  and p1==p4 and p2==p3:
             l0c=[x for x in l0]
             for k in range(p1 % 2, p1, 2):
                t=l0c[k]
                l0c[k]=l0c[k+1]
                l0c[k+1]=t
             for k in range(p1+1, len(l0c)-1, 2):
                t=l0c[k]
                l0c[k]=l0c[k+1]
                l0c[k+1]=t
             hl="".join(l0c)
             if hl==l2:
                if dbg:          
                   print("Joined %d %d" % (i,j))
                list=list[0:i+1]+list[i+j+1:]

       
for l in list:
   print(l)



   