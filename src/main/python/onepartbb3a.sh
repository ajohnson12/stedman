#!/bin/sh
# Copyright (c) 2021,2022 Andrew Johnson
# From some odd blocks files attempt to find a 3-part peal based on Queen's
# options: <file> <#comp> <onepart3a.sh options>
onepart.sh "$(findbb.sh "$(expand.py 1357246 1234567 $(head -$2 "$1"  | tail -1 | cut -d ' ' -f 3))" | tail -1)" "${@:3}"

