#!/bin/sh
# Copyright (c) 2021 Andrew Johnson
# Retrieve rows from a composition on CompLib.org
wget https://complib.org/composition/$1/rows -O c$1.rows
