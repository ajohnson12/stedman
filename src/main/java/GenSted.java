import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Generate a node list for Stedman (Erin)
 * (c) Copyright 2017,2019 Andrew Johnson
 * Pair with another group to generate sixes in sets related by the second group.
 * Also indicate the set and instance of the element according to normal groups.
 * @author Andrew Johnson
 * Output
 * <six-end id> <destination plain> <destination bob> <name> <type> <instance>
 */
public class GenSted
{
    int n = 1;
    /** The part-end group */
    List<String>grp;
    /** The super group - sixes related by this group will be of the same type */
    List<String>grp2;
    /** Normal supergroup */
    List<String>grpNormSup;
    /** Group to generate normal supergroup from grp */
    List<String>grpNorm;
    boolean ooc;
    boolean ooc2;
    int type = 1;
    // Row map to identifier
    Map<String,Integer>r = new LinkedHashMap<String,Integer>();
    // Copy of row map as a list
    List<String>rlist = new ArrayList<String>();
    // Instances of normal
    Map<String,Integer>rnorm = new LinkedHashMap<String,Integer>();
    int tyinst[];
    int nb;
    PrintWriter out;
    boolean verbose = true;
    
    public GenSted(PrintWriter o, boolean verbose) {
        out = o;
        this.verbose = verbose;
    }
    
    /**
     * Standard groups
     */
    static final String standard[][] = new String[][] {
        // Bobs only
        {"sted21", "sted21_7_05", "S", "2345671","1357246"},
        {"sted20", "sted20_7_12", "S", "2345167","1352476"},
        {"sted10", "sted10_5_04", "S", "2345167","5432167"},
        {"sted7", "sted7_7_07", "S", "2345671"},
        {"sted6", "sted6_6_32", "S", "2316457","2136547"},
        {"sted5", "sted5_5_05", "S", "2345167"},
        {"sted4", "sted4_6_26", "S", "1352476"},
        {"sted3", "sted3_6_33", "S", "3456127"},
        {"sted2", "sted2_4_07", "S", "5432167"},
        {"sted1", "sted1_0_01", "S"},
        // other
        {"sted168", "sted168_7_03", "S", "7613524", "4725163"},
        {"sted60", "sted60_6_05", "S", "2345167",  "5342617"},
        {"sted24a", "sted24_6_09", "S", "3456127", "2165347"},
        {"sted24b", "sted24_7_28", "S", "2341657", "2134576"},
        {"sted12a", "sted12_6_14", "S", "5146237", "6423157"},
        {"sted12b", "sted12_7_33", "S", "3476521", "7514623"},
        {"sted8", "sted8_6_23", "S", "2341657", "4321567"},
        {"sted4b", "sted4_6_35", "S", "2143567", "2134657"},
        {"sted4c", "sted4_4_04", "S", "2143567", "3412567"},
        // out of course
        {"sted20m", "sted20_5_03", "S", "2345167", "1352467"},
        {"sted10m", "sted10_7_14", "S", "2345176"},
        {"sted8m", "sted8_4_03", "S", "2341567","4321567"},
        {"sted4dm", "sted4_4_06", "S", "2134567","1243567"},
        {"sted4em", "sted4_4_05", "S", "2341567"},
        {"sted2m", "sted2_2_01", "S", "2134567"},
        // bobs only
        {"erin21", "erin21_7_05", "E", "2345671","1357246"},
        {"erin20", "erin20_7_12", "E", "2345167","1352476"},
        {"erin10", "erin10_5_04", "E", "2345167","5432167"},
        {"erin7", "erin7_7_07", "E", "2345671"},
        {"erin6", "erin6_6_32", "E", "2316457","2136547"},
        {"erin5", "erin5_5_05", "E", "2345167"},
        {"erin4", "erin4_6_26", "E", "1352476"},
        {"erin3", "erin3_6_33", "E", "3456127"},
        {"erin2", "erin2_4_07", "E", "5432167"},
        {"erin1", "erin1_0_01", "E"},
        // other
        {"erin168", "erin168_7_03", "E", "7613524", "4725163"},
        {"erin60", "erin60_6_05", "E", "2345167", "5342617"},
        {"erin24a", "erin24_6_09", "E", "3456127", "2165347"},
        {"erin24b", "erin24_7_28", "E", "2341657", "2134576"},
        {"erin12a", "erin12_6_14", "E", "5146237", "6423157"},
        {"erin12b", "erin12_7_33", "E", "3476521", "7514623"},
        {"erin8", "erin8_6_23", "E", "2341657", "4321567"},
        {"erin4b", "erin4_6_35", "E", "2143567", "2134657"},
        {"erin4c", "erin4_4_04", "E", "2143567", "3412567"},
        // out of course
        {"erin20m", "erin20_5_03", "E", "2345167", "1352467"},
        {"erin10m", "erin10_7_14", "E", "2345176"},
        {"erin8m", "erin8_4_03", "E", "2341567","4321567"},
        {"erin4dm", "erin4_4_06", "E", "2134567","1243567"},
        {"erin4em", "erin4_4_05", "E", "2341567"},
        {"erin2m", "erin2_2_01", "E", "2134567"},
        // Caters
        {"sted168_9", "sted168_9_7_03", "231456789QS", "145763289", "716245389"},
        {"erin168_9", "sted168_9_7_03", "123456789SS", "145763289", "716245389"},
        {"sted168_9a", "sted168_9_7_03a", "231456789QS", "231564789", "671342589"},
        {"erin168_9a", "erin168_9_7_03a", "123456789SS", "231564789", "671342589"},
        // Cinques
        {"sted7920", "sted7920_M11", "2314567890EQS", "103E7659824", "4583697120E"},
        {"erin7920", "sted7920_M11", "1234567890ESS", "103E7659824", "4583697120E"},
    };
    /**
     * Well known graphs 
     * @param name
     * @return command line parameters for the graphs
     */
    public static String[]lookup(String name) throws IOException {
        for (String g[] : standard) {
            if (g[0].equals(name) || g[1].equals(name)) {
                if (false)
                    System.err.println("Found "+name);
                return g;
            }
        }
        return null;
    }
    
    public static String defaultArg(String args[], int idx, String def) {
        String ret = args.length > idx &&  args[idx] != null && args[idx].length() > 0 ? args[idx] : def;
        if (ret.length() < def.length()) {
            ret = ret + def.substring(ret.length());
        }
        return ret;
    }
    
    public static String identity(int bells) {
        return "1234567890E".substring(0, bells);
    }
    
    public static void main(String[] args) throws IOException
    {
        boolean verbose = false;
        if (args.length > 0) {
            // check/set verbose flag
            if ("-v".equals(args[0])) {
                verbose = true;
                String newargs[] = new String[args.length - 1];
                System.arraycopy(args, 1, newargs, 0, args.length - 1);
                args = newargs;
            }
            /**
             * sted20 outfile
             */
            String g[] = lookup(args[0]);
            if (g != null) {
                // Named group
                String g2[];
                if (args.length > 1) {
                    g2 = new String[6];
                    System.arraycopy(g,  2,  g2,  0,  g.length - 2);
                    // Outfile
                    g2[5] = args[1];
                } else {
                    g2 = new String[g.length - 1];
                    System.arraycopy(g,  2,  g2,  0,  g.length - 2);
                }
                args = g2;
            } else if (args.length > 1) {
                // Named group with new start
                g = lookup(args[1]);
                if (g != null) {
                    String g2[];
                    g2 = new String[6];
                    System.arraycopy(g,  2,  g2,  0,  g.length - 2);
                    g2[0] = args[0];
                    if (args.length > 2) {
                        g2[5] = args[2];
                    }
                    args = g2;
                }
            }
            //System.err.println("arguments "+Arrays.toString(args));
            String start;
            if (args.length > 0 && args[0].equalsIgnoreCase("E")) {
                start = "1234567SS";
            } else if (args.length > 0 && args[0].length() > 2) {
                start = args[0];
            } else {
                start = "2314567QS";
            }
            int bells = start.length() - 2;
            String rounds = identity(bells);
            String gen1 = defaultArg(args, 1, rounds);
            String gen2 = defaultArg(args, 2, rounds);
            String gen3 = defaultArg(args, 3, rounds);
            String gen4 = defaultArg(args, 4, rounds);
            if (args.length > 5 && args[5] != null && args[5].length() > 0) { 
                PrintWriter pw = new PrintWriter(new File(args[5]), Charset.defaultCharset().name());
                try {
                    GenSted gs = new GenSted(pw, verbose);
                    gs.build(start, gen1, gen2, gen3, gen4);
                } finally {
                    pw.close();
                }
            } else {
                GenSted gs = new GenSted(new PrintWriter(System.out), verbose);
                try {
                    gs.build(start, gen1, gen2, gen3, gen4);
                } finally {
                    gs.out.flush();
                }
            }
            return;
        }
        GenSted gs = new GenSted(new PrintWriter(System.out), verbose);
        //gs.build("2345167", "1234567", "5432167", "1234567"); // 5_2
        //gs.build("1234567", "1234567", "2345167", "5432167"); // 1_10
        //gs.build("5432167", "1234567", "2345167", "1234567"); // 2_5
        //gs.build("1234567", "1234567", "2345167", "1234567"); // 1_5
        //gs.build("2345167", "5432167", "1234567", "1234567"); // D5
        //gs.build("1234567", "1234567", "5432167", "2345167");
        //gs.build("1234567", "1234567", "1234576", "2345167");
        //gs.build("2316457", "2136547", "1234567","1234567");
        //gs.build("2345167", "1234567", "1234567", "1234567"); // C5
        //gs.build("3412567", "1234567", "2341657","1234567");
        //gs.build("1234567", "1234567", "2341657","1234567"); // 1_4
        //gs.build("1234567", "1234567", "1234567", "2345167");
        //gs.build("1234567", "1234567", "5432167", "1234567"); // 1_2
        //gs.build("2316457", "2136547", "1234567","1234567"); //6
        //gs.build("2341657","1234567", "1234567","1234567"); // 4
        //gs.build("1352476", "1234567", "1234567", "1234567"); // 4 : 6.26 from Thurstans
        //gs.build("3412567","1234567", "2341657","1234567"); // 2_4
        //gs.build("761352489", "472516389", "123456789", "123456789");
        //gs.build("2136547", "1234567","2316457","1234567"); //2_6
        //gs.build("2316457","1234567","2136547", "1234567"); //3_6
        //gs.build("2345671", "1234567", "1234567", "1234567"); //7
        //gs.build("1357246","1234567", "1234567","1234567"); // 3
        //4-part: Index 6.35: 2143567; 2134657. 2 or 4 round blocks.
        //gs.build("2143567", "2134657", "1234567", "1234567"); // 4b 6.35
        //4-part: Index 4.04: 2143567; 3412567. 2 or 4 round blocks.
        //gs.build("2143567", "3412567", "1234567", "1234567"); // 4c 4.04
        //gs.build("1234567", "5432167", "1234567", "1234567"); // C2
        gs.build("1234567", "1234567", "1234567", "1234567"); // C1
        //gs.build("2345167", "1352476", "1234567", "1234567"); // 20
        //gs.build("2345167", "5342617", "1234567", "1234567"); // 60
        //gs.build("5146237", "6423157", "1234567", "1234567"); // 12a
        //gs.build("3476521", "7514623", "1234567", "1234567"); // 12b
        //gs.build("2345176", "1234567", "1234567", "1234567"); // 12b
        //gs.build("2134567", "1243567", "1234567", "1234567"); // 4dm 4.06
        //gs.build("2341567", "1234567", "1234567", "1234567"); // 4em 4.05
        //gs.build("2341567", "4321567", "1234567", "1234567"); // 8m
        //gs.build("2345167", "1352467", "1234567", "1234567"); // 20m
        //gs.build("1352467", "1234567", "1234567", "1234567"); // 4em / 20m
        //gs.build("2316457", "1234567", "2136547", "1234567"); //3_6
        //gs.build("2345176", "1234567", "1234567", "1234567"); // 10m
        //gs.build("2345671", "1357246", "1234567", "1234567"); // 21
        //gs.build("1234567", "1234567", "1357246", "1234567"); // 1_3
        //gs.build("1234567", "1234567", "1234567", "1234567"); // 1
        //gs.build("5146237", "1234567", "1234567", "1234567"); // 3 from 12a part
        //gs.build("3614527", "1234567", "1234567", "1234567"); // 2 from 12a part
        //gs.build("2143567", "4215637", "1234567", "1234567"); // 2 from 12a part
        //gs.build("3456127",  "1234567",  "1234567",  "1234567"); // 3-part peal group
        //gs.build("2461357",  "1234567",  "1234567",  "1234567");
        //gs.build("4351762", "1234567", "1234567", "1234567"); // 4 weeks
        //gs.build("1672534",  "1234567",  "1234567",  "1234567"); // 5 part magic
        //gs.build("3215467", "1234567", "1234567", "1234567"); // 2 part from 12a
        //gs.build("2143567", "1234567", "1234567", "1234567"); // 2 from 12-part swap pairs rotate
        //gs.build("2345671",  "1357246",  "1234567",  "1234567"); // 21-part
        //gs.build("1234567","1234567", "3412567", "1234567"); //
        //gs.build("2137564","1234567", "1234567", "1234567"); // Erin 2-part with singles at start and half-way
        //gs.build("1234567","1234567", "3456127", "1234567"); // 1_3
        //gs.build("1234567","1234567", "5432167", "1234567"); // 1_2 e.g. two-part peals
        //gs.build("1234567", "1234567", "2137564","1234567"); // Erin 2-part with singles at start and half-way
        //gs.build("1234576","1234567", "1234567", "1234567"); // 2-part from 10-part out of course
        //gs.build("2136547", "1234567", "1234567", "1234567"); // from 6-part, 2-part
        //gs.build("2316457", "1234567", "1234567", "1234567"); // from 6-part, 3-part
        //gs.build("5432167","1234567", "1234567", "1234567"); // 2-part
        //gs.build("7613524", "4725163", "1234567","1234567"); // 168
    }
    
    private void bb() {
        String ry1 = "1234567SS";
        String ry = ry1;
        System.out.println(ry);
        ry = bob(ry);
        System.out.println(ry);
        ry = bob(ry);
        System.out.println(ry);
        ry = bob(ry);
        System.out.println(ry);
        ry = bob(ry);
        System.out.println(ry);
        ry = bob(ry);
        if (!ry.equals(ry1)) {
            throw new IllegalStateException(ry);
        }
    }
    
    private void build(String gen1, String gen2, String gen3, String gen4) {
        build(null, gen1, gen2, gen3, gen4);
    }
    /*
     * 
     * 2314567QS
     * 2314567SQ
     * 3124567
     * 1234567
     * 
     */
    /**
     * Build the graph.
     * @param gen1 main group
     * @param gen2
     * @param gen3 secondary group
     * @param gen4
     */
    private void build(String start, String gen1, String gen2, String gen3, String gen4) {
        // The main group
        grp = StedToSAT.group(new String[]{gen1,gen2});
        ooc = ooc(gen1) || ooc(gen2);
        if (verbose) {
            System.err.println("ooc="+ooc+" "+ooc(gen1)+" "+ooc(gen2));
            System.err.println("group #1 "+grp);
        }
        
        // The clumping outer group
        grp2= StedToSAT.group(new String[]{gen3,gen4});
        ooc2 = ooc(gen3) || ooc(gen4);
        if (verbose) {
            System.err.println("ooc="+ooc+" "+ooc(gen3)+" "+ooc(gen4));
            System.err.println("group #2 "+grp2);
        }
        
        // For sets of six-ends
        long then = System.currentTimeMillis();
        buildNormal();
        long now = System.currentTimeMillis();
        
        if (verbose) {
            System.err.println("buildNormal() took "+(now-then));
        }
        // Starting six-end
        //String rr = "2314567SS";
        String rr = "2314567QS";
        //String rr = "1234567SS";
        if (start != null) {
            rr = start;
        }
        //rr = "123456789SS";
        nb = rr.length() - 2;
        gensixp(rr);
        // Build until rlist is full
        for (int i = 0; i < rlist.size(); ++i) {
            String r0 = rlist.get(i);
            int i0 = r.get(r0);
            String r1 = plain(r0);
            if (verbose)
                System.err.println("Plain "+r0+" "+r1);
            if (!r.containsKey(r1)) {
                gensixp(r1);
            }
            int ip = r.get(r1);
            String r2 = bob(r0);
            if (verbose)
                System.err.println("Bob "+r0+" "+r2);
            if (!r.containsKey(r2)) {
                gensixp(r2);
            }
            int ib = r.get(r2);
            
            // Identify which normal component we have
            int ty = rnorm.get(r0);
            // Instance of the type
            int tn = ++tyinst[ty];
            out.println(i0+" "+ip+" "+ib+" "+r0+" "+ty+" "+tn);//+" "+r1+" "+r2);
        }
    }
    
    /**
     * Extend a group with another generator.
     * @param grp
     * @param gen
     * @return
     */
    public Set<String>extendGroup(Set<String>grp, String gen) {
        String rounds = identity(gen.length());
        Set<String>normg2 = new LinkedHashSet<String>();
        for (String nm : grp) {
            String x1 = nm;
            do {
                normg2.add(x1);
                x1 = StedToSAT.rep(x1, gen, rounds);
                // System.err.println(x1);
            } while (!x1.equals(nm));
        }
        return normg2;
    }
    
    private String inverse(String el, String rounds) {
        String x = el;
        for (;;) {
            String x1 = StedToSAT.rep(x, el, rounds);
            if (x1.equals(rounds))
                return x;
            x = x1;
        }
    }

    /**
     * Find the inverse, but also check the original is the first
     * of all the cyclic variants - no point in testing more than
     * one of a cyclic variant.
     * @param el
     * @param rounds
     * @return the inverse, or null if the input was not the lowest cyclic.
     */
    private String firstInverse(String el, String rounds) {
        String x = el;
        for (;;) {
            String x1 = StedToSAT.rep(x, el, rounds);
            if (x1.equals(rounds))
                return x;
            else if (x1.compareTo(el) < 0)
                return null;
            x = x1;
        }
    }

    /**
     * Tests if g extends grp in a normal fashion.
     * @param grp
     * @param g
     * @param rounds
     * @return true if g extends grp to a normal supergroup
     */
    private boolean normtest1(List<String>grp, String g, String rounds) {
        // Generate left and right cosets
        Set<String>left = new HashSet<String>();
        Set<String>right = new HashSet<String>();
        for (String g2 : grp) {
            String x1 = StedToSAT.rep(g2, g, rounds);
            String x2 = StedToSAT.rep(g, g2, rounds);
            left.add(x1);
            right.add(x2);
        }
        // Left and right cosets the same => normal
        return left.equals(right);
    }

    /**
     * Tests if g extends grp in a normal fashion.
     * @param grp
     * @param g
     * @param rounds
     * @return true if g extends grp to a normal supergroup
     */
    private boolean normtest2(Collection<String>grp, String g, String rounds) {
        // Look for the inverse
        String invg = firstInverse(g, rounds);
        if (invg == null) {
            // There is another cyclic version of g which is first in alphabetical order
            // For efficiency; we will test the cyclic version another time.
            return false;
        }
        // for g in G, test g.x.g^-1 is in G
        for (String g2 : grp) {
            String x = StedToSAT.rep(g, g2, rounds);
            String x2 = StedToSAT.rep(x, invg, rounds);
            if (!grp.contains(x2))
                return false;
        }
        return true;
    }



    /**
     * Build a normal group for the main group.
     * The largest group for which the supplied group is a normal subgroup.
     */
    private void buildNormal() {
        String rounds = grp.get(0);
        long t1 = System.currentTimeMillis();
        Iterable<String>alt = StedToSAT.altGroup2(rounds);
        long t2 = System.currentTimeMillis();
        if (verbose)
            System.err.println("Alternating group size in "+(t2-t1)+"ms");

        Set<String>grps = new HashSet<String>(grp);
        // Normal supergroup of grp
        Set<String>normg = new LinkedHashSet<String>(grp);
        // Generators to build normg from grp
        List<String>normgen = new ArrayList<String>();
        int altsize = 0;
        for (String g : alt) {
            ++altsize;
            //System.out.println(g+" "+ooc(g)+" "+ooc2(g));
            if (altsize % 1000000 == 0)
                System.err.println("Progress testing normal elements: "+altsize);
            if (normg.contains(g))
                continue;
            // Is this a generator for a normal supergroup?
            //boolean norm1 = normtest1(grp, g, rounds);
            boolean norm2 = normtest2(grps, g, rounds);
            //if (norm1 != norm2)
            //    throw new IllegalStateException("Error "+norm1+" "+norm2+" "+g);
            if (norm2) {
                if (verbose)
                    System.err.println("Normal generator "+g);
                Set<String>normg2 = extendGroup(normg, g);
                if (verbose)
                    System.err.println("Normal group size goes from "+normg.size()+" to "+normg2.size());
                normg = normg2;
                normgen.add(g);
            }
        }
        if (verbose) {
            System.err.println("Normal generators "+normgen);
        }
        if (normgen.isEmpty()) normgen.add(rounds);
        //List<String>ng = StedToSAT.group(normgen.toArray(new String[normgen.size()]));
        //System.err.println("Norm group "+normg);
        // Normal subgroup?
        Set<String>normsub = new HashSet<String>();
        // G * N ?
        Set<String>normo = new LinkedHashSet<String>(grp);
        normsub.add(rounds);
        Set<String>mingens = new HashSet<String>(); 
        for (int s = 1; s <= rounds.length(); ++s) {
            for (String g : normg) {
                if (normsub.contains(g)) continue;
                if (normo.contains(g)) continue;
                Set<String>g1 = extendGroup(normsub, g);
                //System.err.println("Extending "+normsub+" "+g+"*"+g1.size()/normsub.size()+" => "+g1);
                Set<String>g2 = extendGroup(normo, g);
                //System.err.println("Extending "+normo+" "+g+"*"+g2.size()/normo.size()+" => "+g2);
                // Try to select 'powerful' generators
                // Chose the generator if it grows the outer group by more than a fraction of the
                // group generated by the generators
                if (g1.size() / normsub.size() <= s*g2.size() / normo.size()) {
                    normsub = g1;
                    normo = g2;
                    mingens.add(g);
                    if (normo.size() == normg.size())
                        break;
                }
            }
        }
        if (normo.size() != normg.size()) {
            throw new IllegalStateException("Error - min normal generators "+normo.size() +" != " + normg.size());
        }
        if (verbose) {
            System.err.println("Normal subgroup "+normg.size()+" "+normg);
            System.err.println("Normal min subgroup "+normsub);
            System.err.println("Normal min generators "+mingens);
        }
        if (mingens.size() == 0) mingens.add(rounds);
        grpNorm = StedToSAT.group(mingens.toArray(new String[mingens.size()]));
        grpNormSup = new ArrayList<String>(normo);
        tyinst = new int[altsize*4/grpNormSup.size()+1]; // ? for Stedman?
        if (verbose) {
            System.err.println("Normal grpNorm "+grpNorm.size()+" "+grpNorm);
            System.err.println("Normal grpNormSup "+grpNormSup.size()+" "+grpNormSup);
        }
    }
    
    /**
     * Swap to adjacent chars
     * @param s
     * @param i 1-based offset
     * @return
     */
    private String swap(String s, int i) {
        if (i < 0) {
            i = s.length() + i + 1;
        }
        StringBuilder sb = new StringBuilder(s);
        char ch = sb.charAt(i);
        sb.setCharAt(i, sb.charAt(i - 1));
        sb.setCharAt(i - 1, ch);
        return sb.toString();
    }
    
    /**
     * A change with a fixed place
     * @param s
     * @param place 1-based
     * @param lim how far to continue the swapping
     * @return
     */
    private String place(String s, int place, int lim) {
        StringBuilder sb = new StringBuilder(s);
        for (int i = 1; i < lim; ++i) {
            if (i == place)
                continue;
            char ch = sb.charAt(i);
            sb.setCharAt(i, sb.charAt(i - 1));
            sb.setCharAt(i - 1, ch);
            ++i;
        }
        return sb.toString();
    }
    
    private String place(String s, int place) {
        return place(s, place, s.length());
    }
    
    private boolean quick(String s) {
        return s.charAt(s.length() - 2) == 'Q';
    }
    
    private boolean quickNext(String s) {
        return s.charAt(s.length() - 1) == 'Q';
    }
    private boolean slow(String s) {
        return s.charAt(s.length() - 2) == 'S';
    }
    
    private boolean slowNext(String s) {
        return s.charAt(s.length() - 1) == 'S';
    }
    /**
     * 2314567QS
     * ---------
     * 3241576SQ
     * 3425167SQ
     * ---------
     * 4352176QS
     * 3451267QS
     * 
     * 2314567QS
     * ---------
     * 3241657SQ
     * 3426175SQ
     * ---------
     * 4362157QS
     * 3461275QS
     * @param s
     * @return
     */
    private String bob(String s) {
        s = place(s, nb - 2);
        return sixhead(s);
    }
    private String bob2(String s) {
        if (slowNext(s)) {
            return StedToSAT.rep("3425167SQ", s, "2314567QS");
        } else {
            return StedToSAT.rep("3461275QS", s, "3426175SQ");
        }
    }
    
    /**
     * 2314567QS
     * ---------
     * 3241657SQ
     * 3426175SQ
     * ---------
     * 4362715QS
     * 3467251QS
     * @param s
     * @return
     */
    private String plain(String s) {
        s = place(s, nb);
        return sixhead(s);
    }
    private String plain2(String s) {
        if (slowNext(s)) {
            return StedToSAT.rep("3426175SQ", s, "2314567QS");
        } else {
            return StedToSAT.rep("3467251QS", s, "3426175SQ");
        }
    }
    

    public String sixgen(String r1, int t) {
        String r = r1;
        if (r1.length()- 2 == 7)
            r = StedToSAT.rep("2314567QS",r1,"1234567SQ");
        else if (r1.length() - 2 == 9) {
            // Stedman / Erin test
            int d = r1.charAt(9) == r1.charAt(10) ? 3 : 6;
            if (t % d != 2) {
                // Usually rotate and swap Q S
                r = StedToSAT.rep("231456789SQ",r1,"123456789QS");
            } else {
                // Get the intermediate rows for Caters
                r = StedToSAT.rep("213547698SQ",r1,"123456789QS");
            }
        } else if (r1.length() - 2 == 11) {
            r = StedToSAT.rep("2314567890EQS",r1,"1234567890ESQ");
        }
        return r;
    }
    /**
     * Generate the six-ends in a six
     * Get an canonical start - e.g. quick rather than slow
     * six-end Q
     * six-head Q
     * six-end S
     * six-head S
     * rotate
     * 
     * apply second group to generate a whole
     * related set.
     * @param rr
     * @return
     */
    private String gensixp(String rr) {
        if (quickNext(rr)) {
            // Make Quick six first
            rr = swap(rr, -2);
        }
        rr = normalize(rr);
        for (String g: grp2) {
            String r1 = rr;
            int t = 0;
            do {
                String r0 = r1;
                do {
                    String r2 = StedToSAT.rep(r1, g, grp2.get(0));
                    if (!r.containsKey(r2)) {
                        genrow(r2);
                    }
                    // Next possible six-end
                    //r1 = StedToSAT.rep("231456789",r1,"123456798");
                    r1 = sixgen(r1, t++);
                    //System.out.println("r1="+r1+" t="+t);
                } while (!r1.equals(r0));
                if (ooc) {
                    r1 = sixhead(r1);
                }
            } while (!r1.equals(rr));
            //System.out.println("Total "+t+" "+rr);
        }
        return rr;
    }
    private String gensix(String r1) {
        genrow(r1);
        r1 = StedToSAT.rep("231456789",r1,"123456798");
        genrow(r1);
        r1 = StedToSAT.rep("231456789",r1,"123456798");
        genrow(r1);
        r1 = StedToSAT.rep("231456789",r1,"123456798");
        genrow(r1);
        r1 = StedToSAT.rep("231456789",r1,"123456798");
        genrow(r1);
        r1 = StedToSAT.rep("231456789",r1,"123456798");
        genrow(r1);
        r1 = StedToSAT.rep("231456789",r1,"123456798");
        return r1;
    }
    
    private boolean oocfast(String r) {
        if (ooc || ooc2)
            return ooc(r);
        else
            return false;
    }

    /**
     * Out of course. Less efficient.
     * Also doesn't work for Cinques as '0' < '1'.
     * @param r
     * @return
     */
    private boolean ooc2(String r) {
        for (int i = 0; i < r.length() - 1; ++i) {
            if (r.charAt(i) > r.charAt(i + 1)) {
                r = r.substring(0, i)+r.substring(i+1,i+2)+r.substring(i,i+1)+r.substring(i+2);
                return !ooc2(r);
            }
        }
        return false;
    }

    private boolean ooc(String r) {
        return StedToSAT.ooc(r, grp.get(0));
    }

    private String sixhead(String s) {
        if (quick(s)) {
            return place(s, 3, s.length() - 2);
        } else {
            return place(s, 1, s.length() - 2);
        }
    }
    
    private String normalize(String rr) {
        if (oocfast(rr.substring(0,rr.length() - 2))) {
            // swap six head / six end
            String r = sixhead(rr);
            if (!oocfast(r.substring(0,r.length() - 2)))
                rr = r;
        }
        return rr;
    }
    
    private void genrow(String r1) {
        //r1 = normalize(r1);
        rlist.add(r1);
        for (String g : grp) {
            String r2 = StedToSAT.rep(r1, g, grp.get(0));
            //System.out.println(r2+" "+n+" "+g);
            r.put(r2, n);
        }
        ++n;
        int ty = findNormalID(r1);
        // if the group is already set, should be set for everything.
        // otherwise do it here.
        if (ty == 0) {
            addNormalID(r1);
        }
    }

    private int addNormalID(String r1) {
        int ty;
        ty = type;
        ++type;
        for (String g : grpNormSup) {
            String r2 = StedToSAT.rep(r1, g, grpNormSup.get(0));
            Integer prev = rnorm.put(r2, ty);
            if (prev != null) {
                System.err.println("Types error1 "+r2+" "+prev+"->"+ty);
            }
        }
        return ty;
    }

    private int findNormalID(String r1) {
        // Normal group ID
        int ty = 0;
        for (String g : grpNormSup) {
            String r2 = StedToSAT.rep(r1, g, grpNormSup.get(0));
            //System.out.println(r2+" "+n+" "+g);
            if (rnorm.containsKey(r2)) {
                int ty2 = rnorm.get(r2);
                if (ty > 0 && ty2 != ty) {
                    System.err.println("Types error"+r2+" "+ty+" "+ty2);
                }
                ty = ty2;
                // Fast path - don't check the others
                break;
            }
        }
        return ty;
    }
}
