import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Convert Stedman/Erin Triples to SAT.
 * (c) Copyright 2015,2023 Andrew Johnson
 * March 2015 - January 2023
 * 
 * Main variables per six:bits
 * <sequence no.:n><six type:2><Quick/slow:1><plain/bob:1>
 * 
 * For a quick six:
 * dest sequence number = source sequence number when six+q+plain/bob
 * dest six type = when six+q+plain/bob
 * dest quick/slow = slow when 
 * 
 * For a slow six:
 * dest sequence no + 1 = source sequence number when six+s+plain/bob
 * dest six type = when six+q+plain/bob
 * dest quick/slow = quick when 
 * 
 * For source = start
 * No number for this,
 * Dest six = 0000001
 * 
 * For dest = start,
 * source six = last six number
 * 
 * If dest six == source six, disallow that six type/quickslow/call
 * 
 * For performance:
 * 
 * Exclude - two source to same destination
 * Exclude - source type + call linking to wrong destination type
 * Exclude - six type = 0
 * 
 * Further exclusions - calculate the sets of sixes which must have the same number of bobs
 * 
 * Input graph file is like:
 * 1 10 4 2314567QS 1 1
 * 2 13 19 3124567SQ 2 1
 * 3 16 22 1234567QS 3 1
 * 4 9 3 2314567SQ 4 1
 * 5 14 20 3124567QS 5 1
 * 6 15 21 1234567SQ 6 1
 * 7 28 34 3426175QS 7 1
 * 8 37 43 4236175SQ 8 1
 * 9 52 58 2346175QS 9 1
 * 10 27 33 3426175SQ 10 1
 * 11 38 44 4236175QS 11 1
 * 12 51 57 2346175SQ 12 1
 *
 * <six-end> <destination six-end for plain> <destination six-end for bob> <name of six-end (e.g. 1234567SS for Erin)>
 * The final two numbers aren't used - but are normal subgroup identifiers
 * The first of the two is the normal subgroup member identifier, and the second is the instance.
 * When considering possible starts all instances with the same subgroup identifier should find the same result.
 *
 * A special type of search using divg:
 * Consider 10-part group, but arrange sixes in pairs according how they would be in a 5-part group
 * Have a common six-type for the pair, but let the call vary for each of the two.
 * This is like doing a 10-part search, but then taking the result and using it for a 5-part search where the six-types
 * were fixed, but Q-sets of bobs/omits could be used to link things together.
 * Graph file must be specially generated.
 * 
 * Also a special type of search using divb:
 * If this is enabled too then the call type is also fixed per part.
 * Allows multipart searches which link to form n-part peal.
 * E.g. 4-part peals which otherwise could return 4 1-parts, 2 2-parts or 4 4-parts if directly searched.
 * 
 * @author Andrew Johnson
 *
 */
public class StedToSAT
{
    /** Stedman rather than Erin */
    final boolean sted;
    /** Out of course */
    final boolean ooc;
    /** Which six-end to go to after a plain or bob */
    final int next[][] = new int[5041][2];
    /** Which six-end was previous before a plain or bob. Shortcut for some tests. */
    final int prev[][] = new int[5041][2];
    /** If link is not allowed */
    final boolean invalid[][] = new boolean[5041][2];
    /** The name of the six-end */
    final String name[] = new String[5041];
    /** 
     * Value of SAT variables for interpreting the result. Read from .satout file.
     * 10911 is for 1-part peals of Stedman Triples with binary six numbering.
     * 
     * 840: call variables
     * 
     * 2520: six-type variables = 840*3 (Stedman)
     * 1880: six-type variables = 840*2 (Erin)
     * 
     * 7551: sequence numbering 839*9 (Stedman)
     * 8390: sequence numbering 839*10 (Erin)
     * 
     * Extras:
     * +2520: for one of 6 six type (rather than binary) = 840*(6-3) + (Stedman)
     * +840: for one of 3 six type (rather than binary) = 840*(3-2) + (Erin)
     * 
     * +280 - for matching bobs so they come in groups of 3
     * Can need a lot of variables for bob counting and Q-set counting.
     * +45064 bob counting (840 calls) = 22532*2
     * +12344 Q-set counting (840/3) = + 840/3 + 6032*2
     * 
     * = 10912 + 2520 + 280 + (22532*2) + (840/3 + 6032*2)
     */
    final boolean vals[];
    /** Number of sixes */
    final int N;
    /** Number of different sixes in touch for LFSR numbering */
    final int Nlen;
    /** Number of six-ends */
    final int NN;
    /** Number of bits for six numbering */
    final int bb;
    /** Number of different six-ends per six */
    final int sixTypes;
    /** How many bits per six for six-type = log2 sixTypes */
    final int bitsSix;
    /** Where to start numbering types within a six with binary encoding */
    final int typeStart;
    /** First variable for call */
    final int firstCallVar;
    /** First variable for six type */
    final int firstTypeVar;
    /** First variable for sequence */
    final int firstSeqVar;
    /** First variable for call count matching */
    final int firstTVVar;
    /** First variable for counting runs of bobs */
    final int firstCBRun;
    /** First variable for counting bobs */
    final int firstCBVar;
    /** First variable for counting Q-sets */
    final int firstCQSVar;
    /** Next unused variable */
    int nextFreeVar;
    /** secondary grouping - sixes divided by this have the same type */
    final int divg;
    /** secondary grouping - calls for six divided by this have the same call bob or single */
    final int divb;
    /** the zero-based number of the first six - no slot for this six */
    int sixstart = 0;
    /** number of call variables */
    final int ccv;
    /** bob in threes counting vars */
    final int tv;
    /** extra bob run count */
    final int cbrun;
    /** total bobs counting vars */
    final int cbv;
    /** total q-sets counting vars */
    final int cqs;

    /** generators for the original group used to generate the six-end graph */
    final private List<String> gens;
    /** the original group used to generate the six-end graph */
    final List <String>group;
    
    // 0x83c best
    // 0x4204, 0x24204 are also good
    // also 0x8040
    // also 0x8240
    // also 0x8280 for erin2
    // also 0x2810 for sted10
    // also 0x248 for sted10 - UNSAT
    // also 0x5a018 for erin7 UNSAT
    // also 0x2200 for sted10 pp==
    // also 0x10880 ->0x10000,0x10800,0x10880 for sted5 2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P
    // also 0x20 for sted5 2314567QSpp===ppp==p=p====pppp==p==p*0.7(1)
    // also 0x2248 for sted10
    // >>>Found final best from runs 0x40008 18691ms for sted3, with B-blocks
    // >>>Found final best from runs 0x2080 55577ms for sted3, with B-blocks
    // >>>Found final best from runs 0x60 5314ms 3-part with cryptominisat5, no last pass for UNSAT
    // >>>Found final best from runs 0x80 24026ms 3-part with cryptominisat5, with last pass for UNSAT
    // >>>Found final best from runs 0xa6200 1229ms for 10 part with pp==
    // >>>Found final best from runs 0x1000 7127ms for 10 part with 2314567QSr
    // >>>Found best from runs 0x2a88 71820ms and ...
    // >>>Found better 0x2a98 71361ms for 10 part with 2314567QS, still going with Glucose
    // >>>Found final best from runs 0x820000 33487ms with Erin2 256 count - redundant LFSR calcs + 1 inbound six-type
    // >>>Found final best from runs 0x208000 3632ms for sted10
    // >>>Found final best from runs 0x80 24905ms for sted2 run all options 30 seconds, using SAT4J, 0x1,0x3,0x20,0x2000, 0x2082, much worse; 0x4,0x8,0x40,0x80 better
    // >>>Found final best from runs 0xc8 17651ms ditto using CryptoMinisat5 (>>>Found better 0x80 21532ms)
    // >>>Found final best from runs 0x80 26283ms ditto using Glucose 4.1 simp (>>>Found from 0x8 37182ms)(>>>Found from 0x40 36569ms)
    // >>>Found final best from runs 0x200018 5466ms using Lingeling 
    // >>>Found final best from runs 0x13000d0 43714ms for sted10 using Cryptominisat5 --reconfat 0 --reconf 4 --maxsol 5
    // >>>Found final best from runs 0x200084 40925ms for sted5 using Cryptominisat5 --reconfat 0 --reconf 4
    // >>>Found final best from runs 0x84 41432ms for sted5 using Cryptominisat5 --reconfat 0 --reconf 4
    // >>>Found final best from runs 0xb80040 24440ms for sted5 using CaDiCal --sat 2314567QSpp===ppp==p=p==
    // >>>Found final best from runs 0xb00040 24392ms for sted5 using CaDiCal --sat 2314567QSpp===ppp==p=p==
    // >>>Found better 0x800040 24440ms but extra 0x300000 makes no difference for this graph to the generated CNF
    // >>>Found final best from runs 0xb00040 24392ms for sted5 using CaDiCal --sat 2314567QSpp===ppp==p=p==
    // >>>Found final best from runs 0x4 4483ms for erin2 using CaDiCal --sat
    // >>>Found final best from runs 0x1008224 18065ms for sted4c using CaDiCal --sat
    // >>>Found final best from runs 0x1008200 18065ms for sted4c using CaDiCal --sat
    // >>>Found final best from runs 0xc200 20336ms for sted4c using CaDiCal --sat
    // >>>Found final best from runs 0xc0090 5403ms for erin7 using CaDiCal --sat
    // >>>Found final best from runs 0x2080240 19189ms for erin2 using CaDiCal --sat

    // 0x1e3 is a compact, but inefficient encoding
    // Aggressive 0x18fde1c - turns on all options
    final boolean opt1;  //   0x0004  Disallow all zero - only when no subcycles
    final boolean opt2;  //   0x0008  Disallow beyond last
    final boolean opt3;  //   0x0010  Disallow before first
    final boolean opt4;  //   0x0020 !Disallow two inbounds to same six-type - required when subcycles present
    final boolean opt5;  //   0x0040 !Disallow two inbounds to same six
    final boolean opt6;  //   0x0080 !Disallow other inbounds to different six-type in six when this six-type
    final boolean opt7;  //   0x0100 !Disallow no inbounds when this six-type - very important
    final boolean opt8;  //   0x0200  Disallow six-type if no outbounds
    final boolean opt9;  //   0x0400  Check same number of bobs from abc,bca, cab, also check bobs are multiple of 3
    final boolean opt10; //   0x0800  Common source sequence bit => destination sequence bit ignoring call
    final boolean opt11; //   0x1000  Common destinations sequence bit => source sequence bit ignoring call
    final boolean opt12; //   0x2000  Extra bit for six encoding mode. Was force OutOfCourse (to see the performance hit) - doesn't work!!
    final boolean opt13; //   0x4000  If all dests with a call are invalid the call must be opposite
    final boolean opt14; //   0x8000  If all quick outputs of this six are invalid then this six must be slow and v.v.
    final boolean opt15; //  0x10000  If all quick outputs with a call of this six are invalid then this six must be slow or the call must be opposite and v.v.
    final boolean opt16; //  0x20000  At least one inbound six-type must be available for this six
    final boolean opt17; //  0x40000  At least one outbound six-type must be available for this six
    final boolean opt18; //  0x80000  Condense choose one of each clauses
    final boolean opt19; // 0x100000  Disable hybrid LFSR and short LFSR - consider non-linear
    final boolean opt20; // 0x200000  Disable non-linear FSR and short LFSR - consider hybrid
    final boolean opt21; // 0x400000  Disable twisted-ring LFSR (for full length)
    final boolean opt22; // 0x800000  Redundant LFSR calculations
    final boolean opt23; //0x1000000  Force detection of extra invalid LFSR states
    final boolean opt24; //0x2000000  Exclude unreachable LFSR states
    static final int ALL_OPTS = 0x38fde1c; // All the options except ones which disable things
    /**
     * Used to control different encodings of six-ends
     * 0 - direct encoding
     * 1 - twisted-ring, minimum variables test, all variables set, exclude invalids
     * 2 - order encoding
     * 3 - binary
     * 4 - combined direct + call type: NEWCALL
     * 5 - twisted-ring with minimum clauses set/exclude - only valid for one big loop
     * 6 - direct encoding with separate call vars for bob and plain (direct encoding for call-type): COMBINEDCALLTYPE
     * 7 - generic binary encoding
     */
    final int sel;
    final static int TWISTEDRING = 1;
    final static int NEWCALL = 4;
    /** Twisted-ring six-type, no excludes of illegal values */
    final static int TWISTEDRINGXMIN = 5;
    /** Direct encoding of combined six-type and call-type */
    final static int COMBINEDCALLTYPE = 6;
    /** Two variables per call - direct encoding of call type */
    final boolean newcall;
    
    final PrintStream sysout;
    
    /** For performance testing */
    boolean perftime = false;
    /** for showing gates */
    static boolean printsimp = false;
    
    /**
     * Linear Feedback Shift Register taps.
     * See: Table of Linear Feedback Shift Registers by Roy Ward & Tim Molteno.
     * 1-based.
     * For Galois mode,
     * BitNext[1] = Bit[taps[0]]
     * BitNext[2] = Bit[1]
     * ..
     * E.g. taps[1] == 2
     * BitNext[3] = Bit[2] ^ Bit[taps[0]]
     * ...
     * BitNext[n] = Bit[n - 1]
     */
    final static int tapsList[][] = {{},{},{2,1},{3,2},{4,3},{5,3},{6,5},{7,6},{8,6,5,4},
        {9,5},{10,7},{11,9},{12,11,8,6},{13,12,10,9},{14,13,11,9},{15,14},{16,14,13,11},
        {17,14},{18,11},{19,18,17,14},{20,17},{21,19},{22,21},{23,18},{24,23,21,20},
    };
    /**
     * Alternative taps with complex Galois / Fibonacci mode.
     */
    final static int tapsListComplex[][] = {{},{},{},{},{},{5,4,-3},{6,5,-4},{7,6,-2},{8,7,-6},
        {9,8,-4},{10,9,-3},{11,10,-8},{12,9,-4},{13,12,-11},{14,13,-11},{15,14,-13},{16,14,-7},
        {17,16,-14},{18,15,-13},{19,18,-11},{20,19,-16},{21,20,-15},{22,21,-17},{23,22,-21},{24,23,-19}};
    /**
     * Alternative taps with complex non-linear mode.
     */
    final static int tapsListNonLinear[][] = {{},{},{},{},{},{},{},{},{8,-5,-4},
        {},{},{},{12,-8,-5},{},{},{},{},
        {},{},{},{},{},{},{},{}};
    /**
     * Alternative taps if a maximal length sequence is not required.
     */
    final static int tapsList2[][] = {{},{},{},{},{},{},{},{},{8,5},
        {},{},{},{12,11},{13,10},{14,13},{},{16,9},
        {},{},{19,13},{},{},{},{},{24,19},
    };
    /**
     * Lengths with those alternatives taps.
     */
    final static int tapsLen2[] = {0,0,0,0,0,0,0,0,217,
        0,0,0,3255,8001,11811,0,63457,
        0,0,520065,0,0,0,0,16766977,
    };
    
    /**
     * Is this a six-end a quick six?
     * @param i the six-end
     * @return
     */
    public boolean quick(int i) {
        checkSixEnd(i);
        return sted && (i & 1) != 0;
    }
    /**
     * Check that the six-end value is valid.
     * @param i
     */
    private void checkSixEnd(int i) {
        if (i <= 0 || i > NN)
            throw new IllegalArgumentException("" + i);
    }
    /**
     * Type of six ignoring quick/slow distinction
     * @param i The six-end
     * @return 1,2,3 depending on typeStart i.e. how to number e.g. 6 out of 8 values.
     */
    public int sixtype(int i) {
        checkSixEnd(i);
        return ((i - 1) % sixTypes) + typeStart;
    }
    /**
     * Returns the state of the SAT literals for a six-end presuming binary encoding.
     * @param i The six-end
     * @param b The count of which literal (0-based)
     * @return +1 or -1 depending on whether literal should be set or cleared
     */
    public int sixval(int i, int b) {
        int v = sixtype(i);
        int ret = (v & 1 << b) > 0 ? 1 : -1;
        return ret;
    }
    /**
     * Convert an input six-end number to which six.
     * @param i 1-based
     * @return 0-based
     */
    public int six(int i) {
        checkSixEnd(i);
        return (i - 1) / sixTypes;
    }
    /**
     * Find the first six-end of a six
     */
    public int firstsixend(int i) {
        checkSixEnd(i);
        return i - (i - 1) % sixTypes;
    }
    /**
     * Variable for the six type.
     * @param i 1-based
     * @param b 0-based bit number
     * @return variable number
     */
    public int sixvar(int i, int b) {
        if (b < 0 || b >= bitsSix)
            throw new IllegalArgumentException("bit number ="+b);
        int s = six(i) / divg;
        return firstTypeVar+s*bitsSix + b;
    }
    /**
     * Variable for a call
     * @param i 1-based six-end
     * @return variable used for the call
     */
    public int callvar(int i) {
        int s = six(i) / divb;
        if (newcall) s *= 2;
        return firstCallVar+s;
    }
    /**
     * Variable for the sequence number.
     * @param i 1-based six-end number
     * @param b 0-based bit number
     * @return SAT variable
     */
    public int countvar(int i, int b) {
        int s = six(i);
        if (s == sixstart) throw new IllegalArgumentException("Countvar as start: "+i);
        if (b < 0 || b > bb) throw new IllegalArgumentException("bit number="+b);
        if (s > sixstart) --s;
        return firstSeqVar + s * bb + b;
    }

    /** Number of bits to represent 1 .. x.
     *  round up 
     *  4 -> 2
     *  6 -> 3
     */
    private static int log2(int x)
    {
        if (x == 0) return 0;
        return 32 - Integer.numberOfLeadingZeros(x - 1);
    }

    /** Number of bits to represent 1 .. x.
     *  round up 
     *  4 -> 2
     *  6 -> 3
     */
    private static int log2(long x)
    {
        if (x == 0) return 0;
        return 64 - Long.numberOfLeadingZeros(x - 1);
    }

    /**
     * Is a short cycle possible with wrapping?
     * Wrapping N=12, Nlen = 6, counter = Nlen / 2
     * would be ambiguous?
     * 
     * Quick six start: 0Q 1S 2Q 2S 3Q 3S 1Q 1S 2Q 2S 3Q 3S
     * Slow six start:  0S 1Q 1S 2Q 2S 3Q 3S 1Q 1S 2Q 2S 3Q
     * 
     * Wrapping N=15 Nlen=8
     * 0S 1S 2S 3S 4S 5S 6S 7S 1S 2S 3S 4S 5S 6S 7S
     * 
     * @return true if ambiguous, so needs extra state
     */
    private boolean shortCycle() {
        if (sted) {
            return Nlen <= N / 2;
        } else {
            return 2 * Nlen - 1 <= N;
        }
    }

    /**
     * Build a SAT model of Stedman/Erin
     * @param fn bob/plain graph file
     * @param fix length of main loop (negative is how much shorter than full length)
     * @param bobcount Number of bobs, +ve => >= bobcount, -ve => <= -bobcount
     * @param qscount Quick/slow count, +ve => >=qscount, -ve => <= -qscount
     * @param opts An integer controlling different options for the encoding
     * @param divg Outer grouping - the six type matches according to this in sets.
     * @param divb Outer grouping size for bobs - the call type matches for sets of sixes
     * @param extraVars per six
     * @param countedruns number of runs with a count
     * @param outfile The file name for the result
     * @throws IOException
     */
    public StedToSAT(String fn, int fix, int bobcount, int qscount, int opts, int divg, int divb, int extraVars, int countedruns, String outfile) throws IOException {
        // Set the tuning options
        int sel =  (opts & 1 << 13) == 0 ? (opts >> 0) & 3 : ((opts >> 0) & 3) + 4; // scheme
        newcall = sel == NEWCALL;
        opt1 = (opts & 1 << 2) != 0; // 0x4
        opt2 = (opts & 1 << 3) != 0; // 0x8
        opt3 = (opts & 1 << 4) != 0; // 0x10
        opt4 = (opts & 1 << 5) != 0; // 0x20
        opt5 = (opts & 1 << 6) != 0; // 0x40
        opt6 = (opts & 1 << 7) != 0; // 0x80
        opt7 = (opts & 1 << 8) != 0; // 0x100
        opt8 = (opts & 1 << 9) != 0; // 0x200
        opt9 = (opts & 1 << 10) != 0; // && divb % 3 != 0; //divg == 1 && divb == 1; // 0x400
        opt10 = (opts & 1 << 11) != 0; // 0x800
        opt11 = (opts & 1 << 12) != 0; // 0x1000
        opt12 = (opts & 1 << 13) != 0 && false; // 0x2000
        opt13 = (opts & 1 << 14) != 0; // 0x4000
        opt14 = (opts & 1 << 15) != 0; // 0x8000
        opt15 = (opts & 1 << 16) != 0; // 0x10000
        opt16 = (opts & 1 << 17) != 0; // 0x20000
        opt17 = (opts & 1 << 18) != 0; // 0x40000
        opt18 = (opts & 1 << 19) != 0; // 0x80000
        opt19 = (opts & 1 << 20) != 0; // 0x100000
        opt20 = (opts & 1 << 21) != 0; // 0x200000
        opt21 = (opts & 1 << 22) != 0; // 0x400000
        opt22 = (opts & 1 << 23) != 0; // 0x800000
        opt23 = (opts & 1 << 24) != 0; // 0x1000000
        opt24 = (opts & 1 << 25) != 0; // 0x2000000
        perftime = (opts & 1 << 31) != 0; // 0x80000000
        //System.err.println("opt: "+sel+" "+opt1+opt2+opt3+opt4+opt5+opt6+opt7+opt8+opt9);
        if (outfile.equals("-")) {
            sysout = System.out;
        } else {
            // Buffered IO is faster, explicit default encoding for FindBugs
            sysout = new PrintStream(new BufferedOutputStream(new FileOutputStream(outfile), 1024*1024), false, Charset.defaultCharset().name());
        }
        // Load the graph, return [length:30; sted:1; ooc:1]
        int nn = load(fn);
        NN = nn / 4;
        sted = (nn & 2) != 0;
        ooc = (nn & 1) != 0 || opt12;
        if (sted) {
            sixTypes = ooc ? 12 : 6;
            typeStart = ooc ? 4 : 2;
        } else {
            sixTypes = ooc ? 6 : 3;
            typeStart = ooc ? 2 : 1;
        }
        N = NN / sixTypes;
        // The length of one segment, checked using the sequence number
        // Any remaining sixes can be linked in separate loops.
        if (fix >= 0) {
            Nlen = Math.min(fix, N);
        } else {
            Nlen = Math.max(N + fix, 0);
        }
        // If the length is not the full length then cannot use TWISTEDRINGXMIN mode
        if (sel == TWISTEDRINGXMIN && Nlen != N)
        {
            sel = TWISTEDRING;
        }
        // Set sel
        this.sel = sel;
        // Now sel is set we can calculate the bits per six
        bitsSix = genBitsSix(sixTypes);
        // Bits for sequence number
        // If the counter is less than the full length and could wrap, then add 1 to detect overruns.
        boolean shortCycle = shortCycle();
        bb = Nlen <= 1 ? 0: Math.max(log2((sted ? Nlen / 2 + 1 : Nlen)+(shortCycle ? 1 : 0)), 2);
        // for searching by common six type but allowing choice of calls
        this.divg = divg;
        // Also have same calls by this division - allows search for exact 4-part
        this.divb = divb;
        
        /*
         * Find the original group structure
         */
        long then = System.currentTimeMillis();
        gens = findGroup();
        group = group(gens.toArray(new String[gens.size()]));
        long now = System.currentTimeMillis();
        if (perftime) {
            System.err.println("Found group "+group+" in "+(now-then)+"ms");
        }
        
        int nextVar = 1;
        // Variable base for calls
        firstCallVar = nextVar;
        // Variables for calls
        if (sel != COMBINEDCALLTYPE) {
            ccv = newcall ? 2*N / divb : N / divb;
        } else {
            ccv = 0;
        }
        nextVar += ccv;
        // Variable base for six-types
        firstTypeVar = nextVar;
        nextVar += bitsSix * N / divg;
        
        // count for bobs in 3s
        tv = varsForSameBobs(group);
        // for counting runs of bobs
        cbrun = extraVars * N + countedruns * sorting(N, null) * 2;
        // For counting total number of bobs
        if (bobcount != 0) {
            int bobvarstocount = sel != COMBINEDCALLTYPE ? N / divb : NN / divb;
            cbv = sorting(bobvarstocount, null) * 2;
        } else {
            cbv = 0;
        }
        // For counting total number of available Q-sets
        if (qscount != 0) {
            cqs = varsForCountQSets();
        } else {
            cqs = 0;
        }
        
        // First variable for the sequence number
        firstSeqVar = nextVar;
        nextVar += (N - 1) * bb;
        // First variable for finding calls in sets of 3
        firstTVVar = nextVar;
        nextVar += tv;
        // First variable for counting runs of bobs.
        firstCBRun = nextVar;
        nextVar += cbrun;
        // First variable for counting bobs.
        firstCBVar = nextVar;
        nextVar += cbv;
        // First variable for counting Q-sets
        firstCQSVar = nextVar;
        nextVar += cqs;
        // Next free variable
        nextFreeVar = nextVar;
        // Allocate space to hold variables when evaluating solver results
        vals = new boolean[nextVar];
    }
    void setName(int ix, String nm) {
        name[ix] = nm;
    }
    String name(int src) {
        return name[src];
    }
    /**
     * Find a six-end name
     * @param s
     * @return the six-end index (1-based)
     */
    int findName(String s) {
        int found = NN + 1;
        for (int ix = 1; ix <= NN; ++ix) {
            if (name[ix].contains(s)) {
                if (found < ix) {
                    // duplicates so uncertain
                    return NN + 1;
                } else {
                    found = ix;
                }
            }
        }
        return found;
    }
    /**
     * Find a six-end name, allowing for group renaming.
     * @param s
     * @param grp
     * @return the six-end index (1-based)
     */
    int findName(String s, List<String>grp) {
        int found = NN + 1;
        String rounds = grp.get(0);
        for (String el : grp) {
            String s1 = rep(s, el, rounds);
            found = findName(s1);
            //System.err.println("Searching "+s1+" "+found);
            if (found <= NN)
                break;
        }
        return found;
    }
    
    /**
     * In-course or not.
     * @param s
     * @return true if in-course
     */
    private boolean parity(String s) {
        boolean par = true;
        char c[] = s.toCharArray();
        boolean redo;
        do {
            redo = false;
            for (int i = 1; i < c.length; ++i) {
                if (c[i-1] > c[i]) {
                    redo = true;
                    par = !par;
                    char a = c[i-1];
                    c[i-1] = c[i];
                    c[i] = a;
                }
            }
        } while (redo);
        //System.out.println(s+" "+new String(c)+" "+par);
        return par;
    }

    /**
     * Out of course.
     * For each element, see how far it is out of place from the right
     * with respect to elements less than it.
     * Conceptually this is rotating j elements 1 to the left to bring
     * the element home. If i is even this is an odd parity transform.
     * @param r
     * @param rounds
     * @return
     */
    static boolean ooc(String r, String rounds) {
        int t = 0;
        for (int k = r.length(); k-- > 0; ) {
            char c = rounds.charAt(k);
            int u = 0;
            for (int j = r.length(); j-- > 0; ) {
                if (r.charAt(j) == c)
                    break;
                if (rounds.indexOf(r.charAt(j)) < k) {
                    ++u;
                }
            }
            t += u;
        }
        //System.out.println(t+" "+r);
        // Test for odd number of swaps. Bit test keeps SpotBugs happy.
        return (t & 1) == 1;
    }

    /**
     * Whether out of course, excluding QS at end.
     */
    private boolean ooc(String s) {
        return !parity(s.substring(0, s.length() - 2));
    }

    /**
     * Find next permutation in sequence order.
     * 2:
     * How many numbers < 2 stepped over from right
     * 3:
     * How many numbers < 3 stepped over from right
     * *2 + n2
0 1234
1 2134
2 1324
3 2314
4 3124
5 3214
6 1243
7 2143
8 1342
9 2341
10 3142
11 3241
12 1423
13 2413
14 1432
15 2431
16 3412
17 3421
18 4123
19 4213
20 4132
21 4231
22 4312
23 4321
     * 
     * Swap 1 and 2
     * Move 3 to left
     */
    private static String nextPerm(String s, String rounds) {
        char ca[] = s.toCharArray();
        for (char c : rounds.toCharArray()) {
            int k = 0;
            for (int j = ca.length; j-- > 0; ) {
                if (k > 0) {
                    // See if we can move it to the left
                    if (rounds.indexOf(ca[j]) < rounds.indexOf(c)) {
                        // found, do swap
                        ca[j] = c;
                        // Make the old position available to be refilled
                        ca[k] = rounds.charAt(0);
                        // Fill in remaining lower numbers in order
                        int m = 0;
                        for (int l = 0; l < s.length(); ++l) {
                            if (rounds.indexOf(ca[l]) < rounds.indexOf(c)) {
                                ca[l] = rounds.charAt(m++);
                                if (rounds.charAt(m) == c)
                                    break;
                            }
                        }
                        return String.valueOf(ca);
                    }
                } else {
                    // Found the character we are looking for
                    if (ca[j] == c) {
                        k = j;
                    }
                }
            }
        }
        return null;
    }

    static Iterable<String>altGroup1(String rounds)
    {
        // Alternating group
        // Generate Sn or An using  n-way rotation and rotating first 3
        List<String>alt = StedToSAT.group(new String[]{rounds.substring(1)+rounds.substring(0,1),
                rounds.substring(1, 3)+rounds.substring(0, 1)+rounds.substring(3)});
        return alt;
    }

    static Iterable<String>symGroup1(String rounds)
    {
        // Symmetric group
        // Generate Sn using  n-way rotation and swapping first 2
        List<String>alt = StedToSAT.group(new String[]{rounds.substring(1)+rounds.substring(0,1),
                rounds.substring(1, 2)+rounds.substring(0, 1)+rounds.substring(2)});
        return alt;
    }

    /**
     * Generate the symmetric group.
     * @param rounds
     * @return Sn iterable over successive members of the group.
     */
    static Iterable<String>symGroup2(final String rounds)
    {
        return new Iterable<String>() {
            @Override
            public Iterator<String> iterator() {
                return new Iterator<String>() {
                    String r = rounds;
                    @Override
                    public boolean hasNext() {
                        return r != null;
                    }

                    @Override
                    public String next() {
                        if (r == null)
                            throw new NoSuchElementException();
                        String ret = r;
                        r = nextPerm(r, rounds);
                        return ret;
                    }

                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException();
                    }
                };
            }
        };
    }

    /**
     * Generate the alternating group.
     * @param rounds
     * @return An iterable over successive members of the group.
     */
    static Iterable<String>altGroup2(final String rounds)
    {
        return new Iterable<String>() {
            @Override
            public Iterator<String> iterator() {
                return new Iterator<String>() {
                    String r = rounds;
                    @Override
                    public boolean hasNext() {
                        return r != null;
                    }

                    @Override
                    public String next() {
                        if (r == null)
                            throw new NoSuchElementException();
                        String ret = r;
                        do {
                            r = nextPerm(r, rounds);
                        } while (r != null && ooc(r, rounds));
                        return ret;
                    }

                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException();
                    }
                };
            }
        };
    }

    /**
     * Loads a file containing the 'six' numbers encoding the graph.
     * Format:
     * <six> <next six via plain> <next six via bob> <six name> <optional>
     * @param fn
     * @return number of entries (1-based)*4 + sted*2 + ooc;
     * @throws FileNotFoundException
     * @throws IOException
     */
    private int load(String fn) throws FileNotFoundException, IOException
    {
        boolean sted1 = true;
        boolean ooc1 = false;
        int entries = 0;
        Scanner sc = new Scanner(new File(fn), Charset.defaultCharset().name());
        try {
            while (sc.hasNextLine()) {
                String s = sc.nextLine();
                String sn[] = s.split(" ", 5);
                int from = Integer.parseInt(sn[0]);
                int to1 = Integer.parseInt(sn[1]);
                int to2 = Integer.parseInt(sn[2]);
                next[from][0] = to1;
                next[from][1] = to2;
                prev[to1][0] = from;
                prev[to2][1] = from;
                setName(from, sn[3]);
                // See if an out-of-course row is found
                if (!ooc1 && ooc(sn[3])) {
                    ooc1 = true;
                }
                // All Caters (or 13, 17...) is like out-of-course as any of the 6 rows can be false
                if ((sn[3].length() - 2) % 4 == 1) {
                    ooc1 = true;
                }
                // Stedman test - see if destination odd/even always differs from source
                sted1 &= ((from ^ to1) & 1) != 0;
                sted1 &= ((from ^ to2) & 1) != 0;
                entries = Math.max(entries, from);
            }
        } finally {
            sc.close();
        }
        return entries*4 + (sted1 ? 2 : 0) + (ooc1 ? 1 : 0);
    }

    /**
     * Swap pairs apart from a place
     * @param b characters to swap
     * @param p place to be made (1-based)
     * @param mx maximum length to swap
     */
    private void place(char b[], int p, int mx)
    {
        for (int i = 0; i < b.length - 1 && i < mx;)
        {
            if (i + 1 == p) {
                ++i;
            } else {
                char t = b[i];
                b[i] = b[i + 1];
                b[i + 1] = t;
                i += 2;
            }
        }
    }
    
    /**
     * Calculate the effect of a call.
     * @param s the name of the six-end
     * @param bob
     * @return the new six-end
     */
    private String call(String s, boolean bob) {
        boolean slowNext = s.charAt(s.length() - 1) == 'S';
        char b[] = s.toCharArray();
        if (bob) {
            place(b, b.length - 4, b.length);
        } else {
            place(b, b.length - 2, b.length);
        }
        if (slowNext) {
            // = 3.1.3.1.3
            place(b, 1, b.length - 2);
        } else {
            // = 1.3.1.3.1
            place(b, 3, b.length - 2);
        }
        return String.valueOf(b);
    }

    public String callName(int call) {
        return call == 0 ? "plain" : "bob";
    }
    
    /**
     * Match six-type to variables.
     * All variables need to match.
     * @param st
     * @return
     */
    public boolean matchSixVal(int st) {
        // See if six value matches
        int g[] = genVal(st);
        for (int x: g) {
            // 0 is an omitted variable
            if (x != 0 && !eval(x)) return false;
        }
        return true;
    }
    
    /**
     * Match six-type to variables.
     * Any variable needs to match.
     * @param st
     * @return
     */
    public boolean matchSixVal2(int st) {
        // See if six value matches
        int g[] = genVal(st);
        for (int x: g) {
            // 0 is an omitted variable
            if (x != 0 && eval(x)) return true;
        }
        return false;
    }
    /*
     * translate variables from SAT solver back into a touch
     * SAT
     * -1 -2 3 4 5 0
     * 
     * or
     * -1 2 0
     * 
     * or
     * s SATISFIABLE
     * v -1 -2 -3 0
     * 
     * Sometimes multiple solutions can be provided, with another section
     * s SATISFIABLE
     * v -1 -2
     * v -3
     * v 0
     */
    private boolean loadVars(String fn, int groupsize) throws FileNotFoundException, IOException
    {
        // Load the variables
        boolean found = false;
        Scanner sc = new Scanner(new File(fn), Charset.defaultCharset().name());
        try {
            do {
                int iprev = readSolution(sc);

                if (iprev <= 0) {
                    // No more variables read
                    if (iprev < 0) {
                        print("c Indeterminate solution(s) from SAT solution file "+fn);
                        print("c INDET");
                    }
                    break;
                }
                if (!found) {
                    found = true;
                    print("c Decoded solution(s) from SAT solution file "+fn);
                }
                StringBuilder sb = extractSolution(groupsize);
                sb.insert(0, "c ");
                print(sb.toString());
            } while (true);
        } finally {
            sc.close();
        }
        return found;
    }
    
    
    /**
     * Read SAT variables from output file.
     * 
     * UNSAT
     * 
     * SAT
     * -1 -2 -3 -4 5 -6 -7 -8 9 10
     * 
     * or
     * 
     * -1 2 0
     * 
     * or
     * s SATISFIABLE
     * v -1 -2 -3 0
     * 
     * Sometimes multiple solutions can be provided, with another section.
     * s SATISFIABLE
     * v -1 -2
     * v -3
     * v 0
     * 
     * @param sc
     * @return last variable read
     */
    private int readSolution(Scanner sc)
    {
        int iprev = 0;
        boolean first = true;
        // Look for the start of the variable list
        while (sc.hasNext()) {
            String tok = sc.next();
            if ("v".equals(tok)) break;
            if ("SAT".equals(tok)) break;
            if ("UNSAT".equals(tok)) {
                return 0;
            }
            if ("INDET".equals(tok)) {
                return -1;
            }
            if ("s".equals(tok) && sc.hasNext()) {
                tok = sc.next();
                if ("SATISFIABLE".equals(tok)) break;
                if ("UNSATISFIABLE".equals(tok)) {
                    return 0;
                }
                if ("INDETERMINATE".equals(tok)) {
                    return -1;
                }
            }
            
            if (first && "-1".equals(tok)) {
                vals[1] = false;
                iprev = 1;
                break;
            }
            if (first && "1".equals(tok)) {
                vals[1] = true;
                iprev = 1;
                break;
            }
            first = false;
            // Skip the remainder of the line
            sc.nextLine();
        }

        int firstMissingVar = 0;
        // Read the remaining variables
        for (int i; sc.hasNext(); ) {
            if (!sc.hasNextInt()) {
                String n = sc.next();
                if ("v".equals(n)) continue;
                System.err.println("Unexpected token:"+n);
                continue;
            }
            i = sc.nextInt();
            int missingVar = 0;
            if (i > 0) {
                if (i < vals.length) {
                    vals[i] = true;
                } else {
                    missingVar = i;
                }
            } else if (i < 0) {
                if (-i < vals.length) {
                   vals[-i] = false;
                } else {
                    missingVar = -i;
                }
            } else {
                // End of solution
                break;
            }
            if (Math.abs(i) > iprev + 1) {
                if (firstMissingVar != 0) {
                    if (firstMissingVar == iprev)
                       System.err.println("Ignoring out of range variable "+i+" >= "+vals.length);
                    else
                       System.err.println("Ignoring out of range variables "+firstMissingVar+" to "+iprev+" >= "+vals.length);
                    firstMissingVar = 0;
                }
                if (iprev + 2 == Math.abs(i)) {
                    System.err.println("Missing variable "+(iprev + 1));
                } else {
                    System.err.println("Missing variables "+(iprev + 1)+" to "+(Math.abs(i) - 1));
                }
            }
            iprev = Math.abs(i);
            if (firstMissingVar == 0 && missingVar != 0)
                firstMissingVar = missingVar;
        }
        if (firstMissingVar != 0) {
           if (firstMissingVar == iprev)
              System.err.println("Ignoring out of range variable "+firstMissingVar+" >= "+vals.length);
           else
               System.err.println("Ignoring out of range variables "+firstMissingVar+" to "+iprev+" >= "+vals.length);
            firstMissingVar = 0;
        }
        return iprev;
    }
    
    /**
     * See if this six-end has a complete Q-set.
     * @param s
     * @return
     */
    boolean hasQset(int s) {
        // Q-set walk
        int d1 = prev[next[s][1]][0];
        int d2 = prev[next[d1][1]][0];
        int d3 = prev[next[d2][1]][0];
        if (d3 != s) {
            // Error
            throw new IllegalStateException("no Q-set loop "+s+" "+d1+" "+d2+" "+d3);
        }
        return matchSixVal(s) && matchSixVal(d1) && matchSixVal(d2);
    }
    
    /**
     * Translate a SAT solution in variables to a readable touch.
     * @param groupsize
     * @return
     */
    private StringBuilder extractSolution(int groupsize)
    {
        int plain = 0;
        int segs = 0;
        int rbs = 0;
        int qsetsixes = 0;
        String segstart = "";
        // Record which sixes have been used
        boolean usedSix[] = new boolean[six(NN)+1];
        StringBuilder sb = new StringBuilder();
        // Find the type of the starting six by trial and error.
        // Consider sixstart first, then all the others.
        for (int rt = 0; rt < 2; ++rt) {
            for (int st = 1; st <= NN; ++st) {
                int sx = six(st);
                // On the first pass find the six with the unnumbered starting six
                if (rt == 0 && sx != sixstart) continue;
                if (usedSix[sx]) continue;
                // See if six value matches
                if (!matchSixVal(st)) continue;

                // Go from the starting six
                int ix = st;
                String startsixend = name(ix);
                sb.append(startsixend);
                ++segs;
                String sixend = startsixend;
                for (int i = 0; i < N; ++i) {
                    if (usedSix[six(ix)]) {
                        System.err.println("Bad used six-end "+ix);
                    }
                    usedSix[six(ix)] = true;
                    int plainvar = callvaltest(ix, 0);
                    boolean havePlain = !eval(plainvar);
                    int bobvar = callvaltest(ix, 1);
                    boolean haveBob = !eval(bobvar);
                    if (havePlain == haveBob) {
                        System.err.println("Bad call "+ix+" "+plainvar+" "+havePlain+" "+bobvar+" "+haveBob);
                    }
                    boolean call = haveBob;
                    char cv = call ? '-' : 'P';
                    if (cqs > 0) {
                        // Show where the Q-sets are
                        if (hasQset(ix)) {
                            cv = call ? '~' : 'r';
                            ++qsetsixes;
                        } else {
                            cv = call ? '=' : 'p';
                        }
                    }
                    if (!call) ++plain;
                    sb.append(cv);
                    sixend = call(sixend, call);
                    ix = next[ix][call ? 1 : 0];
                    // Got back to the start, forming a loop?
                    if (ix == st) break;
                    if (!matchSixVal(ix)) {
                        System.err.println("Bad type "+ix);
                    };
                }
                // See how many times this part repeats.
                int n = 0;
                String partend = startsixend;
                do {
                    partend = rep(sixend, partend, startsixend);
                    ++n;
                } while (!partend.equals(startsixend));
                int nb = groupsize / n;
                sb.append("*" + n + "(" + nb + ")");
                rbs += nb;
                
                if (rt == 0) {
                    // Record first segment so we can put it last
                    segstart = sb.toString();
                    sb.setLength(0);
                    // and find the other parts in order
                    break;
                }
            }
        }
        // Put the length limited segment last
        sb.append(segstart);
        sb.append(" ");
        sb.append(plain);
        sb.append(' ');
        sb.append(segs);
        if (cqs > 0) {
            sb.append(' ');
            sb.append(qsetsixes/3);
        }
        sb.insert(0, (N*6)+"!"+rbs+" ");
        return sb;
    }
    /**
     * Encode a multi-part touch.
     * 2314567QSpp===*2(1)5432167QSp==p*1(2)
     * also allow
     * 2314567QSpp===p*2.3(1)
     * @param s
     * @return start (last block)
     */
    int encodeMultiTouch(String s) {
        print("c Initial blocks");
        Pattern p = Pattern.compile("([1-90E]+[QS][QS])([^*]+)\\*(\\d+(\\.\\d+)?).*");
        int start = 0;
        String[] segments = s.split("\\)");
        if (segments.length > 1) {
            print("c "+s);
        }
        for (String s2 : segments) {
            Matcher m = p.matcher(s2);
            if (m.matches()) {
                s2 = m.group(1);
                String pt = m.group(2);
                float fct = Float.parseFloat(m.group(3));
                s2 = expandTouch(s2, pt, fct);
                print("c Block before and after expansion");
                print("c "+m.group(1)+m.group(2)+"*"+m.group(3));
            }
            start = encodeTouch(s2, group);
        }
        sixstart = start;
        return start;
    }
    private String expandTouch(String s2, String pt, float fct) {
        int ct = (int)fct;
        for (int i = 1; i <= ct; ++i) {
            s2 += pt;
        }
        // Remainder
        s2 += pt.substring(0, (int)(pt.length() * (fct - ct)));
        return s2;
    }
    
    /**
     * Encode a single touch
     * @param s
     * @return start
     */
    int encodeTouch(String s, List<String>grp) {
        print("c "+s);
        String s2 = expandMofN(s);
        if (!s2.equals(s)) {
            s = s2;
            print("c "+s);
        }
        // find the starting six
        String px = s.replaceFirst("[^1-90EQS].*", "");
        int ix = 1;
        if (px.length() >= 9) {
            ix = findName(px, grp);
            if (ix <= NN) {
                s = s.substring(px.length());
            }
        }
        encodeTouch(ix, s);
        return six(ix);
    }
    /**
     * Expand a [n/m] to combinations of p,=
     * @param s
     * @return
     */
    String expandMofN(String s) {
        Pattern p = Pattern.compile("([^\\[]*)\\[((?:0x|0X|#)\\p{XDigit}+|\\d+)/((?:0x|0X|#)\\p{XDigit}+|\\d+)]"/*\\]*/+"(.*)");
        Matcher m = p.matcher(s);
        if (m.matches()) {
            long v1 = Long.decode(m.group(2));
            long v2 = Long.decode(m.group(3));
            StringBuilder sb = new StringBuilder(m.group(1));
            for (long i = (1L<<log2(v2))/2; i != 0; i >>=1) {
                if ((v1 & i) != 0) {
                    sb.append('=');
                } else {
                    sb.append('p');
                }
            }
            sb.append(expandMofN(m.group(4)));
            return sb.toString();
        } else {
            return s;
        }
    }

    void invalidate(int ix, int call)
    {
        invalid[ix][call] = true;
    }

    /**
     * Encode touch given starting six number and calls.
     * @param st starting six number
     * @param s
     */
    void encodeTouch(int st, String s) {
        int ix = st;
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            int call = 0;
            boolean savecall = true;
            boolean savesix = true;
            boolean sametype = false;
            switch (c) {
                case 'p':
                case 'P':
                    // Force six type and plain
                    break;
                case '=':
                case '-':
                    // Force six type and bob
                    call = 1;
                    break;
                case 'r':
                    // Force six type, ring plain, but allow Q-set type call
                    savecall = false;
                    break;
                case '~':
                    // Force six type, ring bob, but allow Q-set type call
                    call = 1;
                    savecall = false;
                    break;
                case 'R':
                    // Ring and force plain for this six, allow any six-type
                    savesix = false;
                    break;
                case '_':
                    // Ring and force bob for this six, allow any six-type
                    call = 1;
                    savesix = false;
                    break;
                case 'L':
                    // Advance to next six with plain, allow anything
                    savesix = false;
                    savecall = false;
                    break;
                case '.':
                    // Advance to next six with bob, allow anything
                    call = 1;
                    savesix = false;
                    savecall = false;
                    break;
                case '#':
                    // Advance to next six with a bob,
                    // this six can be any type, but the next by a bob must be the same
                    // Used to force B-blocks
                    call = 1;
                    sametype = true;
                    savesix = false;
                    savecall = false;
                    break;
                default:
                    throw new IllegalArgumentException(""+c+"\n"+s);
            }
            //print ("c "+name(ix)+c+" set six type="+savesix+" set call="+savecall+" call="+call);
            if (savesix) {
                for (int v : genVal(ix)) {
                    if (v != 0)
                        print(v);
                }
                for (int s1 = firstsixend(ix); s1 <= NN && six(s1) == six(ix); ++s1) {
                    if (s1 == ix)
                        continue;
                    invalidate(s1, call);
                }
            }
            if (sametype) {
                // Walk through all six-ends
                for (int s1 = firstsixend(ix); s1 <= NN && six(s1) == six(ix); ++s1) {
                    int d1 = next[s1][call];
                    int gs1[] = genGate(s1, 0);
                    int gd1[] = genVal(d1);
                    print("c same type from/to "+s1+"<->"+d1);
                    if (s1 == d1) {
                        print("c omit self-loop");
                        continue;
                    }
                    // Forward
                    for (int d : gd1) {
                        if (d != 0) {
                            print(gs1, d);
                        }
                    }
                    // Reverse
                    int gs2[] = genVal(s1);
                    int gd2[] = genGate(d1, 0);
                    for (int d : gs2) {
                        if (d != 0) {
                            print(gd2, d);
                        }
                    }
                }
            }
            if (savecall) {
                if (savesix || sel != COMBINEDCALLTYPE) {
                    print(callvalset(ix, call));
                } else {
                    print("c " + c);
                    // Appears to be wrong way round but is not
                    if (call != 0) {
                        print(convNotBob(Collections.singleton(firstsixend(ix))));
                    } else {
                        print(convNotPlain(Collections.singleton(firstsixend(ix))));
                    }
                }
                for (int s1 = firstsixend(ix); s1 <= NN && six(s1) == six(ix); ++s1) {
                    invalidate(s1, 1 - call);
                }
            }
            ix = next[ix][call];
            if (ix == st && i < s.length() - 1) {
                // check for repeated cycle
                if (repeated(s, i + 1)) {
                    print("c repeated as "+s.substring(i + 1));
                    break;
                }
            }
        }
        //print("c done "+name[ix]);
    }
    
    /**
     * Copy value of one variable to another.
     * @param gate when to apply this rule
     * @param srcA source six-end
     * @param tapA LFSR tap (1-based)
     * @param destD dest six-end
     * @param tapD LFSR tap (1-based)
     */
    void COPY(int gate[], int srcA, int tapA, int destD, int tapD) {
        int varA = countvar(srcA, tapA - 1);
        int varD = countvar(destD, tapD - 1);
        print("c EQV "+srcA+":"+tapA+" = "+destD+":"+tapD);
        print(gate, varA, -varD);
        print(gate, -varA, varD);
    }
    
    /**
     * Copy value from one variable to another pair.
     * Destinations must agree.
     * destD1:tapD and destD2:tapD must agree for rule to apply
     * @param gate when to apply this rule
     * @param srcA source six-end
     * @param tapA LFSR tap
     * @param destD1 dest six-end
     * @param destD2 dest six-end
     * @param tapD dest tap
     */
    void COPY_COMMON_DEST(int gate[], int srcA, int tapA, int destD1, int destD2, int tapD) {
        int varA = countvar(srcA, tapA - 1);
        int varD1 = countvar(destD1, tapD - 1);
        int varD2 = countvar(destD2, tapD - 1);
        print("c EQV "+srcA+":"+tapA+" = "+destD1+":"+tapD+" or "+destD2+":"+tapD+"");
        print(gate, varA, -varD1, -varD2);
        print(gate, -varA, varD1, varD2);
    }
    
    /**
     * Copy value from one variable to another.
     * both srcA1:tapA and srcA2:tapA must have the same value for the copy to occur
     * @param gate when to apply
     * @param srcA1 source six-end
       @param srcA2 source six-end
       @param tapA LFSR tap (1-based)
     * @param destD dest six-end
     * @param tapD LFSR tap (1-based)
     */
    void COPY_COMMON_SRC(int gate[], int srcA1, int srcA2, int tapA, int destD, int tapD) {
        int varA1 = countvar(srcA1, tapA - 1);
        int varA2 = countvar(srcA2, tapA - 1);
        int varD = countvar(destD, tapD - 1);
        print("c EQV "+srcA1+":"+tapA+" or "+srcA2+":"+tapA+" = "+destD+":"+tapD);
        print(gate, varA1, varA2, -varD);
        print(gate, -varA1, -varA2, varD);
    }
    
    /**
     * Invert a variable.
     * @param gate
     * @param srcA source six-end
     * @param tapA LFSR tap (1-based)
     * @param destD destination six-end
     * @param tapD LFSR tap (1-based)
     */
    void INVERT(int gate[], int srcA, int tapA, int destD, int tapD) {
        int varA = countvar(srcA, tapA - 1);
        int varD = countvar(destD, tapD - 1);
        print("c NOT "+srcA+":"+tapA+" = "+destD+":"+tapD);
        print(gate, -varA, -varD);
        print(gate, varA, varD);
    }
    
    /**
     * Invert a variable when both destinations agree.
     * both destD1:tapD and destD2:tapD must have the same value for the copy to occur
     * @param gate
     * @param srcA source six-end
     * @param tapA LFSR tap (1-based)
     * @param destD1 destination six-end
     * @param destD2 destination six-end
     * @param tapD LFSR tap (1-based)
     */
    void INVERT_COMMON_DEST(int gate[], int srcA, int tapA, int destD1, int destD2, int tapD) {
        int varA = countvar(srcA, tapA - 1);
        int varD1 = countvar(destD1, tapD - 1);
        int varD2 = countvar(destD2, tapD - 1);
        print("c NOT "+srcA+":"+tapA+" = "+destD1+":"+tapD+" or "+destD2+":"+tapD);
        print(gate, -varA, -varD1, -varD2);
        print(gate, varA, varD1, varD2);
    }
    
    /**
     * Invert a variable when both sources agree
     * @param gate
     * @param srcA1 source six-end
     * @param srcA2 source six-end
     * @param tapA LFSR tap (1-based)
     * @param destD destination six-end
     * @param tapD LFSR tap (1-based)
     */
    void INVERT_COMMON_SRC(int gate[], int srcA1, int srcA2, int tapA, int destD, int tapD) {
        int varA1 = countvar(srcA1, tapA - 1);
        int varA2 = countvar(srcA2, tapA - 1);
        int varD = countvar(destD, tapD - 1);
        print("c (NOT "+srcA1+":"+tapA+") or (NOT "+srcA2+":"+tapA+") = "+destD+":"+tapD);
        print(gate, -varA1, -varA2, -varD);
        print(gate, varA1, varA2, varD);
    }
    
    /**
     * XOR two variables giving a third when gate is active.
     * @param gate
     * @param srcA source six-end
     * @param tapA LFSR tap (1-based)
     * @param srcB source six-end
     * @param tapB LFSR tap (1-based)
     * @param destD destination six-end
     * @param tapD LFSR tap (1-based)
     */
    void XOR(int gate[], int srcA, int tapA, int srcB, int tapB, int destD, int tapD) {
        int varA = countvar(srcA, tapA - 1);
        int varB = countvar(srcB, tapB - 1);
        int varD = countvar(destD, tapD - 1);
        print("c "+srcA+":"+tapA+" XOR "+srcB+":"+tapB+" = "+destD+":"+tapD);
        print(gate, varA, varB, -varD);
        print(gate, -varA, -varB, -varD);
        
        print(gate, varA, -varB, varD);
        print(gate, -varA, varB, varD);
    }
    
    /**
     * XOR two variables giving two destinations
     * which agree when gate is active.
     * both destD1:tapD and destD2:tapD must have the same value for the operation to occur
     * @param gate
     * @param srcA source six-end
     * @param tapA LFSR tap (1-based)
     * @param srcB source six-end
     * @param tapB LFSR tap (1-based)
     * @param varD1 destination six-end
     * @param varD2 destination six-end
     * @param tapD LFSR tap (1-based)
     */
    void XOR(int gate[], int srcA, int tapA, int srcB, int tapB, int destD1, int destD2, int tapD) {
        int varA = countvar(srcA, tapA - 1);
        int varB = countvar(srcB, tapB - 1);
        int varD1 = countvar(destD1, tapD - 1);
        int varD2 = countvar(destD2, tapD - 1);
        print("c "+srcA+":"+tapA+" XOR "+srcB+":"+tapB+" = "+destD1+":"+tapD+" or "+destD2+":"+tapD);
        print(gate, varA, varB, -varD1, -varD2);
        print(gate, -varA, -varB, -varD1, -varD2);
        
        print(gate, varA, -varB, varD1, varD2);
        print(gate, -varA, varB, varD1, varD2);
    }
    
    /**
     * XOR two pairs of variables, setting the destination when the
     * sources agree.
     * srcAB1:tapA XOR srcAB1:tapB must agree with
     * srcAB2:tapA XOR srcAB1:tapB for the operation to occur.
     * @param gate
     * @param gate
     * @param srcAB1 source six-end
     * @param srcAB2 source six-end
     * @param tapA LFSR tap (1-based)
     * @param tapB LFSR tap (1-based)
     * @param destD destination six-end
     * @param tapD LFSR tap (1-based)
     */
    void XOR_COMMON_SRC(int gate[], int srcAB1, int srcAB2, int tapA, int tapB, int destD, int tapD) {
        int varA1 = countvar(srcAB1, tapA - 1);
        int varA2 = countvar(srcAB2, tapA - 1);
        int varB1 = countvar(srcAB1, tapB - 1);
        int varB2 = countvar(srcAB2, tapB - 1);
        int varD = countvar(destD, tapD - 1);
        print("c ("+srcAB1+":"+tapA+" XOR "+srcAB1+":"+tapB+") or "+
                "("+srcAB2+":"+tapA+" XOR "+srcAB2+":"+tapB+") = "+destD+":"+tapD);
        print(gate, varA1, varB1, varA2, varB2, -varD);
        print(gate, varA1, varB1, -varA2, -varB2, -varD);
        print(gate, -varA1, -varB1, varA2, varB2, -varD);
        print(gate, -varA1, -varB1, -varA2, -varB2, -varD);

        print(gate, varA1, -varB1, varA2, -varB2, varD);
        print(gate, varA1, -varB1, -varA2, varB2, varD);
        print(gate, -varA1, varB1, varA2, -varB2, varD);
        print(gate, -varA1, varB1, -varA2, varB2, varD);
    }
    
    /**
     * Non-linear FSR calculation.
     * srcA:tapA XOR (srcB:tapB OR srcC:tapC)
     * @param gate
     * @param srcA source six-end
     * @param tapA LFSR tap (1-based)
     * @param srcB source six-end
     * @param tapB LFSR tap (1-based)
     * @param srcC
     * @param tapC LFSR tap (1-based)
     * @param destD destination six-end
     * @param tapD LFSR tap (1-based)
     */
    void XOR_OR(int gate[], int srcA, int tapA, int srcB, int tapB, int srcC, int tapC, int destD, int tapD) {
        int varA = countvar(srcA, tapA - 1);
        int varB = countvar(srcB, tapB - 1);
        int varC = countvar(srcC, tapC - 1);
        int varD = countvar(destD, tapD - 1);
        print("c "+srcA+":"+tapA+" XOR ("+srcB+":"+tapB+" OR "+srcC+":"+tapC+") = "+destD+":"+tapD);
        print(gate, varA, varB, varC, -varD);
        print(gate, -varA, -varB, -varD);
        print(gate, -varA, -varC, -varD);
        
        print(gate, -varA, varB, varC, varD);
        print(gate, varA, -varB, varD);
        print(gate, varA, -varC, varD);
    }
    
    /**
     * Non-linear FSR calculation for common destination.
     * srcA:tapA XOR (srcB:tapB OR srcC:tapC)
     * destD1:tapD and destD2:tapD must agree for the operation to occur.
     * @param gate
     * @param srcA source six-end
     * @param tapA LFSR tap (1-based)
     * @param srcB source six-end
     * @param tapB LFSR tap (1-based)
     * @param srcC source six-end
     * @param tapC LFSR tap (1-based)
     * @param destD1 destination six-end
     * @param destD2 destination six-end
     * @param tapD LFSR tap (1-based)
     */
    void XOR_OR(int gate[], int srcA, int tapA, int srcB, int tapB, int srcC, int tapC, int destD1, int destD2, int tapD) {
        int varA = countvar(srcA, tapA - 1);
        int varB = countvar(srcB, tapB - 1);
        int varC = countvar(srcC, tapC - 1);
        int varD1 = countvar(destD1, tapD - 1);
        int varD2 = countvar(destD2, tapD - 1);
        print("c "+srcA+":"+tapA+" XOR ("+srcB+":"+tapB+" OR "+srcC+":"+tapC+") = "+destD1+":"+tapD+" or "+destD2+":"+tapD);
        print(gate, varA, varB, varC, -varD1, -varD2);
        print(gate, -varA, -varB, -varD1, -varD2);
        print(gate, -varA, -varC, -varD1, -varD2);
        
        print(gate, -varA, varB, varC, varD1, varD2);
        print(gate, varA, -varB, varD1, varD2);
        print(gate, varA, -varC, varD1, varD2);
    }
    
    /**
     * Non-linear FSR calculation for common source sets.
     * srcABC1:tapA XOR (srcABC1:tapB OR srcABC1:tapC)
     * srcABC2:tapA XOR (srcABC2:tapB OR srcABC2:tapC)
     * @param gate
     * @param srcABC1 source six-end
     * @param srcABC2 source six-end
     * @param tapA LFSR tap (1-based)
     * @param tapB LFSR tap (1-based)
     * @param tapC LFSR tap (1-based)
     * @param destD destination six-end
     * @param tapD LFSR tap (1-based)
     */
    void XOR_OR(int gate[], int srcABC1, int srcABC2, int tapA, int tapB, int tapC, int destD, int tapD) {
        int varA1 = countvar(srcABC1, tapA - 1);
        int varB1 = countvar(srcABC1, tapB - 1);
        int varC1 = countvar(srcABC1, tapC - 1);
        int varA2 = countvar(srcABC2, tapA - 1);
        int varB2 = countvar(srcABC2, tapB - 1);
        int varC2 = countvar(srcABC2, tapC - 1);
        int varD = countvar(destD, tapD - 1);
        print("c ("+srcABC1+":"+tapA+" XOR ("+srcABC1+":"+tapB+" OR "+srcABC1+":"+tapC+") or "+
                "("+srcABC2+":"+tapA+" XOR ("+srcABC2+":"+tapB+" OR "+srcABC2+":"+tapC+") = "+destD+":"+tapD);
        print(gate, varA1, varB1, varC1, varA2, varB2, varC2, -varD);
        print(gate, varA1, varB1, varC1, -varA2, -varB2, -varD);
        print(gate, varA1, varB1, varC1, -varA2, -varC2, -varD);
        print(gate, -varA1, -varB1, varA2, varB2, varC2, -varD);
        print(gate, -varA1, -varB1, -varA2, -varB2, -varD);
        print(gate, -varA1, -varB1, -varA2, -varC2, -varD);
        print(gate, -varA1, -varC1, varA2, varB2, varC2, -varD);
        print(gate, -varA1, -varC1, -varA2, -varB2, -varD);
        print(gate, -varA1, -varC1, -varA2, -varC2, -varD);

        print(gate, -varA1, varB1, varC1, -varA2, varB2, varC2, varD);
        print(gate, -varA1, varB1, varC1, varA2, -varB2, varD);
        print(gate, -varA1, varB1, varC1, varA2, -varC2, varD);
        print(gate, varA1, -varB1, -varA2, varB2, varC2, varD);
        print(gate, varA1, -varB1, varA2, -varB2, varD);
        print(gate, varA1, -varB1, varA2, -varC2, varD);
        print(gate, varA1, -varC1, -varA2, varB2, varC2, varD);
        print(gate, varA1, -varC1, varA2, -varB2, varD);
        print(gate, varA1, -varC1, varA2, -varC2, varD);
    }
    
    int clauses = 0;
    int literals = 0;
    boolean print;
    boolean eval;
    /**
     * Print out a clause or count the clauses.
     * Ignores variables which are 0.
     * Also evaluate the clause if the actual value of the variables is available,
     * and indicate if the clause is false.
     * @param args list of ints (converted to Integers) or array of int, each non-zero element printed
     */
    public void print(Object ... args) {
        Set<Integer>dups = new LinkedHashSet<Integer>();
        StringBuilder sb = new StringBuilder();
        boolean val = false;
        boolean redundant = false;
        for (Object o : args) {
            if (o instanceof Integer) {
                if ((Integer)o != 0) {
                    int o1 = (Integer)o;
                    sb.append(' ').append(o1);
                    if (!dups.add(o1)) {
                        print("c duplicate literal "+o1);
                    } else if (dups.contains(-o1)) {
                        print("c redundant clause with opposite literals "+o1+" "+(-o1));
                        redundant = true;
                    }
                    val |= eval(o1);
                }
            } else if (o instanceof int[]) {
                for (int o2 : (int[])o) {
                    if (o2 != 0) {
                        sb.append(' ').append(o2);
                        if (!dups.add(o2)) {
                            print("c duplicate literal "+o2);
                        } else if (dups.contains(-o2)) {
                            print("c redundant clause with opposite literals "+o2+" "+(-o2));
                            redundant = true;
                        }
                        val |= eval(o2);
                    }
                }
            } else {
                throw new IllegalArgumentException("unexpected argument "+o.getClass()+" "+o);
            }
        }
        sb.append(' ').append(0);
        if (print) {
            sysout.println(sb.substring(1));
        } else {
            ++clauses;
            for (int i = 1; i < sb.length(); ++i) {
                if (sb.charAt(i) == ' ')
                    ++literals;
            }
        }
        if (eval) {
            if (!val) {
                sysout.println("c eval false "+sb.substring(1));
            }
        }
    }
    /**
     * Evaluate a literal
     * @param svar variable number, negative to negate result of evaluation
     * @return
     */
    private boolean eval(int svar)
    {
        boolean val;
        if (svar < 0)
            val = !vals[-svar];
        else
            val = vals[svar];
        return val;
    }
    public void print(String s) {
        if (print)
            sysout.println(s);
    }
    
    public void close() throws IOException {
        if (sysout != System.out) {
            sysout.flush();
            sysout.close();
            if (sysout.checkError()) {
                throw new IOException("Error writing output");
            };
        }
    }
    /**
     * Actually generate the SAT code.
     */
    public void gen() {
        long t1 = System.currentTimeMillis();
        print("c Stedman/Erin to SAT conversion by Andrew Johnson");
        print(toString());
        print("c Generators "+gens+" group order "+group.size());
        int count = sted ? Nlen / 2 : Nlen - 1;
        // The longest possible short cycle
        int maxshortcycle = sted ? N / 2 - count : N - 1 - count;
        // Allow 217/31/7/1 counter if we haven't disabled hybrid/non-linear; will require disable of other cycles if not a full cycle N==Nlen
        boolean allowShort = (!opt19 && !opt20);
        boolean useTwisted = Nlen == N && !opt21;
        int[] taps = lfsrTaps(bb, count, useTwisted, allowShort, opt19, opt20);
        boolean lfsrFirst[] = initLFSR(taps);
        boolean lfsrLast[] = initLFSR(taps);
        boolean lfsrBeyondLast[] = initLFSR(taps);
        boolean lfsrBeforeFirst[] = initLFSR(taps);
        // Run LFSR N - 2 times to find last state
        // E.g. N = 6 step with 1,2,3,4
        // Stedman N = 6 step 1,3,
        // Quick six start: 0Q 1S 2Q 2S 3Q 3S
        // Slow six start:  0S 1Q 1S 2Q 2S 3Q
        // Erin 14
        // 0S 1S 2S 3S 4S 5S
        if (bb > 0 && bb > taps[0]) {
            print("c LFSR taps "+Arrays.toString(taps)+" twisted ring ["+bb+", "+(taps[0]+1)+"]");
        } else {
            print("c LFSR taps "+Arrays.toString(taps));
        }
        print("c 1 "+Arrays.toString(lfsrLast));
        for (int i = 2; i <= count; i += 1) {
            nextStateLFSR(lfsrLast, taps);
            print("c "+i+" "+Arrays.toString(lfsrLast));
        }
        for (int i = 2; i <= count + 1; i += 1) {
            nextStateLFSR(lfsrBeyondLast, taps);
        }
        boolean disallowZero = opt1 && N == Nlen;
        boolean shortCycle = shortCycle();
        /*
         * If a short cycle is possible then we must block a loop in LFSR values,
         * either with beyondLast, or beforeFirst.
         */
        boolean testBeyondLast = (opt2 || (shortCycle && !opt3)) && !Arrays.equals(lfsrFirst, lfsrBeyondLast);
        boolean tmp[] = initLFSR(taps);
        int max = 0;
        for (int i = 1; i <= (1 << bb) - 1; i += 1) {
            System.arraycopy(tmp, 0, lfsrBeforeFirst, 0, bb);
            nextStateLFSR(tmp, taps);
            if (Arrays.equals(tmp, lfsrFirst)) {
                //System.err.println("Found start after "+i+Arrays.toString(lfsrBeforeFirst));
                max = i;
                break;
            }
        }
        boolean lfsrInvalids[][] = findInvalids(count, maxshortcycle, taps, opt23, !disallowZero);
        boolean testBeforeFirst = opt3
                && !Arrays.equals(lfsrLast, lfsrBeforeFirst)
                && !(testBeyondLast
                && Arrays.equals(lfsrBeyondLast, lfsrBeforeFirst));
        int fdistances[] = buildDistance(sixstart * sixTypes + 1, next, false);
        int bdistances[] = buildDistance(sixstart * sixTypes + 1, prev, true);
        print("c disallowZero "+disallowZero);
        print("c beyondLast "+testBeyondLast+" "+(count + 1)+" "+Arrays.toString(lfsrBeyondLast));
        print("c beforeFirst "+testBeforeFirst+" "+max+" "+Arrays.toString(lfsrBeforeFirst));
        // Also remove redundant LFSR state comparisons
        print("c exclude invalids "+Arrays.deepToString(lfsrInvalids));
        for (int i : listInvalids(lfsrInvalids)) {
            stateFromNum(i, tmp);
            print("c excluded "+Arrays.toString(tmp));
            if (disallowZero && Arrays.equals(tmp, new boolean[bb])) {
                disallowZero = false;
                print("c disallowZero covered by excluded invalids");
            }
            if (testBeyondLast && Arrays.equals(tmp, lfsrBeyondLast)) {
                testBeyondLast = false;
                print("c beyondLast covered by excluded invalids");
            }
            if (testBeforeFirst && Arrays.equals(tmp, lfsrBeforeFirst)) {
                testBeforeFirst = false;
                print("c beforeFirst covered by excluded invalids");
            }
        }
        print("c start of call variables: "+callvar(1)+" "+ccv);
        print("c start of six type variables: "+sixvar(1,0)+" "+(bitsSix * N / divg));
        print("c start of sequence number variables: "+countvar(six(1) == sixstart ? 1+sixTypes : 1, 0)+" "+((N - 1) * bb));
        print("c start of calls in threes counting: "+firstTVVar+ " "+tv);
        print("c start of required call sequence variables "+firstCBRun+ " "+cbrun);
        print("c start of bob counting: "+firstCBVar+ " "+cbv);
        print("c start of Q-set counting: "+firstCQSVar+ " "+cqs);
        print("c starting six: "+sixstart+" "+name(sixstart * sixTypes + 1));
        int vars = nextFreeVar - 1;
        print("p cnf "+vars+" "+clauses);
        print("c literals "+literals);

        // start is lfsr=1
        // 14 sixes numbered as follows - start six has no number (0)
        // Stedman
        // 0Q 1S 2Q 2S 3Q 3S 4Q 4S 5Q 5S 6Q 6S 7Q 7S
        // or
        // 0S 1Q 1S 2Q 2S 3Q 3S 4Q 4S 5Q 5S 6Q 6S 7Q
        // Erin
        // 0S 1S 2S 3S 4S 5S 6S 7S 8S 9S 10S 11S 12S 13S
        for (int src = 1; src <= NN; ++src) {
            print("c "+name(src)+" "+src);
            // Six type, Q/S, space for call type
            for (int call = 0; call < 2; ++call) {
                int callval = callvaltest(src, call);
                int[] gate = genGate(src, callval);
                int dest = next[src][call];
                print("c "+src+"->"+dest+" with "+callName(call));
                // Disallow loop back to self
                // as this could break some of the start/end conditions
                if ((N == Nlen || sted || Nlen > 1 && six(src) == sixstart || src != dest) && six(src) == six(dest)) {
                    // Disallow back to same six
                    print("c disallow loop to same six");
                    print(gate);
                } else if (Nlen == 1 && six(src) == sixstart && dest != src) {
                    // If fixed length of 1 and this is the starting six, must point back to this
                    print("c force loop to start for length 1");
                    print(gate);
                } else if (dest == src) {
                    print("c destination is same as source");
                } else {
                    // Type + Q/S for destination six
                    int dtype[] = genVal(dest);
                    for (int dv : dtype) {
                        // 3
                        if (dv != 0) {
                            print(gate, dv);
                        }
                    }
                }
                if (bb == 0) {
                    // No slot numbering
                } else if (six(src) == six(dest)) {
                    if (src != dest) {
                        // Disallow back to same six
                        print("c already disallowed loop to same different six-end in same six, so no slot numbering");
                    } else if (six(src) != sixstart) {
                        // count variables all zero special case - if omitted could be any value
                        print("c self-loop, force LFSR value to zero");
                        for (int b = 0; b < bb; ++b) {
                            print(gate, -countvar(src, b));
                        }
                    }
                } else if (six(src) == sixstart) {
                    // From start
                    print("c from start");
                    for (int b = 0; b < bb; ++b) {
                        int sgn = lfsrFirst[b] ? 1 : -1;
                        // bb
                        print(gate, sgn * countvar(dest, b));
                    }
                } else if (six(dest) == sixstart) {
                    // Back to start
                    // bb
                    print("c to start");
                    for (int b = 0; b < bb; ++b) {
                        int sgn = lfsrLast[b] ? 1 : -1;
                        print(gate, sgn * countvar(src, b));
                    }
                } else if (quick(src)) {
                    // Same ID for successor to quick six
                    // 2*bb
                    print("c quick to slow six; same slot number");
                    for (int b = 0; b < bb; ++b) {
                        COPY(gate, src, b + 1, dest, b + 1);
                    }
                } else {
                    // LFSR sequence
                    // 2*bb+2*taps.length
                    // Next ID
                    print("c next slot number");
                    int c = taps.length - 1;
                    if (taps[c] < 0) {
                        if (taps[c - 1] > 0) {
                            // Complex
                            print("c Hybrid LFSR");
                            XOR(gate, src, taps[0], src, -taps[c], dest, 1);
                            if (opt22) {
                                // XOR experiment as dest -taps[c] + 1 == src -taps[c]
                                print("c redundant calculation");
                                XOR(gate, src, taps[0], dest, -taps[c] + 1, dest, 1);
                            }
                            --c;
                        } else {
                            // NLFSR opcode 00011110
                            //Positive
                            //lfsr[0] = (!A&B | !A&C | A&!B&!C);
                            // Negative
                            //lfsr[0] = !(A&B | A&C | !A&!B&!C);
                            // XOR
                            //lfsr[0] = A^(B|C);
                            //000  A  B  C -D
                            //11x -A -B    -D
                            //1x1 -A    -C -D
                            //100 -A  B  C  D
                            //01x  A -B     D
                            //0x1  A    -C  D
                            print("c Non-linear FSR");
                            XOR_OR(gate, src, taps[0], src, -taps[c-1], src, -taps[c], dest, 1);
                            c -= 2;
                        }
                    } else {
                        // Wrap
                        COPY(gate, src, taps[0], dest, 1);
                    }
                    // Shifted bits
                    for (int b = 1; b < bb; ++b) {
                        if (c == 0 && b == taps[c]) {
                            // Twisted-ring for remaining bits
                            print("c twisted ring");
                            // Invert
                            INVERT(gate, src, bb, dest, b + 1);
                        } else if (c > 0 && b == taps[c]) {
                            // XOR
                            XOR(gate, src, b, src, taps[0], dest, b + 1);
                            if (opt22 && taps[taps.length - 1] >= 0) {
                                // XOR experiment as dest 1 == src taps[0]
                                print("c redundant calculation");
                                XOR(gate, src, b, dest, 1, dest, b + 1);
                            }
                            --c;
                        } else {
                            // Shift
                            COPY(gate, src, b, dest, b + 1);
                        }
                    }
                }
                // At most one
                // Disallow 2 inbounds to same six-end = dest
                // Required when multiple loops are possible
                // Disallow 2 inbounds to same six of dest
                // Advance from this inbound to end
                // Perf: 5%
                if (!opt4 || Nlen < N || !opt5) {
                    //for (int src2 = src; src2 <= NN; ++src2) {
                    for (int d1 = firstsixend(dest); d1 <= NN && six(d1) == six(dest); ++d1) { // Performance optimization rather than walking all sixes
                        for (int call2 = 0; call2 < 2; ++call2) {
                            int src2 = prev[d1][call2]; // Performance optimization
                            // Only need to do pair of inbounds once
                            if (src2 < src) continue;
                            if (src2 == src && call2 <= call) continue;
                            if (six(next[src2][call2]) == six(dest)) {
                                // Call
                                if (six(src) == six(src2)) {
                                    print("c omitted as two inbounds from same six: "+src+"=>"+dest+","+src2+"=>"+next[src2][call2]);
                                } else if (next[src2][call2] == dest) {
                                    // Same six-end
                                    if (!opt4 || Nlen < N) {
                                        int callval2 = callvaltest(src2, call2);
                                        int[] gate2 = genGate(src2, callval2);
                                        print("c disallow two inbounds to same six-end: "+src+","+src2+"=>"+dest);
                                        if (callval == -callval2) {
                                            print("c skip as call variables are opposite: "+callval+" "+callval2);
                                        } else {
                                            print(gate, gate2);
                                        }
                                    }
                                } else {
                                    // Different six end
                                    if (!opt5) {
                                        int callval2 = callvaltest(src2, call2);
                                        int[] gate2 = genGate(src2, callval2);
                                        print("c disallow two inbounds to same six: "+src+"=>"+dest+","+src2+"=>"+next[src2][call2]);
                                        if (callval == -callval2) {
                                            print("c skip as call variables are opposite: "+callval+" "+callval2);
                                        } else {
                                            print(gate, gate2);// @FIXME duplicate literals from sted1_3, gate=[-458,7], gate2=[-462,7], src=74, src2=84;src=289,src2=296;next[289]->396,408, next[296]=391,403,call2=0
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } /* end of call loop */
            
            /*
             * 
             */
            if (newcall && src == this.firstsixend(src)) {
                // -1 -2 0
                // 1 2 0
                print("c exclusive calls "+src);
                int c1 = this.callvaltest(src, 0);
                int c2 = this.callvaltest(src, 1);
                print(c1, c2);
                int c3 = this.callvalset(src, 1);
                int c4 = this.callvalset(src, 0);
                print(c3, c4);
            }
            
            // Redundant constraints below here - for improved performance
            
            // Consider src as a six
            // Given this six-end
            // Disallow other inputs which go to a different six-end
            // Also build a list of inbounds to this exact six-end
            int[] gate = genGate(src, 0);
            int [][]gates = new int[2][];
            int ip[] = new int[2];
            int s = 0;
            int [][]allinbounds = new int[sixTypes * 2][];
            int si = 0;
            int [][]allinbounds2 = new int[sixTypes * 2][];
            int si2 = 0;
            int sources[] = new int[sixTypes * 2];
            int si3 = 0;
            boolean selfloop = false;
            //for (int src2 = 1; src2 <= NN; ++src2) {
            for (int d1 = firstsixend(src); d1 <= NN && six(d1) == six(src); ++d1) { // Performance optimization rather than walking all sixes
                for (int call2 = 0; call2 < 2; ++call2) {
                    int src2 = prev[d1][call2]; // Performance optimization
                    if (six(next[src2][call2]) == six(src)) {
                        int callval2 = callvaltest(src2, call2);
                        int[] gate2 = genGate(src2, callval2);
                        // See if to exact six-end
                        if (next[src2][call2] == src) {
                            if (six(src) == six(src2) && src != src2) {
                                // omit
                                print("c "+src+"<omitted as other input is in the same six>"+src2+"=>"+next[src2][call2]);
                            } else {
                                ip[s] = src2;
                                gates[s++] = gate2;
                                selfloop |= src == src2;
                            }
                        } else if (!opt6) {
                            if (six(src) == six(src2)) {
                                print("c "+src+"<omitted as other input is in the same six>"+src2+"=>"+next[src2][call2]);
                            } else {
                                print("c "+src+"<bad input to other six-type when this six-type>"+src2+"=>"+next[src2][call2]);
                                if (sixvar(src, 0) == sixvar(src2, 0)) {
                                    print("c source six-type vars aliased to destination"); // @FIXME? src=1, src2=58 call2=1, next[src2][call2] = 3,gate=-841, gate2=-844,-10; src2=55 call2=1, next[src2][call2] = 4,gate=-841, gate2=-841,-10
                                    print(gate2);
                                    // Avoids this below
                                    // dup 1_10 -841 -841 -10 0
                                } else {
                                    print(gate, gate2);
                                }
                            }
                        }
                        // If the source is the same six but not a self-loop we can skip
                        if (six(src) != six(src2) || src2 == next[src2][call2]) {
                            // Add call var? - but could be big expansion
                            if (!sted) {
                                allinbounds[si++] = genGate(src2, callval2); // huge expansion of 4096 for Stedman
                            } else {
                                allinbounds[si++] = genGate(src2, 0); // 65 seconds
                                // And the call separately - sometimes common bobs between quick and slow sixes
                                allinbounds2[si2++] = new int[]{callval2}; // 44 seconds
                            }
                            sources[si3++] = src2;
                        }
                    }
                }
            }

            // Force at least one inbound when this six-end
            // Important for performance
            if (!opt7) {
                // Ensure at least one input
                // If no inputs to six-end, disallow state
                if (s < 1) throw new IllegalStateException("Not enough inbounds to "+src+" "+name(src));
                if (selfloop) {
                    print("c "+src+"<self-loop so always at least one input to six-end>"+ip[0]+(s >= 2 ? " "+ip[1] : ""));
                } else {
                    print("c "+src+"<at least one input to six-end>"+ip[0]+(s >= 2 ? " "+ip[1] : ""));
                    chooseOneEach(gates, s, gate);
                }
            }
            
            /*
             * Ensure that at least one six-end elsewhere could be a source for this six
             */
            if (opt16 && src == firstsixend(src)) {
                if (si > 0) {
                    print("c "+src+"<at least one input to six> "+Arrays.toString(sources));
                    chooseOneEach(allinbounds, si);
                }
                if (si2 > 0) {
                    print("c "+src+"<at least one call suitable for input to six> "+Arrays.toString(sources));
                    //print("c "+Arrays.deepToString(allinbounds2));
                    chooseOneEach(allinbounds2, si2);
                }
            }
            
            // If all of the outputs of this six with a bob are invalid, the call must be a plain and v.v.
            // Only do this once per six
            // Also build list of all inputs to the six
            if ((opt13 || opt17) && src == firstsixend(src)) {
                // Determine call
                int g[][] = new int[sixTypes][];
                int gall[][] = new int[sixTypes * 2][];
                int i2 = 0;
                for (int call2 = 0; call2 < 2; ++call2) {
                    int i1 = 0;
                    int callvars[] = new int[sixTypes];
                    int i3 = 0;
                    for (int d1 = firstsixend(src); d1 <= NN && six(d1) == six(src); ++d1, ++i1) {
                        g[i1] = genGate(next[d1][call2], 0);
                        gall[i2++] = g[i1];
                        // Experimental - for combined six-type and call-type var
                        int callval2 = callvalset(d1, 1 - call2);
                        callvars[i3++] = callval2;
                    }
                    if (opt13) {
                        // Force opposite call
                        int callval2 = callvalset(src, 1 - call2);
                        print("c "+src+"<determine call as "+callName(1 - call2)+" if no outputs with "+callName(call2));
                        if (callvars[0] != callvars[1]) {
                            chooseOneEach(g, i1, callvars);
                        } else {
                            // callval2 must be the same for all six-ends in the six
                            chooseOneEach(g, i1, callval2);
                        }
                    }
                }
                if (opt17) {
                    print("c "+src+"<at least one output six-end available from six>");
                    chooseOneEach(gall, i2);
                }
            }

            /*
             * If all outputs from quick six-ends are blocked then the six-ends must be slow
             * and vice versa.
             * qd1p + qd2b + qd3p + qd4b + qd5p + qd6b + ss1 + ss2 + ss3
             * Also consider
             * plain + qd1 + qd3  + qd5 + ss1 + ss2 + ss3
             * bob + qd2 + qd4 + qd6 + ss1 + ss2 + ss3
             * Could this work for Erin?
             * p1 b1 p2 b2 p3 b3
             */
            if (sted && (opt14 || opt15) && (src == firstsixend(src) || src == firstsixend(src) + 1)) {
                int g[][] = new int[sixTypes*3/2][];
                for (int cl3 = opt14 ? -1 : 0; cl3 < (opt15 ? 2 : 0); ++cl3) {
                    int i1 = 0;
                    for (int d1 = firstsixend(src); d1 <= NN && six(d1) == six(src); ++d1) {
                        if (quick(d1) == quick(src)) {
                            for (int call2 = 0; call2 < 2; ++call2) {
                                int p = next[d1][call2];
                                if (cl3 < 0) {
                                    g[i1++] = genGate(p, 0);
                                } else if (cl3 == call2) {
                                    g[i1++] = genGate(p, 0);
                                }
                            }
                        } else {
                            // Opposite type of six-end in this six
                            g[i1++] = genGate(d1, 0);
                        }
                    }
                    if (cl3 < 0) {
                        print("c "+src+"<determine six as "+(!quick(src)?"quick":"slow")+" because no destinations from "+(quick(src)?"quick":"slow"));
                        chooseOneEach(g, i1);
                    } else {
                        print("c "+src+"<determine six as "+(!quick(src)?"quick":"slow")+" because no destinations with "+callName(cl3)+" from "+(quick(src)?"quick":"slow"));
                        int callval2 = callvaltest(src, cl3);
                        chooseOneEach(g, i1, callval2);
                    }
                }
            }
            
            /*
             * Common source slot number gives fixed slot number bits for this six.
             */
            if (bb > 0 && opt10 && six(ip[0]) != sixstart && (s < 2 || six(ip[1]) != sixstart) && six(src) != sixstart) {
                // Common source
                if (s < 1) throw new IllegalStateException("Not enough inbounds to "+src+" "+name(src));
                boolean sameSrcSix = s < 2 || six(ip[0]) == six(ip[1]);
                print("c "+src+"<common source slot number>"+ip[0]+(s >= 2 ? " "+ip[1] : ""));
                if (sameSrcSix) {
                    if (s == 1) {
                        print("c "+src+" has just one source six-end from a different six "+ip[0]);
                    } else {
                        print("c "+src+" has just one six as a source from six-ends "+ip[0]+" "+ip[1]);
                    }
                }
                if (six(src) == six(ip[0]) || (s >= 2 && six(src) == six(ip[1]))) {
                    // Same six for ip[0/1] and src already filtered out, except for self-loops
                    print("c omitted as "+src+" is the same six as a source"); // E.g. erin10
                } else if (quick(ip[0])) {
                    // If ip[1] exists must be quick too
                    print("c quick to slow six; same slot number");
                    for (int b = 0; b < bb; ++b) {
                        if (sameSrcSix) {
                            COPY(gate, ip[0], b + 1, src, b + 1);
                        } else {
                            COPY_COMMON_SRC(gate, ip[0], ip[1], b + 1, src, b + 1);
                        }
                    }
                } else {
                    print("c next slot number");
                    int c = taps.length - 1;
                    if (taps[c] < 0) {
                        if (taps[c-1] >= 0) {
                            // Complex
                            print("c hybrid LFSR");
                            if (sameSrcSix) {
                                XOR(gate, ip[0], taps[0], ip[0], -taps[c], src, 1);
                            } else {
                                XOR_COMMON_SRC(gate, ip[0], ip[1], taps[0], -taps[c], src, 1);
                            }
                            --c;
                        } else {
                            print("c non-linear FSR");
                            if (sameSrcSix) {
                                XOR_OR(gate, ip[0], taps[0], ip[0], -taps[c-1], ip[0], -taps[c], src, 1);
                            } else {
                                XOR_OR(gate, ip[0], ip[1], taps[0], -taps[c-1], -taps[c], src, 1);
                            }
                            c -= 2;
                        }
                    } else {
                        // Wrap
                        if (sameSrcSix) {
                            COPY(gate, ip[0], taps[0], src, 1);
                        } else {
                            COPY_COMMON_SRC(gate, ip[0], ip[1], taps[0], src, 1);
                        }
                    }
                    // Shifted bits
                    for (int b = 1; b < bb; ++b) {
                        if (c == 0 && b == taps[c]) {
                            // Twisted-ring for remaining bits
                            print("c twisted ring");
                            // Invert
                            if (sameSrcSix) {
                                INVERT(gate, ip[0], bb, src, b + 1);
                            } else {
                                INVERT_COMMON_SRC(gate, ip[0], ip[1], bb, src, b + 1);
                            }
                        } else if (c > 0 && b == taps[c]) {
                            // XOR
                            if (sameSrcSix) {
                                XOR(gate, ip[0], b, ip[0], taps[0], src, b + 1);
                            } else {
                                XOR_COMMON_SRC(gate, ip[0], ip[1], b, taps[0], src, b + 1);
                                if (opt22 && taps[taps.length - 1] >= 0) {
                                    // Alternative as countvar(src, 0) should match countvar(ip[0/1], taps[0]-1)
                                    // except for complex counting
                                    // Experimental for performance
                                    print("c redundant calculation");
                                    XOR(gate, src, 1, src, b + 1, ip[0], ip[1], b);
                                }
                            }
                            --c;
                        } else {
                            // Shift
                            if (sameSrcSix) {
                                COPY(gate, ip[0], b, src, b + 1);
                            } else {
                                COPY_COMMON_SRC(gate, ip[0], ip[1], b, src, b + 1);
                            }
                        }
                    }
                }
            }
            
            /*
             * Common destination slot number gives fixed slot number bits for this six.
             */
            if (bb > 0 && opt11 && six(next[src][0]) != sixstart && six(next[src][1]) != sixstart && six(src) != sixstart) {
                // Common dest
                boolean sameDestSix = six(next[src][0]) == six(next[src][1]);
                print("c "+src+"<common destination slot number>"+next[src][0]+" "+next[src][1]);
                if (sameDestSix) {
                    print("c "+src+" has the same two sixes as a destination");
                }
                if (six(src) == six(next[src][0]) || six(src) == six(next[src][1])) {
                    print("c omitted as "+src+" is the same six as a destination");
                } else if (quick(src)) {
                    print("c quick to slow six; same slot number");
                    for (int b = 0; b < bb; ++b) {
                        if (sameDestSix) {
                            COPY(gate, src, b + 1, next[src][0], b + 1);
                        } else {
                            COPY_COMMON_DEST(gate, src, b + 1, next[src][0], next[src][1], b + 1);
                        }
                    }
                } else {
                    print("c next slot number");
                    int c = taps.length - 1;
                    if (taps[c] < 0) {
                        if (taps[c - 1] >= 0) {
                            // complex
                            print("c hybrid LFSR");
                            if (sameDestSix) {
                                XOR(gate, src, taps[0], src, -taps[c], next[src][0], 1);
                            } else {
                                XOR(gate, src, taps[0], src, -taps[c], next[src][0], next[src][1], 1);
                            }
                            --c;
                        } else {
                            print("c non-linear FSR");
                            if (sameDestSix) {
                                XOR_OR(gate, src, taps[0], src, -taps[c-1], src, -taps[c], next[src][0], 1);
                            } else {
                                XOR_OR(gate, src, taps[0], src, -taps[c-1], src, -taps[c], next[src][0], next[src][1], 1);
                            }
                            c -= 2;
                        }
                    } else {
                        // Wrap
                        if (sameDestSix) {
                            COPY(gate, src, taps[0], next[src][0], 1);
                        } else {
                            COPY_COMMON_DEST(gate, src, taps[0], next[src][0], next[src][1], 1);
                        }
                    }
                    // Shifted bits
                    for (int b = 1; b < bb; ++b) {
                        if (c == 0 && b == taps[c]) {
                            // Twisted-ring for remaining bits
                            print("c twisted ring");
                            // Invert
                            if (sameDestSix) {
                                INVERT(gate, src, bb, next[src][0], b + 1);
                            } else {
                                INVERT_COMMON_DEST(gate, src, bb, next[src][0], next[src][1], b + 1);
                            }
                        } else if (c > 0 && b == taps[c]) {
                            // XOR
                            if (sameDestSix) {
                                XOR(gate, src, taps[0], src, b, next[src][0], b + 1);
                            } else {
                                XOR(gate, src, taps[0], src, b, next[src][0], next[src][1], b + 1);
                            }
                            --c;
                        } else {
                            // Shift
                            if (sameDestSix) {
                                COPY(gate, src, b, next[src][0], b + 1);
                            } else {
                                COPY_COMMON_DEST(gate, src, b, next[src][0], next[src][1], b + 1);
                            }
                        }
                    }
                }
            }
            
            /*
             * No outputs available from six-end implies disallowing this six-end.
             * Avoids having to try each call.
             */
            if (opt8) {
                // If no outputs from six-end, disallow state
                print("c "+src+"<at least one output from six-end>"+next[src][0]+" "+next[src][1]);
                int cv1;
                int cv2;
                if (false && true) {
                    // Not required as if call means one destination isn't used
                    // then the other would be with this six type, and the normal
                    // succession enforces this
                    cv1 = callvaltest(src, 0);
                    cv2 = callvaltest(src, 1);
                    // complicated predecessor as either prevtype or prevcall must be false
                    cv1 = callvaltest(prev[next[src][0]][1], 0);
                    print("c "+prev[next[src][0]][1]+"->"+next[prev[next[src][0]][1]][1]+" with a bob "+cv1);
                    cv2 = callvaltest(prev[next[src][1]][0], 1);
                    print("c "+prev[next[src][1]][0]+"->"+next[prev[next[src][1]][0]][0]+" with a plain "+cv2);
                } else {
                    cv1 = 0;
                    cv2 = 0;
                }
                int so = 0;
                boolean selfloop2 = false;
                gates = new int[2][];
                for (int call = 0; call < 2; ++call) {
                    if (next[src][call] == src) {
                        selfloop2 = true;
                    } else if (six(src) != six(next[src][call])) {
                        gates[so++] = genGate(next[src][call], 0);
                    }
                }
                if (selfloop2) {
                    print("c "+src+"<self-loop so always at least one output from six-end>"+next[src][0]+" "+next[src][1]);
                } else {
                    chooseOneEach(gates, so, gate);
                }
            }
            
            /*
             * Disallow invalid six-type variables for this six (required).
             */
            if (src == firstsixend(src)) {
                print("c Invalid six-types "+src);
                int rests[][] = genInvalid(src);
                for (int rest[] : rests) {
                    // Disallow invalid six types
                    print(rest);
                }
                /*
                 * Performance optimizations
                 */
                if (six(src) != sixstart) {
                    int z[] = new int[bb];
                    if (disallowZero) {
                        // Disallow sequence number 0
                        print("c Disallow sequence 0");
                        for (int b = 0; b < taps[0]/*bb*/; ++b) {
                            z[b] = countvar(src, b);
                        }
                        print(z);
                    }
                    if (testBeyondLast) {
                        print("c Disallow sequence beyond last");
                        for (int b = 0; b < bb; ++b) {
                            z[b] = countvar(src, b) * (lfsrBeyondLast[b] ? -1 : 1);
                        }
                        print(z);
                    }
                    if (testBeforeFirst) {
                        print("c Disallow sequence before first");
                        for (int b = 0; b < bb; ++b) {
                            z[b] = countvar(src, b) * (lfsrBeforeFirst[b] ? -1 : 1);
                        }
                        print(z);
                    }
                    for (int i = 0; i < lfsrInvalids.length; i += 2) {
                        print("c Disallow some invalid LSFR values");
                        for (int b = 0; b < bb; ++b) {
                            // Certain variables are ignored
                            z[b] = countvar(src, b) * (lfsrInvalids[i][b] ? -1 : 1) * (lfsrInvalids[i + 1][b] ? 0 : 1);
                        }
                        print(z);
                    }
                    // Exclude unreachable LFSR values. Idea from Neng-Fa Zhou
                    if (opt24 && fdistances[six(src)] > 1) {
                        boolean lfsr[] = initLFSR(taps);
                        for (int i = 1; i <= count && i < fdistances[six(src)]; ++i) {
                            print("c Disallow unreachable sequence from start "+i+" distance "+fdistances[six(src)]);
                            print("c "+i+" "+Arrays.toString(lfsr));
                            for (int b = 0; b < bb; ++b) {
                                z[b] = countvar(src, b) * (lfsr[b] ? -1 : 1);
                            }
                            print(z);
                            nextStateLFSR(lfsr, taps);
                        }
                    }
                    if (opt24 && bdistances[six(src)] > 1) {
                        boolean lfsr[] = initLFSR(taps);
                        for (int i = 1; i <= count; ++i) {
                            int dist = count - i + 1;
                            if (dist < bdistances[six(src)] && dist == 1)
                            {
                                print("c Disallow unreachable sequence to end "+dist+" distance "+bdistances[six(src)]);
                                print("c "+i+" "+Arrays.toString(lfsr));
                                for (int b = 0; b < bb; ++b) {
                                    z[b] = countvar(src, b) * (lfsr[b] ? -1 : 1);
                                }
                                print(z);
                            }
                            nextStateLFSR(lfsr, taps);
                        }
                        // Check we get to the end
                        if (!Arrays.equals(lfsr, lfsrBeyondLast))
                            throw new IllegalStateException("LFSR "+Arrays.toString(lfsr)+" "+Arrays.toString(lfsrBeyondLast));
                    }
                }
            }
        }
        long t2 = System.currentTimeMillis();
        if (perftime) {
            System.err.println("Generated "+clauses+" clauses in "+(t2-t1)+"ms");
        }
        return;
    }
    
    /**
     * At least one item from g must completely match, or else extra is true.
     * All not match => extra is true.
     * All not match without extra is prohibited.
     * Choose one each from non-zero values in g and make opposite.
     * Exclude whole group if contains two equal values with opposite signs.
     * Skip duplicated value (replace with 0).
     * [1,3,5]
     * [5,-6]
     * extra
     * 
     * so 1,3,5,-6
     * 
     * [-1,-5,extra]
     * [-1,6,extra]
     * [-3,-5,extra]
     * [-3,6,extra]
     * [-5,extra]
     * [-5,6,extra]
     * 
     * so if extra evaluates to false,
     * @param g
     * @param gl length in g
     * @param extra arguments always used
     */
    private void chooseOneEach(int[][] g, int gl, Object ... extra)
    {
        // Used to condense the choices by omitting zeroes
        int g2[][] = new int[g.length][];
        int s1 = 1;
        for (int i1 = 0; i1 < gl; ++i1) {
            int s2 = 0;
            for (int v : g[i1]) {
                if (v != 0) ++s2;
            }
            s1 *= s2;
            // condense
            g2[i1] = new int[s2];
            s2 = 0;
            for (int v : g[i1]) {
                if (v != 0) g2[i1][s2++] = v;
            }
        }
        if (s1 >= 4096) {
            // Performance check - presuming that chooseOneEach is not essential
            //System.err.println("Too big chooseOneEach: "+s1+" "+Arrays.deepToString(g));
            return;
        }
        // Replace with condensed version
        g = g2;
        
        /*
         * Condense processing - see if a clause is subsumed by another.
         * Condensed processing is very slow.
         */
        boolean condensed = false;
        if (opt18) {
            // See if any overlap between choices
         l: for (int i = 0; i < gl; ++i) {
                for (int j = i + 1; j < gl; ++i) {
                    for (int v1: g[i]) {
                        if (v1 == 0) continue;
                        for (int v2: g[j]) {
                            if (v2 == 0) continue;
                            if (v1 == v2)
                                condensed = true;
                                break l;
                        }
                    }
                }
            }
            if (!condensed) {
                print("c Condensed processing omitted as no duplicates in "+s1);
            }
        }
        Set<Set<Integer>>all = new HashSet<Set<Integer>>();
        
        /*
         * Get in order of varying last element most rapidly.
         * E.g. [a/5][b/7][c/2]
         * s1 = 5*7*2=70
         * dv = 70, dv /= 5
         * dv = 14, dv /= 7
         * dv = 2, dv /= 2
         * dv = 1
         */
        int gg[] = new int[gl];
        t: for (int t = 0; t < s1; ++t) {
            int dv = s1;
            for (int i1 = 0; i1 < gl; ++i1) {
                int s2 = 0;
                for (int v : g[i1]) {
                    if (v != 0) ++s2;
                }
                dv /= s2;
                int t3 = t / dv % s2;
                int s3 = 0;
                for (int v : g[i1]) {
                    if (v != 0) {
                        if (s3 == t3) {
                            // Note the opposite sign
                            gg[i1] = -v;
                            for (int i2 = 0; i2 < i1; ++i2) {
                                // Two literals opposite signed - will always be true
                                if (gg[i1] == -gg[i2]) {
                                    print("c omit clause with opposite literals "+gg[i1]+" "+gg[i2]);
                                    continue t;
                                }
                                // Duplicated literal - omit literal
                                if (gg[i1] == gg[i2]) {
                                    //print("c omit duplicated literal with "+gg[i1]+" "+gg[i2]);
                                    gg[i1] = 0;
                                    break;
                                }
                            }
                        }
                        ++s3;
                    }
                }
            }
            
            if (condensed) {
                // Build sets to check
                // Build a set
                Set<Integer>set1 = new HashSet<Integer>();
                for (int b : gg) {
                    if (b != 0) {
                        set1.add(b);
                    }
                }
                // See if subset of existing
                for (Iterator<Set<Integer>>it = all.iterator(); it.hasNext(); ) {
                    Set<Integer>set2 = it.next();
                    //System.err.println("Size s1="+set1.size()+" s2="+set2.size());
                    if (set2.size() >= set1.size() && set2.containsAll(set1)) {
                        // New is smaller, so remove existing as new will trigger first
                        //System.err.println("New clause "+set1+" subsumes "+set2);
                        it.remove();
                    } else if (set1.size() >= set2.size() && set1.containsAll(set2)) {
                        // New is bigger than existing, so remove new
                        //System.err.println("Old clause "+set2+" subsumes "+set1);
                        continue t;
                    }
                }
                all.add(set1);
            } else {
                //print(gate, gg, callval2);
                if (extra.length > 0) {
                    // Unwrap and rewrap
                    Object allargs[] = new Object[extra.length + 1];
                    allargs[0] = gg;
                    for (int i = 0; i < extra.length; ++i) {
                        allargs[i + 1] = extra[i];
                    }
                    print(allargs);
                } else {
                    print(gg);
                }
            }
        }
        
        if (condensed) {
            print("c condensed using subsumption from "+s1+" to "+all.size()+" clauses");
            //System.err.println("Condensed using subsumption from "+s1+" to "+all.size()+" clauses");
            for (Set<Integer>set1 : all) {
                gg = new int[set1.size()];
                int ix = 0;
                for (int i : set1) {
                    gg[ix++] = i;
                }
                if (extra.length > 0) {
                    // Unwrap and rewrap
                    Object allargs[] = new Object[extra.length + 1];
                    allargs[0] = gg;
                    for (int i = 0; i < extra.length; ++i) {
                        allargs[i + 1] = extra[i];
                    }
                    print(allargs);
                } else {
                    print(gg);
                }
            }
        }
    }
    
    /**
     * Length of full length LFSR + twisted-ring
     * @param b
     * @param tw
     * @return
     */
    int tapsLen(int b, int tw) {
        if (tw > 0) {
            return lcm(tapsLen(b - tw, 0), tw * 2);
        } else if (b < 2) {
            return 0;
        } else {
            return (1 << b) - 1;
        }
    }
    
    /**
     * Highest common factor.
     * @param a
     * @param b
     * @return
     */
    int hcf(int a, int b) {
        while (b > 0) {
            int c = b;
            b = a % b;
            a = c;
        }
        return a;
    }
    
    /**
     * Lowest common multiple.
     * @param a
     * @param b
     * @return
     */
    int lcm(int a, int b) {
        return a * (b / hcf(a, b));
    }
    
    /**
     * Length of short length LFSR + twisted-ring
     * @param b
     * @param tw
     * @return
     */
    int tapsLen2(int b, int tw) {
        if (tw > 0) {
            return lcm(tapsLen2(b - tw, 0), tw * 2);
        } else
            return tapsLen2[b];
    }
    /**
     * Get taps for Linear feedback shift register,
     * with associated twisted ring counter for the top bits.
     * @param bb bits to use
     * @param n minimum length of cycle
     * @param useTwisted If looking for multiple loops, then cannot use twisted as 0=/>0
     * need sequence 0000->0000 for independent loops.
     * @param allowShort Allow cycles with multiple loops - filter out later if not full length
     * @param disableHybrid disable hybrid LFSR modes
     * @param disableNonLinear disable non-linear feedback shift register modes
     * @return sequence of taps, 1-based.
     */
    int[] lfsrTaps(int bb, int n, boolean useTwisted, boolean allowShort, boolean disableHybrid, boolean disableNonLinear)
    {
        if (n > tapsLen(bb, 0)) {
            throw new IllegalArgumentException(n + " > " + tapsLen(bb, 0) + " with " + bb + " bits");
        }
        int taps[] = tapsList[bb];
        // Twisted ring bits - normally at most 2
        int tw = useTwisted ? 2 : 0;
        // Ordinary Galois LFSR
        for (int i = 0; i <= tw; ++i) {
            if (i < bb && n <= tapsLen(bb, i) && tapsList[bb - i].length <= taps.length) {
                // Longer twisted ring for same or shorter length taps
                //System.out.println("Taps bb="+bb+" tw="+i+" "+tapsLen(bb,i));
                taps = tapsList[bb - i];
            }
        }
        if (!disableHybrid) {
            for (int i = 0; i <= tw; ++i) {
                if (i < bb && n <= tapsLen(bb, i) && tapsListComplex[bb - i].length > 0 && tapsListComplex[bb - i].length <= taps.length) {
                    // Longer twisted ring for same or shorter length taps
                    //System.out.println("Taps bb="+bb+" tw="+i+" "+tapsLen(bb,i));
                    taps = tapsListComplex[bb - i];
                }
            }
        }
        if (!disableNonLinear) {
            for (int i = 0; i <= tw; ++i) {
                if (i < bb && n <= tapsLen(bb, i) && tapsListNonLinear[bb - i].length > 0 && tapsListNonLinear[bb - i].length <= taps.length) {
                    // Longer twisted ring for same or shorter length taps
                    //System.out.println("Taps bb="+bb+" tw="+i+" "+tapsLen(bb,i));
                    taps = tapsListNonLinear[bb - i];
                }
            }
        }
        /*
         * Alternative non-maximal length counters. E.g. an 8-bit one with period 217.
         * It also has other values with period 31,7,1 (1 for all zeroes).
         * We would get multiple solutions of SAT if a subcycle of length a multiple of
         * 7 or 31 were present as the counter could have all zeroes, or the
         * values which repeat every 7 or 31.
         * E.g. sted2, with a 4-course and 26-course block.
         * Choose if fewer taps, or allows bigger twisted ring.
         */
        if (allowShort) {
            for (int i = 0; i <= tw; ++i) {
                if (i < bb && n <= tapsLen2(bb, i) && (
                        tapsList2[bb - i].length < taps.length ||
                        tapsList2[bb - i].length == taps.length && tapsList2[bb - i][0] < taps[0])) {
                    //System.out.println("Taps2 bb="+bb+" tw="+i+" "+tapsLen(bb,i));
                    taps = tapsList2[bb - i];
                }
            }
        }
        return taps;
    }
    /**
     * Returns the literal for a six of type src, normally positive
     * for plain, negative for bob.
     * Evaluates to false (unsatisfied) when the call of the type occurs
     * ensuring something else in the clause is then set.
     * @param src six-end
     * @param call 0 for plain, 1 for bob
     * @return
     */
    private int callvaltest(int src, int call)
    {
        if (newcall) {
            return -(callvar(src) + call);
        }
        if (sel == COMBINEDCALLTYPE) {
            // new scheme, combined six-type and call-type
            int s = (src - 1) % sixTypes;
            return -sixvar(src, s*2+call);
        }
        int callval2;
        if (call == 0) {
            callval2 = callvar(src);
        } else {
            callval2 = -callvar(src);
        }
        return callval2;
    }
    /**
     * Returns the literal for a six of type src, normally negative
     * for plain, positive for bob.
     * Used when the rest of the clause is unset to require this type of
     * call.
     * @param src
     * @param call 0 or 1
     * @return literal
     */
    private int callvalset(int src, int call) {
        return -callvaltest(src, call);
    }

    /**
     * Simple encoding for Erin
     */
    static int [][]t3vals = {
        {1,-1},{-1,1},{-1,-1}
    };
    static int [][]a3vals = {
        {-1,1},{1,-1},{1,1}
    };
    static int [][]b3vals = {
        {-1,-1}
    };
    /*
     * sel == 6 (COMBINEDCALLTYPE)
     * New scheme - combine six-end with six-exit
     * 100000 first six-end with plain
     * 010000 first six-end with bob
     * 001000
     * 000100
     * 000010
     * 000001
     * 
     * call var does all the setting
     * 
     * a => b + c
     * !a + b + c
     * 
     * so the test could be all done by the call var
     * 
     * setting
     * 
     */
    /*
     * For each sel value:
     * Values for test
     * Values for set
     * Disallowed values
     */
    static int [][][][]all3 = {
        {genDirect(3, -1), genDirect(3, 1), genDirectX(3)},
        {t3vals, a3vals, b3vals},
        {genOrder(3, -1), genOrder(3, 1), genOrderX(3)},
        {genLog(3, -1), genLog(3, 1), genLogXS(3)},
        {genDirect(3, -1), genDirect(3, 1), genDirectX(3)},
        {genTwistedRing(3, -1), genTwistedRing(3, 1), genTwistedRingXmin(3)},
        {genDirectPair(3,0,1), genDirectPair(3,0,-1), genDirectX(3*2)}, // For new scheme
    };
    static int [][][][]all6 = {
        {genDirect(6, -1), genDirect(6, 1), genDirectX(6)},
        {genTwistedRing(6, -1), genTwistedRingSet(6, 1), genTwistedRingX(6)},
        {genOrder(6, -1), genOrder(6, 1), genOrderX(6)},
        {genLog(6, -1), genLog(6, 1), genLogXS(6)},
        {genDirect(6, -1), genDirect(6, 1), genDirectX(6)},
        {genTwistedRing(6, -1), genTwistedRing(6, 1), genTwistedRingXmin(6)},
        {genDirectPair(6,0,1), genDirectPair(6,0,-1), genDirectX(12)},
    };
    static int [][][][]all12 = {
        {genDirect(12, -1), genDirect(12, 1), genDirectX(12)},
        {genTwistedRing(12, -1), genTwistedRingSet(12, 1), genTwistedRingX(12)},
        {genOrder(12, -1), genOrder(12, 1), genOrderX(12)},
        {genLog(12, -1), genLog(12, 1), genLogXS(12)},
        {genDirect(12, -1), genDirect(12, 1), genDirectX(12)},
        {genTwistedRing(12, -1), genTwistedRing(12, 1), genTwistedRingXmin(12)},
        {genDirectPair(12,0,1), genDirectPair(12,0,-1), genDirectX(12*2)},
    };

    /**
     * Direct encoding.
     * One hot variable.
     * @param n number of values
     * @param v +1 or -1, used to multiply with the variable to get the literal.
     * @return
     */
    private static int[][] genDirect(int n, int v) {
        int r[][] = new int[n][n];
        for (int i = 0; i < n; ++i) {
            r[i][i] = v;
        }
        r = simplify(r);
        return r;
    }
    
    /**
     * Direct pair encoding.
     * One of two hot variable, for plain/bob encoding with six-type.
     * @param n number of values
     * @param v1 +1 or -1, used to multiply with the variable to get the literal.
     * @param v2 non-hot value
     * @return
     */
    private static int[][] genDirectPair(int n, int v1, int v2) {
        int r[][] = new int[n][n*2];
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n*2; ++j) {
                r[i][j] = (j / 2 == i) ? v1 : v2;
            }
        }
        r = simplify(r);
        return r;
    }
    
    /**
     * Invalid values for direct encoding.
     * Pairwise exclude 2.
     * All ensure at least 1.
     * @param n
     * @return
     */
    private static int[][] genDirectX(int n) {
        int r[][] = new int[1+n*(n-1)/2][n];
        int i = 0;
        // Not two
        for (int j = 0; j < n - 1; ++j) {
            for (int k = j + 1; k < n; ++k, ++i) {
                r[i][j] = 1;
                r[i][k] = 1;
            }
        }
        // At least one
        for (int j = 0; j < n; ++j) {
            r[i][j] = -1;
        }
        r = simplifyX(r);
        return r;
    }

    /**
     * Twisted-ring encoding.
     * E.g.
     * 1111 1xx1
     * 0111 01xx
     * 0011 x01x
     * 0001 xx01
     * 0000 0xx0
     * 1000 10xx
     * 1100 x10x
     * 1110 xx10
     * 
     * For 1 of 3:
     * 00 00
     * 01 01
     * 11 11
     * 
     * For 1 of 5
     * 000
     * 001
     * 011
     * 111
     * 110
     * 
     * @param n number of values
     * @param v +1 or -1, used to multiply with the variable to get the literal.
     * @return
     */
    private static int[][] genTwistedRing(int n, int v) {
        int m = (n + 1) / 2;
        int r[][] = new int[n][m];
        int i = 0;
        r[i][0] = -v;
        r[i][m - 1] = -v;
        ++i;
        for (int j = 1; j < m; ++j, ++i) {
            r[i][j - 1] = v;
            r[i][j] = -v;
        }
        r[i][0] = v;
        r[i][m - 1] = v;
        ++i;
        for (int j = 1; j < m && i < n; ++j, ++i) {
            r[i][j - 1] = -v;
            r[i][j] = v;
        }
        r = simplify(r);
        return r;
    }
    /**
     * Twisted-ring encoding complete
     * E.g.
     * 0000 0xx0
     * 1000 10xx
     * 1100 x10x
     * 1110 xx10
     * 1111 1xx1
     * 0111 01xx
     * 0011 x01x
     * 0001 xx01
     * 
     * @param n number of values
     * @param v +1 or -1, used to multiply with the variable to get the literal.
     * @return
     */
    private static int[][] genTwistedRingSet(int n, int v) {
        int m = (n + 1) / 2;
        int r[][] = new int[n][m];
        int i = 0;
        for (int j = 0; j < m; ++j, ++i) {
            for (int k = 0; k < m; ++k) {
                r[i][k] = k >= j ? -v : v;
            }
        }
        for (int j = 0; j < m && i < n; ++j, ++i) {
            for (int k = 0; k < m; ++k) {
                r[i][k] = k >= j ? v : -v;
            }
        }
        //r = simSel(r);
        return r;
    }
    
    /**
     * Invalid values for twisted-ring.
     * 101x
     * 010x
     * x101
     * x010
     * 
     * Consider 12 values
     * Need six bits
     * 111111
     * 011111
     * 001111
     * 000111
     * 000011
     * 000001
     * 000000
     * 100000
     * 110000
     * 111000
     * 111100
     * 111110
     * 
     * Need to exclude any 0 / 1 / 0 or 1 / 0 / 1
     * If starts with 1, exclude any 0/1 transition
     * If starts with 0, exclude any 1/0 transition
     * n - 4
     * 
     * 0 1 0 x x x
     * 1 0 1 x x x
     * 0 x 1 0 x x
     * 1 x 0 1 x x
     * 0 x x 1 0 x
     * 1 x x 0 1 x
     * 0 x x x 1 0
     * 1 x x x 0 1
     * 
     * Consider odd number bits
     * 00
     * 01
     * 11
     * 10 - excluded
     * so add 1 for odd one
     * 
     * 2 has no excluded
     * 3 has 1 excluded
     * 11
     * 10
     * 00
     * 01<= invalid
     * 4 has no
     * 5 has 3 bits, round up,
     * @param n
     * @return
     */
    private static int[][] genTwistedRingX(int n) {
        int m = (n + 1) / 2;
        int r[][] = new int[n + (n%2) - 4 + (n%2)][m];
        int i = 0;
        for (int j = 2; j < m; ++j) {
            r[i][0] = 1;
            r[i][j - 1] = -1;
            r[i][j] = 1;
            ++i;
            r[i][0] = -1;
            r[i][j - 1] = 1;
            r[i][j] = -1;
             ++i;
        }
        if (n % 2 != 0) {
            // Exclude the last state of a normal twisted-ring seqeunce for an odd n
            r[i][m - 2] = -1;
            r[i][m - 1] = 1;
        }
        r = simplifyX(r);
        return r;
    }

    /**
     * Generate minimum clause twisted-ring exclude set.
     * At-least-one is enforced via decode
     * Strictly we do not need at-most-one, though it really helps
     * performance, and is required for multiple loops.
     * Sometime we just want the minimum clauses though.
     * @param n
     * @return
     */
    private static int[][] genTwistedRingXmin(int n) {
        return new int[0][0];
    }

    /**
     * Order encoding.
     * 00000
     * 10000
     * 11000
     * 11100
     * 11110
     * 11111
     * 
     * @param n
     * @param v
     * @return
     */
    private static int[][] genOrder(int n, int v) {
        int m = n - 1;
        int r[][] = new int[n][m];
        int i = 0;
        r[i][0] = -v;
        ++i;
        for (int j = 1; j < m; ++j, ++i) {
            r[i][j - 1] = v;
            r[i][j] = -v;
        }
        r[i][m - 1] = v;
        ++i;
        if (v < 0) {
            simplify(r);
        }
        return r;
    }
    /**
     * Order encoding
     * Exclude
     * 01xxx
     * x01xx
     * xx01x
     * xxx01
     * @param n
     * @return
     */
    private static int[][] genOrderX(int n) {
        int m = n - 1;
        int r[][] = new int[n - 2][m];
        for (int i = 0, j = 1; j < m; ++j, ++i) {
            r[i][j - 1] = -1;
            r[i][j] = 1;
        }
        r = simplifyX(r);
        return r;
    }
    
    private static void genBinRow(int r[], int val, int v) {
        for (int j = 0; j < r.length; ++j) {
            r[j] = (val & (1 << j)) != 0 ? v : -v;
        }
    }
    /**
     * Log or binary encoding.
     * 000 000
     * 100 100
     * 010 010
     * 110 110
     * 001 0x1
     * 101 1x1
     * 
     * exclude
     * 011
     * 111
     * 
     * 0000
     * 1000
     * 0100
     * 1100
     * 0010
     * 1010
     * 0110
     * 1110
     * 0001
     * 1001
     * 0101
     * 1101
     * @param n
     * @param v
     * @return
     */
    private static int[][] genLog(int n, int v) {
        int m = log2(n);
        int r[][] = new int[n][m];
        for (int i = 0; i < n; ++i) {
            genBinRow(r[i], i, v);
        }
        /*
         *  Optimisation for test, not set, otherwise more
         *  propagation is needed to fully set a value
         */
        
        if (v < 0 || true) {
            r = simplify(r);

        }
        return r;
    }
    /**
     * For log encoding, not all bits are needed, but haven't implemented that yet
     * 000
     * 100
     * 010
     * 110
     * 001
     * 101
     * 
     * 011 <= invalid, only need to disable x11
     * 111 <= invalid
     * @param n
     * @return
     */
    static int[][] genLogX(int n, int start) {
        int m = log2(n + start);
        int nm = 1 << m;
        int r[][] = new int[nm - n][m];
        for (int i = 0; i < start; ++i) {
            for (int j = 0; j < m; ++j) {
                r[i][j] = (i & (1 << j)) > 0 ? 1 : -1;
            }
        }
        for (int i = start + n; i < nm; ++i) {
            for (int j = 0; j < m; ++j) {
                r[i - n][j] = (i & (1 << j)) > 0 ? 1 : -1;
            }
        }
        return r;
    }
    static int[][] genLogX(int n) {
        return genLogX(n, 0);
    }
    static int[][] genLogXS(int n) {
        return simplifyX(genLogX(n));
    }
    static {
        //System.out.println(Arrays.deepToString(genDirect(6,1)));
        //System.out.println(Arrays.deepToString(genLog(6,1)));
        //System.out.println(Arrays.deepToString(genLogX(6)));
    }
    private int[] genGate12(int src, int callvar)
    {
        int s = (src - 1) % sixTypes;
        int tval[] = all12[sel][0][s];
        if (sel == COMBINEDCALLTYPE && callvar != 0) {
            return new int[]{callvar};
        }
        int gate[] = new int[tval.length+1];
        for (int i = 0; i < tval.length; ++i) {
            gate[i] = sixvar(src, i) * tval[i];
        }
        gate[tval.length] = callvar;
        return gate;
    }
    private int[] genGate6(int src, int callvar)
    {
        int s = (src - 1) % sixTypes;
        int tval[] = all6[sel][0][s];
        if (sel == COMBINEDCALLTYPE && callvar != 0) {
            return new int[]{callvar};
        }
        int gate[] = new int[tval.length+1];
        for (int i = 0; i < tval.length; ++i) {
            gate[i] = sixvar(src, i) * tval[i];
        }
        gate[tval.length] = callvar;
        return gate;
    }
    private int[] genGate3(int src, int callvar)
    {
        int s = (src - 1) % sixTypes;
        int tval[] = all3[sel][0][s];
        if (sel == COMBINEDCALLTYPE && callvar != 0) {
            tval = new int[tval.length];
        }
        int gate[] = new int[tval.length+1];
        for (int i = 0; i < tval.length; ++i) {
            gate[i] = sixvar(src, i) * tval[i];
        }
        gate[tval.length] = callvar;
        return gate;
    }
    private int genBitsSix(int types) {
        if (sel >= 7);
        else if (sixTypes == 6) return all6[sel][0][0].length;
        else if (sixTypes == 3) return all3[sel][0][0].length;
        else if (sixTypes == 12) return all12[sel][0][0].length;
        return log2(types);
    }
    
    /**
     * Generate a list of variables which are true (or false if
     * the variable is negative in the list)
     * if the six is of this type and the call variable is of this value.
     * Evaluates to false if this six-end is active with the callvar.
     * For sel==4 and callvar=0 (no call var) works differently.
     * E.g. Erin, sel=4, 6 variables
     * 100,101,102,103,104,105
     * 100 =. six-end type 1, plain
     * 101 =. six-end type 2, bob
     * @param src six-end identifier
     * @param callvar Variable for the call, positive for plain or negative for bob, 0 to ignore.
     * @return List of variables, skip variables in list which are 0.
     */
    private int[] genGate(int src, int callvar)
    {
        if (sel >= 7);
        else if (sixTypes == 6) return genGate6(src, callvar);
        else if (sixTypes == 3) return genGate3(src, callvar);
        else if (sixTypes == 12) return genGate12(src, callvar);
        // Binary/log encoding
        int gate[] = new int[bitsSix + 1];
        for (int i = 0; i < bitsSix; ++i) {
            gate[i] = -sixvar(src, i) * sixval(src, i);
        }
        gate[bitsSix] = callvar;
        return gate;
    }
    private int[] genVal12(int src)
    {
        int s = (src - 1) % sixTypes;
        int aval[] = all12[sel][1][s];
        int gate[] = new int[aval.length];
        for (int i = 0; i < aval.length; ++i) {
            gate[i] = sixvar(src, i) * aval[i];
        }
        return gate;
    }
    private int[] genVal6(int src)
    {
        int s = (src - 1) % sixTypes;
        int aval[] = all6[sel][1][s];
        int gate[] = new int[aval.length];
        for (int i = 0; i < aval.length; ++i) {
            gate[i] = sixvar(src, i) * aval[i];
        }
        return gate;
    }
    private int[] genVal3(int src)
    {
        int s = (src - 1) % sixTypes;
        int aval[] = all3[sel][1][s];
        int gate[] = new int[aval.length];
        for (int i = 0; i < aval.length; ++i) {
            gate[i] = sixvar(src, i) * aval[i];
        }
        return gate;
    }
    private int[] genVal(int src)
    {
        if (sel >= 7);
        else if (sixTypes == 6) return genVal6(src);
        else if (sixTypes == 3) return genVal3(src);
        else if (sixTypes == 12) return genVal12(src);
        // Binary/log encoding
        int gate[] = new int[bitsSix];
        for (int i = 0; i < bitsSix; ++i) {
            gate[i] = sixvar(src, i) * sixval(src, i);
        }
        return gate;
    }
    private int[][] genInvalid12(int src) {
        // Disallow six type=010, 101
        int bvalss[][] = all12[sel][2];
        int rest[][] = new int[bvalss.length][];
        for (int i = 0; i < bvalss.length; ++i) {
            rest[i] = new int[bvalss[i].length];
            for (int j = 0; j < bvalss[i].length; ++j) {
                rest[i][j] = sixvar(src, j) * -bvalss[i][j];
            }
        }
        return rest;
    }
    private int[][] genInvalid6(int src) {
        // Disallow six type=010, 101
        int bvalss[][] = all6[sel][2];
        int rest[][] = new int[bvalss.length][];
        for (int i = 0; i < bvalss.length; ++i) {
            rest[i] = new int[bvalss[i].length];
            for (int j = 0; j < bvalss[i].length; ++j) {
                rest[i][j] = sixvar(src, j) * -bvalss[i][j];
            }
        }
        return rest;
    }
    private int[][] genInvalid3(int src) {
        // Disallow six type=00
        int bvalss[][] = all3[sel][2];
        int rest[][] = new int[bvalss.length][];
        for (int i = 0; i < bvalss.length; ++i) {
            rest[i] = new int[bvalss[i].length];
            for (int j = 0; j < bvalss[i].length; ++j) {
                rest[i][j] = sixvar(src, j) * -bvalss[i][j];
            }
        }
        return rest;
    }
    private int[][] genInvalid(int src) {
        if (sel >= 7);
        else if (sixTypes == 6) return genInvalid6(src);
        else if (sixTypes == 3) return genInvalid3(src);
        else if (sixTypes == 12) return genInvalid12(src);
        // Binary/log encoding
        // Disallow six type=00
        int rest[][];
        rest = genLogX2(src, typeStart);
        rest = simplifyX(rest);
        //System.out.println(Arrays.deepToString(rest));
        return rest;
    }
    int[][] genLogX2(int src, int start) {
        int[][] rest;
        rest = new int[(1 << bitsSix) - sixTypes][];
        int j = 0;
        for (int i = 0; i < (1 << bitsSix); ++i) {
            if (i < start || i >= start + sixTypes) {
                rest[j] = new int[bitsSix];
                for (int b = 0; b < bitsSix; ++b) {
                    int ret = (i & 1 << b) > 0 ? 1 : -1;
                    rest[j][b] = sixvar(src, b) * -ret ;
                }
                ++j;
            }
        }
        return rest;
    }
    
    /**
     * Simplify exclusion clauses.
     * If two clauses almost the same exist - merge and
     * zero single literal which differs in sign.
     * 0 means don't care for the comparison
     * @param rest clauses - all to be excluded
     * @return condensed
     */
    static int[][] simplifyX1(int[][] rest1) {
        // Exclude binary values [0, typeStart) [typeStart + sixTypes, 1 << bitsSix)
        // Simplify expressions
        int [][]rest = rest1.clone();
        int j = rest.length;
        boolean found = true;
        while (found && j > 1) {
            found = false;
            for (int i = 0; i < j; ++i) {
              int differ = 0;
              l: for (int k = 0; k < j; ++k) {
                    if (k == i) continue;
                    // Compare two rows
                    differ = -1;
                    for (int b = 0; b < rest[i].length; ++b) {
                        // Do they differ including in don't cares (0)
                        if (rest[i][b] != rest[k][b]) {
                            if (differ != -1 || rest[i][b] != -rest[k][b]) {
                                // We already have another difference, or they are different variables, so give up
                                continue l;
                            } else {
                                differ = b;
                            }
                        }
                    }
                    found = true;
                    //System.out.println("Simplify i="+i+" k="+k+" differ="+differ);
                    //System.out.println(Arrays.toString(rest[i]));
                    //System.out.println(Arrays.toString(rest[k]));
                    if (differ >= 0) {
                        // One difference at differ, so ignore that bit position
                        rest[i] = rest[i].clone();
                        rest[i][differ] = 0;
                        //System.out.println(Arrays.toString(rest[i]));
                    }
                    // condense as row i covers row k
                    for (int l = k + 1; l < j; ++l) {
                        rest[l - 1] = rest[l];
                    }
                    --j;
                }
            }
        }
        if (j < rest.length) {
            if (printsimp)
                System.out.println("X>"+Arrays.deepToString(rest1));
            rest = Arrays.copyOf(rest, j);
            if (printsimp)
                System.out.println("X<"+Arrays.deepToString(rest));
        } else {
            if (printsimp) {
                System.out.println("X="+Arrays.deepToString(rest));
            }
        }
        return rest;
    }
    static int[][] simplifyX(int[][] rest1) {
        int r1[][] = simplifyX1(rest1);
        int r2[][] = rest1.clone();
        for (int i = 0; i < r2.length / 2; ++i) {
            int t[] = r2[i];
            r2[i] = r2[r2.length - 1 -i];
            r2[r2.length - 1 -i] = t;
        }
        r2 = simplifyX1(r2);
        if (r1.length <= r2.length) {
            return r1;
        } else {
            return r2;
        }
    }
    /**
     * Simplify selection clauses.
     * If a clause has a bit position not significant for comparison,
     * exclude it.
     * @param rest clauses - all to be excluded
     * @return condensed
     */
    static int[][] simplify(int[][] rest) {
        int [][]restcopy = rest.clone();
        int j = rest.length;
        boolean found;
        int bx = j > 0 ? rest[0].length : 0;
        do {
            found = false;
            for (int i = 0; i < j; ++i) {
                // See if there is a don't care bit position for all the others
                l1: for (int b = 0; b < bx; ++b) {
                    if (rest[i][b] == 0) {
                        // already a don't care
                        continue;
                    }
                    l2: for (int k = 0; k < j; ++k) {
                        //
                        if (i == k)
                            continue;
                        for (int b2 = 0; b2 < bx; ++b2) {
                            if (b == b2) {
                                // look at the different bits
                                continue;
                            }
                            if (rest[i][b2] != 0 && rest[k][b2] != 0 && rest[i][b2] != rest[k][b2]) {
                                // A real difference
                                continue l2;
                            }
                        }
                        // Match without bit b, so bit b is significant
                        continue l1;
                    }
                    // Not significant, so can ignore this bit, create a copy
                    rest[i] = rest[i].clone();
                    rest[i][b] = 0;
                    found = true;
                }
            }
        } while (found);
        if (printsimp) {
            if (!Arrays.deepEquals(rest, restcopy)) {
                System.out.println(">"+Arrays.deepToString(restcopy));
                System.out.println("<"+Arrays.deepToString(rest));
            } else {
                System.out.println("="+Arrays.deepToString(rest));
            }
        }
        return rest;
    }
    
    /**
     * Re-encode options from unpacked form.
     * @return the options the user would have given
     */
    public int opts() {
        return (sel & 3)
                | (opt1 ? (1 << 2) : 0)
                | (opt2 ? (1 << 3) : 0)
                | (opt3 ? (1 << 4) : 0)
                | (opt4 ? (1 << 5) : 0)
                | (opt5 ? (1 << 6) : 0)
                | (opt6 ? (1 << 7) : 0)
                | (opt7 ? (1 << 8) : 0)
                | (opt8 ? (1 << 9) : 0)
                | (opt9 ? (1 << 10) : 0)
                | (opt10 ? (1 << 11) : 0)
                | (opt11 ? (1 << 12) : 0)
                | ((sel & 4) != 0 ? (1 << 13) : 0)
                | (opt13 ? (1 << 14) : 0)
                | (opt14 ? (1 << 15) : 0)
                | (opt15 ? (1 << 16) : 0)
                | (opt16 ? (1 << 17) : 0)
                | (opt17 ? (1 << 18) : 0)
                | (opt18 ? (1 << 19) : 0)
                | (opt19 ? (1 << 20) : 0)
                | (opt20 ? (1 << 21) : 0)
                | (opt21 ? (1 << 22) : 0)
                | (opt22 ? (1 << 23) : 0)
                | (opt23 ? (1 << 24) : 0)
                | (opt24 ? (1 << 25) : 0)
                ;
    }
    public String toString() {
        return "c StedToSAT: "+(sted?"Stedman":"Erin")+", "+N+" sixes, "+(ooc ? "out of course" : "in course")+", sequence length="+Nlen+", sequence bits="+bb+", six types="+sixTypes+", bitsSix="+bitsSix+", opts=0x"+Integer.toHexString(opts());
    }
    /**
     * Initial state of LFSR
     */
    boolean[] initLFSR(int taps[])
    {
        return initLFSR(bb, taps);
    }
    /**
     * Initial state of LFSR
     */
    boolean[] initLFSR(int bits, int taps[])
    {
        boolean ret[] = new boolean[bits];
        if (bits > 0)
            ret[0] = true;
        return ret;
    }
    /**
     * Initial state of LFSR
     */
    boolean[] initLFSR1(int taps[]) {
        return initLFSR1(bb, taps);
    }
    /**
     * Initial state of LFSR
     */
    boolean[] initLFSR1(int bb, int taps[]) {
        boolean ret[] = new boolean[bb];
        for (int i : taps) {
            ret[Math.abs(i) - 1] = true;
        }
        return ret;
    }
    /**
     * Next state of LFSR
     * @param lfsr
     * @param taps in descending order, 1 based
     * 
     * Simple Galois LFSR
     * 7,6
     * 
     *  .--------------<---------------<-.
     *  |                            |   |
     *  V                            |   |
     * [1]->[2]->[3]->[4]->[5]->[6]->@->[7]
     * 
     * Complex Galois/Fibonacci
     * 8,5,-3
     * 
     *  .----<----@----<---.--------<---------.
     *  |         ^        |                  ^
     *  V         |        V                  |
     * [1]->[2]->[3]->[4]->@->[5]->[6]->[7]->[8]
     * 
     */
    void nextStateLFSR(boolean[] lfsr, int[] taps)
    {
        int bb = lfsr.length;
        int c = 1;
        boolean msb = lfsr[taps[0] - 1];
        for (int b = taps[0] - 1; b > 0; --b) {
            if (c < taps.length && b == taps[c]) {
                lfsr[b] = lfsr[b - 1] ^ msb;
                ++c;
            } else {
                lfsr[b] = lfsr[b - 1];
            }
        }
        if (c < taps.length) {
            // for complex, the bit has already been copied up
            // so was in position c-1, but is now at c
            if (c == taps.length - 1) {
                lfsr[0] = msb ^ lfsr[-taps[c]];
            } else {
                // NLFSR
                boolean A = msb;
                boolean B = lfsr[-taps[c]];
                boolean C = lfsr[-taps[c + 1]];
                // opcode = 0x1e, excluded state all zeroes
                // Positive
                lfsr[0] = (!A&B | !A&C | A&!B&!C);
                // Negative
                lfsr[0] = !(A&B | A&C | !A&!B&!C);
            }
        } else {
            //Wrap
            lfsr[0] = msb;
        }
        // Twisted-ring counter
        if (bb > taps[0]) {
            msb  = lfsr[bb - 1];
            for (int b = bb - 1; b > taps[0]; --b) {
                lfsr[b] = lfsr[b - 1];
            }
            lfsr[taps[0]] = !msb;
        }
    }
    
    /**
     * Convert LFSR counter to a number.
     */
    int stateNum(boolean b[]) {
        int r = 0;
        for (int i = 0; i < b.length; ++i) {
            r |= b[i] ? 1 << i : 0;
        }
        return r;
    }
    /**
     * Convert a number to LFSR counter state.
     * @param n
     * @param b
     * @return
     */
    boolean [] stateFromNum(int n, boolean b[]) {
        for (int i = 0; i < b.length; ++i) {
            b[i] =  (n & (1 << i)) !=  0;
        }
        return b;
    }
    /**
     * Extract list of illegal states for the LFSR.
     * The first entry is the states beyond the end of the normal sequence.
     * The second entry is the all zero state for LFSR.
     * The third and fourth etc. are states for non-maximal counters.
     * @param skip how many valid states
     * @param b the initial value
     * @param taps the taps
     */
    List<Set<Integer>> illegalStates(int skip, boolean b[], int []taps) {
        boolean init[] = b.clone();
        Set<Integer>seen = new HashSet<Integer>();
        List<Set<Integer>>invalids = new ArrayList<Set<Integer>>();
        Set<Integer>invalid = new LinkedHashSet<Integer>();
        // If no counter variables we can skip
        int totalStates = b.length > 0 ? 1 << b.length : 0;
        // Walk through valid states
        for (int i = 0; i < skip; ++i) {
            int state = stateNum(b);
            seen.add(state);
            nextStateLFSR(b, taps);
        }
        // End of normal LSFR
        // Walk through remainder
        while (seen.size() < totalStates && !seen.contains(stateNum(b))) {
            int state = stateNum(b);
            seen.add(state);
            invalid.add(state);
            nextStateLFSR(b, taps);
        }
        invalids.add(invalid);
        // Other states - find unused states
        int look = 0;
        while (seen.size() < totalStates) {
            for (; look < totalStates; ++look) {
                if (!seen.contains(look)) {
                    // and walk through all the succeeding states
                    invalid = new LinkedHashSet<Integer>();
                    stateFromNum(look, b);
                    System.arraycopy(b, 0, init, 0, b.length);
                    do {
                        int state = stateNum(b);
                        if (seen.add(state)) {
                            // new, so add to the invalid list
                            invalid.add(state);
                        } else {
                            /*
                             * old, and we have already seen it
                             * so we are repeating
                             */
                            break;
                        }
                        nextStateLFSR(b, taps);
                    } while (!Arrays.equals(init, b));
                    invalids.add(invalid);
                }
            }
        }
        return invalids;
    }

    /**
     * Find mask for LFSR index - all values subject to this mask equal to i
     * are invalid values.
     * @param i value to check
     * @param cover minimum set of segments to cover
     * @param illegals map of all illegal values to which segment of counter operation
     * @return Ignore bits set for counter value i - all similar values under mask also invalid. Also return -1 for invalid.
     */
    int findMask(int i, int cover, Map<Integer,Integer>illegals) {
        // First find possible mask bits
        int m = 0;
        for (int b = 0; b < bb; ++b) {
            int mm = 1 << b;
            int i2 = i ^ mm;
            if (illegals.containsKey(i2)) {
                // build potential mask
                m |= mm;
            }
        }
        
        /*
         * Iterate over different settings of mask bits
         * 011100100
         * 
         */
        //System.out.println("Testing mask0 "+i+" "+Integer.toBinaryString(m));
        int bestc = 0;
        int bestm = -1;
        for (int m2 = 0;;) {
            // Test all of the mask positions
            //System.out.println("Testing mask1 "+Integer.toBinaryString(m2));
            int cv = 0;
            for (int m3 = 0; ; ) {
                //System.out.println("Testing mask2 "+Integer.toBinaryString(m3));
                int i3 = (i & ~m2) | m3;
                if (!illegals.containsKey(i3)) {
                    cv = 0; // Skips over the success part
                    break;
                }
                // Found to still be illegal under the mask
                int seg = illegals.get(i3);
                cv |= 1 << seg;
                // Increment next mask bit
                m3 = ((m3 | ~m2) + 1) & m2;
                if (m3 == 0) break;
            }
            // All values of m3 have worked
            //System.out.println("Found i="+Integer.toBinaryString(m2)+" seg="+Integer.toBinaryString(cv));
            // Choose better segment coverage first, then wider range of bits
            if ((cv & cover) == cover && (Integer.bitCount(cv) > Integer.bitCount(bestc) ||
                    Integer.bitCount(cv) == Integer.bitCount(bestc) &&
                    Integer.bitCount(m2) > Integer.bitCount(bestm)) ) {
                bestm = m2;
                bestc = cv;
                //System.out.println("Found better i="+Integer.toBinaryString(m2)+" seg="+Integer.toBinaryString(cv));
            }
            // Increment next mask bit
            m2 = ((m2 | ~m) + 1) & m;
            if (m2 == 0) break;
        }
        //System.out.println("i="+i+" best mask = "+Integer.toBinaryString(bestm)+" mask bit count="+Integer.bitCount(bestm)+" cover="+Integer.toBinaryString(bestc)+" wantedcover="+Integer.toBinaryString(cover));
        return bestm;
    }
    
    /**
     * Find states of the LFSR which must be excluded.
     * States come in pairs of arrays of booleans.
     * The first is the actual state, the second has a true value if it is a don't care
     * state.
     * @param count number of valid LFSR states
     * @param maxshortcycle longest possible secondary cycle
     * @param taps for the LFSR
     * @param force disallow some primary cycle states
     * @param allowZero allow all zero state
     * @return
     */
    boolean[][] findInvalids(int count, int maxshortcycle, int taps[], boolean force, boolean allowZero) {
        boolean bbx[] = initLFSR(taps);
        List<Set<Integer>>rr = illegalStates(count, bbx, taps);
        if (!force && rr.size() <= 2) {
            // Only primary LFSR states and zeroes are illegal
            //System.err.println("rr.size="+rr.size()+" leaving");
            return new boolean[0][];
        }
        
        // Test all segments required to be masked
        // First segment is also covered by beyondLast or beforeFirst
        // Second segment is the zero values
        int reqd = ((1 << rr.size()) - 1) & ~3;
        
        // Find all the illegal values in one place
        Map<Integer,Integer>illegals = new HashMap<Integer,Integer>();
        for (int ix = 0; ix < rr.size(); ++ix) {
            if (ix >= 2 && rr.get(ix).size() > maxshortcycle) {
                // Could ignore this subcycle as can never completely occur
                // E.g sted2 with loop length of -56, so biggest outer loop is 28
                // with LFSR with 217,1,31,7 cycles
                //System.err.println("short cycles "+ix+" "+maxshortcycle+" "+rr);
                // No requirement to disable this cycle
                reqd &= ~(1 << ix);
            }
            if (allowZero && ix == 1) {
                // If all zeroes are permitted (e.g. for multiple loops)
                continue;
            }
            for (int ii : rr.get(ix)) {
                illegals.put(ii, ix);
            }
        }
        
        if (!force && reqd == 0) {
            //System.err.println("redq=0 and not force, so leave");
            return new boolean[0][];
        }
        
        /*
         * Search for best index and mask which covers all the required cycles, then as
         * many different cycles as possible, then as many free bits as possible.
         */
        int bestm = 0;
        int besti = -1;
        for (Set<Integer>ss : rr) {
            //System.out.println("size "+ss.size());
            for (int ii : ss) {
                int m = findMask(ii, reqd, illegals);
                if(m != -1) {
                    if (besti < 0 || Integer.bitCount(m) > Integer.bitCount(bestm)) {
                        bestm = m;
                        besti = ii;
                    }
                }
            }
        }
        //System.err.println("besti = "+besti+" "+Integer.toBinaryString(bestm));
        if (besti < 0) {
            if ((reqd & bestm) != reqd) {
                throw new IllegalStateException("unable to simply exclude illegal LFSR states. Required mask="+Integer.toBinaryString(reqd)+" actual="+Integer.toBinaryString(bestm));
            }
            return new boolean[0][];
        }
        boolean excludes[][] = new boolean[][]{stateFromNum(besti, new boolean[bb]),stateFromNum(bestm, new boolean[bb])};
        return excludes;
    }
    
    /**
     * List all the invalid values detected by the value and masks.
     */
    List<Integer> listInvalids(boolean[][] m) {
        List<Integer> l = new ArrayList<Integer>();
        boolean b[] = new boolean[bb];
        for (int i = 0; i < 1 << bb; ++i) {
            stateFromNum(i, b);
            l: for (int k = 0; k < m.length; k += 2) {
                for (int j = 0; j < bb; ++j) {
                    if (!m[k + 1][j] && (m[k][j] != b[j])) {
                        // unmasked mismatch
                        continue l;
                    }
                }
                l.add(i);
                break;
            }
        }
        return l;
    }

    /**
     * Calculate distance from/to starting six.
     * Based on Dijkstra's Shortest Path First algorithm.
     * Keep track of Quick/Slow separately.
     * Combine Quick/Slow distances at end.
     * @param from
     * @param nxt the successor array - either next[][] or prev[][]
     * @param backward
     * @return array of distances (six-based, not six-end)
     */
    int [] buildDistance(int from, int nxt[][], boolean backward)
    {
        // Distance array
        int dist[][] = new int[N][2];
        boolean visited[][] = new boolean[N][2];
        // Initialise distances
        for (int i = 0; i < N; ++i)
        {
             for (int q = 0; q < 2; ++q)
             {
                 dist[i][q] = Integer.MAX_VALUE;
             }
        }
        int current = from;
        dist[six(current)][0] = 0;
        dist[six(current)][1] = 0;
        while (!visited[six(current)][quick(current) ? 1 : 0])
        {
            // Walk through destinations
            for (int src = firstsixend(current); src <= NN && six(src) == six(current); ++src)
            {
                //if (quick(src) != quick(current))
                //    continue;
                // Each call
                for (int call = 0; call < 2; ++call)
                {
                    int dest = nxt[src][call];
                    if (backward ? invalid[dest][call] : invalid[src][call])
                        continue;
                    if (!visited[six(dest)][quick(dest) ? 1 : 0])
                    {
                        // Distance calculation. Q->S uses same LFSR value
                        int dl = backward ? (quick(dest) ? 0 : 1): (quick(src) ? 0 : 1);
                        int l = dist[six(current)][quick(src) ? 1 : 0] + dl;
                        // First counted six always has distance 1 (first LFSR value)
                        if (l == 0)
                            l = 1;
                        if (l < dist[six(dest)][quick(dest) ? 1 : 0])
                        {
                            dist[six(dest)][quick(dest) ? 1 : 0] = l;
                        }
                    }
                }
            }
            visited[six(current)][quick(current) ? 1 : 0] = true;
            // Choose next which is closest
            int shortest = Integer.MAX_VALUE;
            for (int i = 1; i <= NN; ++i)
            {
                if (!visited[six(i)][quick(i) ? 1 : 0])
                {
                    if (dist[six(i)][quick(i) ? 1 : 0] < shortest)
                    {
                        shortest = dist[six(i)][quick(i) ? 1 : 0];
                        current = i;
                    }
                }
            }
        }
        int ret[] = new int[N];
        for (int i = 0; i < N; ++i)
        {
            ret[i] = Math.min(dist[i][0], dist[i][1]);
        }
        //System.err.println(Arrays.toString(ret));
        return ret;
    }

    /**
     * Replace - walks through input,
     * finds index of each char in rounds,
     * replaces with corresponding char in gen.
     * Used for transposition or transfigures.
     * @param input to substitute
     * @param gen
     * @param rounds
     * @return
     */
    public static String rep(String input, String gen, String rounds) {
        StringBuilder sb = new StringBuilder();
        //System.out.println("input="+input);
        for (int j = 0; j < input.length(); ++j) {
            char c= input.charAt(j);
            int i = rounds.indexOf(c);
            if (i >= 0) {
                c = gen.charAt(i);
            }
            sb.append(c);
        }
        return sb.toString();
    }
    /**
     * Generate a group
     * @param gen the list of generators
     * @return
     */
    public static List<String>group(String gen[]) {
        return group(gen, 0);
    }

    /**
     * Generate a group subject to a maximum size.
     * @param gen
     * @param mx max size, or 0 for no maximum
     * @return up to mx+1 elements, mx+1 indicates incomplete group
     */
    public static List<String>group(String gen[], int mx) {
        // To return a list
        List<String>out = new ArrayList<String>();
        // Quickly test if new element
        Set<String>out2 = new HashSet<String>();
        String rounds = "1234567890E".substring(0, gen[0].length());
        out.add(rounds);
        out2.add(rounds);
        for (int i = 0; i < out.size(); ++i) {
            String nextout = out.get(i);
            for (String g : gen) {
                String r = rep(nextout, g, rounds);
                if (out2.add(r)) {
                    out.add(r);
                    if (mx > 0 && out.size() > mx) {
                        // Early exit when group is too big.
                        return out;
                    }
                }
            }
        }
        return out;
    }
    /**
     * Find the group which extends the six-ends to the extent.
     * @return
     */
    public List<String>findGroup() {
       Set<String>all = new HashSet<String>();
       boolean ooc1 = false;
       for (int i = 1; i <= NN; ++i) {
           all.add(name(i));
           ooc1 |= ooc(name(i));
       }
       // Alternating or symmetric group
       String rounds = "1234567890E".substring(0, name(1).length() - 2);
       Iterable<String>gr = ooc1 ? symGroup2(rounds) : altGroup2(rounds);

       Set<String>maxall = new HashSet<String>(all);
       for (String g : gr) {
           //for (String t : all) {
           // Just use first two six-ends for QS and SQ as we apply the whole An/Sn group
           for (int i = 1; i <= 2; ++i) {
           //for (int i = 1; i <= all.size(); ++i) {
               String t = name(i);
               //System.out.println("i="+i+" t="+t);
               String rounds1 = gr.iterator().next();
               String nw = rep(t,g,rounds1);
               maxall.add(nw);
           }
       }
       // Optimisation for one-parts
       if (maxall.size() == all.size())
           return Arrays.asList(rounds);
       int gsize = maxall.size() / all.size();
       List<String>gens = new ArrayList<String>();
       return findGroup(all, gsize, gr, gens);
    }
    /**
     * Find a set of generators to complete a group.
     * Restriction - presume only at most 2 generators required (true
     * for all Erin/Stedman Triples groups).
     * Can be quite slow, so has some short-cuts.
     * @param all - all the six-ends required to be expanded
     * @param gsize - the size of the required group
     * @param gr - the set of possible generators
     * @param gens - generators found so far
     * @return the generators of the group
     */
    public List<String>findGroup(Set<String>all, int gsize, Iterable<String>gr, List<String>gens) {
        /* test each group member to see if it adds to the lead-ends */
        /* Group so far - is the identity group if no generators, or the group by the existing generators */
        //int count1 = 0;
        List<String>gensg = gens.size() == 0 ? Arrays.asList(gr.iterator().next()) : group(gens.toArray(new String[gens.size()]));
        if (perftime)
            System.err.println("findGroup initial rows size="+all.size()+" required group size="+gsize+" current generators="+gens+" group size="+gensg.size());
        String rounds = gensg.get(0);
        final int maxdepth = rounds.length() <= 7 ? 2 : 3;
        int ii = 0;
        l: for (String g : gr) {
            ++ii;
            // Already in the group
            if (gensg.contains(g)) {
                continue;
            }
            if (true) {
                // Performance optimisation, check order of permutation divides possible group size
                int o1 = order(g, rounds);
                //System.err.println("order "+o+" "+g);
                if (gsize % o1 != 0) {
                    //System.err.println("bad element order a "+g+" "+o1+" "+gsize);
                    continue;
                }
                // Exclude permutations false within a six
                if (o1 == 2 || o1 == 3) {
                    int fix = fixed(g, rounds);
                    // check for all pairs swapping
                    if (fix == 1 && o1 == 2)
                        continue;
                    // check for 3 bell cycle
                    if (fix == g.length() - 3 && o1 == 3)
                        continue;
                }
                if (gens.size() > 0) {
                    String g2 = rep(g, gens.get(0), rounds);
                    int o2 = order(g2, rounds);
                    //System.err.println("order "+o+" "+g2);
                    if (gsize % o2 != 0) {
                        //System.err.println("bad element order ab "+g+" "+o2+" "+gsize);
                        continue;
                    }
                    int o3 = order(gens.get(0), rounds);
                    int a = lcm(o1, o2);
                    int b = lcm(o2, o3);
                    int c = lcm(a, b);
                    //System.err.println("o1="+o1+" o2="+o2+" o3="+o3+" c="+c);
                    if (gsize > c*2) {
                        // Ad-hoc performance hack - only try 'big' generators!
                        //System.err.println("o1="+o1+" o2="+o2+" o3="+o3+" c="+c);
                        continue;
                    }
                } else {
                    // First generator
                    if (gsize == 60) {
                        if (o1 != 5) {
                            // special case optimization, need one generator of order 5 for group of order 60
                            // Force group of order 60 to have first generator of order 5
                            continue;
                        }
                    } else if (false && gsize == 168) {
                        if (o1 != 7) {
                            // special case optimization, need one generator of order 7 for group of order 168
                            // Force group of order 168 to have first generator of order 7
                            continue;
                        }
                    }
                }
            }

            // New list of generators
            List<String>gens2 = new ArrayList<String>(gens);
            gens2.add(g);
            // Put a limit so we don't generate too big a group
            //++count1;
            List<String>gr2 = group(gens2.toArray(new String[gens2.size()]), gsize);
            //System.err.println("found new group order "+gr2.size()+" "+gens2+ " max="+gsize);
            if (gr2.size() > gsize) {
                //System.err.println("too big "+gr2.size()+" "+gsize+" "+gens2);
                continue;
            }
            //System.err.println("found new group order "+gr2.size()+" "+gens2);
            // Doesn't divide, so won't extend properly
            if (gsize % gr2.size() != 0) {
                continue;
            }
            if (gens2.size() == maxdepth && gr2.size() != gsize) {
                // Performance optimisation
                // Early retry as group with two generators isn't right size
                continue;
            }
            
            // Test out the potential group
            //System.err.println("Testing "+gr2.size()+" "+gsize);
            // Use the whole group, apply group to set, must generate new elements
            String gr2First = gr2.iterator().next();
            int f1 = 0;
            for (String g2 : gr2) {
                ++f1;
                if (gr2First.equals(g2))
                    continue;
                if (gensg.contains(g2)) {
                    //if (perftime) System.err.println("In group "+g2);
                    continue;
                }
                int f2 = 0;
                for (String t : all) {
                    ++f2;
                    String nw = rep(t, g2, gr2First);
                    // Identity?
                    if (nw.equals(t)) {
                        continue;
                    }
                    if (all.contains(nw)) {
                        // hasn't generated distinct element
                        if (perftime) {
                            //System.err.println("Failed group test size "+gr2.size()+" list size "+all.size()+" at "+f1+":"+f2+" with "+g2);
                        }
                        continue l;
                    }
                }
            }
            //System.err.println("Found group order "+gr2.size()+" "+gens2);
            
            if (gr2.size() == gsize) {
                // Exact match
                //System.err.println("return "+gsize+" "+gens2+ " " + gens2);
                return gens2;
            }
            if (gens2.size() < maxdepth) {
                // Recurse with single generator as suggestion
                //System.err.println("Recurse at "+ii+" with "+gens2.size()+" "+gr2.size() + " " + gsize + " " + gens2);
                List<String>g2 = findGroup(all, gsize, gr, gens2);
                if (g2.size() > 0) {
                    return g2;
                }
            }
        }
        //System.err.println("Done "+count1);
        return new ArrayList<String>();
    }
    
    /**
     * Order of a permutation.
     */
    private int order(String g, String rounds) {
        int order = 0;
        String nw2 = g;
        do {
            nw2 = rep(nw2, g, rounds);
            order++;
        } while (!nw2.equals(g));
        return order;
    }

    /**
     * 
     */
    private int fixed(String g, String rounds) {
        int fixed = 0;
        for (int i = 0; i < g.length(); ++i) {
            if (g.charAt(i) == rounds.charAt(i))
                ++fixed;
        }
        return fixed;
    }

    /**
     * Finds set of start six-ends for a six which have the same back three bells.
     * @param r back 3 bells
     * @param grp
     * @return set of six-end numbers
     */
    public Set<Integer>sameThree(String r, List<String>grp) {
        Set<Integer>s1 = new LinkedHashSet<Integer>();
        //System.out.println("Same "+r);
        for (int i = 1; i <= NN; ++i) {
            String nm = name(i);
            int off = nm.length() - 2 - r.length();
            
            for (String g : grp) {
                String nmx = rep(nm, g, grp.get(0));
                // for triples, substring(4, 7)
                if (nmx.substring(off,  off + r.length()).equals(r)) {
                    // Use the first six-end to common up instances
                    s1.add(firstsixend(i));
                }
            }
        }
        return s1;
    }
    
    public int maxsize(Set<Set<Integer>>b) {
        int t = 0;
        // If there is only one set then no variables are needed as we have nothing to compare against
        if (b.size() <= 1)
            return 0;
        for (Set<Integer>c : b) {
            t = Math.max(t,  c.size());
        }
        return t;
    }
    
    public int varsForSameBobs(List<String> grp) {
        Set<Set<Set<Integer>>> a = startSixendsForSameBobs(grp);
        return varsForSameBobs(a);
    }
    public int varsForSameBobs(Set<Set<Set<Integer>>>a) {
        int t = 0;
        for (Set<Set<Integer>>b : a) {
            t += maxsize(b);
        }
        return t;
    }
    
    /**
     * Choose items from set
     * @param a The set
     * @param t The number of items
     * @return A set of sets which t different items from a
     */
    public Set<Set<Integer>>chooseAny(Set<Integer>a, int t) {
        Set<Set<Integer>>r = new LinkedHashSet<Set<Integer>>();
        if (t <= 0) {
            r.add(new LinkedHashSet<Integer>());
            return r;
        }
        for (Integer i : a) {
            Set<Integer>rm = new LinkedHashSet<Integer>(a);
            rm.remove(i);
            Set<Set<Integer>>y = chooseAny(rm, t - 1);
            for (Set<Integer>yy : y) {
                yy.add(i);
                r.add(yy);
            }
        }
        return r;
    }
    
    /**
     * 1 3 5
     * all clear => bob
     * @param s
     * @return
     */
    public int[] convNotPlain(Set<Integer>s) {
        int r[] = new int[s.size()*sixTypes];
        int i = 0;
        for (int v : s) {
            // Examine each six-end, and find the call variables
            int cl0 = 0;
            for (int j = v; j <= NN && six(j) == six(v); ++j) {
                int cl = callvaltest(j, 0);
                if (cl != cl0) {
                    r[i++] = -cl;
                    cl0 = cl;
                }
            }
        }
        return r;
    }
    
    /**
     * 2 4 6
     * all clear => plain
     * @param s
     * @return
     */
    public int[] convNotBob(Set<Integer>s) {
        int r[] = new int[s.size()*sixTypes];
        int i = 0;
        for (int v : s) {
            // Examine each six-end, and find the call variables
            int cl0 = 0;
            for (int j = v; j <= NN && six(j) == six(v); ++j) {
                int cl = callvaltest(j, 1);
                if (cl != cl0) {
                    r[i++] = -cl;
                    cl0 = cl;
                }
            }
        }
        return r;
    }

    
    /**
     * Groups of calls which should be bob or plain the same number of times.
     * Doesn't currently work for out of course groups.
     * @param grp
     * @return
     */
    private Set<Set<Set<Integer>>> startSixendsForSameBobs(List<String> grp)
    {
        Set<Set<Set<Integer>>>a=new LinkedHashSet<Set<Set<Integer>>>();
        if (!opt9 || ooc) return a;
        print("c Same number of bobs from set of sixes");
        Set<String>cache=new LinkedHashSet<String>();
        for (int i = 1; i <= NN; ++i) {
            String nm = name(i);
            if (ooc(nm)) continue;
            // Rotate back three bells
            int len = 3;
            int off = nm.length() - 2 - len; // 4 for triples
            int end = off + len;
            
            String r1 = nm.substring(off, end);
            String r2 = nm.substring(off + 1, end)+nm.substring(off, off + 1);
            String r3 = nm.substring(off + 2, end)+nm.substring(off, off + 2);
            
            if (cache.contains(r1)) {
                // Performance
                continue;
            }
            
            cache.add(r1);
            cache.add(r2);
            cache.add(r3);
            
            Set<Integer>s1 = sameThree(r1, grp);
            Set<Integer>s2 = sameThree(r2, grp);
            Set<Integer>s3 = sameThree(r3, grp);
            Set<Set<Integer>>r=new LinkedHashSet<Set<Integer>>();
            r.add(s1);
            r.add(s2);
            r.add(s3);
            if (a.add(r)) {
                print("c "+r);
            }
        }
        return a;
    }
    
    /**
     * Returns sets of sixes which must have the same number of bobs / plains.
     * Sixes ending abc, bca, cab must have the same number of bobs.
     * Consider sel=4
     * 2,4,6 => bobs
     * 1,3,5 => plain
     * so
     * 2,4,6 as clauses, all zero => plain
     * 1,3,5 as clauses, all zero => bob
     * @param grp
     */
    public Set<Set<Set<Integer>>> sameNumberBobs(List<String>grp) {
        Set<Set<Set<Integer>>> a = startSixendsForSameBobs(grp);
        if (!opt9) return a;
        int tvs = firstTVVar;
        for (Set<Set<Integer>>b : a) {
            print("c Same bob count: "+b);
            int tx = maxsize(b);
            for (int t = 1; t <= tx; ++t, ++tvs) {
                for (Set<Integer>c : b) {
                    // Choose any t
                    Set<Set<Integer>>ch1 = chooseAny(c, t);
                    print("c choose "+t+" set "+ch1);
                    for (Set<Integer>s : ch1) {
                        int r[] = convNotPlain(s);
                        print(r, tvs);
                    }
                    Set<Set<Integer>>ch2 = chooseAny(c, tx - t  + 1);
                    print("c choose "+(tx - t + 1)+" not set "+ch2);
                    for (Set<Integer>s : ch2) {
                        int r[] = convNotBob(s);
                        print(r, -tvs);
                    }
                }
            }
        }
        if (tvs != firstTVVar + tv) {
            throw new IllegalStateException("Expected "+tv+" instead of "+(tvs - firstTVVar)+" variables");
        }
        return a;
    }

    /**
     * Use bitonic count to count bobs
     * @param bobCount
     * +ve means at least n bobs
     * -ve means less than -n bobs
     * 0 means no checking
     * -1 means 0 bobs
     * -2 means 0 or 1 bobs
     */
    public void countBobs(int bobCount) {
        if (bobCount == 0) {
            return;
        } else if (bobCount + 1 <= 0) {
            // Fix the encoded limit
            bobCount++;
        }
        /*
         * Somewhat inefficient for COMBINEDCALLTYPE as we count the bobs vars for
         * one six individually.
         */
        int ncalls = sel != COMBINEDCALLTYPE ? N / divb : NN / divb; 
        int vs[] = new int[ncalls];
        int vn = firstCBVar;
        // Walk through six-ends
        for (int i = 0, j = 1; i < ncalls && j <= NN; ++j) {
            vs[i] = callvalset(j, 1);
            if (i == 0 || vs[i] != vs[i - 1]) {
                // new call variable
                ++i;
            }
        }
        print("c summing ncalls="+ncalls+" bob variables "+vs[0]+" to "+vs[ncalls - 1]);
        int vv[][] = sorting(ncalls);
        // Show the progress of adding
        List<String>evalresult = new ArrayList<String>();
        if (eval) {
            evalresult.add(evalVars(vs));
        }
        for (int v[] : vv) {
            int x = v[0];
            int y = v[1];
            int a = vs[x];
            int b = vs[y];
            int c = vn++;
            int d = vn++;
            print("c compare bobs "+x+" "+y);
            //System.out.println("a="+a+" b="+b+" c="+c+" d="+d);
            // both 1 => msb 1
            print(-a, -b, d);
            // both 0 => lsb 0
            print(a, b, -c);
            print(-a, c);
            print(-b, c);
            print(a, -d);
            print(b, -d);
            vs[x] = c;
            vs[y] = d;
            if (eval) {
                if ((vn - firstCBVar) % ncalls == 0) {
                    evalresult.add(evalVars(vs));
                }
            }
        }
        if (eval) {
            print("c Bitonic sort result");
            for (String s : evalresult) {
                print("c "+s);
            }
        }

        if (bobCount > N) {
            print("c reducing bob count "+bobCount+" to number of sixes "+N);
            bobCount = N;
        }
        if (bobCount > 0) {
            print("c bob count at least "+bobCount+". Final total variables start "+vs[0]+" to "+(vs[0]+ncalls-1));
            // At least this many bobs, round up so if ask 3-part >=601 go to 603
            print(vs[(bobCount + divb - 1) / divb - 1]);
        } else if (bobCount / divb > -ncalls) { // also correct for Integer.MIN_VALUE
            print("c bob count at most "+(-bobCount)+". Final total variables start "+vs[0]+" to "+(vs[0]+ncalls-1));
            // No more than this many bobs
            // E.g. bobcount = -6, divb = 5 so we have 0 (OK), 5 (OK), 10 (not OK) bobs
            print(-vs[-bobCount / divb]);
        } else {
            print("c bob count at most "+(-1L*bobCount)+" is not less than the number of sixes "+(divb*ncalls)+", so no limit. Final total variables start "+vs[0]+" to "+(vs[0]+ncalls-1));
        }

        if (opt9) {
            if (divb * group.size() % 3 != 0) {
                print("c force bob count as a multiple of 3");
                for (int i = 0; i < ncalls; i += 3) {
                    // Allow
                    // 000 000 000
                    // 111 000 000
                    // 111 111 000
                    // Do not allow
                    // 100 000 000
                    // 110 000 000
                    print(-vs[i], vs[i + 1]);
                    print(-vs[i], vs[i + 2]);
                }
            }
        }
    }
    
    /**
     * How many six-ends in this six do not have a Q-set recorded.
     * @param i first six-end
     * @param covered
     * @return
     */
    public int uncovered(int i, boolean covered[]) {
        int firstSixend = firstsixend(i);
        int count = 0;
        for (int j = 0; j < sixTypes; ++j) {
            if (!covered[firstSixend + j]) {
                ++count;
            }
        }
        return count;
    }
    
    /**
     * Approximation number, an overestimate.
     * E.g. sted3 is an overestimate.
     * @return
     */
    public int varsForCountQSets() {
        // Distinct Q-sets possible?
        // E.g. 1-part is 840 /3
        // 2-part estimate = 840/2/3=140, actual = 140
        // 3-part estimate = 840/3/3=93 actual = 92
        // 4-part estimate = 840/4/3=70 actual = 70
        // 6-part estimate = 840/6/3=46 actual = 46
        // 12-part estimate = 840/12/3=23 actual = 22
        // 24-part estimate = 840/24/3=11 actual = 11
        // 60-part estimate = 840/12/3=4 actual = 4
        int n = N / 3;
        // Convert to number of variables
        int v = n + sorting(n, null) * 2;
        return v;
    }
    
    /**
     * Generates a string of 0 or 1 depending on whether the input variables
     * are true or not. Used to print out the result of bitonic summing.
     * @param v
     * @return
     */
    public String evalVars(int v[]) {
        StringBuilder sb = new StringBuilder();
        if (eval) {
            for (int vv: v) {
                if (eval(vv)) {
                    sb.append('1');
                } else {
                    sb.append('0');
                }
            }
        }
        return sb.toString();
    }
    
    /**
     * Use bitonic count to count Q-sets
     * @param qsetCount
     * +ve +n means at least n Q-sets
     * -ve means less than n Q-sets
     * 0 means no checking
     * -1 means 0 Q-sets
     * -2 means 0 or 1 Q-sets
     * 
     * return number of variables used
     */
    public int countQSets(int qsetCount) {
        if (qsetCount == 0) {
            return 0;
        } else if (qsetCount + 1 <= 0) {
            // Fix the encoded limit
            qsetCount++;
        }
        long t1 = System.currentTimeMillis();
        print("c counting Q-sets");
        // Keep track of which six-ends as part of Q-set have been counted
        boolean covered[] = new boolean[NN+1];
        //int found[] = new int[N+1];
        int ds[] = new int[sixTypes * 2];
        int nv = 0;
        // Variables for recording Q-set present or not
        int vs[] = new int[N / 3];
        // Walk through all sixes
        for (int f = 0; f < N; ++f) {
            // Find the best next six to record whether Q-sets are present
            // One Q-set variable can record all complete Q-sets based on that six
            int bestv = 0;
            int besti = 0;
         l: for (int i = 1; i <= NN; i += sixTypes) {
                //System.out.println("i="+i+" NN="+NN);
                int c0 = uncovered(i, covered);
                for (int j = 0; j < sixTypes; ++j) {
                    int s = i + j;
                    if (covered[s]) {
                        // Mark as not interesting
                        ds[j*2] = 0;
                        ds[j*2+1] = 0;
                        continue;
                    }
                    // Q-set walk
                    int d1 = prev[next[s][1]][0];
                    int d2 = prev[next[d1][1]][0];
                    int d3 = prev[next[d2][1]][0];
                    if (d3 != s) {
                        // Error
                        throw new IllegalStateException("no Q-set loop "+s+" "+d1+" "+d2+" "+d3);
                    }
                    // Record the other six types as interesting
                    ds[j*2] = d1;
                    ds[j*2+1] = d2;
                }
                Arrays.sort(ds);
                int pv = 0;
                int v = c0 * 2 * sixTypes / 1; // Heuristic quality function
                for (int j = 0; j < ds.length; ++j) {
                    if (ds[j] == pv) {
                        // duplicate so go to next
                        continue;
                    }
                    pv = ds[j];
                    if (pv >= i && pv < i + sixTypes) {
                        // already covered by this six
                        //System.err.println("duplicated "+pv+" "+i);
                        // not a good choice so abort
                        continue l;
                    }
                    int c1 = uncovered(pv,covered);
                    v += c1;
                }
                //v = 1; // no preference
                if (bestv < v) {
                    bestv = v;
                    besti = i;
                }
            }
            if (besti > 0) {
                // Found a candidate six
                //found[f] = besti;
                int qs = 0;
                int qsetVar = firstCQSVar + nv;
                vs[nv] = qsetVar;
                ++nv;
                for (int j = 0; j < sixTypes; ++j) {
                    int s = besti + j;
                    if (!covered[besti + j]) {
                        // New possible Q-set
                        covered[s] = true;
                        int d1 = prev[next[s][1]][0];
                        int d2 = prev[next[d1][1]][0];
                        covered[d1] = true;
                        covered[d2] = true;
                        ++qs;
                        print("c q-set "+s+" "+d1+" "+d2);
                        // set variable
                        int g1[] = genGate(s, 0);
                        int g2[] = genGate(d1, 0);
                        int g3[] = genGate(d2, 0); 
                        // All members present
                        print("c set Q-set variable " + qsetVar);
                        print(g1, g2, g3, qsetVar);
                        // six-end for this six present, but not one of the others present
                        print("c clear Q-set variable " + qsetVar);
                        int vv[][] = new int[1][];
                        vv[0] = g2;
                        chooseOneEach(vv, 1, g1, -qsetVar);
                        vv[0] = g3;
                        chooseOneEach(vv, 1, g1, -qsetVar);
                    } else {
                        // Six-end not counted by this Q-set variable
                        int g1[] = genGate(s, 0);
                        print(g1, -qsetVar);
                    }
                }
                if (perftime) {
                    System.out.println("Found "+f+"/"+N+" "+besti+" "+qs+" q-sets "+(N/3)+" "+nv);
                }
            } else {
                // No more uncovered six-ends
                break;
            }
        }
        long t2 = System.currentTimeMillis();

        // Sorting to count Q-sets
        //System.out.println(Arrays.toString(vs));
        int vn = firstCQSVar + nv;
        print("c summing "+nv+" Q-set variables "+firstCQSVar+" to "+(vn-1));
        int vv[][] = sorting(nv);
        // Show the progress of adding
        List<String>evalresult = new ArrayList<String>();
        if (eval) {
            evalresult.add(evalVars(vs));
        }
        for (int v[] : vv) {
            int x = v[0];
            int y = v[1];
            int a = vs[x];
            int b = vs[y];
            int c = vn++;
            int d = vn++;
            print("c compare Q-sets "+x+" "+y);
            //System.out.println("a="+a+" b="+b+" c="+c+" d="+d);
            // both 1 => msb 1
            print(-a, -b, d);
            // both 0 => lsb 0
            print(a, b, -c);
            print(-a, c);
            print(-b, c);
            print(a, -d);
            print(b, -d);
            vs[x] = c;
            vs[y] = d;
            if (eval) {
                if ((vn - firstCQSVar) % nv == 0) {
                    evalresult.add(evalVars(vs));
                }
            }
        }
        if (eval) {
            print("c Bitonic sort result");
            for (String s : evalresult) {
                print("c "+s);
            }
        }
        if (vn < firstCQSVar + cqs) {
            // spare variables
            if (nextFreeVar >= firstCQSVar && nextFreeVar <= firstCQSVar + cqs) {
                // At the end, so just adjust the total
                nextFreeVar = vn;
            } else {
                // Different range, so set the spares to avoid multiple solutions
                for (int v = vn; v < firstCQSVar + cqs; ++v) {
                    print("c unused variable");
                    print(-v);
                }
            }
        }

        if (qsetCount > nv) {
            print("c reducing Q-set count "+qsetCount+" to maximum number of Q-sets "+nv);
            qsetCount = nv;
        }
        if (qsetCount > 0) {
            print("c Q-set count at least "+qsetCount+". Final total variables start "+vs[0]+" to "+(vs[0]+nv-1));
            print(vs[qsetCount - 1]);
        } else if (qsetCount > -nv) {  // also correct for Integer.MIN_VALUE
            print("c Q-set count at most "+(-qsetCount)+". Final total variables start "+vs[0]+" to "+(vs[0]+nv-1));
            print(-vs[-qsetCount]);
        } else {
            print("c Q-set count at most "+(-1L*qsetCount)+" compared to "+nv+" possible Q-sets, so no limit. Final total variables start "+vs[0]+" to "+(vs[0]+nv-1));
        }
        
        long t3 = System.currentTimeMillis();
        if (perftime) {
            System.out.println("Count Q-sets "+(t2-t1)+"ms "+(t3-t2)+"ms");
        }
        return nv + vv.length * 2;
    }
    
    /**
     * Bitonic sort.
     * @param n number of items to be compared
     * @return return list of pairs of slots which need to be compared (0-based)
     */
    public static int[][] sorting(int n) {
        int nx = sorting(n, null);
        int v[][] = new int[nx][];
        sorting(n, v);
        return v;
    }
    /**
     * Generate the bitonic network.
     * @param n the number of inputs to count
     * @param v a suitable array for the output, or null
     * @return the number of pairs, can be used to allocate v
     */
    static int sorting(int n, int v[][]) {
        int nv = 0;
        for (int s = 2; s < n+n; s *= 2) {
            // Orange box
            for (int a = 0; a < n; a += s) {
                for (int b = 0; b < s / 2; ++b) {
                    int x = a + b;
                    int y = a + s - b - 1;
                    if (x < n && y < n) {
                        if (v != null) {
                            v[nv] = new int[]{x,y};
                        }
                        nv++;
                    }
                }
            }
            // Red box
            for (int t = s / 2; t >= 2; t /= 2) {
                for (int a = 0; a < n; a += t) {
                    for (int b = 0; b < t / 2; ++b) {
                        int x = a + b;
                        int y = a + b + t / 2;
                        if (y < n) {
                            if (v != null) {
                                v[nv] = new int[]{x,y};
                            }
                            nv++;
                        }
                    }
                }
            }
        }
        return nv;
    }
    
    /**
     * Does this string repeat itself?
     * E.g.
     * abbabbab repeats from offset 3.
     * @param The string to be checked
     * @param the offset for the comparison
     */
    public boolean repeated(String str, int off) {
        for (int i = off; i < str.length(); ++i) {
            if (str.charAt(i) != str.charAt(i - off))
                return false;
        }
        return true;
    }
    
    /**
     * Match any of the group aliases of a row
     * 
     */
    String rowGroupMatch(String row, String match) {
        for (String el : group) {
            String s1 = rep(row, el, group.get(0));
            if (s1.matches(match)) {
                return s1;
            }
        }
        return null;
    }
    
    /**
     * Prohibit a touch.
     * *QS
     * 2314567..
     * @param touch Match row, followed by calls
     */
    public void prohibitRun(String touch) {
        print("c Prohibit instance of "+touch);
        /* 2314567QS ??1[456][456][456]?QS */
        Pattern p = Pattern.compile("((?:(?:[1-90E?*]*(?:\\[[1-90E]+\\])?)*)[QS]?[QS]?)(.*)");
        Matcher m = p.matcher(touch);
        m.matches();
        String matchrow = m.group(1);
        // Regex - convert ? to single char wildcard, * to any
        String matchrowregex = matchrow.replace("?", ".").replace("*", ".*").replaceAll("^$", ".*");
        String callseq = m.group(2);
        int runlen = callseq.length();
        List<int[]> gates = new ArrayList<int[]>();
        int visited[] = new int[N];
        l: for (int i = 1; i <= NN; ++i) {
            // Match the row
            String row = name(i);
            String rowmatch = rowGroupMatch(row, matchrowregex);
            if (rowmatch == null) {
                print("c skip "+row+callseq+" as does not match "+matchrow);
                continue;
            }
            print("c consider "+row+callseq+" as "+rowmatch+" matches "+matchrow);
            Arrays.fill(visited, 0);
            gates.clear();
            boolean prevsavesix = false;
            int d = i;
            int j;
            for (j = 0; j < runlen; ++j) {
                visited[six(d)] = d;
                boolean savecall;
                boolean savesix;
                int call;
                char c = callseq.charAt(j);
                switch (c)
                {
                    case '-':
                    case '=':
                        call = 1;
                        savesix = true;
                        savecall = true;
                        break;
                    case '~':
                        // Currently doesn't properly restrict six-type
                        call = 1;
                        savesix = true;
                        savecall = false;
                        break;
                    case '_':
                        call = 1;
                        savesix = false;
                        savecall = true;
                        break;
                    case '.':
                        call = 1;
                        savesix = false;
                        savecall = false;
                        break;
                    case 'P':
                    case 'p':
                        call = 0;
                        savesix = true;
                        savecall = true;
                        break;
                    case 'r':
                        call = 0;
                        savesix = true;
                        savecall = false;
                        break;
                    case 'R':
                        call = 0;
                        savesix = false;
                        savecall = true;
                        break;
                    case 'L':
                        call = 0;
                        savesix = false;
                        savecall = false;
                        break;
                    default:
                        throw new IllegalArgumentException(""+c+"\n"+callseq);
                }
                int callval2 = savecall ? callvaltest(d, call) : 0;
                if (savesix && !prevsavesix) {
                    int gate[] = genGate(d, callval2);
                    gates.add(gate);
                } else {
                    gates.add(new int[] {callval2});
                }
                // We know the six type if the six type is fixed, or the previous was and the call is known
                prevsavesix = (savesix || prevsavesix) && savecall;
                d = next[d][call];
                if (visited[six(d)] == d) {
                    // loop but repeated so exit
                    if (repeated(callseq, j + 1)) {
                        print("c loop so omit repeated "+callseq.substring(j + 1));
                        j++;
                        break;
                    } else {
                        if (!callseq.matches("[LrRpP.~_=-]+"))
                            throw new IllegalArgumentException(""+callseq.replaceAll("[LrRpP.~_=-]", "")+"\n"+callseq);
                        print("c impossible as loop with different following sequence "+row+callseq.substring(j + 1));
                        continue l;
                    }
                } else if (visited[six(d)] != 0) {
                    print("c impossible as loop to different six-end "+row+callseq.substring(0, j+1)+" "+name(d)+" was "+name(visited[six(d)]));
                    continue l;
                }
            }
            print("c prohibit "+row+callseq.substring(0, j));
            if (gates.isEmpty())
                print(genGate(i, 0));
            else
                print(gates.toArray());
        }
    }
    
    /**
     * Need P--P
     * P--P => set var
     * P--- => clear
     * P-P  => clear
     * PP   => clear
     * -    => clear
     * @param callseq
     * @param startvar
     * @return array of counting variables
     */
    public int[] requireRun(String touch, int startvar) {
        boolean oldmode1 = false;
        boolean oldmode2 = false;
        if (touch.equals(""))
            return new int[0];
        print("c Require instance of "+touch+" using counting variables from "+startvar+" to "+(startvar+N-1));
        /* 2314567QS ??1[456][456][456]?QS */
        Pattern p = Pattern.compile("((?:(?:[1-90E?*]*(?:\\[[1-90E]+\\])?)*)[QS]?[QS]?)(.*)");
        Matcher m = p.matcher(touch);
        m.matches();
        String matchrow = m.group(1);
        // Regex - convert ? to single char wildcard, * to any
        String matchrowregex = matchrow.replace("?", ".").replace("*", ".*").replaceAll("^$",".*");
        String callseq = m.group(2);
        int runlen = callseq.length();
        List<int[]> gates = new ArrayList<int[]>();
        int all[] = new int[N];
        int visited[] = new int[N];
        l: for (int i = 1; i <= NN; ++i) {
            // Match the row
            String row = name(i);
            // Can share the counting variable per six
            int var = startvar + six(i);
            Arrays.fill(visited, 0);
            gates.clear();
            boolean prevsavesix = false;
            int [] gate = genGate(i, 0);
            String rowmatch = rowGroupMatch(row, matchrowregex);
            if (rowmatch == null) {
                print("c skip "+row+callseq+" as does not match "+matchrow);
                print(gate, -var);
                continue;
            }
            print("c consider "+row+callseq+" as "+rowmatch+" matches "+matchrow);
            int d = i;
            /*
             * See if only one six-type matches.
             * If more than one matches, then we must check the starting six
             * before clearing the count var
             */
            int firstgate [] = new int[]{};
            for (int d1 = firstsixend(d); d1 <= NN && six(d1) == six(d); ++d1) {
                if (d1 == d)
                    continue;
                if (rowGroupMatch(name(d1), matchrowregex) != null)
                    firstgate = gate;
            }
            int j;
            for (j = 0; j < runlen; ++j) {
                visited[six(d)] = d;
                boolean savesix;
                boolean savecall;
                int call;
                char c = callseq.charAt(j);
                switch (c)
                {
                    case '-':
                    case '=':
                        call = 1;
                        savesix = true;
                        savecall = true;
                        break;
                    case '~':
                        // Currently doesn't properly restrict six-type
                        call = 1;
                        savesix = true;
                        savecall = false;
                        break;
                    case '_':
                        call = 1;
                        savesix = false;
                        savecall = true;
                        break;
                    case '.':
                        call = 1;
                        savesix = false;
                        savecall = false;
                        break;
                    case 'P':
                    case 'p':
                        call = 0;
                        savesix = true;
                        savecall = true;
                        break;
                    case 'r':
                        call = 0;
                        savesix = true;
                        savecall = false;
                        break;
                    case 'R':
                        call = 0;
                        savesix = false;
                        savecall = true;
                        break;
                    case 'L':
                        call = 0;
                        savesix = false;
                        savecall = false;
                        break;
                    default:
                         throw new IllegalArgumentException(""+c+"\n"+callseq);
                }
                // Prohibit any run which ends with the opposite call
                if (savecall)
                {
                    print("c do not count "+row+callseq.substring(0, j)+(call == 1 ? "P" : "-"));
                    int callval2 = callvaltest(d, 1 - call);
                    if (savesix && !prevsavesix) {
                        gate = genGate(d, callval2);
                        gates.add(gate);
                    } else {
                        gates.add(new int[] {callval2});
                    }
                    gates.add(new int[] {-var});
                    if (oldmode1) {
                        print(gates.toArray());
                    } else {
                        print(firstgate, callval2, -var);
                    }
                    // Remove the test for a bad type/call and setting the var
                    gates.remove(gates.size() - 1);
                    gates.remove(gates.size() - 1);
                }
                if (j > 0 && savesix && !prevsavesix) {
                    print("c do not count "+row+callseq.substring(0, j)+" not "+callseq.charAt(j));
                    for (int d1 = firstsixend(d); d1 <= NN && six(d1) == six(d); ++d1) {
                        if (d1 == d)
                            continue;
                        gate = genGate(d1, 0);
                        gates.add(gate);
                        gates.add(new int[] {-var});
                        if (oldmode2) {
                            print(gates.toArray());
                        } else {
                            print(firstgate, gate, -var);
                        }
                        // Remove the test for a bad type/call and setting the var
                        gates.remove(gates.size() - 1);
                        gates.remove(gates.size() - 1);
                    }
                }
                // Enforce
                int callval3 = savecall ? callvaltest(d, call) : 0;
                if (savesix && !prevsavesix) {
                    gate = genGate(d, callval3);
                } else {
                    gate = new int[]{callval3};
                }
                gates.add(gate);
                // We know the six type if the six type is fixed, or the previous was and the call is known
                prevsavesix = (savesix || prevsavesix) && savecall;
                d = next[d][call];
                if (visited[six(d)] == d) {
                    // loop
                    // loop but repeated so exit
                    if (repeated(callseq, j + 1)) {
                        print("c loop so omit repeated "+callseq.substring(j + 1));
                        j++;
                        break;
                    } else {
                        if (!callseq.matches("[LrRpP.~_=-]+"))
                            throw new IllegalArgumentException(""+callseq.replaceAll("[LrRpP.~_=-]", "")+"\n"+callseq);
                        print("c impossible as loop with different following sequence "+callseq.substring(j + 1));
                        gates.add(new int[] {-var});
                        print(gates.toArray());
                        continue l;
                    }
                } else if (visited[six(d)] != 0) {
                    print("c impossible as loop to different six-end "+row+callseq.substring(0, j+1)+" "+name(d)+" was "+name(visited[six(d)]));
                    gates.add(new int[] {-var});
                    print(gates.toArray());
                    continue l;
                }
            }
            print("c count "+row+callseq.substring(0, j));
            // For just 2314567QS we do need to test the six-end
            if (gates.isEmpty())
                gates.add(gate);
            gates.add(new int[] {var});
            print(gates.toArray());
            all[six(i)] = var;
        }
        return all;
    }

    /**
     * Used to count how similar to an example touch
     * Need P--P
     * P--P => set var
     * P--- => clear
     * P-P  => clear
     * PP   => clear
     * -    => clear
     * @param callseq
     * @param startvar
     * @return array of counting variables
     */
    public int[] similarRun(String touch, int startvar) {
        if (touch.equals(""))
            return new int[0];
        print("c Match similar instance of "+touch+" using counting variables from "+startvar+" to "+(startvar+N-1));
        int all[] = new int[N];
        /* 2314567QSP-=.L*1(1) */
        Pattern p = Pattern.compile("([1-90E]+[QS][QS])([p=r~L.R_P-]+)\\*(\\d+(\\.\\d+)?)\\(\\d+\\)(.*)");
        do
        {
            Matcher m = p.matcher(touch);
            if (!m.matches())
            {
                throw new IllegalArgumentException(touch);
            }
            String matchrow = m.group(1);
            String callseq = m.group(2);
            float fct = Float.parseFloat(m.group(3));
            callseq = expandTouch("", callseq, fct);
            touch = m.group(5);
            int runlen = callseq.length();
            List<int[]> gates = new ArrayList<int[]>();
            int visited[] = new int[N];
            // Match the row
            int i = findName(matchrow, group);
            String row = name(i);
            Arrays.fill(visited, 0);
            gates.clear();
            int [] gate = genGate(i, 0);
            print("c consider "+row+callseq);
            int d = i;
            /*
             * See if only one six-type matches.
             * If more than one matches, then we must check the starting six
             * before clearing the count var
             */
            int firstgate [] = new int[]{};
            int j;
            for (j = 0; j < runlen; ++j) {
                // Counting variable per six
                int var = startvar + six(d);
                all[six(d)] = var;
                visited[six(d)] = d;
                gates.clear();
                boolean savesix;
                boolean savecall;
                int call;
                char c = callseq.charAt(j);
                switch (c)
                {
                case '-':
                case '=':
                    call = 1;
                    savesix = true;
                    savecall = true;
                    break;
                case '~':
                    call = 1;
                    savesix = true;
                    savecall = false;
                    break;
                case '_':
                    call = 1;
                    savesix = false;
                    savecall = true;
                    break;
                case '.':
                    call = 1;
                    savesix = false;
                    savecall = false;
                    break;
                case 'P':
                case 'p':
                    call = 0;
                    savesix = true;
                    savecall = true;
                    break;
                case 'r':
                    call = 0;
                    savesix = true;
                    savecall = false;
                    break;
                case 'R':
                    call = 0;
                    savesix = false;
                    savecall = true;
                    break;
                case 'L':
                    call = 0;
                    savesix = false;
                    savecall = false;
                    break;
                default:
                    throw new IllegalArgumentException(""+c+"\n"+callseq);
                }
                // Prohibit any run which ends with the opposite call
                if (savecall)
                {
                    print("c do not count "+row+callseq.substring(0, j)+(call == 1 ? "P" : "-"));
                    int callval2 = callvaltest(d, 1 - call);
                    if (savesix) {
                        gate = genGate(d, callval2);
                        gates.add(gate);
                    } else {
                        gates.add(new int[] {callval2});
                    }
                    gates.add(new int[] {-var});
                    print(firstgate, callval2, -var);
                }
                if (savesix) {
                    print("c do not count "+row+callseq.substring(0, j)+" not "+callseq.charAt(j));
                    for (int d1 = firstsixend(d); d1 <= NN && six(d1) == six(d); ++d1) {
                        if (d1 == d)
                            continue;
                        gate = genGate(d1, 0);
                        gates.add(gate);
                        gates.add(new int[] {-var});
                        print(firstgate, gate, -var);
                    }
                }
                // Enforce
                print("c count "+row+callseq.substring(0, j));
                int callval3 = savecall ? callvaltest(d, call) : 0;
                if (savesix) {
                    gate = genGate(d, callval3);
                } else {
                    gate = new int[]{callval3};
                }
                gates.add(gate);
                print(firstgate, gate, var);
                d = next[d][call];
                if (visited[six(d)] == d) {
                    // loop
                    // loop but repeated so exit
                    if (repeated(callseq, j + 1)) {
                        print("c loop so omit repeated "+callseq.substring(j + 1));
                        j++;
                        break;
                    } else {
                        if (!callseq.matches("[LrRpP.~_=-]+"))
                            throw new IllegalArgumentException(""+callseq.replaceAll("[LrRpP.~_=-]", "")+"\n"+callseq);
                        print("c impossible as loop with different following sequence "+callseq.substring(j + 1));
                        gates.add(new int[] {-var});
                        print(gates.toArray());
                        break;
                    }
                } else if (visited[six(d)] != 0) {
                    print("c impossible as loop to different six-end "+row+callseq.substring(0, j+1)+" "+name(d)+" was "+name(visited[six(d)]));
                    gates.add(new int[] {-var});
                    print(gates.toArray());
                    break;
                }
            }
        }
        while (!touch.isEmpty());
        // Check for unused variables
        boolean foundUnused = false;
        for (int i = 0; i < N; ++i) {
            if (all[i] == 0) {
                if (!foundUnused) {
                    print("c unused variables for matching");
                    foundUnused = true;
                }
                print(-(startvar + i));
            }
        }
        return all;
    }
    
    /**
     * Use bitonic count to count run
     * @param vars variables to be counted (some may be 0)
     * @param rCount encoded run count
     * @param mCount maximum run count
     * @param exact equality rather than greater than or equal to etc.
     * @Param vn0 first new variable to use
     * +ve means at least n runs
     * -ve means less than -n runs
     * -1 means 0 bobs
     * -2 means 0 or 1 bobs
     */
    public int countRuns(int vars[], int rCount, int mCount, boolean exact, int vn0) {
        int ci = 0;
        for (int i : vars) {
            if (i != 0)
                ++ci;
        }
        int nruns = ci;
        int vs[] = new int[nruns];
        ci = 0;
        for (int i : vars) {
            if (i != 0)
                vs[ci++] = i;
        }
        int vn = vn0;
        if (nruns > 0)
            print("c summing nruns="+nruns+" variables "+vs[0]+" to "+vs[nruns - 1]);
        else {
            print("c no runs to sum");
            if (rCount > 0) {
                print("c impossible to have " + rCount + " runs");
                print();
            }
            return vn - vn0;
        }
        int vv[][] = sorting(nruns);
        // Show the progress of adding
        List<String>evalresult = new ArrayList<String>();
        if (eval) {
            evalresult.add(evalVars(vs));
        }
        for (int v[] : vv) {
            int x = v[0];
            int y = v[1];
            int a = vs[x];
            int b = vs[y];
            int c = vn++;
            int d = vn++;
            print("c compare runs "+x+" "+y);
            //System.out.println("a="+a+" b="+b+" c="+c+" d="+d);
            // both 1 => msb 1
            print(-a, -b, d);
            // both 0 => lsb 0
            print(a, b, -c);
            print(-a, c);
            print(-b, c);
            print(a, -d);
            print(b, -d);
            vs[x] = c;
            vs[y] = d;
            if (eval) {
                if ((vn - vn0) % nruns == 0) {
                    evalresult.add(evalVars(vs));
                }
            }
        }
        if (eval) {
            print("c Bitonic sort result");
            for (String s : evalresult) {
                print("c "+s);
            }
        }

        if (rCount > nruns) {
            print("c reducing run count "+rCount+" to number of possible runs "+nruns);
            rCount = nruns;
        }
        if (rCount < -nruns - 1) {
            print("c reducing run count "+(-rCount - 1)+" to number of possible runs "+nruns);
            rCount = -nruns - 1;
        }
        if (mCount > nruns) {
            print("c reducing range run count "+mCount+" to number of possible runs "+nruns);
            mCount = nruns;
        }
        if (exact)
        {
            if (rCount >= 0) {
                print("c run count exactly "+rCount+". Final total variables start "+vs[0]+" to "+(vs[0]+nruns-1));
                if (rCount > 0)
                    print(vs[rCount - 1]);
                if (rCount < nruns)
                    print(-vs[rCount]);
            } else {
                print("c run count not equal to "+(-rCount - 1)+". Final total variables start "+vs[0]+" to "+(vs[0]+nruns-1));
                if (-rCount - 1 == 0)
                    print(vs[-rCount - 1]);
                else if (-rCount - 1 < nruns)
                    print(-vs[-rCount - 2], vs[-rCount - 1]);
                else
                    print(-vs[-rCount - 2]);
            }
        } else {
            if (rCount >= 0) {
                if (rCount > 0) {
                    print("c run count at least "+rCount+". Final total variables start "+vs[0]+" to "+(vs[0]+nruns-1));
                    print(vs[rCount - 1]);
                }
                if (mCount > rCount && mCount < nruns)
                {
                    print("c run count at most "+(mCount)+". Final total variables start "+vs[0]+" to "+(vs[0]+nruns-1));
                    // No more than this many runs
                    print(-vs[mCount]);
                }
            } else if (-rCount - 1 < nruns){
                print("c run count at most "+(-rCount - 1)+". Final total variables start "+vs[0]+" to "+(vs[0]+nruns-1));
                // No more than this many runs
                print(-vs[-rCount - 1]);
            }
        }
        return vn - vn0;
    }

    public int callsequence(String s, int v) {
        int r;
        if (s.startsWith("!")) {
            prohibitRun(s.substring(1));
            r = 0;
        } else {
            // >2:PPP
            // >=2
            // 2:2314567??=P
            // !=2:
            int i = s.indexOf(':');
            int cnt = 0;
            int mcnt = 0;
            boolean exact = false;
            if (i > 0)
            {
                /*
                 * >=12:
                 * >10:
                 * <10:
                 * <=8:
                 * !=3:
                 * =3:
                 */
                if (s.startsWith(">=")) {
                    cnt = Integer.parseInt(s.substring(2, i));
                    exact = false;
                } else if (s.startsWith("<=")) {
                    cnt = -Integer.parseInt(s.substring(2, i)) - 1;
                    exact = false;
                } else if (s.startsWith("<>")) {
                    cnt = -Integer.parseInt(s.substring(2, i)) - 1;
                    exact = true;
                } else if (s.startsWith(">")) {
                    cnt = Integer.parseInt(s.substring(1, i)) + 1;
                    exact = false;
                } else if (s.startsWith("<")) {
                    cnt = -Integer.parseInt(s.substring(1, i));
                    exact = false;
                } else if (s.startsWith("=")) {
                    cnt = Integer.parseInt(s.substring(1, i));
                    exact = true;
                } else {
                    int j = s.substring(0, i).indexOf("..");
                    if (j == -1) {
                        cnt = Integer.parseInt(s.substring(0, i));
                        exact = false;
                    } else {
                        cnt = Integer.parseInt(s.substring(0, j));
                        mcnt = Integer.parseInt(s.substring(j + 2, i));
                        exact = cnt == mcnt;
                    }
                }
                s = s.substring(i + 1);
            }
            int all[] = s.contains(")") ? similarRun(s, v) : requireRun(s, v);
            r = all.length;
            if (all.length > 0) {
                if (cnt == 0 && !exact && mcnt == 0) {
                    // At least one
                    print("c Require at least one run");
                    print(all);
                } else {
                    r += countRuns(all, cnt, mcnt, exact, v + all.length);
                }
            }
        }
        return r;
    }
    
    public void callsequences(String seqs) {
        int c = firstCBRun;
        for (String s : seqs.split(";")) {
            c += callsequence(s, c);
        }
        if (c < firstCBRun + cbrun) {
            // spare variables
            if (nextFreeVar >= firstCBRun && nextFreeVar <= firstCBRun + cbrun) {
                // At the end, so just adjust the total
                nextFreeVar = c;
            } else {
                // Different range, so set the spares to avoid multiple solutions
                for (int v = c; v < firstCBRun + cbrun; ++v) {
                    print("c unused variable");
                    print(-v);
                }
            }
        }
    }
    
    /**
     * Decode a limit.
     * 30 - at least 30
     * -30 - 30 or fewer
     * 0 no restriction
     * -0 0 or fewer
     * -1 1 or fewer
     * @param s the command line argument
     * @return the +ve number or n-1 for a negative number or -0
     */
    public static int decodeLimit(String s) {
        int ret = Integer.decode(s);
        if (s.startsWith("-")) {
            ret -= 1;
        }
        return ret;
    }

    public void palindrome(String palindrome) {
        if (palindrome.length() == 0)
            return;
        String rounds = "1234567890E".substring(0, palindrome.length());
        print("c Using palindrome " + palindrome);
        for (int s = 1; s <= NN; ++s) {
            String sixend = name(s);
            String sixhead = sixhead(sixend);
            String ps = rep(sixhead, palindrome, rounds);
            String ps2 = sixhead(ps);
            print("c six-heads "+sixhead+" "+ps);
            print("c six-ends  "+sixend+" "+ps2);
            int d1 = findName(ps, group);
            boolean reverse = true;
            if (d1 > NN) {
                d1 = findName(ps2, group);
                reverse = false;
            }
            boolean forcebob = palindrome.contains("-");
            if (six(s) == six(d1)) {
                if (s == d1) {
                    print("c apex " + s + " " + sixend);
                    if (forcebob) {
                        // Force call and six-type
                        for (int c = 0; c < 2; ++c) {
                            int call = callvaltest(s, c);
                            int gates[] = genGate(s, call);
                            int p = reverse ? prev[d1][c] : d1;
                            if (forcebob) {
                                if (next[s][c] != p)
                                {
                                    int vald[] = genVal(p);
                                    for (int dx : vald) {
                                        if (dx != 0)
                                            print(gates, dx);
                                    }
                                }
                                int dx = callvalset(p, c);
                                if (dx != -call)
                                    print(gates, dx);
                            }
                        }
                    }
                } else {
                    // Disallow
                    print("c disallow invalid apex " + s + " " + sixend + " "+ d1 + " " + name(d1));
                    int gate[] = genGate(s, 0);
                    print(gate);
                }
            } else {
                // Force same six-type and possibly call
                print("c palindrome force same " + s + " " + sixend + " " + d1 + " " + name(d1) +" reverse="+reverse);
                // Check this works for COMBINEDCALLTYPE
                if (sel != COMBINEDCALLTYPE || !forcebob) {
                    // Force six-type
                    int gates[] = genGate(s, 0);
                    int vald[] = genVal(d1);
                    for (int dx : vald) {
                        if (dx != 0)
                            print(gates, dx);
                    }
                }
                if (forcebob) {
                    // Force call and six-type
                    for (int c = 0; c < 2; ++c) {
                        int call = callvaltest(s, c);
                        int gates[] = genGate(s, call);
                        int p = reverse ? prev[d1][c] : d1;
                        if (forcebob) {
                            if (next[s][c] != p)
                            {
                                int vald[] = genVal(p);
                                for (int dx : vald) {
                                    if (dx != 0)
                                        print(gates, dx);
                                }
                            }
                            int dx = callvalset(p, c);
                            if (dx != -call)
                                print(gates, dx);
                        }
                    }
                }
            }
        }
    }

    public String sixhead(String sixend)
    {
        char b[] = sixend.toCharArray();
        if (b[b.length - 2] == 'Q') {
            place(b, 3, sixend.length() - 2);
        } else if (b[b.length - 2] == 'S') {
            place(b, 1, sixend.length() - 2);
        } else {
            throw new IllegalArgumentException(sixend);
        }
        return new String(b);
    }

    public static void main(String args[]) throws IOException {
        if (args.length == 0) {
            System.out.println("Stedman/Erin Triples to SAT converter");
            System.out.println("Copyright (c) 2015,2023 Andrew Johnson");
            System.out.println("StedToSAT <link file> [<initial touch>] [<short loop length>] [<satout file>]\n"
                            + " [<options:int>] [<generated sat file>]\n"
                            + " [<bob count: +ve means >=, -ve means <= >]\n"
                            + " [<divide six type>] [<divide call type>]\n"
                            + " [Q-set count: +ve means >=, -ve means <= >]\n"
                            + " [<touch restrictions>[;<touch>]: <touch>=[!][row][calls] ! means not allowed]\n"
                            + " [<palindrome> | <palindrome>- palindrome fixing six-types or six & call-types]"
                            );
            System.exit(1);
        }
        int fix = args.length > 2 && args[2].length() > 0 ? Integer.parseInt(args[2]) : Integer.MAX_VALUE;
        int opts = args.length > 4 && args[4].length() > 0 ? Long.decode(args[4]).intValue() : 0;
        String outfile = args.length > 5 ? args[5] : "-";
        int bobcount;
        if (args.length > 6 && args[6].length() > 0) {
            bobcount = decodeLimit(args[6]);
        } else {
            bobcount = 0;
        }
        int divg = args.length > 7 && args[7].length() > 0 ? Integer.parseInt(args[7]) : 1;
        int divb = args.length > 8 && args[8].length() > 0 ? Integer.parseInt(args[8]) : 1;
        int qscount;
        if (args.length > 9 && args[9].length() > 0) {
            qscount = decodeLimit(args[9]);
        } else {
            qscount = 0;
        }
        
        int countrunvars = 0;
        int countedruns = 0;
        String matchcalls = "";
        if (args.length > 10 && args[10].length() > 0) {
            
            /*
             * !P-P  not plain, bob, plain
             * ----- at least one bob bob bob bob bob
             * !P-P;P---P;P-----P
             * 
             * 
             */
            String s[] = args[10].split(";");
            for (String s2 : s) {
                if (s2.length() > 0 && !s2.startsWith("!")) {
                    ++countrunvars;
                    if (s2.contains(":"))
                        ++countedruns;
                }
            }
            matchcalls = args[10];
        }
        String palindrome = "";
        if (args.length > 11)
            palindrome = args[11];
        StedToSAT s = new StedToSAT(args[0], fix, bobcount, qscount, opts, divg, divb, countrunvars, countedruns, outfile);

        /*
        String part = "pp===ppp==p=p====pppp==p==p====pp=p==p=pp=~_~====pprpp==~R=p==pp==p=pppp=r=rpp=p==p=";
        String touch2 = "2314567QSpp===ppp==p=p====pppp==p==p====pp=p==p=pp=~_~====pprpp==~R=p==pp==p=pppp=r=rpp=p==p=";
        touch = "2314567QSpp===ppp==p=p====pppp==p==p====pp=p==p=pp"; // "=~_~====pprpp==~R=p==pp==p=pppp=r=rpp=p==p=";
        touch = "2314567QSpp===ppp==p=p====pppp"; // 30 seconds!
        touch = "2314567QSpp===ppp==p=p====ppp"; // 12 seconds now for 5 part
        touch = "2314567QS"+part+"*2(1)";
        */
        String touch;
        if (args.length > 1) {
            touch = args[1];
        } else {
            touch = "";
        }

        
        // Dry run to count variables, clauses
        s.setPrintMode(false);
        // Needed here to find the starting six
        s.encodeMultiTouch(touch);
        s.gen();
        s.sameNumberBobs(s.group);
        s.callsequences(matchcalls);
        s.countBobs(bobcount);
        s.countQSets(qscount);
        s.palindrome(palindrome);
        
        if (args.length > 3 && args[3].length() > 0) {
            // Set the variables from the last solution in .satout - allows checking while generating clauses
            s.eval = s.loadVars(args[3], s.group.size());
        }
        
        // Actually generate CNF
        s.setPrintMode(true);
        s.gen();
        s.sameNumberBobs(s.group);
        s.callsequences(matchcalls);
        s.countBobs(bobcount);
        s.countQSets(qscount);
        s.palindrome(palindrome);
        // Now print the restrictions for the touch at the end
        s.encodeMultiTouch(touch);
        
        if (args.length > 3 && args[3].length() > 0) {
            // Print out the touch discovered from the .satout file
            s.loadVars(args[3], s.group.size());
        }
        s.close();
    }
    /**
     * Whether to print or count the variables/clauses.
     * @param mode
     */
    private void setPrintMode(boolean mode)
    {
        if (mode) {
            print = true;
        } else {
            clauses = 0;
        }
    }
}
