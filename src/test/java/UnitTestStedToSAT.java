/**
 * (c) Copyright 2018,2021 Andrew Johnson
 * Unit tests for StedToSAT
 * Particulars LFSR generation
 */
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

public class UnitTestStedToSAT {

    @Test
    public void testTapsLen8_0() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 0, 0, 0, 0, 1, 1, 0, 0, "-");
        int tl = ss.tapsLen(8, 0);
        assertThat(tl, equalTo(255));
    }

    @Test
    public void testTapsLen8_2() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 0, 0, 0, 0, 1, 1, 0, 0, "-");
        int tl = ss.tapsLen(8 + 2, 2);
        assertThat(tl, equalTo(255 * 4));
    }

    @Test
    public void testTapsBB() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        assertThat(ss.bb, equalTo(8));
    }

    @Test
    public void testTapsBB2() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        int taps[] = ss.lfsrTaps(8, 255, false, true, false, false);
        boolean b[] = ss.initLFSR(taps);
        List<Set<Integer>>states = ss.illegalStates(0, b, taps);
        int total = 0;
        for (Set<Integer>s : states) {
            total += s.size();
        }
        assertThat(total, equalTo(1 << ss.bb));
    }

    @Test
    public void testTapsBB3() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        int taps[] = ss.lfsrTaps(8, 255, false, true, false, false);
        boolean b[] = ss.initLFSR(taps);
        List<Set<Integer>>states = ss.illegalStates(0, b, taps);
        b = ss.initLFSR(taps);
        boolean b1[] = ss.initLFSR(taps);
        for (int i = 1; i < 255; ++i) {
            int j = ss.stateNum(b);
            assertThat("i="+i, true, equalTo(states.get(0).contains(j)));
            ss.nextStateLFSR(b, taps);
            assertThat("Iteration: "+i, b, not(b1));
        }
    }

    /**
     * Check that an error on closing the print stream is detected.
     * @throws IOException
     */
    @Test
    public void testPrintError() throws IOException {
        File of = File.createTempFile("readonly", ".sat");
        StedToSAT ss = null;
        IOException c = null;
        ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, of.getPath());
        ss.encodeMultiTouch("");
        ss.gen();
        // Force an error to be detected on close
        ss.sysout.close();
        ss.sysout.append('x');
        try {
            ss.close();
        } catch (IOException e) {
            c = e;
        } finally {
            try {
                ss.close();
            } catch (IOException e) {
                // Ignore errors on tidy-up
            }
            assertTrue(of.getPath(), of.delete());
        }
        assertNotNull(c);
    }

    @Rule
    public ErrorCollector collector = new ErrorCollector();

    /**
     * standard counter, no twisted ring, allow short cycles
     * @throws IOException
     */
    @Test
    public void testTapsBB4() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        for (int n = 2 ; n <= 1023; ++n) {
            int taps[] = ss.lfsrTaps(32-Integer.numberOfLeadingZeros(n), n, false, true, false, false);
            //System.out.println("   n="+n+" t="+taps.length+" "+Arrays.toString(taps));
            // Minimal or top-bottom counter
            if (n > 217 && n <=255) {
                collector.checkThat("n="+n, taps.length, anyOf(equalTo(2),equalTo(3)));
            } else {
                collector.checkThat("n="+n, taps.length, equalTo(2));
            }
            
        }
    }
    
    /**
     * When using twisted ring we can always get enough - except for 
     * 128 - 254 can used twisted + smaller counter
     * 
     * @throws IOException
     */
    @Test
    public void testTapsBB5() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        for (int n = 2 ; n <= 1023; ++n) {
            int taps[] = ss.lfsrTaps(32-Integer.numberOfLeadingZeros(n), n, true, true, false, false);
            //System.out.println("n="+n+" t="+taps.length+" tw "+Arrays.toString(taps));
            if (n == 255) {
                collector.checkThat("n="+n, taps.length, anyOf(equalTo(2),equalTo(3)));
            } else {
                collector.checkThat("n="+n, taps.length, equalTo(2));
            }
            
        }
    }

    /**
     * don't allow short cycles for LFSR
     * @throws IOException
     */
    @Test
    public void testTapsBB6() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        for (int n = 2 ; n <= 1023; ++n) {
            int taps[] = ss.lfsrTaps(32-Integer.numberOfLeadingZeros(n), n, true, false, false, false);
            //System.out.println("n="+n+" t="+taps.length+" allow short "+Arrays.toString(taps));
            if (n == 255) {
                collector.checkThat("n="+n, taps.length, anyOf(equalTo(2),equalTo(3)));
            } else {
                collector.checkThat("n="+n, taps.length, equalTo(2));
            }
        }
    }
    
    /**
     * No twisted ring, no short cycles
     * @throws IOException
     */
    @Test
    public void testTapsBB7() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        for (int n = 2 ; n <= 1023; ++n) {
            int taps[] = ss.lfsrTaps(32-Integer.numberOfLeadingZeros(n), n, false, false, false, false);
            //System.out.println("n="+n+" t="+taps.length+" allow short "+Arrays.toString(taps));
            if (n >= 128 && n <=255) {
                collector.checkThat("n="+n, taps.length, anyOf(equalTo(2),equalTo(3)));
            } else {
                collector.checkThat("n="+n, taps.length, equalTo(2));
            }
            
        }
    }
    

    /**
     * no different when allowing twisted ring and permitting/not short cycles
     * @throws IOException
     */
    @Test
    public void testTapsBB8() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        for (int n = 2 ; n <= 1023; ++n) {
            int taps1[] = ss.lfsrTaps(32-Integer.numberOfLeadingZeros(n), n, true, false, false, false);
            int taps2[] = ss.lfsrTaps(32-Integer.numberOfLeadingZeros(n), n, true, true, false, false);
            if (!(n >= 128*4 && n <= 217*4)) {
                collector.checkThat("n="+n, Arrays.toString(taps1), equalTo(Arrays.toString(taps2)));
            }
        }
    }
    
    /**
     * no different when not allowing twisted ring and permitting/not short cycles
     * @throws IOException
     */
    @Test
    public void testTapsBB9() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        for (int n = 2 ; n <= 1023; ++n) {
            //assumeThat("n="+n, false, equalTo(n >= 128 && n <= 217));
            int taps1[] = ss.lfsrTaps(32-Integer.numberOfLeadingZeros(n), n, false, false, false, false);
            int taps2[] = ss.lfsrTaps(32-Integer.numberOfLeadingZeros(n), n, false, true, false, false);
            if (!(n >= 128 && n <= 217)) {
                collector.checkThat("n="+n, Arrays.toString(taps1), equalTo(Arrays.toString(taps2)));
            }
        }
    }
    
    /**
     * allow twisted ring and check actually twisted
     * @throws IOException
     */
    @Test
    public void testTapsBB10() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        for (int n = 8 ; n <= 1023; ++n) {
            int bb = 32-Integer.numberOfLeadingZeros(n);
            int taps[] = ss.lfsrTaps(bb, n, true, true, false, false);
            //System.out.println("   n="+n+" t="+taps.length+" "+Arrays.toString(taps));
            int tw = bb - taps[0];
            int expected = 2;
            if (n == (1 << bb) - 1) expected = 0;
            if (n == (1 << bb) - 2) expected = 1;
            if (n == (1 << bb) - 3) expected = 1;
            // 217 length counter
            if (n == 509 || n == 510) expected = 0;
            if (n > 217*4 && n <= 255*4) expected = 1;
            collector.checkThat("n="+n, tw, equalTo(expected));
        }
    }
    
    /**
     * Check that all the lengths needed for a Hamiltonian cycle group search
     * only require 2 taps, and 2-bit twisted ring whenever possible.
     * @throws IOException
     */
    @Test
    public void testTapsGroupLen() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        for (int d = 1 ; d <= 840; ++d) {
            if (840 % d != 0) continue;
            int g = 840 / d;
            if (g % (3*3) == 0) continue;
            if (g % (3*5) == 0 && g != 60) continue;
            if (g % (5*7) == 0) continue;
            if (g % (2*7) == 0 && g != 168) continue;
            if (g == 40) continue;
            // Stedman/Erin group divisors only now
            // Chose Stedman / Erin lengths
            for (int n : new int[]{d / 2, d - 1}) {
                //System.out.println(g+" "+n);
                int bb = 32-Integer.numberOfLeadingZeros(n);
                int taps[] = ss.lfsrTaps(bb, n, true, true, false, false);
                collector.checkThat("n="+n, taps.length, equalTo(2));
                boolean b[] = ss.initLFSR(bb, taps);
                int actual = lfsrLength(ss, taps, b);
                System.out.println("   n="+n+" actual="+actual+" t="+taps.length+" "+Arrays.toString(taps));
                int tw = bb - taps[0];
                int expected = 2;
                if (n < 3) expected = 0;
                else if (n == (1 << bb) - 1) expected = 0;
                else if (n < 8) expected = 1;
                else if (n > 12 && n < 16) expected = 1;
                // Check that full length runs have twisted bits
                collector.checkThat("n="+n, tw, equalTo(expected));
            }
        }
    }
    
    /**
     * See which LFSR taps are used for short cycles
     * @throws IOException
     */
    @Test
    public void testTapsShortLen() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        for (int d = 2 ; d <= 840; ++d) {
            for (int n : new int[]{d}) {
                //System.out.println(g+" "+n);
                int bb = 32-Integer.numberOfLeadingZeros(n);
                int taps[] = ss.lfsrTaps(bb, n, false, true, false, false);
                int expectedTaps;
                if (n > 217 && n <= 255) {
                    expectedTaps = 3;
                } else {
                    expectedTaps = 2;
                }
                collector.checkThat("n="+n, taps.length, equalTo(expectedTaps));
                boolean b[] = ss.initLFSR(bb, taps);
                int actual = lfsrLength(ss, taps, b);
                //System.out.println("   n="+n+" actual="+actual+" t="+taps.length+" "+Arrays.toString(taps));
                int tw = bb - taps[0];
                int expected = 0;
                collector.checkThat("n="+n, tw, equalTo(expected));
                // Use actual
                collector.checkThat("n="+n, actual >= n, equalTo(true));
            }
        }
    }
    
    /**
     * Check actual length
     * @throws IOException
     */
    @Test
    public void testTapsBBlenFF() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        for (int n = 2 ; n <= 1023; ++n) {
            int bb = 32-Integer.numberOfLeadingZeros(n);
            int taps[] = ss.lfsrTaps(bb, n, false, false, false, false);
            //System.out.println("   n="+n+" t="+taps.length+" "+Arrays.toString(taps));
            boolean b[] = ss.initLFSR(bb, taps);
            int len = lfsrLength(ss, taps, b);
            collector.checkThat("n="+n, true, equalTo(len >= n));
        }
    }
    
    /**
     * Check actual length
     * @throws IOException
     */
    @Test
    public void testTapsBBlenFT() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        for (int n = 2 ; n <= 1023; ++n) {
            int bb = 32-Integer.numberOfLeadingZeros(n);
            int taps[] = ss.lfsrTaps(bb, n, false, true, false, false);
            //System.out.println("   n="+n+" t="+taps.length+" "+Arrays.toString(taps));
            boolean b[] = ss.initLFSR(bb, taps);
            int len = lfsrLength(ss, taps, b);
            collector.checkThat("n="+n, true, equalTo(len >= n));
        }
    }
    
    /**
     * Check actual length
     * @throws IOException
     */
    @Test
    public void testTapsBBlenTF() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        for (int n = 2 ; n <= 1023; ++n) {
            int bb = 32-Integer.numberOfLeadingZeros(n);
            int taps[] = ss.lfsrTaps(bb, n, true, false, false, false);
            //System.out.println("   n="+n+" t="+taps.length+" "+Arrays.toString(taps));
            boolean b[] = ss.initLFSR(bb, taps);
            int len = lfsrLength(ss, taps, b);
            collector.checkThat("n="+n, true, equalTo(len >= n));
        }
    }
    
    /**
     * Check actual length
     * @throws IOException
     */
    @Test
    public void testTapsBBlenTT() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        for (int n = 2 ; n <= 1023; ++n) {
            int bb = 32-Integer.numberOfLeadingZeros(n);
            int taps[] = ss.lfsrTaps(bb, n, true, true, false, false);
            //System.out.println("   n="+n+" t="+taps.length+" "+Arrays.toString(taps));
            boolean b[] = ss.initLFSR(bb, taps);
            int len = lfsrLength(ss, taps, b);
            collector.checkThat("n="+n, true, equalTo(len >= n));
        }
    }
    
    
    /**
     * Preference for LFSR taps.
     * Choose fewer taos,
     * Choose bigger taps, by absolute value.
     * @param a
     * @param b
     * @return
     */
    int compareTo(int a[], int b[]) {
        if (a.length < b.length)
            return -1;
        if (a.length > b.length)
            return 1;
        for (int i = 0; i < a.length; ++i) {
            if (Math.abs(a[i]) > Math.abs(b[i]))
                return -1;
            if (Math.abs(a[i]) < Math.abs(b[i]))
                return 1;
        }
        return 0;
    }

    /**
     * Generate 2 tap LFSR
     * @throws IOException
     */
    @Test
    public void test2Taps() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        for (int n = 2; n <= 16; ++n) {
            int mx = 0;
            int taps[] = new int[2];
            int best[] = taps;
            taps[0] = n;
            for (int i = taps[0] - 1; i >= 1; --i) {
                taps[1] = i;
                boolean b[] = ss.initLFSR(n, taps);
                //System.out.println("Primary = "+Arrays.toString(taps2));

                int total = lfsrLength(ss, taps, b);

                //System.out.println(Arrays.toString(taps)+" "+total);
                //System.out.println("Divide="+divide(val(taps)));
                //if (primitive(taps) != (total == (1 << n) - 1)) {
                    //System.out.println(Arrays.toString(taps)+" "+total+" "+primitive(taps)+" "+(total == (1 << n) - 1));
                //}
                
                if (total >= mx) {
                    // Bigger
                    // fewer taps
                    // bigger first tap
                    if (total > mx || compareTo(taps, best) < 0) {
                        mx = total;
                        best = taps.clone();
                        //System.out.println("Better");
                    }
                }

            }
            //System.out.println("n="+n+" mx="+mx+" "+Arrays.toString(best)+" "+(mx==(1 << n) - 1));
            int mxx = (1 << n) - 1;
            assertThat("n="+n, true, equalTo(mx <= mxx));
            assertThat("n="+n, true, equalTo(mx >= 1 << n-1));
        }
    }  

    public void testTapsHybrid() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        for (int n = 3; n <= 24; ++n) {
            int mx = 0;
            int taps[] = new int[3];
            int best[] = taps;
            taps[0] = n;
            for (int i = taps[0] - 1; i >= 1; --i) {
                taps[1] = i;
                for (int j = i - 1; j > 0; --j) {
                    taps[2] = j == i ? 0 : -j;
                    int taps2[] = taps[2] != 0 ? taps : new int[] { taps[0], taps[1] };
                    boolean b[] = ss.initLFSR(n, taps);
                    // System.out.println("Primary = "+Arrays.toString(taps2));

                    int total = lfsrLength(ss, taps2, b);

                    // System.out.println("Primary = "+states.get(0).size()+"
                    // "+Arrays.toString(taps2));
                    System.out.println(Arrays.toString(taps)+" "+total+" "+primitive(taps)+" "+(total == (1 << n) - 1));

                    if (total >= mx) {
                        // Bigger
                        // fewer taps
                        // bigger first tap
                        if (total > mx || compareTo(taps2, best) < 0) {
                            mx = total;
                            best = taps2.clone();
                            // System.out.println("Better");
                        }
                    }
                }
            }
            // System.out.println("n="+n+" mx="+mx+" "+Arrays.toString(best));
            // Check that a maximal length sequence is possible
            assertThat("n=" + n, mx, equalTo((1 << n) - 1));
        }
    }
    
    /**
     * Find the length of a LFSR.
     * @param ss
     * @param taps
     * @param b
     * @return the length
     */
    private int lfsrLength(StedToSAT ss, int[] taps, boolean[] b) {
        int total = 1;
        boolean b0[] = b.clone();
        boolean seen[] = new boolean[1 << b.length];
        int s = ss.stateNum(b);
        seen[s] = true;
        for (;;++total) {
            ss.nextStateLFSR(b, taps);
            if (Arrays.equals(b, b0)) break;
            s = ss.stateNum(b);
            if (seen[s]) {
                // repeat not to beginning, so useless
                return -total;
            }
            seen[s] = true;
        }
        return total;
    }

    /**
     * Check LFSR is maximal length
     * @throws IOException
     */
    @Test
    public void testTapsMaximal() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        for (int taps[] : StedToSAT.tapsList) {
            if (taps.length == 0) continue;
            int n = taps[0];
            boolean b[] = ss.initLFSR(n, taps);
            int count = lfsrLength(ss, taps, b);
            int mx = (1 << n) - 1;
            assertThat("n="+n, count, equalTo(mx));
        }
    }
    
    /**
     * Test complete taps are maximal.
     * @throws IOException
     */
    @Test
    public void testTapsComplex() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        for (int taps[] : StedToSAT.tapsListComplex) {
            if (taps.length == 0) continue;
            int n = taps[0];
            boolean b[] = ss.initLFSR(n, taps);
            //System.out.println("Testing "+Arrays.toString(taps));
            int count = lfsrLength(ss, taps, b);
            int mx = (1 << n) - 1;
            assertThat("n="+n, count, equalTo(mx));
        }
    }

    /**
     * Check two taps are long even if not maximal
     * @throws IOException
     */
    @Test
    public void testTaps2() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        for (int i = 0; i < StedToSAT.tapsList2.length; ++i) {
            int taps[] = StedToSAT.tapsList2[i];
            if (taps.length == 0) continue;
            int n = taps[0];
            boolean b[] = ss.initLFSR(n, taps);

            int size = lfsrLength(ss, taps, b);
            
            int stateSize = StedToSAT.tapsLen2[i];
            //System.out.println("bb="+i+" stateSize="+stateSize+" size="+size);
            assertThat("state size bb="+i, stateSize, equalTo(size));

            int mx = (1 << n) - 1;
            // Expect non-maximal length counters
            //System.out.println("n="+n+" "+Arrays.toString(taps)+" size="+size+" "+states.get(0));
            assertThat("n="+n, true, equalTo(size < mx));
            assertThat("n="+n, true, equalTo(size >= 1 << n-1));
            
        }
    }

    @Test
    public void testTapsFull2() throws IOException {
        int bb = 2;
        checkFull(bb);
    }

    @Test
    public void testTapsFull3() throws IOException {
        int bb = 3;
        checkFull(bb);
    }

    @Test
    public void testTapsFull4() throws IOException {
        int bb = 4;
        checkFull(bb);
    }

    @Test
    public void testTapsFull5() throws IOException {
        int bb = 5;
        checkFull(bb);
    }

    @Test
    public void testTapsFull6() throws IOException {
        int bb = 6;
        checkFull(bb);
    }

    @Test
    public void testTapsFull7() throws IOException {
        int bb = 7;
        checkFull(bb);
    }


    @Test
    public void testTapsFull8() throws IOException {
        int bb = 7;
        checkFull(bb);
    }

    @Test
    public void testTapsFull9() throws IOException {
        int bb = 9;
        checkFull(bb);
    }


    @Test
    public void testTapsFull10() throws IOException {
        int bb = 10;
        checkFull(bb);
    }

    private void checkFull(int bb) throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        int n = (1 << bb) - 1;
        int taps[] = ss.lfsrTaps(bb, n, false, false, false, false);
        boolean b[] = ss.initLFSR(bb, taps);
        int count = lfsrLength(ss, taps, b);
        assertThat(count, equalTo(n));
    }

    @Test
    public void testTapsShort2() throws IOException {
        int bb = 2;
        checkShort(bb);
    }

    @Test
    public void testTapsShort3() throws IOException {
        int bb = 3;
        checkShort(bb);
    }

    @Test
    public void testTapsShort4() throws IOException {
        int bb = 4;
        checkShort(bb);
    }

    @Test
    public void testTapsShort5() throws IOException {
        int bb = 5;
        checkShort(bb);
    }

    @Test
    public void testTapsShort6() throws IOException {
        int bb = 6;
        checkShort(bb);
    }

    @Test
    public void testTapsShort7() throws IOException {
        int bb = 7;
        checkShort(bb);
    }


    @Test
    public void testTapsShort8() throws IOException {
        int bb = 7;
        checkShort(bb);
    }

    @Test
    public void testTapsShort9() throws IOException {
        int bb = 9;
        checkShort(bb);
    }


    @Test
    public void testTapsShort10() throws IOException {
        int bb = 10;
        checkShort(bb);
    }

    private void checkShort(int bb) throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        int n = (1 << bb) / 2;
        int taps[] = ss.lfsrTaps(bb, n, false, true, false, false);
        boolean b[] = ss.initLFSR(bb, taps);
        int count = lfsrLength(ss, taps, b);
        assertThat(true, equalTo(count >= n));
    }
    
    @Test
    public void checkShort217() throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        int bb = 8;
        int n = 217;
        int taps[] = ss.lfsrTaps(bb, n, false, true, false, false);
        boolean b[] = ss.initLFSR(bb, taps);
        int count = lfsrLength(ss, taps, b);
        assertThat(true, equalTo(count >= n));
    }

    @Test
    public void testTapsShortTwist2() throws IOException {
        int bb = 2;
        checkShortTwist(bb);
    }

    @Test
    public void testTapsShortTwist3() throws IOException {
        int bb = 3;
        checkShortTwist(bb);
    }

    @Test
    public void testTapsShortTwist4() throws IOException {
        int bb = 4;
        checkShortTwist(bb);
    }

    @Test
    public void testTapsShortTwist5() throws IOException {
        int bb = 5;
        checkShortTwist(bb);
    }

    @Test
    public void testTapsShortTwist6() throws IOException {
        int bb = 6;
        checkShortTwist(bb);
    }

    @Test
    public void testTapsShortTwist7() throws IOException {
        int bb = 7;
        checkShortTwist(bb);
    }


    @Test
    public void testTapsShortTwist8() throws IOException {
        int bb = 7;
        checkShortTwist(bb);
    }

    @Test
    public void testTapsShortTwist9() throws IOException {
        int bb = 9;
        checkShortTwist(bb);
    }


    @Test
    public void testTapsShortTwist10() throws IOException {
        int bb = 10;
        checkShortTwist(bb);
    }

    private void checkShortTwist(int bb) throws IOException {
        StedToSAT ss = new StedToSAT(graph.getPath(), 255, 0, 0, 0, 1, 1, 0, 0, "-");
        int n = (1 << bb) / 2;
        int taps[] = ss.lfsrTaps(bb, n, true, true, false, false);
        boolean b[] = ss.initLFSR(bb, taps);
        int count = lfsrLength(ss, taps, b);
        assertThat(true, equalTo(count >= n));
    }

    @Test
    public void testLog() {
        for (int i = 3; i <= 256; ++i) {
            boolean print = false;
            int bs = 0, bv = Integer.MAX_VALUE;
            for (int s = 0; s < i && Integer.numberOfLeadingZeros(i + s - 1) >= Integer.numberOfLeadingZeros(i - 1); ++s) {
                int x[][] = StedToSAT.genLogX(i, s);
                int y[][] = StedToSAT.simplifyX(x);
                if (s == 0 && y.length > 1)
                    print = true;
                if (print) {
                    //System.out.println("i="+i+" "+" s="+s+" "+x.length+" "+y.length);
                }
                if (y.length < bv) {
                    bv = y.length;
                    bs = s;
                }
            }
            int expectedMax = 32 - Integer.numberOfLeadingZeros(i - 1) - 1;
            if (bv > expectedMax || bs != 0) {
                System.out.println("i="+i+" "+" s="+bs+" "+bv+" ex="+expectedMax);
                System.out.println(Arrays.deepToString(StedToSAT.simplifyX(StedToSAT.genLogX(i, 0))));
                System.out.println(Arrays.deepToString(StedToSAT.simplifyX(StedToSAT.genLogX(i, bs))));
            }
            assertThat("i="+i, bv <= expectedMax, equalTo(true));
        }
    }
    
    @Test
    public void testSym() {
        int n = 7;
        String r = "1234567890E";
        String rounds = r.substring(0, n);
        char rc[] = rounds.toCharArray();
        Arrays.sort(rc);
        Map<String,Integer>m = new HashMap<String,Integer>();
        int t = 0;
        for (String g : StedToSAT.symGroup2(rounds)) {
            t = t + 1;
            assertThat(m.keySet(), not(hasItem(g)));
            m.put(g, t);
            char gc[] = g.toCharArray();
            Arrays.sort(gc);
            assertThat(gc, equalTo(rc));
        };
        int f = 1;
        for (int i = n; i > 0; --i) {
            f *= i;
        }
        assertThat(t, equalTo(f));
    }

    @Test
    public void testOOC1() {
        String g = "1234567";
        String r = "1234567890E";
        int n = g.length();
        String rounds = r.substring(0, n);
        assertThat(StedToSAT.ooc(g, rounds), equalTo(false));
    }

    @Test
    public void testOOC2() {
        String g = "2345617";
        String r = "1234567890E";
        int n = g.length();
        String rounds = r.substring(0, n);
        assertThat(StedToSAT.ooc(g, rounds), equalTo(true));
    }

    private int val(int taps[]) {
        int v = 1;
        int mn = 0;
        for (int t : taps) {
            if (t < 0) {
                mn = t;
            }
            v |= 1 << t;
        }
        if (mn < 0) {
            v = v + (v - 1) >> (-mn);
        }
        return v;
    }
    
    private boolean primitive(int taps[]) {
        int a = val(taps);
        /*
         *   1000 4->2+ 1 = 3
         *  10000 5->3 + 1 = 4
         * 100000 6->3 + 1
         */
        int mx = 32 - Integer.numberOfLeadingZeros(a);
        mx = (mx + 1) / 2 + 2;
        int mb = 1 << mx;
        if (mb > a)
            mb = a;
        for (int b = 2; b < mb; ++b) {
            if (rem(a, b) == 0) {
                //System.out.println(Integer.toBinaryString(a));
                //System.out.println(Integer.toBinaryString(b));
                return false;
            }
        }
        return true;
    }
    
    /*
     * 10101
     * 111
     *  1001
     *  111
     *  0111
     *  0111
     *  
     * ----
     * 0011
     * 
     */
    
    private int rem(int a, int b) {
        do {
            int i1 = Integer.numberOfLeadingZeros(a);
            int i2 = Integer.numberOfLeadingZeros(b);
            if (i2 >= i1) {
                a = a ^ (b << (i2 - i1));
                continue;
            } else {
                return a;
            }
        } while (true);
    }
    
    /**
     * Divide 2**k + 1 by a using mod-2 polynomial.
     * Like running LFSR.
     * Primitive polynomial test
     * @param a
     * @return k
     */
    int divide(int a) {
        int n = 0;
        int t = Integer.highestOneBit(a);
        t = 1;
        do {
            //System.out.println(Integer.toBinaryString(a)+" "+n+" "+Integer.toBinaryString(t));
            int sh1 = Integer.numberOfLeadingZeros(t);
            int sh2 = Integer.numberOfLeadingZeros(a);
            if (sh1 == sh2) {
                t ^= a;
                if (t == 1)
                    break;
            }
            t <<= 1;
            n += 1;
        } while (true);
        return n;
    }
    
    static File graph;

    /**
     * A simple graph to create StedToSAT
     * @throws IOException
     */
    @BeforeClass
    public static void setupGraph() throws IOException {
        graph = TestStedToSAT.createGraphFile("erin1");
    }

    @Before
    public void setup() throws IOException {
    }

    @AfterClass
    public static void teardownGraph() throws IOException {
        graph.delete();
    }

}
