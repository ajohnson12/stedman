/**
 * (c) Copyright 2018,2023 Andrew Johnson
 * Test driver for Stedman to SAT conversion.
 * Generates graph files.
 * Generates different Stedman and Erin groups.
 * Runs the result using SAT4J.
 * Checks the result.
 */
//import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.reader.DimacsReader;
import org.sat4j.reader.ParseFormatException;
import org.sat4j.reader.Reader;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;
import org.sat4j.tools.ModelIterator;

@RunWith(Parameterized.class)
public class TestStedToSAT
{
    private static final String CYGWIN_HOME = "C:\\cygwin64";
    private static final String CYGWIN_PATH = CYGWIN_HOME+"\\bin";
    private static final String CYGWIN_WIN_USER_PATH = wintocygfile(System.getenv("USERPROFILE") != null ? System.getenv("USERPROFILE") : System.getProperty("user.home"));
    /** Turn on all optimisations - apart from flags which disable optimisations */
    private static final int ALL_OPTS = StedToSAT.ALL_OPTS;
    static final String graphs[][] = new String[][] {
        // Bobs only
        {"sted21", "sted21_7_05", "S", "2345671","1357246"},
        {"sted20", "sted20_7_12", "S", "2345167","1352476"},
        {"sted10", "sted10_5_04", "S", "2345167","5432167"},
        {"sted7", "sted7_7_07", "S", "2345671"},
        {"sted6", "sted6_6_32", "S", "2316457","2136547"},
        {"sted5", "sted5_5_05", "S", "2345167"},
        {"sted4", "sted4_6_26", "S", "1352476"},
        {"sted3", "sted3_6_33", "S", "3456127"},
        {"sted2", "sted2_4_07", "S", "5432167"},
        {"sted1", "sted1_0_01", "S"},
        // other
        {"sted168", "sted168_7_03", "S", "7613524", "4725163"},
        {"sted60", "sted60_6_05", "S", "2345167",  "5342617"},
        {"sted24a", "sted24_6_09", "S", "3456127", "2165347"},
        {"sted24b", "sted24_7_28", "S", "2341657", "2134576"},
        {"sted12a", "sted12_6_14", "S", "5146237", "6423157"},
        {"sted12b", "sted12_7_33", "S", "3476521", "7514623"},
        {"sted8", "sted8_6_23", "S", "2341657", "4321567"},
        {"sted4b", "sted4_6_35", "S", "2143567", "2134657"},
        {"sted4c", "sted4_4_04", "S", "2143567", "3412567"},
        // out of course
        {"sted20m", "sted20_5_03", "S", "2345167", "1352467"},
        {"sted10m", "sted10_7_14", "S", "2345176"},
        {"sted8m", "sted8_4_03", "S", "2341567","4321567"},
        {"sted4dm", "sted4_4_06", "S", "2134567","1243567"},
        {"sted4em", "sted4_4_05", "S", "2341567"},
        {"sted2m", "sted2_2_01", "S", "2134567"},
        {"sted2m_1234576m", "sted2_2_01", "S", "1234576"},
        // bobs only
        {"erin21", "erin21_7_05", "E", "2345671","1357246"},
        {"erin20", "erin20_7_12", "E", "2345167","1352476"},
        {"erin10", "erin10_5_04", "E", "2345167","5432167"},
        {"erin7", "erin7_7_07", "E", "2345671"},
        {"erin6", "erin6_6_32", "E", "2316457","2136547"},
        {"erin5", "erin5_5_05", "E", "2345167"},
        {"erin4", "erin4_6_26", "E", "1352476"},
        {"erin3", "erin3_6_33", "E", "3456127"},
        {"erin2", "erin2_4_07", "E", "5432167"},
        {"erin1", "erin1_0_01", "E"},
        // other
        {"erin168", "erin168_7_03", "E", "7613524", "4725163"},
        {"erin60", "erin60_6_05", "E", "2345167", "5342617"},
        {"erin24a", "erin24_6_09", "E", "3456127", "2165347"},
        {"erin24b", "erin24_7_28", "E", "2341657", "2134576"},
        {"erin12a", "erin12_6_14", "E", "5146237", "6423157"},
        {"erin12b", "erin12_7_33", "E", "3476521", "7514623"},
        {"erin8", "erin8_6_23", "E", "2341657", "4321567"},
        {"erin4b", "erin4_6_35", "E", "2143567", "2134657"},
        {"erin4c", "erin4_4_04", "E", "2143567", "3412567"},
        // out of course
        {"erin20m", "erin20_5_03", "E", "2345167", "1352467"},
        {"erin10m", "erin10_7_14", "E", "2345176"},
        {"erin8m", "erin8_4_03", "E", "2341567","4321567"},
        {"erin4dm", "erin4_4_06", "E", "2134567","1243567"},
        {"erin4em", "erin4_4_05", "E", "2341567"},
        {"erin2m", "erin2_2_01", "E", "2134567"},

        { "sted10_20", "", "S", "2345167", "5432167", "1352476", "1234567"},
        { "sted1_10", "sted1_10_5_04", "S", "1234567", "1234567", "2345167", "5432167"},
        { "sted1_2", "sted1_2_4_07", "S", "1234567", "1234567", "1234567", "5432167"},
        { "sted1_3", "sted1_3_6_33", "S", "1234567", "1234567", "3456127", "1234567"},
        { "sted2_3", "", "S", "3215467", "1234567", "2316457", "1234567" },

        { "sted2from6", "", "S", "3215467" },

        // Caters
        { "erin168_9", "erin168_9", "123456789SS", "145763289", "716245389", "123456789", "123456789"},
    };

    // A way of encoding the string "-0"
    private static final int MINUS_0 = Integer.MIN_VALUE;

    /**
     * Returns the set of tests
     * @return Array of test options
     * Expected result
     * graph
     * start
     * 
     */
    @Parameterized.Parameters(name = "{index}: graph {1} {2} {3} maxsol={10} opt={11}")
    public static Collection<?> parms() {
        List<Object[]> ret = Arrays.asList(new Object[][] {
            // First 5 are satisfiable, Stedman, Erin, in and out of course, and caters, so are good to try with all options
            { "3124567QSP---P---------P--P--P---P--P---------P--P-*5(4)", "sted20", "3124567QSP---P*1(1)", 840 },
            { "2314567QSPP--PP--PPPPPPPP----PPPPP--PPPPP--PPPPPPPP*5(4)", "sted20m", "2314567QSPP--PP--PPPPPPPP----", 840 },
            { "1234567SSP-P-P-PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PPP-P--PP-P-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----PPP----P-P-P----P-P-P-P-PP-PPPPP--PPPP-PP-PPP-P-P-PP-P-P----P-P-P----P-P-P--PPPP-PP-P-P-P-P----P-P-P----PPP----P-P-P----P-P-PP--P-PPPP----P-PPP--PPPP-PP-P-PP--P-P-P----P-P-P----PPP----P-P-P----P-P-PP-P-P-P-P----P-P-P----P-P-PP-P-P-PPP-PP-PPPP--PPPPP-PP-P-P-PPP-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----PPP----P-P-P----P-*1(2)", 
                "erin2", "1234567SSP-P-P-PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PPP-P--PP-P-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----PPP----P-P-P----P-P-P-P-PP-PPPPP--PPPP-PP-PPP-P-P-PP-P-P----P-P-P----P-P-P--PPPP-PP-P-P-P-P----P-P-P----PPP----P-P-P----P-P-PP--P-PPPP----P-PPP--PPPP-PP-P-PP--P-P-P----P-P-P----PPP----P-P-P----P-P-PP-P-P-P-P----P-P-P----P-P-PP-P-P-PPP-PP-PPPP--PPPPP-PP-P-P-PPP-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----PPP----P-P-P----P-*0.26(2)", 840 },
            { "252!4 1234567SSP-P--PP-P----PPPP-P--PP-P-P----P-P-P----P-*5(4) 18 1", "erin20m", "1234567SSp", 840 },
            // Caters
            { "421365798SSP-P-PP---P-P----PP-PPPPPP--PPPP-PP--PP---P----P-P---PPP-P-PPPP-PPP---P-*3(56)", "erin168_9", "421365798SSP-P-PP---P-P----PP-PPPPPP--PPPP-PP--PP---P----P-P---PPP-P-PPPP-PPP---P-*3(56)234618597SSPP-PP-PPP*3(56)423617589SS-*7(24)436281957SS-----PP*2(84)268451937SS-----PP*2(84)253816479SSP-PP--P----PPP-PP*4(42)321547698SSP-PPP----P-PP-P------PPP--PP-P--P-P---PP-PPP-PP-P-PPP-P---PPP*3(56)", 0, 0, 1, 1, 0, "", "", 2},

            // These two are good SAT problems for testing with different options Stedman / Erin / full / short
            { "2314567QSP----P-P-PPPP--P----PP----PP---PP-PP-PP--P-----P----P---------PP------*3(4)", "sted12a", "2314567QSL....L.L.LLLL..L....LL....pp===pp=pp=pp", 840 },
            { "P-PP-PPP--PP-P-P-P-PPP", "erin12b", "1234567SS", 27, 0, 1, 1, 0, "", "", 3 },
            
            //{ "UNSAT", "sted12b", "2314567QS=======p=", 840 },
            { "2314567QSPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PP", "sted4em", "2314567QSPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PP*5(4)", 840 },
            
            // Test different short cycles
            { "2346175SQ--*5(12)3467251SQPP-P---P--*5(12)3124567QS--*5(12)", "sted60", "3467251SQPP-P*1(1)3124567QSr*1(1)", 0 },
            { "2346175SQ--*5(12)3467251SQPP-P---P--*5(12)3124567QS--*5(12)", "sted60", "3467251SQPP-P*1(1)3124567QS", 2 },
            { "UNSAT", "sted60", "3124567QS", 4 },
            { "UNSAT", "sted60", "3124567QS", 6 },
            { "UNSAT", "sted60", "3124567QS", 8 },
            { "3124567QS--*5(12)2346175SQ--*5(12)4637251QS----------*1(60)", "sted60", "4637251QS~*1(60)", 10 },
            { "UNSAT", "sted60", "3124567QS", 12},
            { "UNSAT", "sted60", "3124567QS", -2}, // -2 is the same as 12 = (14-2)
            { "3124567QSP-PP--------P-*5(12)", "sted60", "3124567QSp=", 14 },
            { "1234567SQPPP-PPP-P----P*3(20)", "sted60", "1234567SQP", 840 }, 
            // also loop to self, both at start and not, with length limit or not
            { "UNSAT", "erin168" },
            { "30!", "erin168", "", 0 },
            { "1234567SSPP--*7(24)4236175SSP*7(24)", "erin168", "4236175SS", 1 },
            { "4236175SSP*7(24)1234567SSPP--*7(24)", "erin168", "1234567SS", 4 },
            { "2314567SS-*5(4)", "erin20", "1435267SSPPPPPPP*1(20)", 0 },
            { "2314567SS-*5(4)", "erin20", "1435267SSPPPPPPP*1(20)", 7 },
            { "2314567SS-*5(4)", "erin20", "2314567SS", 1 },
            
            // Test 10-parts with loop back to self in starting six and elsewhere
            { "2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP--------PPPPP---P-P--PP--P-PPPP-P-PPP-P--P-*2(5)", "sted10", "2314567QSpp===ppp==" },
            { "3467251QS---PPP--P-P----PPPP--P--P----PP-P--P-PP--------PPPPP---P-P--PP--P-PPPP-P-PPP-P--P-PP*2(5)", "sted10", "3467251QS===ppp==p=" },
            { "2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PP-PP-P--P-*2(5)", "sted10", "2314567QSpp===ppp==", -2 },
            { "3467251QS---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PP-PP-P--P-PP*2(5)", "sted10", "3467251QS===ppp==p=", -2 },

            // short block in 1-part
            { "3425167QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PP-----PPPPP---P-P--PP--P-PPPP-P---PPP-P--P-*1(1)3627451QS--P----PPPPP--P----PPPPP*1(1)2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP---P--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PP-----PPPPP---P-P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP---P--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-*1(1)",
                "sted1", "3425167QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PP-----PPPPP---P-P--PP--P-PPPP-P---PPP-P--P-*1(1)3627451QS--P----PPPPP--P----PPPPP*1(1)2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP---P--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PP-----PPPPP---P-P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP---P--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-*0.4(1)", 586}, 

            // 2-part as a 1-part search
            { "2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP---P--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PP-----PPPPP---P-P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PP", /* just check the beginning for both solutions */
                    "sted1", "2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP---P--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PP-----PPPPP---P-P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-*0.7(1)", 840, 0, 1, 1, 0, "", "", 2 },

            /*
             * This has a solution with 56 sixes, and 364 sixes, so needs a counter up to 364/2 = 182.
             * However a 217 period counter isn't suitable unless states which recur after 7 or 31 are excluded. 
             */
            { "2641375SQPPP----PPPPPPPP----PPPPPPPPPPPP--PPPPPPPPPP--PP--PPPPPPP*2(1)2314567QSPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPP----PPP--PPPPP--PPPPPPPPPP--PP--PPPPPPPP----PPPPP--PPPPP--PPPPPPPPPP--PP--PPPPPPPP----PPPPP--PPPPP--PPPPPPPPPPPP--PPPPP--PPPPP--PPPPPPPPPP--PP--PPPPPPPP------PPP--PPPPP----PPPPPPPP--PP--PPPPPP*2(1)",
                        "sted2", "2641375SQPPP----PPPPPPPP----PPPPPPPPPPPP--PPPPPPPPPP--PP--PPPPPPP*0.5(1)2314567QSPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPP----PPP--PPPPP--PPPPPPPPPP--PP--PPPPPPPP----PPPPP--PPPPP--PPPPPPPPPP--PP--PPPPPPPP----PPPPP--PPPPP--PPPPPPPPPPPP--PPPPP--PPPPP--PPPPPPPPPP--PP--PPPPPPPP------PPP--PPPPP----PPPPPPPP--PP--PPPPPP*2(1)", -56 },
            /* allow a 217 period counter, but exclude the invalid states */
            { "2641375SQPPP----PPPPPPPP----PPPPPPPPPPPP--PPPPPPPPPP--PP--PPPPPPP*2(1)2314567QSPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPP----PPP--PPPPP--PPPPPPPPPP--PP--PPPPPPPP----PPPPP--PPPPP--PPPPPPPPPP--PP--PPPPPPPP----PPPPP--PPPPP--PPPPPPPPPPPP--PPPPP--PPPPP--PPPPPPPPPP--PP--PPPPPPPP------PPP--PPPPP----PPPPPPPP--PP--PPPPPP*2(1)",
                            "sted2", "2641375SQPPP----PPPPPPPP----PPPPPPPPPPPP--PPPPPPPPPP--PP--PPPPPPP*0.5(1)2314567QSPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPP----PPP--PPPPP--PPPPPPPPPP--PP--PPPPPPPP----PPPPP--PPPPP--PPPPPPPPPP--PP--PPPPPPPP----PPPPP--PPPPP--PPPPPPPPPPPP--PPPPP--PPPPP--PPPPPPPPPP--PP--PPPPPPPP------PPP--PPPPP----PPPPPPPP--PP--PPPPPP*2(1)", -56, 0, 1, 1, 0, "", "", 1, 0x1000000 },
            
            /*
             * solution requires 256 counter - so needs 4 taps or complex taps
             */
            { "4125367SSPP----P-P-P----P*2(1)2346175SSP----P-P-P----PPP----P-P-P----PP*1(2)1435267SS-P-P--PPPP-PP-P-P-PPP----PPPP-P--PP-P-P----P-P-P----P*1(2)4261375SSP----PPP----P-P-*2(1)3167452SSPPPP-PP-P-PPP--*2(1)6275431SS-PPP----P-P-P----PPP----P-P-P---*1(2)1234567SSP-P-P-PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PPP-P--PP-P----PPPP-P--PP-PPPP--P-PPP----PPPP-P--PP-PPPPP--PPPP-PP-PPP-P--PPPP-PP-P-P-P-P----P-P-P----PPP----P-P-P----P-P-PP--P-PPPP----PPP-P--PPPP-PP-P-P-PPP-PP-PPPP--PPPPP-PP-P-P-PPP----PPPP-P--PP-P-P----P-P-P----P-*2(1)",
                "erin2", "1234567SSP-P-P-PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PPP-P--PP-P----PPPP-P--PP-PPPP--P-PPP----PPPP-P--PP-PPPPP--PPPP-PP-PPP-P--PPPP-PP-P-P-P-P----P-P-P----PPP----P-P-P----P-P-PP--P-PPPP----PPP-P--PPPP-PP-P-P-PPP-PP-PPPP--PPPPP-PP-P-P-PPP----PPPP-P--PP-P-P----P-P-P----P-*0.4(1)", 256 },
            // also try common source / dest options
            { "4125367SSPP----P-P-P----P*2(1)2346175SSP----P-P-P----PPP----P-P-P----PP*1(2)1435267SS-P-P--PPPP-PP-P-P-PPP----PPPP-P--PP-P-P----P-P-P----P*1(2)4261375SSP----PPP----P-P-*2(1)3167452SSPPPP-PP-P-PPP--*2(1)6275431SS-PPP----P-P-P----PPP----P-P-P---*1(2)1234567SSP-P-P-PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PPP-P--PP-P----PPPP-P--PP-PPPP--P-PPP----PPPP-P--PP-PPPPP--PPPP-PP-PPP-P--PPPP-PP-P-P-P-P----P-P-P----PPP----P-P-P----P-P-PP--P-PPPP----PPP-P--PPPP-PP-P-P-PPP-PP-PPPP--PPPPP-PP-P-P-PPP----PPPP-P--PP-P-P----P-P-P----P-*2(1)",
                "erin2", "1234567SSP-P-P-PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PPP-P--PP-P----PPPP-P--PP-PPPP--P-PPP----PPPP-P--PP-PPPPP--PPPP-PP-PPP-P--PPPP-PP-P-P-P-P----P-P-P----PPP----P-P-P----P-P-PP--P-PPPP----PPP-P--PPPP-PP-P-P-PPP-PP-PPPP--PPPPP-PP-P-P-PPP----PPPP-P--PP-P-P----P-P-P----P-*0.4(1)", 256, 0, 1, 1, 0, "", "", 1, 0x181c },
            
            // now a 254 counter - so excluded state are also before first/after last
            { "4125367SSPP----P-P-P----P*2(1)6413275SSPP-P-P-P-P----P-P-P----P-P-*2(1)1267453SSP-P-P-P-P----P-P-P----P-P-PP-P-P-P-P----P-P-P----P-P-P*1(2)3167452SSPPPP-PP-P-PPP--*2(1)6275431SS-P-P-PP-P-P-P-P----P-P-P----P-P-PP-P-P-P-P----P-P-P---*1(2)1234567SSP-P-P-PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PPP-P--PP-P----PPPP-P--PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PPP-P--PP-P-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----P-P-P--PP-P----PPPP-P--PP-P-P----P-P-P----PPP----P-P-P----P-P-P--PP-P----PPPP-P--PP-P-P----P-P-P----P-*2(1)",
                  "erin2", "1234567SSP-P-P-PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PPP-P--PP-P----PPPP-P--PP-PPPP--P-PPP----PPPP-P--PP-PPPPP--PPPP-PP-PPP-P--PPPP-PP-P-P-P-P----P-P-P----PPP----P-P-P----P-P-PP--P-PPPP----PPP-P--PPPP-PP-P-P-PPP-PP-PPPP--PPPPP-PP-P-P-PPP----PPPP-P--PP-P-P----P-P-P----P-*0.27(1)", 254, 0, 1, 1, 0, "", "", 1, 0x80001c },
            // and a 7 counter - to cover before first
            { "4125367SSPP----PP-PPP--PPP----P-PP----P*2(5)2467153SS----PPP--PPPPP*2(5)4623175SSP----PPP----PPP----PP*1(10)1267453SS-----*1(10)6271453SSPPPPPPP*1(10)3124567SSPPPPPPP*1(10)",
                  "erin10", "4125367SSPP----PP-PPP--PPP----P-PP----P*2(5)2467153SS----PPP--PPPPP*2(5)4623175SSP----PPP----PPP----PP*1(10)1267453SS-----*1(10)6271453SSPPPPPPP*1(10)3124567SS", 7, 0, 1, 1, 0, "", "", 1, 0x1000010 },
            { "4125367SSPP----PP-PPP--PPP----P-PP----P*2(5)2467153SS----PPP--PPPPP*2(5)4623175SSP----PPP----PPP----PP*1(10)1267453SS-----*1(10)6271453SSPPPPPPP*1(10)3124567SSPPPPPPP*1(10)",
                  "erin10", "4125367SSPP----PP-PPP--PPP----P-PP----P*2(5)2467153SS----PPP--PPPPP*2(5)4623175SSP----PPP----PPP----PP*1(10)1267453SS-----*1(10)6271453SSPPPPPPP*1(10)3124567SS", 7, 0, 1, 1, 0, "", "", 1, 0x1000008 },
            
            // add for a 3-part with a 182 count
            { "1245367SSPPPP--PPPPP--P*1(3)4627153SS-----*1(3)6417253SS-----*1(3)5246173SSPPPPPPP*1(3)5243167SSPP----P*3(1)2647351SS-----*1(3)6237451SS-----*1(3)6715432SS-----*1(3)2675134SS-----*1(3)7465123SS-----*1(3)1765423SS-----*1(3)6475321SS-----*1(3)3175624SS-----*1(3)3567421SS-----*1(3)6537124SS-----*1(3)5173426SS-----*1(3)2573641SS-----*1(3)1234567SSP-P--PPP-P-PPPPPP-PP----PP-P----P-P-P--PPP-P-PPPPPP-P-PPPPPP-P-P----P-PP----PP-PPP--PPP----P-PP----PPP----PP-PPP--P-PP----PP-PPP--PPP----PPPPP--P-P-PPPPPP-P-PPPPPP-P-P----P-P-P----P-*3(1)",
                  "erin3", "1245367SSPPPP--PPPPP--P*1(3)4627153SS-----*1(3)6417253SS-----*1(3)5246173SSPPPPPPP*1(3)5243167SSPP----P*3(1)2647351SS-----*1(3)6237451SS-----*1(3)6715432SS-----*1(3)2675134SS-----*1(3)7465123SS-----*1(3)1765423SS-----*1(3)6475321SS-----*1(3)3175624SS-----*1(3)3567421SS-----*1(3)6537124SS-----*1(3)5173426SS-----*1(3)2573641SS-----*1(3)1234567SSP-P--PPP-P-PPPPPP-PP----PP-P----P-P-P--PPP-P-PPPPPP-P-PPPPPP-P-P----P-PP----PP-PPP--PPP----P-PP----PPP----PP-PPP--P-PP----PP-PPP--PPP----PPPPP--P-P-PPPPPP-P-PPPPPP-P-P----P-P-P----P-*0.8(1)", 182, 0, 1, 1, 0, "", "", 1, 0x001800 },
            // and try using hybrid mode with attempt at redundant LFSR
            { "1245367SSPPPP--PPPPP--P*1(3)4627153SS-----*1(3)6417253SS-----*1(3)5246173SSPPPPPPP*1(3)5243167SSPP----P*3(1)2647351SS-----*1(3)6237451SS-----*1(3)6715432SS-----*1(3)2675134SS-----*1(3)7465123SS-----*1(3)1765423SS-----*1(3)6475321SS-----*1(3)3175624SS-----*1(3)3567421SS-----*1(3)6537124SS-----*1(3)5173426SS-----*1(3)2573641SS-----*1(3)1234567SSP-P--PPP-P-PPPPPP-PP----PP-P----P-P-P--PPP-P-PPPPPP-P-PPPPPP-P-P----P-PP----PP-PPP--PPP----P-PP----PPP----PP-PPP--P-PP----PP-PPP--PPP----PPPPP--P-P-PPPPPP-P-PPPPPP-P-P----P-P-P----P-*3(1)",
                      "erin3", "1245367SSPPPP--PPPPP--P*1(3)4627153SS-----*1(3)6417253SS-----*1(3)5246173SSPPPPPPP*1(3)5243167SSPP----P*3(1)2647351SS-----*1(3)6237451SS-----*1(3)6715432SS-----*1(3)2675134SS-----*1(3)7465123SS-----*1(3)1765423SS-----*1(3)6475321SS-----*1(3)3175624SS-----*1(3)3567421SS-----*1(3)6537124SS-----*1(3)5173426SS-----*1(3)2573641SS-----*1(3)1234567SSP-P--PPP-P-PPPPPP-PP----PP-P----P-P-P--PPP-P-PPPPPP-P-PPPPPP-P-P----P-PP----PP-PPP--PPP----P-PP----PPP----PP-PPP--P-PP----PP-PPP--PPP----PPPPP--P-P-PPPPPP-P-PPPPPP-P-P----P-P-P----P-*0.8(1)", 182, 0, 1, 1, 0, "", "", 1, 0xa01800 },
            // and try using non-linear mode with attempt at redundant LFSR
            { "1245367SSPPPP--PPPPP--P*1(3)4627153SS-----*1(3)6417253SS-----*1(3)5246173SSPPPPPPP*1(3)5243167SSPP----P*3(1)2647351SS-----*1(3)6237451SS-----*1(3)6715432SS-----*1(3)2675134SS-----*1(3)7465123SS-----*1(3)1765423SS-----*1(3)6475321SS-----*1(3)3175624SS-----*1(3)3567421SS-----*1(3)6537124SS-----*1(3)5173426SS-----*1(3)2573641SS-----*1(3)1234567SSP-P--PPP-P-PPPPPP-PP----PP-P----P-P-P--PPP-P-PPPPPP-P-PPPPPP-P-P----P-PP----PP-PPP--PPP----P-PP----PPP----PP-PPP--P-PP----PP-PPP--PPP----PPPPP--P-P-PPPPPP-P-PPPPPP-P-P----P-P-P----P-*3(1)",
                      "erin3", "1245367SSPPPP--PPPPP--P*1(3)4627153SS-----*1(3)6417253SS-----*1(3)5246173SSPPPPPPP*1(3)5243167SSPP----P*3(1)2647351SS-----*1(3)6237451SS-----*1(3)6715432SS-----*1(3)2675134SS-----*1(3)7465123SS-----*1(3)1765423SS-----*1(3)6475321SS-----*1(3)3175624SS-----*1(3)3567421SS-----*1(3)6537124SS-----*1(3)5173426SS-----*1(3)2573641SS-----*1(3)1234567SSP-P--PPP-P-PPPPPP-PP----PP-P----P-P-P--PPP-P-PPPPPP-P-PPPPPP-P-P----P-PP----PP-PPP--PPP----P-PP----PPP----PP-PPP--P-PP----PP-PPP--PPP----PPPPP--P-P-PPPPPP-P-PPPPPP-P-P----P-P-P----P-*0.8(1)", 182, 0, 1, 1, 0, "", "", 1, 0x901800 },
            // and try using a 4-tap mode
            { "1245367SSPPPP--PPPPP--P*1(3)4627153SS-----*1(3)6417253SS-----*1(3)5246173SSPPPPPPP*1(3)5243167SSPP----P*3(1)2647351SS-----*1(3)6237451SS-----*1(3)6715432SS-----*1(3)2675134SS-----*1(3)7465123SS-----*1(3)1765423SS-----*1(3)6475321SS-----*1(3)3175624SS-----*1(3)3567421SS-----*1(3)6537124SS-----*1(3)5173426SS-----*1(3)2573641SS-----*1(3)1234567SSP-P--PPP-P-PPPPPP-PP----PP-P----P-P-P--PPP-P-PPPPPP-P-PPPPPP-P-P----P-PP----PP-PPP--PPP----P-PP----PPP----PP-PPP--P-PP----PP-PPP--PPP----PPPPP--P-P-PPPPPP-P-PPPPPP-P-P----P-P-P----P-*3(1)",
                          "erin3", "1245367SSPPPP--PPPPP--P*1(3)4627153SS-----*1(3)6417253SS-----*1(3)5246173SSPPPPPPP*1(3)5243167SSPP----P*3(1)2647351SS-----*1(3)6237451SS-----*1(3)6715432SS-----*1(3)2675134SS-----*1(3)7465123SS-----*1(3)1765423SS-----*1(3)6475321SS-----*1(3)3175624SS-----*1(3)3567421SS-----*1(3)6537124SS-----*1(3)5173426SS-----*1(3)2573641SS-----*1(3)1234567SSP-P--PPP-P-PPPPPP-PP----PP-P----P-P-P--PPP-P-PPPPPP-P-PPPPPP-P-P----P-PP----PP-PPP--PPP----P-PP----PPP----PP-PPP--P-PP----PP-PPP--PPP----PPPPP--P-P-PPPPPP-P-PPPPPP-P-P----P-P-P----P-*0.8(1)", 182, 0, 1, 1, 0, "", "", 1, 0xb01800 },
            // and a Stedman search with a counter of 420 / 2 
            { "3124567QS---PP--------PPPPPP--P------P--PP--PP------P-----PP-P---PP---P---P-----PP---PPPPP-P--------P---PPP---P------P--P---P--P--P---PPPPPPP-P-PP------PP--------PPPPPP--P------P--PP--PP------P-----PP-P---PP---P---P-----PP---PPPPP-P--------P---PPP---P------P--P---P--P--P---PPPPPPP-P-PP------PP--------PPPPPP--P------P--PP--PP------P-----PP-P---PP---P---P-----PP---PPPPP-P--------P---PPP---P------P--P---P--P--P---PPPPPPP-P-PP---*1(1)",
                      "sted1", "2345167QSP---PP---P-------PP-P--------P---PPP---P------P--P---P--P--P---PPPPPPP-P-PP------PP--------PPPPPr==*1(1)3124567QS---PP--------PPPPPP--P------P--PP--PP------P-----PP-P---PP---P---P-----PP---PPPPP-P--------P---PPP---P------P--P---P--P--P---PPPPPPP-P-PP---*2.95(2)", 420, 0, 1, 1, 0, "", "", 4, 0x301800 },
            // Test short cycle with non-maximal LFSR - with cycles: 217, 31, 7, 1, disable hybrid/non-linear condense
            { "2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP---P--P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PP-----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP---P-P--PP--P-PPPP-P-PPP-P--P-*2(1)",
                        "sted2", "2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP---P--P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PP-----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP---P-P--PP--P-PPPP-P-PPP-P--P-*0.3(1)", -48, 0, 1, 1, 0, "", "", 1, 0x381800},

            { "2314567QSPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPPPP--PPPPPPPP----PPPPP--PPP----PPPPPPPPPPPP--PPPPP--PPPPP----PPPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPP----PPPPPPPP------PPPPPPPP------PP*2(2)",
                "sted4em", "2314567QSPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPPPP--PPPPPPPP----PPPPP--PPP----PPPPPPPPPPPP--PPPPP--PPPPP----PPPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PPPPPPPPPP----PPPPPPPP------PPPPPPPP------PP*2(2)", -14, 0, 1, 1, 0, "", "", 2 }, // possible bug, only one solution?
            { "2314567QSPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PP", "sted4em", "2314567QSPPPPPPPP--PPPPP--PPPPP----PPPPPPPP--PP--PP*5(4)", 840 },
            { "2314567QSP---P---------P---------P-----P---------P----P---------P-P---------PPP-P------PP-P--P---------P---------P---P--P-P---------P---------P--P-P--P----P-P--P-----P-------P----*1(4)",
                    "sted4dm", "3456271QS----------*1(4)6127453SQ----------*1(4)5416273SQ----------*1(4)7523614QS-P----P-PP*1(4)2314567QSP---P---------P---------P-----P---------P----P---------P-P---------PPP-P------PP-P--P---------P---------P---P--P-P---------P---------P--P-P--P----P-P--P-----P-------P----*0.9(4)", -40, 0, 1, 1, 0, "", "", 1 },
            { "2314567QS----P-----P-----P---P-P--P------P--P------P--P----P-----P---P---P------P-P-P----P--P-P----P--P-P----PP---------P--P---P---------P-P-PP------P-PP------P-PP----PP-P---------P---------P-P---P-----P-----P----P-----*1(4)",
                    "sted4dm", "2314567QS----P-----P-----P---P-P--P------P--P------P--P----P-----P---P---P------P-P-P----P--P-P----P--P-P----PP---------P--P---P---------P-P-PP------P-PP------P-PP----PP-P---------P---------P-P---P-----P-----P----P-----*0.8(4)", 840},
            { "2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP---P--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PP-----PPPPP---P-P--PP--P-PPPP-P---",
                    "sted2", "2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP---P--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PP-----PPPPP---P-P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-*0.4(1)", 840, 0, 1, 1, 0, "", "", 2 },

            // call specifiers
            { "2346175SQ--*5(12)3467251SQPP-P---P--*5(12)3124567QS--*5(12)", "sted60", "3124567QS##*5(12)3467251SQ[1/8]R_--L.-*1(1)3124567QSr*1(1)", 0 },

            // Required sequence
            { "PPPPPP", "sted60", "", 0, 0, 1, 1, 0, "PPPPPP;2314567??P;2314567QS", "", 4 },
            { "PPPPPP", "sted60", "", 0, 0, 1, 1, 0, "PPPPPP;2314567??P;2314567QS;>=2:---", "", 2 },
            { "PPPPPP", "sted60", "", 0, 0, 1, 1, 0, "PPPPPP;2314567??P;2314567QS;>1:P--P", "", 2 },
            { "PPPPPP", "sted60", "", 0, 0, 1, 1, 0, "PPPPPP;2314567??P;2314567QS;<2:---", "", 2 },
            { "PPPPPP", "sted60", "", 0, 0, 1, 1, 0, "PPPPPP;2314567??P;2314567QS;<=1:P--P", "", 2 },
            { "PPPPPP", "sted60", "", 0, 0, 1, 1, 0, "PPPPPP;2314567??P;2314567QS;=2:---", "", 2 },
            { "PPPPPP", "sted60", "", 0, 0, 1, 1, 0, "PPPPPP;2314567??P;2314567QS;<>2:---", "", 2 },
            { "PPPPPP", "sted60", "", 0, 0, 1, 1, 0, "PPPPPP;2314567??P;2314567QS;>=0:---", "", 2 },
            { "PPPPPP", "sted60", "", 0, 0, 1, 1, 0, "PPPPPP;2314567??P;2314567QS;>=1:---", "", 2 },
            { "PPPPPP", "sted60", "", 0, 0, 1, 1, 0, "PPPPPP;2314567??P;2314567QS;>0:---", "", 2 },
            { "PPPPPP", "sted60", "", 0, 0, 1, 1, 0, "PPPPPP;2314567??P;2314567QS;>1:---", "", 2 },
            { "PPPPPP", "sted60", "", 0, 0, 1, 1, 0, "PPPPPP;2314567??P;2314567QS;<4:---", "", 4 },
            { "PPPPPP", "sted60", "", 0, 0, 1, 1, 0, "PPPPPP;2314567??P;2314567QS;<=4:---", "", 4 },
            { "", "sted60", "", 0, 0, 1, 1, 0, "1..2:P-P", "", 14 },
            // testing flexible calls
            { "PP", "sted60", "", 840, 0, 1, 1, 0, "2314567QSpp~.=_rLRppppp", "", 2 },
            { "PP", "sted60", "", 840, 0, 1, 1, 0, "2314567QSrr_~..LLRppLLp", "", 2 },
            // Prohibited and required
            { "-PPP", "sted60", "", 0, 0, 1, 1, 0, "!-----;-PPP-;--;--P;!--PPP--PP-P;!1234567QSPP;!3124567QS", "", 13 },
            { "PP", "sted60", "", 840, 0, 1, 1, 0, "!2314567QSpp~.=_rLRppppp", "", 18 },
            { "PP", "sted60", "", 840, 0, 1, 1, 0, "!2314567QSrr_~..LLRppLLp", "", 18 },
            // Match proportion of touch
            { "PP", "sted10", "", 0, 0, 1, 1, 0, ">=76:2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP--------PPPPP---P-P--PP--P-PPPP-P-PPP-P--P-*2.01(5)", "", 4}, 
            { "PP", "sted10", "", 0, 0, 1, 1, 0, ">=80:2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP--------PPPPP---P-P--PP--P-PPPP-P-PPP-P--P-*2.01(5)", "", 2},
            { "PP", "sted10", "", 0, 0, 1, 1, 0, ">=82:2314567QSRL_.~rrr~~r~r~~~~rrrr~~r~~r~~~~rr~r~~r~rr~~~~~~~~rrrrr~~~r~r~~rr~~r~rrrr~r~rrr~r~~r~*2.01(5)", "", 4},
            { "PP", "sted10", "", 0, 0, 1, 1, 0, "58..60:2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP--------PP-PP-P--P-*1(10)", "", 2},

            // Palindrome
            { "2314567SQ---P--PP--P",
                "sted12a", "2314567SQ---P--PP--P", 840, 0, 1, 1, 0, "", "3615427", 3 },
            { "2314567SQ---P--PP--P",
                "sted12a", "2314567SQ---P--PP--P", 840, 0, 1, 1, 0, "", "3615427-", 1 },

            // Count Q-sets
            { "UNSAT",
                "sted12a", "2314567QSP----", 840, 0, 1, 1, 25, "", "", 0 },
            { "2314567QSp====ppp========pp====p==p====pp=====p===p====p==p=p==p======ppp======*2(6)",
              "sted12a", "2314567QSP----", 840, 0, 1, 1, MINUS_0, "", "", 1 },
            { "2314567QSp====p=p=pppp==p====pp====p",
                  "sted12a", "2314567QSP----", 840, 0, 1, 1, 1, "", "", 2 },
            { "2314567QSp====p",
                      "sted12a", "2314567QSP----", 840, 0, 1, 1, -1, "", "", 3 },
            { "2314567QSp====p",
                      "sted12a", "2314567QSP----", 840, 0, 1, 1, -25, "", "", 3 },
            { "2314567QS====p=====p=====p===p=r==r======r==r======r==r====p=====p===p=" + 
                    "==p===~==r~p=p====p=~p=p====p=~p=p====pr======~==r==p===r=========r=p=pp=~====p=" + 
                    "pp=~==~=p=pp=~==rp=r=========r=========r=p===p=====p=====p====p=====*1(4)",
                      "sted4dm", "2314567QS----P-----P-----P---P-P--P------P--P------P--P----P-----P---P---P------P-P-P--" + 
                              "--P--P-P----P--P-P----PP---------P--P---P---------P-P-PP------P-PP------P-PP----" + 
                              "PP-P---------P---------P-", 840, 0, 1, 1, 8, "", "", 1 },
            { "2314567QS====p=====p=====p===p=r==r======r==r======r==r====p=====p===p=" + 
                    "==p===~==r~p=p====p=~p=p====p=~p=p====pr======~==r==p===r=========r=p=pp=~====p=" + 
                    "pp=~==~=p=pp=~==rp=r=========r=========r=p===p=====p=====p====p=====*1(4)",
                      "sted4dm", "2314567QS----P-----P-----P---P-P--P------P--P------P--P----P-----P---P---P------P-P-P--" + 
                              "--P--P-P----P--P-P----PP---------P--P---P---------P-P-PP------P-PP------P-PP----" + 
                              "PP-P---------P---------P-", 840, 0, 1, 1, -8, "", "", 1 },
            { "UNSAT", "sted4dm", "2314567QS----P-----P-----P---P-P--P------P--P------P--P----P-----P---P---P------P-P-P--" + 
                              "--P--P-P----P--P-P----PP---------P--P---P---------P-P-PP------P-PP------P-PP----" + 
                              "PP-P---------P---------P-", 840, 0, 1, 1, 9, "", "", 0 },
            { "UNSAT", "sted4dm", "2314567QS----P-----P-----P---P-P--P------P--P------P--P----P-----P---P---P------P-P-P--" + 
                    "--P--P-P----P--P-P----PP---------P--P---P---------P-P-PP------P-PP------P-PP----" + 
                    "PP-P---------P---------P-", 840, 0, 1, 1, -7, "", "", 0 },
            { "2314567QS===p====p========p======p==pp====ppp==p==p======p===p==p=p====p==p=====p======p==pp=pr====r==r======r=rr======r==r=rpp==p==p======p===p==p=p====p==p=====p===p===p====p====p====ppr==p====p=p=p==p======pp=p======p==p====pp=pp==p======p=====p==p====p=r==p===p======p==p==rpp=p==p======pp=p======p==p====pp=pp==p==p=====p=pp======p==p=prr==p==p======p===r==r=r====r==r=====p======p==pr=rp====p=====p==p======p===p====p======*1(2)",
              "sted2m_1234576m", "2314567QS---P----P--------P------P--PP----PPP--P--P------P---P--P-P----P--P-----P------P--PP-PP----P--P------P-PP------P--P-PPP--P--P------P---P--P-P----P--P-----P---P---P----P----P----PPP--P----P-P-P--P------PP-P------P--P----PP-PP--P------P-----P--P----P-P--P---P------P--P--PPP-P--P------PP-P------P--P----PP-PP--P--P-----P-PP------P--P-PPP--P--P------P---P--P-P----P--P-----P------P--PP-PP----P-----P--P------P---P----P------*0.8(1)", 840, 0, 1, 1, 7, "", "", 1},

            // Count bobs
            { "UNSAT",
                "sted12a", "2314567QSP----", 840, 75, 1, 1, 0, "", "", 0 },
            { "UNSAT",
              "sted12a", "2314567QSP----", 840, MINUS_0, 1, 1, 0, "", "", 0 },
            { "2314567QSP----P",
                  "sted12a", "2314567QSP----", 840, 50, 1, 1, 0, "", "", 2 },
            { "2314567QSP----P",
                      "sted12a", "2314567QSP----", 840, -49, 1, 1, 0, "", "", 1 },
            { "2314567QSP----P",
                      "sted12a", "2314567QSP----", 840, -75, 1, 1, 0, "", "", 3 },
            { "2314567QSPP--PP--PPPPPPPP----PPPPP--PPPPP--PPPPPPPP*5(4)", "sted20m", "2314567QSPP--PP--", 840, 12},
            { "UNSAT", "sted20m", "2314567QSPP", 840, -11}, // not more than 11 bobs
           
            
            // Divide by types, count Q-sets
            { "c 5040!1 2314567QSpp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=r=~=rpp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=r=rpp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=r=~=rpp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=~pp==p====pprpp==p====pp~pp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=~pp==p====pp~pp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=r=~=~=~=~=rpp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=~pp==p====pprpp==p====pp~pp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=~pp==p====pprpp==p====pp~pp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=~pp==p====pprpp==p====pp~pp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=~pp==p====pp~pp=p==p=*1(1) 402 1",
           "sted1_10", "2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P---------PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-*0.7(1)", 840, -438, 10, 1, 10 },

            // Divide by bob and type, count Q-sets
            { "c 5040!1 2314567QSr==r====p=====p=========p=p====pp==pp====pp==pp==pr==r======r==r======r==r====p=====p=====r==r======r==r======r==rpp====r==r======r==r======r==rp=p====pp==pp====pp==pppp==pp==pp==p========p=====pp===p========p=====p=========p=p====pp==pp====pp========pp==pppppr==r======r==r======r==r====p=====p=========p=p====pp==pp====pp==pp==pr==r======r==r======r==r====p=====p=====r==r======r==r======r==rpp====r==r======r==r======r==rp=p====pp==pp====pp==pppp==pp==pp==p========p=====pp===p========p=====p=========p=p====pp==pp====pp========pp==pppppr==r======r==r======r==r====p=====p=========p=p====pp==pp====pp==pp==pr==r======r==r======r==r====p=====p=====r==r======r==r======r==rpp====r==r======r==r======r==rp=p====pp==pp====pp==pppp==pp==pp==p========p=====pp===p========p=====p=========p=p====pp==pp====pp========pp==pppppr==r======r==r======*1(1) 237 1",
                    "sted1_3", "2314567QSP--P----P-----P---------P-P----PP--PP----PP--PP--PP--P------P--P------P--P----P-----P-----P--P------P--P------P--PPP----P--P------P--P------P--PP-P----PP--PP----PP--PPPP--PP--PP--P--------P-----PP---P--------P-----P---------P-P----PP--PP----PP--------PP--PPPPPP--P------P--P------*0.7(1)", 840, -603, 3, 3, 10 },

            { "2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP---P--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PP-----PPPPP---P-P--PP--P-PPPP-P---",
                        "sted1_2", "2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP---P--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PP-----PPPPP---P-P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-*0.40(1)", 840, 0, 2, 2, 0, "", "", 2 },

            // Divide by types
            { "3124567QS---PP--------PPPPPP--P------P--PP--PP------P-----PP-P---PP---P---P-----PP---PPPPP-P--------P---PPP---P------P--P",
              "sted2_3", "3124567QS---PP--------PPPPPP--P------P--PP--PP------P-----PP-P---PP---P---P-----PP---PPPPP-P--------P---PPP---P------P--P---P--P--P---PPPPPPP-P-PP---*0.6(2)", 840, 0, 3, 1, 0, "", "", 81, 0 },
            { "3124567QS---PP--------PPPPPP--P------P--PP--PP------P-----PP-P---PP---P---P-----PP---PPPPP-P--------P---PPP---P------P--P",
                  "sted2_3", "3124567QS---PP--------PPPPPP--P------P--PP--PP------P-----PP-P---PP---P---P-----PP---PPPPP-P--------P---PPP---P------P--P---P--P--P---PPPPPPP-P-PP---*0.6(2)", 840, 0, 3, 1, 0, "", "", 81, ALL_OPTS },
            { "504!", "sted10_20", "2314567QS", 42, 0, 2, 1, 0, "", "", 24, 0 },
            { "504!", "sted10_20", "2314567QS", 42, 0, 2, 1, 0, "", "", 24, ALL_OPTS },
            // provide a start to speed up the tests
            { "504!4 5132476QS", "sted10_20", "3124567QSr.=", 42, 0, 2, 2, 0, "", "", 6, 0 },
            { "504!4 5132476QS", "sted10_20", "3124567QSr.=", 42, 0, 2, 2, 0, "", "", 6, ALL_OPTS },
            { "504!4 5132476QS", "sted10_20", "3124567QSr.=", 42, 0, 1, 2, 0, "", "", 6, 0 },
            { "504!4 5132476QS", "sted10_20", "3124567QSr.=", 42, 0, 1, 2, 0, "", "", 6, ALL_OPTS },
            
            // Multiple solutions
            { "3124567QSP--", "sted20", "3124567QSP--", 840, 0, 1, 1, 0, "", "", 4 },

            // some 6-part Hamiltonian cycles
            { "3124567QS---PP--------PPPPPP--P------P--PP--PP------P-----PP-P---PP---P---P-----PP---PPPPP-P--------P---PPP---P------P--P---P--P--P---PPPPPPP-P-PP---*3(2)",
              "sted6", "3124567QS---PP--------PPPPPP--P------P--PP--PP------P-----PP-P---PP---P---P-----PP---PPPPP-P--------P---PPP---P------P--P---P--P--P---PPPPPPP-P-PP---*3(2)" },
            { "3124567QS---PP--------PPPPPP--P------P--PP--PP------P-----PP-P---PP---P---P-----PP---PPPPP-P--------P---PPP---P------P--P---P--P--P---PPPPPPP-P-PP------PP--------PPPPPP--P------P--PP--PP------P-----PP-P---PP---P---P-----PP---PPPPP-P--------P---PPP---P------P--P---P--P--P---PPPPPPP-P-PP------PP--------PPPPPP--P------P--PP--PP------P-----PP-P---PP---P---P-----PP---PPPPP-P--------P---PPP---P------P--P---P--P--P---PPPPPPP-P-PP---*1(2)",
              "sted2from6", "3124567QS---PP--------PPPPPP--P------P--PP--PP------P-----PP-P---PP---P---P-----PP---PPPPP-P--------P---PPP---P------P--P---P--P--P---PPPPPPP-P-PP---*3(2)" },
            /* too slow and replicated by 420 / 2 search
            { "3124567QS---PP--------PPPPPP--P------P--PP--PP------P-----PP-P---PP---P---P-----PP---PPPPP-P--------P---PPP---P------P--P---P--P--P---PPPPPPP-P-PP------PP--------PPPPPP--P------P--PP--PP------P-----PP-P---PP---P---P-----PP---PPPPP-P--------P---PPP---P------P--P---P--P--P---PPPPPPP-P-PP------PP--------PPPPPP--P------P--PP--PP------P-----PP-P---PP---P---P-----PP---PPPPP-P--------P---PPP---P------P--P---P--P--P---PPPPPPP-P-PP---*1(1)",
              "sted1", "2345167QSP---PP*1(1)3124567QS---PP--------PPPPPP--P------P--PP--PP------P-----PP-P---PP---P---P-----PP---PPPPP-P--------P---PPP---P------P--P---P--P--P---PPPPPPP-P-PP---*3(2)", 420, 0, 1, 1, 0, "", "", 8 },
            */
            
            // some 3 part peals
            { "2314567QSP--P----P-----P---------P-P----PP--PP----PP--PP--PP--P------P--P------P--P----P-----P-----P--P------P--P------P--PPP----P--P------P--P------P--PP-P----PP--PP----PP--PPPP--PP--PP--P--------P-----PP---P--------P-----P---------P-P----PP--PP----PP--------PP--PPPPPP--P------P--P------*3(1)",
                "sted3", "2314567QSP--P----P-----P---------P-P----PP--PP----PP--PP--PP--P------P--P------P--P----P-----P-----P--P------P--P------P--PPP----P--P------P--P------P--PP-P----PP--PP----PP--PPPP--PP--PP--P--------P-----PP---P--------P-----P---------P-P----PP--PP----PP--------PP--PPPPPP--P------P--P------*0.4(1)", 840, -201, 1, 1, 0, "", "", 1 },
            { "2314567QSP--P----P-----P---------P-P----PP--PP----PP--PP--PP--P------P--P------P--P----P-----P-----P--P------P--P------P--PP",
                "sted3", "2314567QSP--P----P-----P---------P-P----PP--PP----PP--PP--PP--P------P--P------P--P----P-----P-----P--P------P--P------P--PPP----P--P------P--P------P--PP-P----PP--PP----PP--PPPP--PP--PP--P--------P-----PP---P--------P-----P---------P-P----PP--PP----PP--------PP--PPPPPP--P------P--P------*0.4(1)", 0, 0, 1, 1, 0, "", "", 8 },
            // 1-part Erin
            { "UNSAT", "erin1", "1234567SSP-P-P-PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PPP-P--PP-P-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----PPP----P-P-P----P-P-P-P-PP-PPPPP--PPPP-PP-PPP-P-P-PP-P-P----P-P-P----P-P-P--PPPP-PP-P-P-P-P----P-P-P----PPP----P-P-P----P-P-PP--P-PPPP----P-PPP--PPPP-PP-P-PP--P-P-P----P-P-P----PPP----P-P-P----P-P-PP-P-P-P-P----P-P-P----P-P-PP-P-P-PPP-PP-PPPP--PPPPP-PP-P-P-PPP-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----PPP----P-P-P----P-*0.90(2)", 840 },
            { "1234567SSP-P-P-PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PPP-P--PP-P-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----PPP----P-P-P----P-P-P-P-PP-PPPPP--PPPP-PP-PPP-P-P-PP-P-P----P-P-P----P-P-P--PPPP-PP-P-P-P-P----P-P-P----PPP----P-P-P----P-P-PP--P-PPPP----P-PPP--PPPP-PP-P-PP--P-P-P----P-P-P----PPP----P-P-P----P-P-PP-P-P-P-P----P-P-P----P-P-PP-P-P-PPP-PP-PPPP--PPPPP-PP-P-P-PPP-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----PPP----P-P-P----P-*1(1)",
              "erin1", "4125367SS-P-P--PPPP-PP-P-P-Prp=..=pLpL.L..LL.L=L=..~p=p=*1(1)3146275SS-P--PP*1(1)2346175SSP----P-P-P----P*1(1)1234567SSP-P-P-PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PPP-P--PP-P-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----PPP----P-P-P----P-P-P-P-PP-PPPPP--PPPP-PP-PPP-P-P-PP-P-P----P-P-P----P-P-P--PPPP-PP-P-P-P-P----P-P-P----PPP----P-P-P----P-P-PP--P-PPPP----P-PPP--PPPP-PP-P-PP--P-P-P----P-P-P----PPP----P-P-P----P-P-PP-P-P-P-P----P-P-P----P-P-PP-P-P-PPP-PP-PPPP--PPPPP-PP-P-P-PPP-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----PPP----P-P-P----P-*0.99(2)", 420, 0, 1, 1, 0, "", "", 8 },
            { "5627413SSPP-P-PPP--PPPP-PP-P-PPP--PPPP-*1(1)1234567SSP-P-P-PP-P-P----P-P-P----P-P-P-P-PP-P-P----P-P-P----PPP----P-P-P----P-P-P-P-PP-PPPP--P-PPP----PPPP-P--PP-P-P----P-P-P----P-P-P--PP-P-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----P-P-P--PP-P-PP-PPPP--PPPPP-PP-P-P-P-P----P-P-P----P-P-PP-P-P-P-P----P-P-P----PPP----P-P-P----P-P-PP-P-P-P-P----P-P-P----P-P-PP--P-PPPP----P-PPP--PPPP-PP-PPP-P--PP-P-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----PPP----P-P-P----P-P-P-P-PP-P-P----P-P-P----P-P-P-P-PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PPP-P-P-PP-P-P----P-P-P----PPP----P-P-P----P-P-P--PPPP-PP-P-P-P-P----P-P-P----P-P-PP-P-P-P-P----P-P-P----PPP----P-P-P----P-P-PP--P-PPPP----P-PPP--PPPP-PP-P-PP--P-P-P----P-P-P----PPP----P-P-P----P-P-PP-P-P-P-P----P-P-P----P-P-PP-P-P-PPP-PP-PPPP--PPP-P-PP-PPPP--PPPPP-PP-P-P-PPP-PP-PPPP--PPP-P----PPPP-P--PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PP*1(1)",
                                                 "erin1", "1234567SSP-P-P-PP-P-P----P-P-P----P-P-P-P-PP-P-P----P-P-P----PPP----P-P-P----P-P-P-P-PP-PPPP--P-PPP----PPPP-P--PP-P-P----P-P-P----P-P-P--PP-P-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----P-P-P--PP-P-PP-PPPP--PPPPP-PP-P-P-P-P----P-P-P----P-P-PP-P-P-P-P----P-P-P----PPP----P-P-P----P-P-PP-P-P-P-P----P-P-P----P-P-PP--P-PPPP----P-PPP--PPPP-PP-PPP-P--PP-P-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----PPP----P-P-P----P-P-P-P-PP-P-P----P-P-P----P-P-P-P-PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PPP-P-P-PP-P-P----P-P-P----PPP----P-P-P----P-P-P--PPPP-PP-P-P-P-P----P-P-P----P-P-PP-P-P-P-P----P-P-P----PPP----P-P-P----P-P-PP--P-PPPP----P-PPP--PPPP-PP-P-PP--P-P-P----P-P-P----PPP----P-P-P----P-P-PP-P-P-P-P----P-P-P----P-P-PP-P-P-PPP-PP-PPPP--PPP-P-PP-PPPP--PPPPP-PP-P-P-PPP-PP-PPPP--PPP-P----PPPP-P--PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PP*0.95(1)", -30},
            { "3124567QSP---P---------P--P--P---P--P---------P--P-", "sted4", "3124567QSP---P---------P--P--P---P--P---------P--P-*5(1)", 840 },


            // Test other groups
            { "UNSAT", "sted168" },
            { "UNSAT", "sted168", "", 0 },
            { "UNSAT", "sted24a" },
            { "UNSAT", "sted24a", "", 0 },
            { "UNSAT", "sted24b" },
            { "UNSAT", "sted24b", "", 0 },
            { "UNSAT", "sted21" },
            { "240!", "sted21", "", 0, 0, 1, 1, 0, "", "", 100 },
            { "2314567QSPP--P----P---PP-PP-P-P-P----PP--PP", "sted12b", "2314567QSPP--P----P---PP-PP-P-P-P----PP--", -10, 0, 1, 1, 0, "", "", 2 }, 
            { "UNSAT", "sted10m", "3124567QSPP--P--P------P---P--P-P----P--P-----P------P--PP-PP----P--P-*0.5(1)" },
            { "504!", "sted10m", "3124567QSPP--P--P------P---P--P-P----P--P-----P------P--PP-PP----P--P-*0.5(1)", 0, 0, 1, 1, 0, "", "", 12 },
            { "UNSAT", "sted8", "2314567QSpppp==pp==pp==pp==pp==" },
            { "UNSAT", "sted8", "2314567QSpppp==pp==pp==pp==pp==", 0 },
            { "UNSAT", "sted8m", "2314567QSpppp==pp==pp==pp==pp==" },
            { "UNSAT", "sted8m", "2314567QSpppp==pp==pp==pp==pp==", 0 },
            { "UNSAT", "sted7", "2314567QSppppppppppp" },
            { "4267351SQ----------*1(7)", "sted7", "3264751QSpp*1(1)4372156SQpp*1(1)2435176SQp*1(1)4362517SQpp*1(1)3274615QSp=*1(1)3264175QSp*1(1)3274561QSp*1(1)", 0, 0, 1, 1, 0, "", "", 4 },
            { "2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP--------PPPPP---P-P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP--------PPPPP---P-P--PP--P-PPPP-P-PPP-P--P-*1(5)",
              "sted5",  "2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP--------PPPPP" },
            { "2314567QS----P--PPPP-P-P----P------PP---------P----P-----P--PP-PP-PP---PP----PP----P--PPPP-P-P----P------PP---------P----",
              "sted4b", "2314567QS----P--PPPP-P-P----P------PP---------P----P-----P--PP-PP-PP---PP----PP*0.9(1)", 840, 0, 1, 1, 0, "", "", 4 },
            { "UNSAT", "sted4c", "2314567QS-P---PP-PP-P-P-P----PP--PPPP-P-----P-P--------P-P---PP--P---*2(1)" },
            { "2314567QS-P---PP-PP-P-P-P----PP--PPPP-P-----P-P--------P-P---PP--P----P---PP-PP-P-P-P----PP--PPPP-P-----P-P--------P-P---PP--P----P---PP-PP-P-P-P----PP--PPPP-P-----P-P--------P-P---PP--P---*1(4)",
            "sted4c", "2314567QS-P---PP-PP-P-P-P----PP--PPPP-P-----P-P--------P-P---PP--P---*3(1)", -30 },
            { "1234567QS-P------P---P--P-P----P--P-----P------P--PP-PP----P--P------P-PP------P--P-PPP--P--P------P---P--P-P----P--P-----P------P--PP-PP----P--P------P-PP------P--P-PPP--P--P------P---P--P-P----P--P-----P------P--PP-PP----P--P------P-PP------P--P-PPP--P--P------P---P--P-P----P--P-----P------P--PP-PP----P--P------P-PP------P--P-PPP--P--P------P---P-",
              "sted2m", "1234567QS-P------P---P--P-P----P--P-----P------P--PP-PP----P--P------P-PP------P--P-PPP--P-*4(2)", -10, 0, 1, 1, 0, "", "", 16 },
            { "2314567QS---P----P--------P------P--PP----PPP--P--P------P---P--P-P----P--P-----P------P--PP-PP----P--P------P-PP------P--P-PPP--P--P------P---P--P-P----P--P-----P---P---P----P----P----PPP--P----P-P-P--P------PP-P------P--P----PP-PP--P------P-----P--P----P-P--P---P------P--P--PPP-P--P------PP-P------P--P----PP-PP--P--P-----P-PP------P--P-PPP--P--P------P---P--P-P----P--P-----P------P--PP-PP----P-----P--P------P---P----P------*1(2)",
                  "sted2m_1234576m", "2314567QS---P----P--------P------P--PP----PPP--P--P------P---P--P-P----P--P-----P------P--PP-PP----P--P------P-PP------P--P-PPP--P--P------P---P--P-P----P--P-----P---P---P----P----P----PPP--P----P-P-P--P------PP-P------P--P----PP-PP--P------P-----P--P----P-P--P---P------P--P--PPP-P--P------PP-P------P--P----PP-PP--P--P-----P-PP------P--P-PPP--P--P------P---P--P-P----P--P-----P------P--PP-PP----P-----P--P------P---P----P------*0.8(1)", 840, 0, 1, 1, 0, "", "", 1},

            { "UNSAT", "erin60" },
            { "84!", "erin60", "", 0, 0, 1, 1, 0, "", "", 10 },
            { "UNSAT", "erin24a" },
            { "210!", "erin24a", "", 0, 0, 1, 1, 0, "", "", 22 },
            { "UNSAT", "erin24b" },
            { "210!", "erin24b", "", 0, 0, 1, 1, 0, "", "", 2 },
            { "UNSAT", "erin21" },
            { "240!", "erin21", "", 0, 0, 1, 1, 0, "", "", 14 },
            { "UNSAT", "erin20" },
            { "252!", "erin20", "", 0, 0, 1, 1, 0, "", "", 5 },
            { "UNSAT", "erin12a" },
            // Q-set count 2 would reduce total from 1094 to 944 but very slow on Glucose, 101 / 64 with prefix 1234567SS~
            { "420!", "erin12a", "1234567SS~", 0, 0, 1, 1, 0, "", "", 101 },
            { "UNSAT", "erin12b" },
            { "420!", "erin12b", "", 0, 0, 1, 1, 0, "", "", 6 },
            { "UNSAT", "erin10" },
            // Also test self-loops with common source / destinations
            { "1234567SSP-P--PP-P----PPPP-P--PP-P-P----P-P-P----P-*5(2)", "erin10", "1234567SSP-P--PP-P----PPPP-P--PP-P-P----P-P-P----P-*5(2)", 42, 0, 1, 1, 0, "", "", 2, 0x1800 },
            { "UNSAT", "erin10m", "3124567SS-PP--P---P---P-PP--PP-PP-PP-PPP-P---PPP--P*0.5(2)" },
            { "504!", "erin10m", "3124567SS-PP--P---P---P-PP--PP-PP-PP-PPP-P---PPP--P*0.5(2)", 0, 0, 1, 1, 0, "", "", 8 }, // bug should be 4 
            { "UNSAT", "erin8", "3124567SSPP-P-P-P-PPPPP---P-PP-PPP--PP-P-P-P-PP*0.4(1)" },
            { "630!", "erin8", "3124567SSpp=p=p=p=pp", 0 },
            { "UNSAT", "erin8m", "2154376SSPPPPPP-P-PPPPPP-P-PPPPPP-P-*1(8)" },
            { "630!", "erin8m", "3124567SSPPP--PP*2(4)3145267SSPPPPPPP*1(8)", 0, 0, 1, 1, 0, "", "", 512 }, /* @FIXME - should be 1 */
            { "UNSAT", "erin7", "1234567SSpppp" },
            { "3124567SS", "erin6", "3124567SSrppp==p~p~p==ppp~pr====rp~p====prppp==p~p~p==ppp~pr====rp~pppppp~p~p==ppp~p~p==ppp~p~p====p*3(2)", 91, -81, 1, 1, 0, "", "", 8 },
            { "1647253SS---P-PP--P-P-P----P-P-P----P-P-PP--P-PPPP-*5(1)1234567SSP-P--PP-P----PPPP-P--PP-P-P----P-P-P----PPP----P-P-P----P-P-P--PP-P----PPPP-P--PP-PPPP--P-P-P----P-P-P----P-P-PP--P-PPPP----PP*5(1)", "erin5", "1234567SSP-P--PP-P----PPPP-P--PP-P-P----P-P-P----*0.7(1)", 126 },
            { "4627153SSPP---PP---*1(4)2647351SSPP---PP---*1(4)3647152SSP-P--P-P--*1(4)7461253SSP--P-*2(2)3247651SS-P-P--P-P-*1(4)3127654SS-PP--*2(2)1234567SS-PP---P--PP-PP-PP-PPP-P---PPP--P-PP---P--PP-PP-PP-PPP-P---PPP--P-PP---P--PP-PP-PP-PPP-P---PPP--P-PP---P--PP-PP-PP-PPP-P---PPP--P-PP---P--PP-PP-PP-PPP-P---PPP--P*1(4)", 
                "erin4", "1234567SS-PP---P--PP-PP-PP-PPP-P---PPP--P*1(1)", 160 },
            { "",
                    "erin4b", "3124567SSPP--PP--PP--PP--PP--PP--PP--*1(12)7416352SSPPPPPPP*1(12)3764152SSP--PP--PP--PP--PP--PP--PP--P*1(12)1632475SSPPPPPPP*1(12)", 0 },
            { "3124567SSP--PPP-PP-P---PPPPP-P-P-P-PP--PPP-PP-P---PPPPP-P-P-P-PP--PPP-PP-P---PPPPP-P-P-P-P*1(4)2467153SSP--P-PPPP--P-PPPP--P-PPP*1(4)7625431SS-P-PPPP--P-PPPP--P-PPPP-*1(4)4675231SS-P-PP--PPP-PP-P---PPPPP-P-P-P-PP--PPP-PP-P---PPPPP-P-P-P-PP--PPP-PP-P---PPPPP-P-P*1(4)",
                        "erin4c", "3124567SSP--PPP-PP-P---PPPPP-P-P-P-P*3(4)6432175SSPP--P-PP*3(4)7652341SSP-PPPP--*3(4)1675324SS-P-PP--PPP-PP-P---PPPPP-P-P*3(4)", 0},
            { "1246375SSPPPPPPP*1(4)1423576SSPP----P-PPPP--PP-P----PPP----PPP----P-PPPP--PP-P----PPP----P*1(4)5137264SSPPPPPP-P-PPPPPP-P-PPPPPP-P-*1(4)3521746SSP-PPPPPP-P-PPPPPP-P-PPPPPP-*1(4)2154376SSPPPPPPP*1(4)2467153SS-----*1(4)6421357SSPPPPPPP*1(4)4612735SSPPPPPPP*1(4)5421637SS-----*1(4)4267351SS-----*1(4)4531267SSPPPPPPP*1(4)5426371SS-----*1(4)1325764SSPPPPPPP*1(4)3175624SS-----*1(4)7251634SS-----*1(4)2475631SS-----*1(4)5164237SSPPPPPPP*1(4)4376521SS-----*1(4)2135476SSPPPPPPP*1(4)",
                            "erin4dm", "1246375SSPPPPPPP*1(4)1423576SSPP----P-PPPP--PP-P----PPP----PPP----P-PPPP--PP-P----PPP----P*1(4)5137264SSPPPPPP-P-PPPPPP-P-PPPPPP-P-*1(4)3521746SSP-PPPPPP-P-PPPPPP-P-PPPPPP-*1(4)2154376SSPPPPPPP*1(4)2467153SS-----*1(4)6421357SSPPPPPPP*1(4)4612735SSPPPPPPP*1(4)5421637SS-----*1(4)4267351SS-----*1(4)4531267SSPPPPPPP*1(4)5426371SS-----*1(4)1325764SSPPPPPPP*1(4)3175624SS-----*1(4)7251634SS-----*1(4)2475631SS-----*1(4)5164237SSPPPPPPP*1(4)4376521SS-----*1(4)2135476SSPPPPPPP*1(4)", 0, 0, 1, 1, 0, "", "", 1 },
            {    "2135476SS-P-PP-P-P-P-PP-P-P*1(4)1423576SSPPPPPPP*1(4)4315267SSPPPPPPP*1(4)3517264SS-----*1(4)3254176SSP--PPPPP--PPPP*1(4)4261735SS-----*1(4)4261357SSPPPP--PPPPP--P*1(4)2136457SSPP----PPPPP--P*2(2)3241675SSP--PPPPP----PP*2(2)5246173SSPPP--PPPPP--PP*1(4)2541376SSP--PPPPP--PPPP*1(4)4512637SS-P-P-PP-P-P-P-PP-P*1(4)6437152SS--PPPPP--PPPPP*1(4)1637452SS-----*1(4)1543627SS-P-P-PP-P-P-P-PP-P*1(4)7326541SS-----*1(4)2453716SS-----*1(4)7145623SS-----*1(4)3152467SSPPPP--PPPPP--P*1(4)",
                                "erin4em", 
                                "2135476SS-P-PP-P-P-P-PP-P-P*1(4)1423576SSPPPPPPP*1(4)4315267SSPPPPPPP*1(4)3517264SS-----*1(4)3254176SSP--PPPPP--PPPP*1(4)4261735SS-----*1(4)4261357SSPPPP--PPPPP--P*1(4)2136457SSPP----PPPPP--P*2(2)3241675SSP--PPPPP----PP*2(2)5246173SSPPP--PPPPP--PP*1(4)2541376SSP--PPPPP--PPPP*1(4)4512637SS-P-P-PP-P-P-P-PP-P*1(4)6437152SS--PPPPP--PPPPP*1(4)1637452SS-----*1(4)1543627SS-P-P-PP-P-P-P-PP-P*1(4)7326541SS-----*1(4)2453716SS-----*1(4)7145623SS-----*1(4)3152467SSPPPP--PPPPP--P*1(4)",
                                0, 0, 1, 1, 0, "", "", 1 },
            {    "1234567SSP----P-P-P----P-P-PP--P-PPPP----P-PP--P-P-P----P-P-P----P-P-PP--P-PPPP----P-PP--P-P-P----P-P-P----P-P-PP--P-PPPP----P-PP--P-P-P----P-P-P----P-P-PP--P-PPPP----P-PP--P-P-P----P-P-P----P-P-PP--P-PPPP----P-PP--P-P-*1(4)",
                                    "erin4em", 
                                    "1234567SSP----P-P-P----P-P-PP--P-PPPP----P-PP--P-P-*4(4)",
                                    0, 0, 1, 1, 0, "", "", 1 },
            { "1245367SSPP--PPP-PP----PP-P----PPPPP--PPP----P-PP----PP-P--PPPPP--PPP-P-PPP--P-P-P--PPP*1(3)",
              "erin3", "1245367SSPP--PPP-PP----PP-P----PPPPP--PPP----P-PP----PP-P--PPPPP--PPP-P-PPP--P-P-P--PPP*1(3)1234567SSPP----PPP----PP-PPP--P-PP----PPP----PP-P----PPPPP--P-PP----PP-P--PPP-PP----PPP----PP-P--PPP-P-P----P-*0.94(3)", 101, 0, 1, 1, 0, "", "", 4 },
            { "1423657SSPPPPPPP*1(2)1423576SSPP----P-PPPP--PP-P----PPP----PPP----P-PPPP--PP-P----PPP----P*1(2)3425167SSP----PPP----PPP----PP*1(2)4316275SSPPPPPPP*1(2)5137264SSPPPPPP-P-PPPPPP-P-PPPPPP-P-*1(2)3521746SSP-PPPPPP-P-PPPPPP-P-PPPPPP-*1(2)2534176SS-P-PPPPPP-P-PPPPPP-P-PPPPPP*1(2)2154376SSPPPPPPP*1(2)2467153SS-----*1(2)6421357SSPPPPPPP*1(2)4612735SSPPPPPPP*1(2)1624357SS-PPPPP----PPP--PPPP-P----PPP----PPP----P-PP-*1(2)3241567SSPPPPPPP*1(2)5421637SS-----*1(2)5416273SSPPPPPPP*1(2)4153267SSPPPPPP-P-PPPPPP-P-PPPPPP-P-*1(2)4623715SS-----*1(2)4531267SSPPPPPPP*1(2)5426371SS-----*1(2)5236471SSPPPPPPP*1(2)1643725SSPPPPPPP*1(2)3614725SS-----*1(2)6134257SSPPPPPPP*1(2)5134627SS-----*1(2)1325764SSPPPPPPP*1(2)3175624SS-----*1(2)5136274SS-----*1(2)7251634SS-----*1(2)2475631SS-----*1(2)7461523SS-----*1(2)1367254SS-----*1(2)5164237SSPPPPPPP*1(2)4251673SSPPPPPPP*1(2)7146352SSPPPPPPP*1(2)4735612SS-----*1(2)7263541SS-----*1(2)3476512SS-----*1(2)2135476SSPPPPPPP*1(2)",
                  "erin2m", 
              "1423657SSPPPPPPP*1(2)1423576SSPP----P-PPPP--PP-P----PPP----PPP----P-PPPP--PP-P----PPP----P*1(2)3425167SSP----PPP----PPP----PP*1(2)4316275SSPPPPPPP*1(2)5137264SSPPPPPP-P-PPPPPP-P-PPPPPP-P-*1(2)3521746SSP-PPPPPP-P-PPPPPP-P-PPPPPP-*1(2)2534176SS-P-PPPPPP-P-PPPPPP-P-PPPPPP*1(2)2154376SSPPPPPPP*1(2)2467153SS-----*1(2)6421357SSPPPPPPP*1(2)4612735SSPPPPPPP*1(2)1624357SS-PPPPP----PPP--PPPP-P----PPP----PPP----P-PP-*1(2)3241567SSPPPPPPP*1(2)5421637SS-----*1(2)5416273SSPPPPPPP*1(2)4153267SSPPPPPP-P-PPPPPP-P-PPPPPP-P-*1(2)4623715SS-----*1(2)4531267SSPPPPPPP*1(2)5426371SS-----*1(2)5236471SSPPPPPPP*1(2)1643725SSPPPPPPP*1(2)3614725SS-----*1(2)6134257SSPPPPPPP*1(2)5134627SS-----*1(2)1325764SSPPPPPPP*1(2)3175624SS-----*1(2)5136274SS-----*1(2)7251634SS-----*1(2)2475631SS-----*1(2)7461523SS-----*1(2)1367254SS-----*1(2)5164237SSPPPPPPP*1(2)4251673SSPPPPPPP*1(2)7146352SSPPPPPPP*1(2)4735612SS-----*1(2)7263541SS-----*1(2)3476512SS-----*1(2)2135476SSPPPPPPP*1(2)",
                  0, -168, 1, 1, 0, "", "", 1 },
            
            // Experimental options - combined bob/six-type, separate bob/plain vars, binary mode, timings
            { "3124567QSP---P---------P--P--P---P--P---------P--P-*5(4)", "sted20", "3124567QSP---P*1(1)", 840, 0, 1, 1, 0, "", "", 1, 0x2000 },
            { "3124567QSP---P---------P--P--P---P--P---------P--P-*5(4)", "sted20", "3124567QSP---P*1(1)", 840, 0, 1, 1, 0, "", "", 1, 0x2001 },
            { "3124567QSP---P---------P--P--P---P--P---------P--P-*5(4)", "sted20", "3124567QSP---P*1(1)", 840, 0, 1, 1, 0, "", "", 1, 0x80002002 },
            // Test distance optimisation with non-standard start position
            { "504!5 6723451SQPP", "sted10", "6723451SQPP-", 840, 0, 1, 1, 0, "", "", 1, 0x2800280 },
        });
        List<Object[]>l2 = new ArrayList<Object[]>();

        // Add the default parameters
        for (Object[]g : ret) {
            Object[] def = addDefaults(g);
            l2.add(def);
        }

        // Add some mode tests
        int opts[] = { 0, 1, 2, 3, ALL_OPTS, 1|ALL_OPTS,2|ALL_OPTS,3|ALL_OPTS,0|0x2000|ALL_OPTS,1|0x2000|ALL_OPTS,2|0x2000|ALL_OPTS,3|0x2000|ALL_OPTS };
        // Try some with lots of options
        for (int i = 0; i < 6; ++i) {
            Object g[] = ret.get(i);
            for (int o : opts) {
                Object[] def = addDefaults(g);
                def[def.length - 1] = o;
                l2.add(def);
            }
        }
        
        // Add some mode tests
        int opts2[] = { ALL_OPTS };
        // Try some with lots of options
        for (int i = 5; i < 0 * ret.size(); ++i) {
            Object g[] = ret.get(i);
            for (int o : opts2) {
                Object[] def = addDefaults(g);
                def[def.length - 1] = o;
                l2.add(def);
            }
        }

        // Performance and tuning tests
        for (int i = 1; i <= Integer.highestOneBit(ALL_OPTS) || i == 0; i <<= 1) {
            for (int ix : new int[]{5, 6}) {
                Object g[] = ret.get(ix);
                int o = i < 4 ? 0 : i;
                Object[] def = addDefaults(g);
                def[def.length - 1] = o;
                l2.add(def);
            }
        }
        
        //Use when running particular tests
        //int use[]=new int[]{33,34,35,36,37,38,39,40,41,42};
        //int use[]=new int[]{138,139,140,141,142};
        //int use[] = {67,-72}; // 128,-174 => 128-174 inclusive
        //int use[] = {67, -76};
        //int use[] = {49, 167, -174, 207,208};
        int use[]=new int[0];
        for (int i = 0; i < use.length; ++i) {
            if (use[i] < 0) {
                int use2[] = new int[use.length + (-use[i] - use[i - 1] - 1)];
                System.arraycopy(use,  0,  use2,  0,  i);
                for (int j = i; j < i + (-use[i] - use[i - 1]); ++j) {
                    use2[j] = j - i + 1 + use[i - 1];
                }
                int i1 = i + 1;
                int i2 =  i + (-use[i] - use[i - 1]);
                int i3 = use.length - i1;
                System.arraycopy(use,  i1,  use2, i2,  i3);
                use = use2;
            }
        }
        if (use.length > 0) {
            // only selected tests
            List<Object[]>l3 = new ArrayList<Object[]>();
            for (int u : use) {
                l3.add(l2.get(u));
            }
            return l3;
        }
        return l2;
    }

    private static Object[] addDefaults(Object[] g) {
        Object def[] = defaultTestParms();
        if (g[0].equals("UNSAT")) {
            // Expect no solutions
            def[10] = 0;
        }
        System.arraycopy(g, 0,  def,  0,  g.length);
        return def;
    }

    /**
     * Default test parameters.
     * @return
     * Expected result
     * Graph
     * Prefix
     * Length
     * Number of bobs
     * Divide six-type
     * Divide bob
     * Count q-sets
     * required calls
     * palindrome
     * Maximum solutions
     * Default options
     * @return
     */
    private static Object[] defaultTestParms() {
        return new Object[]{"UNSAT","","", 840, 0, 1, 1, 0, "", "", 1, 0x0};
    }
    
    /**
     * Group definition parameters.
     * @param gname
     * @return Parameters - S/E then 2 group generators, two divide group parameters
     */
    public static String[]gparms(String gname) {
        for (String[]g : graphs) {
            if (g[0].equals(gname) || g[1].equals(gname)) {
                // Defaults
                String g2 [] = {"sted1", "S", "1234567", "1234567", "1234567", "1234567"};
                System.arraycopy(g,  2,  g2,  1, g.length - 2);
                g2[0] = g[0];
                g = g2;
                return g;
            }
        }
        throw new IllegalArgumentException(gname);
    }
    
    public static File createGraphFile(String name) throws IOException {
        String g[] = gparms(name);
        File graphfile = File.createTempFile(name+"_", ".pb");
        //graphfile.deleteOnExit();
        long then = System.currentTimeMillis();
        GenSted.main(new String[]{g[1], g[2], g[3], g[4], g[5], graphfile.getPath()});
        long now =System.currentTimeMillis();
        long took = now - then;
        System.out.println("For graph "+name+" took "+took+"ms");
        return graphfile;
    }
    
    /**
     * Slight performance boost.
     */
    private static Map<String,File>graphCache = new HashMap<String,File>();
    
    public File graphfile(String name) throws IOException {
        synchronized(graphCache) {
            if (graphCache.containsKey(name)) {
                return graphCache.get(name);
            } else {
                File g = createGraphFile(name);
                graphCache.put(name,  g);
                return g;
            }
        }
    }
 
    @AfterClass
    public static void missingGraphs() {
        for (String[]g : graphs) {
            if (!graphCache.containsKey(g[0]) && !graphCache.containsKey(g[1])) {
                System.out.println("Omitted graph "+g[0]);
            }
        }
    }
    
    @AfterClass
    public static void cleanup() {
        for (File f : graphCache.values()) {
            if (!f.delete()) {
                System.err.println("Unable to delete "+f);
            };
        }
    }
    
    String expected;
    String graph;
    String prefix;
    int length;
    int bobs;
    int dividet;
    int divideb;
    /** Encoded, "-0" is MINUS_0 */
    int qsets;
    String requiredSeq;
    String palindrome;
    int options;
    long took;
    int timeout = 60;
    int maxsol = 1;
    int hashCNF;
    /**
     * Create a test case
     * @param expected the expect prefix to match for all the results, or "UNSAT"
     * @param graph local name for the graph
     * @param prefix prefix to seed the search with
     * @param length required length for the first block
     * @param bobs min/max (-ve) number of bobs
     * @param dividet same six type for {dividet} adjacent sixes
     * @param divideb same six type for {dividet} adjacent sixes
     * @param qsets min/max (-ve) number of Q sets
     * @param requiredSeq required sixes / excluded sixes
     * @param palindrome palindrome transfigure
     * @param maxsols maximum solutions
     * @param options binary coded options for encoding
     */
    public TestStedToSAT(String expected, String graph, String prefix, int length, int bobs, int dividet, int divideb, int qsets, String requiredSeq, String palindrome, int maxsols, int options) {
        this.expected = expected;
        this.graph = graph;
        this.prefix = prefix;
        this.length = length;
        this.bobs = bobs;
        this.dividet = dividet;
        this.divideb = divideb;
        this.qsets = qsets;
        this.options = options;
        this.maxsol = maxsols;
        this.requiredSeq = requiredSeq;
        this.palindrome = palindrome;
    }

    private int factorial(int n) {
        int r = 1;
        for (int i = 1; i <= n; ++i)
            r *= i;
        return r;
    }

    @Test
    public void testGraphName() throws IOException {
        File graphfile = graphfile(graph);
        Scanner s = new Scanner(graphfile, Charset.defaultCharset().name());
        Pattern p = Pattern.compile("([A-Za-z]+)([0-9]+)(.*)");
        Matcher m = p.matcher(graph);
        int ln = 0;
        int maxlen=0;
        int minlen=9999;
        boolean foundStedQS = false;
        boolean foundStedSQ = false;
        boolean foundErin = false;
        try {
            while (s.hasNextLine()) {
                String l = s.nextLine();
                foundErin |= (l.contains("SS"));
                foundStedQS |= (l.contains("QS"));
                foundStedSQ |= (l.contains("SQ"));
                String parts[] = l.split(" ");
                maxlen = Math.max(maxlen, parts[3].length());
                minlen = Math.min(minlen, parts[3].length());
                ln++;
                assertThat("graph line length check line:"+ln+" : "+l, minlen, equalTo(maxlen));
            }
        } finally {
            s.close();
        }
        if (m.matches()) {
            int bells = maxlen - 2;
            String type = m.group(1);
            int group = Integer.parseInt(m.group(2));
            String subtype = m.group(3);
            boolean ooc = subtype.endsWith("m");
            boolean erin = type.equals("erin");
            int expected = factorial(bells) / 2 * (erin ? 1 : 2) * (ooc ? 2 : 1) / group;
            assertThat(ln, equalTo(expected));
            assertThat(erin, equalTo(foundErin));
        }
        assertThat(foundStedQS, equalTo(foundStedSQ));
        assertThat(foundStedQS, not(equalTo(foundErin)));
    }
    
    /**
     * Graph of form
     * 1 4 10 2314567QS 10 2
     * @throws IOException
     */
    @Test
    public void testGraph() throws IOException {
        File graphfile = graphfile(graph);
        Scanner s = new Scanner(graphfile,Charset.defaultCharset().name());
        int dest[] = new int[5041];
        Set<String> names = new HashSet<String>();
        int ln = 0;
        try {
            while (s.hasNextLine()) {
                String l = s.nextLine();
                ln++;
                String ll[] = l.split(" ");
                int i1 = Integer.parseInt(ll[0]);
                int i2 = Integer.parseInt(ll[1]);
                int i3 = Integer.parseInt(ll[2]);
                assertThat("Expected line number", i1, equalTo(ln));
                dest[i2]++;
                dest[i3]++;
                assertThat("Expected fields", ll.length, equalTo(6));
                boolean n = names.add(ll[3]);
                assertThat("Unique name "+ll[3], n, equalTo(true));
            }
        } finally {
            s.close();
        }
        for (int j = 1; j <= ln; ++j) {
            assertThat("Destination "+j, dest[j], equalTo(2));
        }
        // At least one line
        assertThat(ln, not(equalTo(0)));
    }
    

    /**
     * Check a touch against a graph that it is true and complete.
     * @param touch
     * @throws IOException
     */
    public void checkGraph(String touch) throws IOException {
        File graphfile = graphfile(graph);
        Scanner s = new Scanner(graphfile, Charset.defaultCharset().name());
        int dest[] = new int[5041];
        int destroute[][] = new int[5041][2];
        HashMap<String, Integer> names = new HashMap<String, Integer>();
        int ln = 0;
        try {
            while (s.hasNextLine()) {
                String l = s.nextLine();
                ln++;;
                String ll[] = l.split(" ");
                int i1 = Integer.parseInt(ll[0]);
                int i2 = Integer.parseInt(ll[1]);
                int i3 = Integer.parseInt(ll[2]);
                assertThat("Expected line number", i1, equalTo(ln));
                dest[i2]++;
                dest[i3]++;
                assertThat("Expected fields", ll.length, equalTo(6));
                Integer prev = names.put(ll[3], i1);
                assertThat("Unique name "+ll[3], prev, equalTo(null));
                destroute[i1][0] = i2;
                destroute[i1][1] = i3;
            }
        } finally {
            s.close();
        }
        for (int j = 1; j <= ln; ++j) {
            assertThat("Destination "+j, dest[j], equalTo(2));
        }
        
        boolean found[] = new boolean[ln + 1];
        //System.out.println("Touch "+touch);
        while (touch.length() > 0) {
            int i;
            for (i = 0; i < touch.length(); ++i) {
                char c = touch.charAt(i);
                if ("1234567890QS".indexOf(c, 0) < 0) {
                    break;
                }
            }
            String pf = touch.substring(0, i);
            int st = names.get(pf);
            int ix = st;
            assertThat(false, equalTo(found[ix]));
            found[ix] = true;
            for (; i < touch.length(); ++i) {
                char c = touch.charAt(i);
                //System.out.println("Considering "+c+" at "+ix);
                switch (c) {
                case 'P':
                case 'p':
                case 'r':
                    ix = destroute[ix][0];
                    if (ix != st) {
                        assertThat(touch.substring(0, i+1), false, equalTo(found[ix]));
                        found[ix] = true;
                    }
                    break;
                case '-':
                case '=':
                case '~':
                    ix = destroute[ix][1];
                    if (ix != st) {
                        assertThat(touch.substring(0, i+1), false, equalTo(found[ix]));
                        found[ix] = true;
                    }
                    break;
                case '*':
                    assertThat(touch.substring(0, i+1), ix, equalTo(st));
                    break;
                default:
                    break;
                }
                if (c == ')') {
                    ++i;
                    break;
                }
            }
            touch = touch.substring(i);
        }
        
        // Now check by sixes, so count the rung six-ends
        int ff = 0;
        for (int i = 1; i <= ln; ++i) {
            if (found[i]) {
                ++ff;
            }
        }
        assertThat(0, equalTo(ln % ff));
        // Guess the type
        int sixn = ln / ff;
        // Erin, Stedman, Stedman out of course
        assertThat(sixn, anyOf(equalTo(3), equalTo(6), equalTo(12)));
        // Only one six-end of each six should be used
        for (int i = 1; i <= ln; i += sixn) {
            int f2 = 0;
            for (int j = 0; j < sixn; ++j) {
                if (found[i + j]) {
                    ++f2;
                }
            }
            assertThat(f2, equalTo(1));
        }
    }
    
    @Rule
    public ErrorCollector collector = new ErrorCollector();
    
    /**
     * Check a CNF file.
     * Check CNF header.
     * Check zero termination.
     * Check no repeated clauses.
     * @param cnf
     * @throws IOException
     */
    public int testCNF(File cnf) throws IOException {
        long then = System.currentTimeMillis();
        /*
         * would skip the entire test case so don't use
        // Ignore special group compositions for the moment
        assumeThat(dividet, equalTo(1));
        assumeThat(divideb, equalTo(1));
        // Also ignore special B-blocks for the moment
        assumeThat(prefix, not(containsString("#")));
        // problems with: order and binary encoding, ensure call to six, outputs from six
        assumeThat(options & 0x72003, equalTo(0));
         */
        // These options current generate duplicate clauses
        boolean expectedDupClauses = dividet != 1 ||
                divideb != 1 ||
                prefix.contains("#") ||
                (options & 0x72003) != 0;
        boolean expectedDupLiterals = dividet != 1 ||
                divideb != 1 || 
                (options & 0x02003) != 0;
        
        int variablescnf = 0;
        int clausescnf = 0;
        int variables = 0;
        int ln = 0;
        int clausecount = 0;
        HashMap<String, Integer>allclauses2 = new HashMap<String, Integer>();
        // Additional buffering for performance
        BufferedInputStream source = new BufferedInputStream(new FileInputStream(cnf), 1024*1024);
        try {
            Scanner s = new Scanner(source, Charset.defaultCharset().name());
            try {
                while (s.hasNextLine()) {
                    ln++;
                    String l = s.nextLine();
                    String ll[] = l.split(" ");
                    if (ll[0].equals("p") && ll[1].equals("cnf")) {
                        variablescnf = Integer.parseInt(ll[2]);
                        clausescnf = Integer.parseInt(ll[3]);
                        if (allclauses2.isEmpty()) {
                            // performance ?
                            allclauses2 = new HashMap<String, Integer>(clausescnf);
                        }
                        System.out.println("0x"+Integer.toHexString(this.options)+" "+l);
                    } else if (ll[0].equals("c")) {
                        continue;
                    } else {
                        int clauseLen = ll.length;
                        int cl[] = new int[clauseLen];
                        int clplus[] = new int[clauseLen];
                        boolean foundzero = false;
                        int cli = 0;
                        for (String lit : ll) {
                            assertThat(l, foundzero, equalTo(false));
                            int vv = Integer.parseInt(lit);
                            if (vv == 0) {
                                foundzero = true; 
                            }
                            variables = Math.max(variables, Math.abs(vv));
                            clplus[cli] = Math.abs(vv);
                            cl[cli++] = vv;
                        }
                        // Sort ignoring last zero for canonical version
                        Arrays.sort(cl, 0, cli - 1);
                        Arrays.sort(clplus, 0, cli - 1);
                        // check for duplicates
                        // duplicate literals are pointless
                        String msg = cnf.getName()+":"+ln+":"+l;
                        if (!expectedDupLiterals) {
                            for (int i = 1; i < cl.length; ++i) {
                                collector.checkThat(msg, cl[i], not(equalTo(cl[i - 1])));
                            }
                        }
                        // opposite literals not allowed - redundant clauses
                        for (int i = 1; i < clauseLen; ++i) {
                            if (clplus[i] == clplus[i -1]) {
                                // Found a positive duplicate - perhaps a negative/positive one
                                for (int j = 0; j < clauseLen; ++j) {
                                    if (cl[j] == -clplus[i]) {
                                        // Negative instance
                                        for (int k = j; k < cl.length; ++k) {
                                            // Matches positive instance
                                            collector.checkThat(msg, cl[k], not(equalTo(-cl[j])));
                                        }
                                        break;
                                    }
                                }
                            }
                        }

                        // and generate stable name
                        String clname = Arrays.toString(cl);
                        collector.checkThat(msg, true, equalTo(foundzero));
                        // duplicate clauses
                        if (!expectedDupClauses && clausecount - allclauses2.size() < 10) {
                            // only report the first few errors
                            collector.checkThat("duplicate clauses " + cnf.getName()+":"+ln+":"+allclauses2.get(clname)+":"+l, allclauses2.get(clname), equalTo(null));
                        }
                        allclauses2.put(clname, ln);
                        ++clausecount;
                    }
                }
            } finally {
                s.close();
            }
        } finally {
            source.close();
        }
        collector.checkThat(cnf.getName()+" CNF variables", variablescnf, equalTo(variables));
        collector.checkThat(cnf.getName()+" CNF clauses", clausescnf, equalTo(clausecount));
        long now = System.currentTimeMillis();
        long took = now - then;
        System.out.println("Took "+took+"ms to check "+cnf.getName());
        return allclauses2.hashCode();
    }
    
    //@Ignore
    @Test
    public void test1() throws IOException, ParseFormatException {
        // assumeThat((options & 0x800), not(equalTo(0)));
        took = time1();
    }
    
    public long time1() throws IOException, ParseFormatException {
        //assumeThat(qsets, not(equalTo(0)));
        //assumeThat(length, equalTo(2));
        //assumeThat(graph, equalTo("sted60"));
        String hexopt = "0x"+Integer.toHexString(options);
        String ret = "UNSAT";
        File satfile = File.createTempFile(graph +"_" + hexopt + "_", ".sat");
        // satfile.deleteOnExit();
        File satoutfile = File.createTempFile(graph +"_" + hexopt + "_", ".satout");
        //satoutfile.deleteOnExit();
        File graphfile = graphfile(graph);
        System.out.println("Graph file "+graphfile.length()+" "+graphfile.getName());
        String bobsString = bobs == MINUS_0 ? "-0" : ""+bobs; 
        String qsetsString = qsets == MINUS_0 ? "-0" : ""+qsets; 
        long then1 = System.currentTimeMillis();
        StedToSAT.main(new String[]{graphfile.getPath(), prefix, ""+length, "", hexopt, satfile.getPath(), bobsString, ""+dividet, ""+divideb, qsetsString, requiredSeq, palindrome});
        long now1 = System.currentTimeMillis();
        long took1 = now1 - then1;
        System.out.println("SAT file length "+satfile.length()+" "+satfile.getName()+" in "+took1+"ms");
        
        // Check format of the CNF
        hashCNF = testCNF(satfile);
        
        int sols = solveSAT(satfile, satoutfile, hexopt);
        
        // Process the result
        int solcount = 0;
        int containsCount = 0;
        int dupscount = 0;
        String solutions[] = new String[sols];
        if (sols >= 1) {
            // Only reprocess if some solutions found
            then1 = System.currentTimeMillis();
            StedToSAT.main(new String[]{graphfile.getPath(), prefix, ""+length, satoutfile.getPath(), hexopt, satfile.getPath(), bobsString, ""+dividet, ""+divideb, qsetsString, requiredSeq});
            now1 = System.currentTimeMillis();
            System.out.println("SAT file length "+satfile.length()+" "+satfile.getName()+" in "+took1+"ms");
            Scanner sc = new Scanner(satfile,Charset.defaultCharset().name());
            // Look for the last touch line
            try {
                while (sc.hasNextLine()) {
                    String l = sc.nextLine();
                    if (l.contains("!")) {
                        System.out.println(l);
                        for (int si = 0; si < solcount; ++si)
                        {
                            if (solutions[si].equals(l))
                            {
                                // A duplicate
                                ++dupscount;
                                break;
                            }
                        }
                        solutions[solcount] = l;
                        ++solcount;
                        if (ret.equals("UNSAT")) {
                            // Record the first solution
                            ret = l;
                        }
                        if (l.contains(expected)) {
                            ++containsCount;
                        }
                    }
                }
            } finally {
                sc.close();
            }
        } else {
            Scanner sc = new Scanner(satoutfile,Charset.defaultCharset().name());
            try {
                if (sc.hasNextLine()) {
                    String l = sc.nextLine();
                    if (l.equals("s INDETERMINATE") || l.equals("INDET")) {
                        ret = "INDET";
                    }
                }
            } finally {
                sc.close();
            }
        }
        
        // Check against expected for one solution
        assertThat(ret, containsString(expected));
        // Internal consistency
        assertThat(sols, equalTo(solcount));
        assertThat("Expected number of solutions", solcount - dupscount, equalTo(maxsol));
        assertThat(expected+"\n"+Arrays.toString(solutions), containsCount, equalTo(solcount));
        // Duplicate solutions are sometimes possible with the six-type encoding
        if (!((options & 3) == 1 && (options & (1 << 13)) != 0)) 
            assertThat("No duplicate solutions expected", dupscount, equalTo(0));
        
        // Check ret satisfies the graph
        if (ret.contains("!")) {
            String rets[] = ret.split(" ");
            checkGraph(rets[2]);
        }
        
        // Clean-up - files will be retained in the event of an error
        if (!satfile.delete()) {
            System.err.println("Unable to delete SAT file "+satfile);
        }
        if (!satoutfile.delete()) {
            System.err.println("Unable to delete SAT result file "+satoutfile);
        }
        return took;
    }

    static int mode = 1;

    private int solveSAT(File satfile, File satoutfile, String hexopt)
            throws FileNotFoundException, ParseFormatException, IOException {
        switch (mode) {
        case 1:
            return solveSAT4J(satfile, satoutfile, hexopt);
        case 2:
            return solveCryptominisat5(satfile, satoutfile, hexopt);
        case 3:
            return solveGlucoseN(satfile, satoutfile, hexopt);
        case 4:
            return solveLingelingN(satfile, satoutfile, hexopt);
        case 5:
            return solveCadicalN(satfile, satoutfile, hexopt);
        case 6:
            return solveKissatN(satfile, satoutfile, hexopt);
        case 12:
            return solveCryptominisat5b(satfile, satoutfile, hexopt);
        case 13:
            return solveGlucoseNb(satfile, satoutfile, hexopt);
        case 14:
            return solveLingelingNb(satfile, satoutfile, hexopt);
        case 15:
            return solveCadicalNb(satfile, satoutfile, hexopt);
        case 16:
            return solveKissatNb(satfile, satoutfile, hexopt);
        default:
            return 0;
        }
    }
    
    /**
     * Solve using SAT4J.
     * Solve for multiple solutions - do one more to confirm all retrieved.
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws FileNotFoundException
     * @throws ParseFormatException
     * @throws IOException
     */
    private int solveSAT4J(File satfile, File satoutfile, String hexopt)
            throws FileNotFoundException, ParseFormatException, IOException {
        ISolver solver = SolverFactory.newDefault();
        solver.setTimeout(timeout); // e.g. 60 seconds
        Reader reader;
        if (maxsol >= 1) {
            // Multiple solutions
            ModelIterator mi = new ModelIterator(solver, maxsol + 1);
            reader = new DimacsReader(mi);
        } else {
            reader = new DimacsReader(solver);
        }
        long totaltook = 0;
        long took1 = 0;
        int sols = 0;
        PrintWriter out = new PrintWriter(satoutfile,Charset.defaultCharset().name());
        try {
            // CNF filename is given on the command line
            long then = System.currentTimeMillis();
            try {
                IProblem problem = reader.parseInstance(satfile.getPath());
                long now1 = System.currentTimeMillis();
                took1 = now1 - then;
                System.out.println("Took "+took1+"ms to parse");
                then = System.currentTimeMillis();
                for (sols = 0; sols < maxsol + 1 && problem.isSatisfiable(); ) {
                    ++sols;
                    long now = System.currentTimeMillis();
                    took1 = now - then;
                    totaltook += took1;
                    System.out.println("Satisfiable ! in "+took1+"ms for "+hexopt);
                    reader.decode(problem.model(),out);
                    // Doesn't otherwise add a new line, so 2 solutions joined on one line
                    out.println();
                    then = System.currentTimeMillis();
                }
                long now = System.currentTimeMillis();
                took1 += now - then;
                totaltook += took1;
                if (sols == 0) {
                    System.out.println("Unsatisfiable ! in "+took1+"ms for "+hexopt);
                    out.println("UNSAT");
                } else if (sols < maxsol + 1) {
                    System.out.println("Unsatisfiable ! after "+sols+ " solutions in "+took1+"ms for "+hexopt);
                    out.println("UNSAT");
                }
            } catch (FileNotFoundException e) {
                throw e;
            } catch (ParseFormatException e) {
                throw e;
            } catch (IOException e) {
                throw e;
            } catch (ContradictionException e) {
                long now = System.currentTimeMillis();
                took1 = now - then;
                totaltook += took1;
                System.out.println("Unsatisfiable (trivial)! in "+took1+"ms");
                out.println("UNSAT");
            } catch (TimeoutException e) {
                long now = System.currentTimeMillis();
                took1 = now - then;
                totaltook += took1;
                System.out.println("Timeout, sorry! after "+took1+"ms for "+hexopt);
                out.println("INDET");
            }
        } finally {
            out.close();
        }
        took = totaltook;
        System.out.println("SATout size "+satoutfile.length()+" "+satoutfile.getName()+" took "+totaltook+"ms in total for "+hexopt);
        return sols;
    }
    
    /**
     * Convert a Windows format of filename to a Cygwin format.
     */
    private static String wintocygfile(String fn) {
        fn = fn.replace(File.separator, "/");
        if (fn.charAt(1) == ':') {
            // C: -> /cygdrive/c
            fn = "/cygdrive/"+fn.substring(0, 1).toLowerCase(Locale.ENGLISH)+fn.substring(2);
        }
        return fn;
    }
    
    /**
     * Convert a Cygwin format of filename to Windows format.
     */
    private static String cygtowinfile(String fn) {
        if (fn.startsWith("/cygdrive/")) {
             // /cygdrive/c -> C:
            fn = fn.substring(10, 11).toUpperCase(Locale.ENGLISH)+":"+fn.substring(11).replace("/", File.separator);
        } else if (fn.startsWith("/")) {
            fn = CYGWIN_HOME+fn.replace("/", File.separator);
        } else {
            fn = fn.replace("/", File.separator);
        }
        return fn;
    }
    
    /**
     * Solve cnf for up to maxsol solutions using CryptoMinisat via bash
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveCryptominisat5b(File satfile, File satoutfile, String hexopt) throws IOException {
        // Have one more solution to check all are found - N SAT + 1 UNSAT
        // --reconfat=0 --reconf=15
        String cmdarray[] = {CYGWIN_PATH+File.separator+"bash", "-c", "/usr/local/bin/cryptominisat5 --reconfat=0 --reconf=4 --verb=0 --maxsol="+(maxsol+1)+" --maxtime="+timeout+" --dumpresult "+wintocygfile(satoutfile.getPath())+" "+wintocygfile(satfile.getPath())};
        return solveExternalCommand(satfile, satoutfile, hexopt, cmdarray, CYGWIN_PATH);
    }
    
    /**
     * Solve cnf for up to maxsol solutions using CryptoMinisat
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveCryptominisat5(File satfile, File satoutfile, String hexopt) throws IOException {
        // Have one more solution to check all are found - N SAT + 1 UNSAT
        String cmdarray[] = {cygtowinfile("/usr/local/bin/cryptominisat5"), "--reconfat=0", "--reconf=4", "--verb=0", "--maxsol="+(maxsol+1), "--maxtime", ""+timeout, "--dumpresult", wintocygfile(satoutfile.getPath()), wintocygfile(satfile.getPath())};
        return solveExternalCommand(satfile, satoutfile, hexopt, cmdarray, CYGWIN_PATH);
    }

    private static final String GLUCOSE_4_2_1_CYGWIN_PATH = CYGWIN_WIN_USER_PATH + "/git/glucose/sources/simp/glucose_static.exe";
    private static final String GLUCOSE_4_1_CYGWIN_PATH = CYGWIN_WIN_USER_PATH + "/Downloads/glucose-syrup-4.1/simp/glucose_static.exe";
    private static final String GLUCOSE_CYGWIN_PATH =  GLUCOSE_4_2_1_CYGWIN_PATH;
    /**
     * Solve one instance using Glucose via bash
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveGlucose1(File satfile, File satoutfile, String hexopt) throws IOException {
        // Just finds one solution
        String cmdarray[] = {CYGWIN_PATH+File.separator+"bash", "-c", GLUCOSE_CYGWIN_PATH + " -verb=0 -model "+wintocygfile(satfile.getPath())+" "+wintocygfile(satoutfile.getPath())};
        return solveExternalCommand(satfile, satoutfile, hexopt, cmdarray, CYGWIN_PATH);
    }
    
    /**
     * Solve one instance using Glucose
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveGlucose1a(File satfile, File satoutfile, String hexopt) throws IOException {
        // Just finds one solution
        String cmdarray[] = {cygtowinfile(GLUCOSE_CYGWIN_PATH), "-verb=0", "-model", wintocygfile(satfile.getPath()), wintocygfile(satoutfile.getPath())};
        return solveExternalCommand(satfile, satoutfile, hexopt, cmdarray, CYGWIN_PATH);
    }
    
    /**
     * Solve N instances using Glucose
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveGlucoseNb(File satfile, File satoutfile, String hexopt) throws IOException {
        if (maxsol == 0) {
            return solveGlucose1(satfile, satoutfile, hexopt);
        }
        String cmdarray[] = {CYGWIN_PATH+File.separator+"bash", "-c", GLUCOSE_CYGWIN_PATH + " -verb=0 -model "+wintocygfile(satfile.getPath())+" "+wintocygfile(satoutfile.getPath())};
        return solveExternalCommandN(satfile, satoutfile, hexopt, cmdarray, CYGWIN_PATH);
    }
    
    /**
     * Solve N instances using Glucose
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveGlucoseN(File satfile, File satoutfile, String hexopt) throws IOException {
        if (maxsol == 0) {
            return solveGlucose1a(satfile, satoutfile, hexopt);
        }
        String cmdarray[] = {cygtowinfile(GLUCOSE_CYGWIN_PATH), "-verb=0", "-model", wintocygfile(satfile.getPath()), wintocygfile(satoutfile.getPath())};
        return solveExternalCommandN(satfile, satoutfile, hexopt, cmdarray, CYGWIN_PATH);
    }

    private static final String LINGELING_CYGWIN_PATH = CYGWIN_WIN_USER_PATH + "/git/lingeling/lingeling.exe"; 
    /**
     * Solve 1 instance using Lingeling via bash
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveLingeling1(File satfile, File satoutfile, String hexopt) throws IOException {
        String cmdarray[] = {CYGWIN_PATH+File.separator+"bash", "-c", "LINGELING_CYGWIN_PATH" + "--verbose=0 -T "+timeout+" "+wintocygfile(satfile.getPath())};
        return solveExternalCommand(satfile, satoutfile, hexopt, cmdarray, CYGWIN_PATH);
    }
    
    /**
     * Solve 1 instance using Lingeling
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveLingeling1a(File satfile, File satoutfile, String hexopt) throws IOException {
        String cmdarray[] = {cygtowinfile(LINGELING_CYGWIN_PATH), "--verbose=0", "-T", ""+timeout, wintocygfile(satfile.getPath())};
        return solveExternalCommand(satfile, satoutfile, hexopt, cmdarray, CYGWIN_PATH);
    }

    /**
     * Solve N instances using Lingeling via bash
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveLingelingNb(File satfile, File satoutfile, String hexopt) throws IOException {
        if (maxsol == 0) {
            return solveLingeling1(satfile, satoutfile, hexopt);
        }
        String cmdarray[] = {CYGWIN_PATH+File.separator+"bash", "-c", LINGELING_CYGWIN_PATH + " --verbose=0 -T "+timeout+" "+wintocygfile(satfile.getPath())};
        return solveExternalCommandN(satfile, satoutfile, hexopt, cmdarray, CYGWIN_PATH);
    }
    
    /**
     * Solve N instances using Lingeling
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveLingelingN(File satfile, File satoutfile, String hexopt) throws IOException {
        if (maxsol == 0) {
            return solveLingeling1a(satfile, satoutfile, hexopt);
        }
        String cmdarray[] = {cygtowinfile(LINGELING_CYGWIN_PATH), "--verbose=0",  "-T", ""+timeout, wintocygfile(satfile.getPath())};
        return solveExternalCommandN(satfile, satoutfile, hexopt, cmdarray, CYGWIN_PATH);
    }

    private static final String CADICAL_CYGWIN_PATH = CYGWIN_WIN_USER_PATH + "/git/cadical/build/cadical.exe"; 
    /**
     * Solve 1 instance using CaDiCal via bash
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveCadical1(File satfile, File satoutfile, String hexopt) throws IOException {
        String cmdarray[] = {CYGWIN_PATH+File.separator+"bash", "-c", "CADICAL_CYGWIN_PATH" + "--sat --quiet -t "+timeout+" "+wintocygfile(satfile.getPath())};
        return solveExternalCommand(satfile, satoutfile, hexopt, cmdarray, CYGWIN_PATH);
    }

    /**
     * Solve 1 instance using Cadical
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveCadical1a(File satfile, File satoutfile, String hexopt) throws IOException {
        String cmdarray[] = {cygtowinfile(CADICAL_CYGWIN_PATH), "--sat", "--quiet", "-t", ""+timeout, wintocygfile(satfile.getPath())};
        return solveExternalCommand(satfile, satoutfile, hexopt, cmdarray, CYGWIN_PATH);
    }

    /**
     * Solve N instances using CaDiCal via bash
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveCadicalNb(File satfile, File satoutfile, String hexopt) throws IOException {
        if (maxsol == 0) {
            return solveCadical1(satfile, satoutfile, hexopt);
        }
        String cmdarray[] = {CYGWIN_PATH+File.separator+"bash", "-c", CADICAL_CYGWIN_PATH + "--sat --quiet -t "+timeout+" "+wintocygfile(satfile.getPath())};
        return solveExternalCommandN(satfile, satoutfile, hexopt, cmdarray, CYGWIN_PATH);
    }

    /**
     * Solve N instances using CaDiCal
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveCadicalN(File satfile, File satoutfile, String hexopt) throws IOException {
        if (maxsol == 0) {
            return solveCadical1a(satfile, satoutfile, hexopt);
        }
        String cmdarray[] = {cygtowinfile(CADICAL_CYGWIN_PATH), "--sat", "--quiet",  "-t", ""+timeout, wintocygfile(satfile.getPath())};
        return solveExternalCommandN(satfile, satoutfile, hexopt, cmdarray, CYGWIN_PATH);
    }

    private static final String KISSAT_CYGWIN_PATH = CYGWIN_WIN_USER_PATH + "/git/kissat/build/kissat.exe"; 
    /**
     * Solve 1 instance using Kissat via bash
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveKissat1(File satfile, File satoutfile, String hexopt) throws IOException {
        String cmdarray[] = {CYGWIN_PATH+File.separator+"bash", "-c", "KISSAT_CYGWIN_PATH" + "--sat --quiet --time="+timeout+" "+wintocygfile(satfile.getPath())};
        return solveExternalCommand(satfile, satoutfile, hexopt, cmdarray, CYGWIN_PATH);
    }

    /**
     * Solve 1 instance using Kissat
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveKissat1a(File satfile, File satoutfile, String hexopt) throws IOException {
        String cmdarray[] = {cygtowinfile(KISSAT_CYGWIN_PATH), "--sat", "--quiet", "--time="+timeout, wintocygfile(satfile.getPath())};
        return solveExternalCommand(satfile, satoutfile, hexopt, cmdarray, CYGWIN_PATH);
    }

    /**
     * Solve N instances using Kissat via bash
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveKissatNb(File satfile, File satoutfile, String hexopt) throws IOException {
        if (maxsol == 0) {
            return solveKissat1(satfile, satoutfile, hexopt);
        }
        String cmdarray[] = {CYGWIN_PATH+File.separator+"bash", "-c", KISSAT_CYGWIN_PATH + "--sat --quiet --time="+timeout+" "+wintocygfile(satfile.getPath())};
        return solveExternalCommandN(satfile, satoutfile, hexopt, cmdarray, CYGWIN_PATH);
    }

    /**
     * Solve N instances using Kissat
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveKissatN(File satfile, File satoutfile, String hexopt) throws IOException {
        if (maxsol == 0) {
            return solveKissat1a(satfile, satoutfile, hexopt);
        }
        String cmdarray[] = {cygtowinfile(KISSAT_CYGWIN_PATH), "--sat", "--quiet",  "--time="+timeout, wintocygfile(satfile.getPath())};
        return solveExternalCommandN(satfile, satoutfile, hexopt, cmdarray, CYGWIN_PATH);
    }

    /**
     * Solve one instance using external command.
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @return
     * @throws IOException
     */
    private int solveExternalCommand(File satfile, File satoutfile, String hexopt, String cmdarray[], String pathadd) throws IOException {
        ProcessBuilder pb = new ProcessBuilder(cmdarray);
        pb.redirectErrorStream(true);
        if (pathadd != null && pathadd.length() != 0) { 
            Map<String, String> env = pb.environment();
            // System.out.println(env);
            // Difference between Eclipse run and debug mode!
            String pathenv = "PATH";
            String oldpath = env.get(pathenv);
            if (oldpath==null) {
                pathenv = "Path";
                oldpath = env.get(pathenv);
            }
            if (!oldpath.endsWith(pathadd) && !oldpath.startsWith(pathadd+File.pathSeparator) && !oldpath.contains(File.pathSeparator+pathadd+File.pathSeparator)) {
                String newpath = CYGWIN_PATH+File.pathSeparator+oldpath;
                env.put(pathenv, newpath);
                //System.out.println(pathenv+"="+newpath);
            }
        }
        boolean cmdUsesSysout = true;
        for (String cm : cmdarray) {
            if (cm.contains(satoutfile.getName())) {
                cmdUsesSysout = false;
            }
        }
        int rc = 0;
        long then = System.currentTimeMillis();
        long totaltook = 0;
        long took1 = 0;

        int satcount = 0;
        try {
            Process p = pb.start();
            InputStream pin = p.getInputStream();
            try {
                try {
                    InputStreamReader r = new InputStreamReader(pin, Charset.defaultCharset());
                    try {
                        BufferedReader input = new BufferedReader(r);
                        try {
                            PrintWriter pw = cmdUsesSysout ? new PrintWriter(satoutfile, Charset.defaultCharset().name()) : null;
                            try {
                                String line;
                                while ((line = input.readLine()) != null) {
                                    System.out.println(line);
                                    if (line.startsWith("c s UNKNOWN")) {
                                        // Fix up a solver!
                                        line = "s INDETERMINATE";
                                    } else if (line.startsWith("c UNKNOWN")) {
                                        // Fix up a solver!
                                        line = "s INDETERMINATE";
                                    }
                                    if (pw != null && (line.startsWith("s ") || line.startsWith("v ")) ) {
                                        pw.println(line);
                                    }
                                    if (line.contains("s SATISFIABLE")) {
                                        ++satcount;
                                        long now = System.currentTimeMillis();
                                        took1 = now - then;
                                        System.out.println("Took "+took1+"ms for a solution");
                                        totaltook += took1;
                                        then = now;
                                    } else if (line.contains("s UNSATISFIABLE")) {
                                        long now = System.currentTimeMillis();
                                        took1 = now - then;
                                        System.out.println("Took "+took1+"ms to confirm no solution");
                                        totaltook += took1;
                                        then = now;
                                    } else if (line.contains("s INDETERMINATE")) {
                                        long now = System.currentTimeMillis();
                                        took1 = now - then;
                                        System.out.println("Took "+took1+"ms to not being able to determine whether there is a solution or not");
                                        totaltook += took1;
                                        then = now;
                                    }
                                }
                            } finally {
                                if (pw != null) {
                                    pw.close();
                                }
                            }
                        } finally {
                            input.close();
                        }
                        try {
                            rc = p.waitFor();
                            p = null;
                        } catch (InterruptedException e) {

                        }
                    } finally {
                        r.close();
                    }
                } finally {
                    pin.close();
                }
            } finally {
                if (p != null) {
                    p.destroy();
                }
            }    
        } finally {
            long now = System.currentTimeMillis();
            took1 = now - then;
            System.out.println("Took "+took1+"ms to finish");
            totaltook += took1; 
            took += totaltook;
            System.out.println("SATout size "+satoutfile.length()+" "+satoutfile.getName()+" took "+totaltook+"ms in total for "+hexopt+" rc="+rc);
        }
        return satcount;
    }
    
    /**
     * parse the variables state from a result file for a single result
     * @param result
     * @return a list of variables, positive for true
     * SAT
     * 1 -2 -3
     * 
     * UNSAT
     * 
     * INDET
     * 
     * 1 -2 -3
     * 
     * c comment
     * s SATISFIABLE
     * v 1 -2 -3
     * v -
     * 
     * c comment
     * s UNSATISFIABLE
     * 
     * c comment
     * s INDETERMINATE
     * 
     * @throws IOException
     */
    List<Integer> parseResult(File result) throws IOException {
        Scanner s = new Scanner(result, Charset.defaultCharset().name());
        List<Integer>vals = new ArrayList<Integer>();
        try {
            if (!s.hasNextInt() || s.hasNext("SAT|UNSAT|INDET")) {
                s.nextLine();
            } else {
                while (s.hasNext()) {
                    s.next();
                    if (s.hasNext("SATISFIABLE||UNSATISFIABLE||INDETERMINATE"))
                        break;
                    s.nextLine();
                }
            }
            while (s.hasNext()) {
                if (s.hasNextInt()) {
                    int v = s.nextInt();
                    vals.add(v);
                    if (v == 0)
                        break;
                } else {
                    s.next();
                }
            }
        } finally {
            s.close();
        }
        return vals;
    }
    
    /**
     * Add the solved integers inverted to file2 after having copied file f,
     * but adjust cnf header for extra clauses.
     * @param f1
     * @param f2
     * @param solved
     * @throws IOException
     */
    private void append(File f1, File f2, List<List<Integer>>solved) throws IOException {
        Scanner s1 = new Scanner(f1, Charset.defaultCharset().name());
        try {
            PrintWriter pw = new PrintWriter(f2, Charset.defaultCharset().name());
            try {
                while (s1.hasNextLine()) {
                    String l = s1.nextLine();
                    if (l.startsWith("p cnf ")) {
                        // Fix header
                        String l2[] = l.split(" ");
                        int clauses = Integer.parseInt(l2[3]);
                        pw.println("p cnf "+l2[2]+" "+(clauses+solved.size()));
                    } else {
                        pw.println(l);
                    }
                }
                // Add blocking clauses to remove this solution next time
                for (List<Integer>v : solved) {
                    for (int i = 0; i < v.size(); ++i) {
                        pw.print(-v.get(i));
                        if (i < v.size()) {
                            pw.print(' ');
                        };
                    }
                    // End the line
                    pw.println();
                }
            } finally {
                pw.close();
            }
        } finally {
            s1.close();
        }
    }
    
    /**
     * Solve using an external solver, but call it multiple times.
     * @param satfile
     * @param satoutfile
     * @param hexopt
     * @param cmdarray
     * @param pathadd
     * @return
     * @throws IOException
     */
    private int solveExternalCommandN(File satfile, File satoutfile, String hexopt, String cmdarray[], String pathadd) throws IOException {
        // find n solutions
        int count = 0;
        List<List<Integer>>solved = new ArrayList<List<Integer>>();
        // Related name
        int ix = satoutfile.getName().lastIndexOf('_');
        File tempSatoutfile = File.createTempFile(satoutfile.getName().substring(0, ix + 1), ".satout");
        ix = satfile.getName().lastIndexOf('_');
        File tempSatfile = File.createTempFile(satfile.getName().substring(0, ix + 1), ".sat");
        // Keep the SAT file under a new name
        if (!tempSatfile.delete()) {
            System.err.println("Unable to delete "+tempSatfile);
        }
        if (!satfile.renameTo(tempSatfile)) {
            System.err.println("Unable to rename "+satfile+" to "+tempSatfile);
        }

        do {
            // Add the blocking clauses
            append(tempSatfile, satfile, solved);
            int c = solveExternalCommand(satfile, satoutfile, hexopt, cmdarray, pathadd);
            
            // Append to final file
            appendFile(satoutfile, tempSatoutfile);
            
            if (c == 0) {
                break;
            }
            List<Integer>v = parseResult(satoutfile);
            solved.add(v);

            ++count;
        } while (count <= maxsol);
        
        // Rename back
        if (!satfile.delete()) {
            System.err.println("Unable to delete "+satfile);
        }
        tempSatfile.renameTo(satfile);
        if (!satoutfile.delete()) {
            System.err.println("Unable to delete "+satoutfile);
        }
        tempSatoutfile.renameTo(satoutfile);
        System.out.println("SATout size "+satoutfile.length()+" "+satoutfile.getName()+" took "+took+"ms in total for "+hexopt+" with "+count+" results");
        return count;
    }

    /**
     * Copy the source to the end of the other file.
     * @param source
     * @param appendTo
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void appendFile(File source, File appendTo) throws FileNotFoundException, IOException {
        FileInputStream fis = new FileInputStream(source);
        int cc = 0;
        try {
            FileOutputStream fos = new FileOutputStream(appendTo, true);
            try {
                int r;
                while ((r = fis.read()) >= 0) {
                    ++cc;
                    if (r < 32 && r != '\r' && r != '\n' || r >= 127)
                        throw new IllegalStateException(cc+":"+r);
                    fos.write(r);
                }
                fos.write('\n'); // safety
            } finally {
                fos.close();
            }
        } finally {
            fis.close();
        }
    }
    
    /**
     * Autotune parameters.
     * Walk through each setting, find the best of n = 3 runs.
     * Choose the best add to the list once we have tried everything.
     * Exclude really bad options from next runs.
     * Repeat until adding options does not help.
     * @param argv
     * @throws IOException
     * @throws ParseFormatException
     */
    public static void main(String argv[]) throws IOException, ParseFormatException {
        //System.getProperties().store(System.out, "system config");
        String expected = argv.length > 0 ? argv[0] : "2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P";
        String graph  = argv.length > 1 ? argv[1] : "sted10";
        String prefix  = argv.length > 2 ? argv[2] : "2314567QSpp~"; 
        int length = argv.length > 3 ? Integer.parseInt(argv[3]) : 840;
        int bobs = argv.length > 4 ? Integer.parseInt(argv[4]) :0;
        int divg = argv.length > 5 ? Integer.parseInt(argv[5]) :1;
        int divb = argv.length > 6 ? Integer.parseInt(argv[6]) : 1;
        int countqsets = argv.length > 7 ? Integer.parseInt(argv[7]) : 0;
        int maxsols = argv.length > 8 ? Integer.decode(argv[8]) : 1;
        String requiredSeq = argv.length > 9 ? argv[9] : "";
        int optinit = argv.length > 10 ? Integer.decode(argv[10]) : 0;
        String palindrome = "";
        // Slightly longer run, takes about 90 seconds
        expected = "---PPPPP---";// Allow for reverses
        maxsols = 4;
        prefix = "";
        // 5-part params for about 45 seconds
        expected = "2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P";
        prefix = "2314567QSpp===ppp==p=p==";
        graph= "sted5";
        maxsols= 1;
        if (false) {
        expected = "";
        prefix = "1234567SSp=p==pp~p=pp=pppp==ppp~p====pppp=p==pp=p~p====p=p=p====prp====p=p=p====p~p=p=p=pp=prpp==p=p~p====p=p=p====p~p=pp==p=pppp====p~pp==p=p~p====p=p=p====p~p=pp==p=pppp====p~pp==p=p~p====p=p=p====prp====p=p=p====p~p=pp==p=pppp====prp=p==pprp=pp==p=pppp====p~ppp==pppp=pp=prp=p==pp~p====pppp=p==pp=prpp==p=p~p====p=p=p====p~p=pp=p=p=prp=pp=pppp==ppp~p====pppp=p==pp=p~p====p=p=p====p~p=p=p=pp=p~p====p=p=p====prp====p=p=p====p~*1(2)";
        graph = "erin1";
        maxsols = 0;
        }
        if (false)
        {
            expected = "";
            prefix = "2314567QS--PP--PP---P-----P-P--------P-P---PP--P----P---PP-PP-P-P-P----PP--PP---P-----P-P--------P-P---PP--P----P---PP-PP-P-P-P----PP--PP---P-----P-P--------P-P---PP--P----P---PP-PP-P-P-P--*0.18(4)";
            length = 180;
            graph = "sted4c";
            maxsols = 1;
        }
        if (true)
        {
            expected = "";
            prefix = "";
            length = 840;
            graph = "erin7";
            maxsols = 0;
        }
        expected=null;

        // options 1,2,3 twisted-ring, order, binary are expensive
        int goodopts[];
        int goodoptsall[] = new int[] {0, 1<<2, 1<<3, 1<<4, 1<<5, 1<<6, 1<<7, /* omit as very bad 1<<8,*/ 1<<9, 1<<10, 1<<11, 1<<12, 1<<13, 1<<14, 1<<15, 1<<16, 1<<17, 1<<18, 1<<19, 1<<20, 1<<21, (1<<20) | (1<<21), 1<<22, 1<<23, 1<<24, 1<<25};
        int tries = 3;
        int sectimeout = 60*10; // 10 minutes
        
        // For testing 8-bit registers
        //goodopts = new int[] {0, 0x800, 0x1000, 0x100000, 0x200000, 0x400000, 0x800000};
        
        // encodings
        int goodoptsallmodes[] = new int[] {0, 1, 2, 3, 0x2000, 0x2001, 0x2002, 0x2003, 1<<2, 1<<3, 1<<4, 1<<5, 1<<6, 1<<7, /* omit as very bad 1<<8,*/ 1<<9, 1<<10, 1<<11, 1<<12, 1<<13, 1<<14, 1<<15, 1<<16, 1<<17, 1<<18, 1<<19, 1<<20, 1<<21, (1<<20) | (1<<21), 1<<22, 1<<23, 1<<24, 1<<25};
        
        Map <Integer,Integer>seenCNF = new HashMap<Integer,Integer>();
        // Timing different prefix lengths
        if (false) for (int i = 20; i >= 0; --i) {
            int opt = 0;
            String fullpfx = "pp===ppp==p=p====pppp";
            String expected1 = "2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P";
            int expectedn = 1;
            fullpfx = fullpfx.substring(0,i);
            String pfx;
            if (true) {
                // Successful search
                pfx = fullpfx.substring(0,i);
                expectedn = 1;
            } else {
                // Failing search - last call is wrong
                expected1 = "UNSAT";
                expectedn = 0;
                pfx = fullpfx.substring(0,i-1)+(fullpfx.charAt(i - 1) == 'p' ? "=" : "p");
            }
            long tm = timeMultiple(expected1, "sted5", "2314567QS"+pfx, 840, 0, 1, 1, 0, "", palindrome, expectedn, tries, sectimeout, opt, seenCNF);
            System.out.println("######@ i="+i+" took "+tm+"ms");
        }
        
        
        goodopts = new int[] {0x0,0x80,0x4,0x8,0x40};
        goodopts = goodoptsall;
        int optbest = optinit;
        long tookbest = Long.MAX_VALUE;
        
        do {
            int optbestsf = optbest;
            long tookbestsf = tookbest;
            for (int i = 0; i < goodopts.length; ++i) {
                int oo = goodopts[i];
                int opt = optbest | oo;
                // Disabled option - or already set
                if (opt == optbest && tookbestsf < Long.MAX_VALUE) continue;

                long tookbestrun = timeMultiple(expected, graph, prefix, length, bobs, divg, divb, countqsets,
                        requiredSeq, palindrome, maxsols, (opt == 0 ? tries * 2 : tries), sectimeout, opt, seenCNF);
                
                System.out.println(">>>Found from 0x"+Integer.toHexString(opt)+" "+tookbestrun+"ms");
                if (tookbestrun == -1) {
                    System.out.println(">>>Found run with same CNF as a previous 0x"+Integer.toHexString(opt));
                    // disable
                    goodopts[i] = 0;
                    continue;
                }
                if (tookbestrun < tookbestsf) {
                    // This is better than other options
                    tookbestsf = tookbestrun;
                    optbestsf = opt;
                    System.out.println(">>>Found better 0x"+Integer.toHexString(optbestsf)+" "+tookbestsf+"ms");
                }
                if (tookbestrun > 3 * tookbestsf) {
                    // This is much worse, never do again
                    System.out.println(">>>Found much worse 0x"+Integer.toHexString(opt)+" "+tookbestrun+"ms");
                    // disable
                    goodopts[i] = 0;
                }
            }
            if (tookbestsf < tookbest) {
                // Choose the best option from the last search
                System.out.println(">>>Found best from runs 0x"+Integer.toHexString(optbestsf)+" "+tookbestsf+"ms");
                tookbest = tookbestsf;
                optbest = optbestsf;
            } else {
                break;
            }
        } while (true);
        System.out.println(">>>Found final best from runs 0x"+Integer.toHexString(optbest)+" "+tookbest+"ms");
    }

    private static long timeMultiple(String expected, String graph, String prefix, int length, int bobs,
            int divg, int divb, int countqsets, String requiredSeq, String palindrome, int maxsols, int tries, int sectimeout, int opt, Map <Integer,Integer>seenCNF)
            throws IOException, ParseFormatException {
        long tookbestrun = Long.MAX_VALUE;
        // Extra long runs for first go to warm up
        // Do several runs to find the best
        for (int t = 0; t < tries; ++t) {
            TestStedToSAT c;
            if (expected != null) {
                c = new TestStedToSAT(expected, graph, prefix, length, bobs, divg, divb, countqsets, requiredSeq, palindrome, maxsols, opt);
            } else {
                //c = new TestStedToSAT("UNSAT", "sted12b", "2314567QSppp", 840, 0, 1, 1, 0, "", "", maxsols, opt);
                //c = new TestStedToSAT("2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P", "sted10", "2314567QSpp~~", 840, 0, 1, 1, 0, "", "", maxsols, opt);
                //c = new TestStedToSAT("UNSAT", "sted10", "2314567QSppr~~", 840, 0, 1, 1, 0, "", "", maxsols, opt);
                // >>>Found final best from runs 0x2800280 5307ms with Kissat
                c = new TestStedToSAT("UNSAT", "erin7", "1234567SS", 840, 0, 1, 1, 0, "", "", maxsols, opt);
                //c = new TestStedToSAT("2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P", "sted10", "2314567QSpp==", 840, 0, 1, 1, 0, "", "", maxsols, opt);
                //c = new TestStedToSAT("2314567QSP----P-P-PPPP--P----PP----PP---PP-PP-PP--P-----P----P---------PP------*3(4)", "sted12a", "2314567QSL....L.L.LLLL..L....LL....pp=", 840, 0, 1, 1, 0, "", "", maxsols, opt); 
                //c = new TestStedToSAT("2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P", "sted5", "2314567QSpp===ppp==p=p====pppp==p==p*0.8(1)", 840, 0, 1, 1, 0, "", "", maxsols, opt);
                //c = new TestStedToSAT("", "sted3", "6143572QS==========*1(1)6143257QS==========*1(1)6523174QS==========*1(1)6523417QS==========*1(1)1423675QS==========*1(1)1423567QS==========*1(1)6152473QS==========*1(1)6152347QS==========*1(1)5476123QS----P--------P---------P---------PP-----P-----P---------P*0.62(1)", 200, 0, 1, 1, 0, "", "", 18, opt);
                //c = new TestStedToSAT("2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P", "sted10", "2314567QSr", 840, 0, 1, 1, 0, "", "", 1, opt);
                //c = new TestStedToSAT("---PPPPP---", "sted10", "2314567QS", 840, 0, 1, 1, 0, "", "", 4, opt);
                // 0.215 is about 15 seconds
                //c = new TestStedToSAT("1234567SSP-P-P-PP-PPPPP--PPPP-PP-P-PPP--PPPP-", "erin2", "1234567SSP-P-P-PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PPP-P--PP-P----PPPP-P--PP-PPPP--P-PPP----PPPP-P--PP-PPPPP--PPPP-PP-PPP-P--PPPP-PP-P-P-P-P----P-P-P----PPP----P-P-P----P-P-PP--P-PPPP----PPP-P--PPPP-PP-P-P-PPP-PP-PPPP--PPPPP-PP-P-P-PPP----PPPP-P--PP-P-P----P-P-P----P-*0.2(1)", 256, 0, 1, 1, 0, "", "", 1, opt);
                //c = new TestStedToSAT("2314567QSPP---PPP--P-", "sted2", "2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP---P--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PP-----PPPPP---P-P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-*0.23(1)", 840, 0, 1, 1, 0, "", "", 4, opt );
                //c = new TestStedToSAT("2314567QSPP---PPP--P-", "sted2", "2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP---P--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PP-----PPPPP---P-P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-*0.245(1)", 840, 0, 1, 1, 0, "", "", 4, opt );
                //c = new TestStedToSAT("2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P", "sted5", "2314567QSpp===ppp==p=p====pppp", 840, 0, 1, 1, 0, "", "", maxsols, opt);
                //c = new TestStedToSAT("---PPPPP---", "sted10", "", 840, 0, 1, 1, 0, "", "", 4, opt);
            }
            c.timeout = sectimeout;
            long t1 = c.time1();
            tookbestrun = Math.min(t1, tookbestrun);
            // very bad run, so exit early
            if (t1 > sectimeout * 1000L) {
                break;
            }
            Integer prevopt = seenCNF.get(c.hashCNF);
            if (prevopt != null) {
                if (prevopt != opt) {
                    System.out.println(">>>Opt 0x"+Integer.toHexString(opt)+" has same CNF as 0x" + Integer.toHexString(prevopt));
                    tookbestrun = -1;
                    break;
                }
            } else {
                seenCNF.put(c.hashCNF, opt);
            }
        }
        return tookbestrun;
    }
}
