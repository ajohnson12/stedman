# Stedman and Erin Triples

These programs are for solving the bell ringing problem of bobs-only peals of Stedman and Erin Triples.

With 7 working bells tthere are 7! = 5040 permutations in which the bells can be rung in different orders.

Stedman triples and Erin Triples are two methods of going through the permutations.

This program converts the problem to boolean satisfiability.

Lots of options are available to restrict the search.

- Force the main block to be exactly n sixes long. The remaining sixes must be in round blocks.
- Restrict to at least n bobs
- Restrict to less than or equal to n bobs - use a minus sign
- Restrict to at least m q-sets
- Restrict to less than or equal to m q-sets - use a minus sign
- Restrict certain sixes (seed the search)
 + 2314567QS starting quick six for Stedman
 + 3124567SQ starting slow six for Stedman
 + 1234567SS starting (slow) six for Erin
 + Then the sixes
  - p force the six type and the call (plain)
  - P force the six type and the call (plain)
  - r force the six type, the call can be changed from a plain
  - R the six type can be changed, but must be followed by a plain
  - L the six type can be changed and the call type can be changed from a plain
  - \- force the six type and the call (bob)
  - = force the six type and the call (bob)
  - ~ force the six type, the call can be changed from a bob
  - _ the six type can be changed, but must be followed by a bob
  - . the six type can be changed and the call type can be changed from a bob
 + followed by \*2(3) where \*2 means two copies, the parenthesis is ignored on input but on output indicates the number of copies because of the group.
- Further restrictions
 + restrict[;restrict]*
 + restrict - this call sequence must occur somewhere
 + !restrict - this call sequence must not occur anywhere
 + >3:restrict - this call sequence must occur more than n times
 + >=3:restrict - this call sequence must occur at least n times
 + =3:restrict - this call sequence must occur exactly n times
 + <>3:restrict - this call sequence must not occur exactly n times
 + <=3:restrict - this call sequence must occur n times or fewer
 + <3:restrict - this call sequence must occur fewer than n times
 + 2..3:restrict - this call sequence must occur between m and n times
    + restrict := restrict1 | restrict2
    + restrict1 := [start]? sixcall*
    + restrict2 := restrict3 [restrict3]*
    + restrict3 := start sixcall* multiplier
    + start := [?123456790E](QS|SQ|SS)
    + sixcall := [PprRL-=~_.]
    + multiplier := \*[0-9]+\([0-9]+\)
    + if the multiplier is present then the count determines the number of sixes which match rather the number of runs (the multiplier can be a decimal)
- palindromic restrict - this is a permutation applied to each row/call, and the 
    generated six type must match, and the call type if the palindrome restrict end with '-'
     + 2143567  in-course palindrome (effectively just a normal irregular multi-part)
     + 2143657  out-of-course palindrome; matches six-head to six-end.
     + 2143567- in-course palindrome with call types to match (effectively just an exact multi-part)
     + 2143657- also forces call types to match

# Examples
## Set up
```
# to set up, modify this command
# alias SATSolve=<your solver>
```
## help from program
```
java -cp StedToSAT-1.0-SNAPSHOT.jar StedToSAT
Stedman/Erin Triples to SAT converter
Copyright (c) 2015,2021 Andrew Johnson
StedToSAT <link file> [<initial touch>] [<short loop length>] [<satout file>]
 [<options:int>] [<generated sat file>]
 [<bob count: +ve means >=, -ve means <= >]
 [<divide six type>] [<divide call type>]
 [Q-set count: +ve means >=, -ve means <= >]
 [<touch restrictions>;<touch>]: <touch>=[!][row][calls] ! means not allowed]
 [<palindrome> | <palindrome>- palindrome fixing six-types or six & call-types]

java -cp StedToSAT-1.0-SNAPSHOT.jar GenSted sted1 >sted1.pb
```
## example to show minimum clauses - with the 0x1e3 tuning option
```
java -cp StedToSAT-1.0-SNAPSHOT.jar StedToSAT sted1.pb "" 840 "" 0x01e3 sted1_0x1e3.cnf 0 1 1 0
head -460 sted1_0x1e3.cnf
```
## solve a Sted1 problem - with a 238 call hint
```
java -cp StedToSAT-1.0-SNAPSHOT.jar StedToSAT sted1.pb "2314567QSP--P----P-----P---------P-P----PP--PP----PP--PP--PP--P------P--P------P--P----P-----P-----P--P------P--P------P--PPP----P--P------P--P------P--PP-P----PP--PP----PP--PPPP--PP--PP--P--------P-----PP---P--------P-----P---------P-P----PP--PP----PP--------PP--PPPPPP--P------P--P------*0.85(1)" 840 "" 0x0 sted1.cnf 0 1 1 0
SATSolve sted1.cnf >sted1.out
java -cp StedToSAT-1.0-SNAPSHOT.jar StedToSAT sted1.pb "2314567QSP--P----P-----P---------P-P----PP--PP----PP--PP--PP--P------P--P------P--P----P-----P-----P--P------P--P------P--PPP----P--P------P--P------P--PP-P----PP--PP----PP--PPPP--PP--PP--P--------P-----PP---P--------P-----P---------P-P----PP--PP----PP--------PP--PPPPPP--P------P--P------*0.85(1)" 840 "sted1.out" 0x0 sted1.cnf 0 1 1 0
tail sted1.cnf
```
## solve a simple Sted60 graph
```
java -cp StedToSAT-1.0-SNAPSHOT.jar GenSted sted60 >sted60.pb
java -cp StedToSAT-1.0-SNAPSHOT.jar StedToSAT sted60.pb "" 840 "" 0x0 sted60.cnf
SATSolve sted60.cnf >sted60.out
java -cp StedToSAT-1.0-SNAPSHOT.jar StedToSAT sted60.pb "" 840 "sted60.out" 0x0 sted60.cnf
tail sted60.cnf
```
## solve a simple Sted10 graph
```
java -cp StedToSAT-1.0-SNAPSHOT.jar GenSted sted10 >sted10.pb
java -cp StedToSAT-1.0-SNAPSHOT.jar StedToSAT sted10.pb "" 840 "" 0x0 sted10.cnf
SATSolve sted10.cnf >sted10.out
java -cp StedToSAT-1.0-SNAPSHOT.jar StedToSAT sted10.pb "" 840 "sted10.out" 0x0 sted10.cnf
tail sted10.cnf
```
## example to show minimum clauses - with the 0x1e3 tuning option
```
java -cp StedToSAT-1.0-SNAPSHOT.jar GenSted erin1 >erin1.pb
java -cp StedToSAT-1.0-SNAPSHOT.jar StedToSAT erin1.pb "" 840 "" 0x01e3 erin1_0x1e3.cnf 0 1 1 0
head -880 erin1_0x1e3.cnf
```
## solve an Erin2 graph with a prefix hint
```
java -cp StedToSAT-1.0-SNAPSHOT.jar GenSted erin2 >erin2.pb
java -cp StedToSAT-1.0-SNAPSHOT.jar StedToSAT erin2.pb "1234567SSP-P-P-PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PPP-P--PP-P-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----PPP----P-P-P----P-P-P-P-PP-PPPPP--PPPP-PP-PPP-P-P-PP-P-P----P-P-P----P-P-P--PPPP-PP-P-P-P-P----P-P-P----PPP----P-P-P----P-P-PP--P-PPPP----P-PPP--PPPP-PP-P-PP--P-P-P----P-P-P----PPP----P-P-P----P-P-PP-P-P-P-P----P-P-P----P-P-PP-P-P-PPP-PP-PPPP--PPPPP-PP-P-P-PPP-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----PPP----P-P-P----P-*0.12(2)" 840 "" 0x0 erin2.cnf 0 1 1 0
SATSolve erin2.cnf >erin2.out
java -cp StedToSAT-1.0-SNAPSHOT.jar StedToSAT erin2.pb "1234567SSP-P-P-PP-PPPPP--PPPP-PP-P-PPP--PPPP-PP-PPP-P--PP-P-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----PPP----P-P-P----P-P-P-P-PP-PPPPP--PPPP-PP-PPP-P-P-PP-P-P----P-P-P----P-P-P--PPPP-PP-P-P-P-P----P-P-P----PPP----P-P-P----P-P-PP--P-PPPP----P-PPP--PPPP-PP-P-PP--P-P-P----P-P-P----PPP----P-P-P----P-P-PP-P-P-P-P----P-P-P----P-P-PP-P-P-PPP-PP-PPPP--PPPPP-PP-P-P-PPP-PP-PPPP--PPP-P----PPPP-P--PP-P-P----P-P-P----PPP----P-P-P----P-*0.12(2)" 840 "erin2.out" 0x0 erin2.cnf 0 1 1 0
tail erin2.cnf
```
### find variations of a 10-part peal
```
java StedToSAT sted1.pb $(echo '2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P---------PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-*1(1)' | tr 'P-' 'r~') 840 "" 0 sted1.cnf 0 1 1 1
SATSolve sted1.cnf >sted1.out
java StedToSAT sted1.pb $(echo '2314567QSPP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P-PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P---PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP-P---------PPP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-PP---PPP--P-P----PPPP--P--P----PP-P--P-PP-PPP--P--PP--P-PPPP--PP--P----PPPPP--P----PP-PP-P--P-*1(1)' | tr 'P-' 'r~') 840 sted1.out 0 sted1.cnf 0 1 1 1
grep '!' sted1.cnf
java StedToSAT sted1.pb "2314567QS" 840 sted1.out 0 sted1.cnf 0 1 1 0 '>=810:2314567QSpp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=~pp==p====pprpp==p====pp~pp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=~pp==p====pprpp==p====pp~pp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=r=~=~=~=~=rpp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=~pp==p====pp~pp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=~pp==p====pprpp==p====pp~pp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=r=~=rpp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=r=rpp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=r=~=rpp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=~pp==p====pp~pp=p==p=pp===ppp==p=p====ppp
SATSolve sted1.cnf >sted1.out
java StedToSAT sted1.pb "2314567QS" 840 sted1.out 0 sted1.cnf 0 1 1 0 '>=810:2314567QSpp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=~pp==p====pprpp==p====pp~pp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=~pp==p====pprpp==p====pp~pp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=r=~=~=~=~=rpp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=~pp==p====pp~pp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=~pp==p====pprpp==p====pp~pp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=r=~=rpp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=r=rpp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=r=~=rpp=p==p=pp===ppp==p=p====pppp==p==p====pp=p==p=pp=ppp==p==pp==p=pppp=~pp==p====pp~pp=p==p=pp===ppp==p=p====ppp
grep '!' sted1.cnf
```
## Useful restrictions
- `=711:-` Exactly 711 bobs
- `<=528:-` Less than or equal to 528 bobs
- `=600:P` Exactly 600 plains
- `!P.........P.........P` all Q-sets must be bobbed
- `!-LLLLLLLLLLLLL-LLLLLLLLLLLLL-` all Q-sets must be plained
- `=275:???????QS~~~~~~~~~~` exactly 275/5 = 55 complete B-blocks, possibly linked via Q-sets
- `=275:???????QS----------` exactly 275/5 = 55 complete B-blocks as free blocks
- `=10:???????QS=========r.LLL..L....L==========` exactly 10/5 = 2 paired B-blocks as free blocks 
(possibly not quite correct as one block could have a plain)
- `=60:???????QS~~~~~~~~~r.LLL..L....L~~~~~~~~~~` exactly 60/5 = 12 paired B-blocks, possibly linked via Q-sets
- `!??????7??-;!?????7???-` 7 not bobbed behind
- `!????7??P-P;!??????7P-P` no single bobs when the 7 is behind
- `!-------` no runs of 7 or more bobs
- `!P-----P` no runs of exactly 5 bobs
- `!P---P` no runs of exactly 3 bobs
- `<=18:P-P` less than or equal to 18 isolated bobs
